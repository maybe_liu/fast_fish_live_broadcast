//
//  MBShareCollectionCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShareCollectionCell.h"

@implementation MBShareCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bottomTitle.textColor = Color_666666;
    self.bottomTitle.font = kFont(12);
}
-(void)setTopImage:(NSString *)topImage {
    _topImage = topImage;
    self.topImageView.image = [UIImage imageNamed:_topImage];
}

-(void)setTitleString:(NSString *)titleString {
    _titleString = titleString;
    self.bottomTitle.text = _titleString;
}




@end
