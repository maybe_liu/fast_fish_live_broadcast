//
//  MBShareView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShareView.h"
#import "MBShareCollectionCell.h"
#import "MBShareManager.h"

@class MBShareManager;
@interface MBShareView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic, strong) UIView                 *customContentView;
@property(nonatomic, strong) MBBaseCollectionView   *myCollection;
@property (strong, nonatomic)  UIView                 *bottomLineView;
@property (strong, nonatomic)  UIButton               *cancelButton;
@property (strong, nonatomic)  UILabel                *topTitleLabel;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *imageArray;
@property(nonatomic, strong) UICollectionViewFlowLayout * layout;



@end

static NSInteger const cols = 4;
static CGFloat const margin = 0;
static NSString * const MBShareCollectionCellID = @"MBShareCollectionCellID";


@implementation MBShareView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        self.titleArray = @[@"微信好友",@"朋友圈",@"QQ好友",@"QQ空间",@"微博",@"系统好友"];
//        self.imageArray = @[@"share_unweixn",@"share_pengyouquan",@"share_QQ",@"share_zore",@"share_sina",@"share_haoyou"];
        self.titleArray = @[@"微信好友"];
        self.imageArray = @[@"share_unweixn"];
        [self initUI];
        [self layoutChildViews];
    }
    return self;
}

-(void)initUI {
    [self addSubview:self.customContentView];
    [self.customContentView addSubview:self.myCollection];
    [self.customContentView addSubview:self.topTitleLabel];
    [self.customContentView addSubview:self.bottomLineView];
    [self.customContentView addSubview:self.cancelButton];
}
-(void)layoutChildViews {
//    [super layoutSubviews];
    MB_WeakSelf(self);
    [self.customContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(weakself);
        make.bottom.equalTo(weakself.mas_bottom).offset(-kTabSafeBottomMargin);
        make.height.mas_equalTo(266);
    }];
//    dispatch_after(0.3, dispatch_get_main_queue(), ^{
//        [UIView animateWithDuration:0.3 animations:^{
//            [weakself.customContentView mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.bottom.equalTo(weakself.mas_bottom).offset(-kTabSafeBottomMargin);
//            }];
//            [weakself layoutIfNeeded];
//        } completion:^(BOOL finished) {
//
//        }];
//
//    });
    
    [self.topTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(weakself.customContentView.mas_left).offset(12);
        make.top.mas_equalTo(weakself.customContentView.mas_top).offset(20);
    }];
    [self.myCollection mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakself.customContentView.mas_left).offset(5);
        make.right.equalTo(weakself.customContentView.mas_right).offset(-5);
        make.top.equalTo(weakself.customContentView.mas_top).offset(50);
        make.bottom.equalTo(weakself.customContentView.mas_bottom).offset(-55);
    }];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(weakself.customContentView);
        make.height.mas_equalTo(0.5);
        make.bottom.equalTo(weakself.customContentView.mas_bottom).offset(-53);
    }];
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(weakself.customContentView);
        make.height.mas_equalTo(55);
    }];
}
-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.customContentView.bounds byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.customContentView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.customContentView.layer.mask = maskLayer;
    MB_WeakSelf(self);
    
    
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.titleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBShareCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBShareCollectionCellID forIndexPath:indexPath];
    cell.topImage = self.imageArray[indexPath.row];
    cell.titleString = self.titleArray[indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self removeFromSuperview];
     MBSharePlatformType type;
        switch (indexPath.row) {
            case 0: type  = MBSharePlatformTypeWeChat; break;
//            case 1: type  = MBSharePlatformTypeFriendCircle; break;
//            case 2: type  = MBSharePlatformTypeQQ; break;
//            case 3: type  = MBSharePlatformTypeQzone; break;
//            case 4: type  = MBSharePlatformTypeWeibo; break;
//            case 5: type  = MBSharePlatformTypeSystemFriend; break;
//            default:type  = MBSharePlatformTypeSystemFriend;break;
        }
    [[MBShareManager shareMBShareManager] mb_singleShareWithSharePlatform:type];
}



#pragma mark - event
-(void)cancelButtonClick {
    [self removeFromSuperview];
    [[MBShareManager shareMBShareManager] mb_removeShareView];
}

#pragma mark - getter and setter
-(UIView *)customContentView {
    if (_customContentView == nil) {
        _customContentView = [[UIView alloc]init];
        _customContentView.backgroundColor = Color_White;
    }
    return _customContentView;
}
-(UILabel *)topTitleLabel {
    if (_topTitleLabel == nil) {
        _topTitleLabel = [[UILabel alloc]init];
        _topTitleLabel.text = @"分享到";
        _topTitleLabel.textColor = Color_333333;
        _topTitleLabel.font = kFont(14);
    }
    return _topTitleLabel;
}
-(UIView *)bottomLineView {
    if (_bottomLineView == nil) {
        _bottomLineView = [[UIView alloc]init];
        _bottomLineView.backgroundColor = Color_Line;
    }
    return _bottomLineView;
}
-(UIButton *)cancelButton {
    if (_cancelButton == nil) {
        _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:Color_333333 forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = kFont(16);
        [_cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelButton;
}
-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        CGFloat cellWidth = (kScreenWidth - 30) / cols;
        layout.minimumLineSpacing = margin;
        layout.minimumInteritemSpacing = margin;
        CGFloat cellHeight = 80;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}
- (MBBaseCollectionView *)myCollection{
    if (_myCollection == nil) {
        _myCollection = [[MBBaseCollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:self.layout];
        _myCollection.backgroundColor = Color_White;
        _myCollection.bounces = NO;
        _myCollection.showsVerticalScrollIndicator = NO;
        _myCollection.showsHorizontalScrollIndicator = NO;
        _myCollection.dataSource = self;
        _myCollection.delegate = self;
        _myCollection.scrollEnabled = NO;
        [_myCollection registerNib:[UINib nibWithNibName:NSStringFromClass([MBShareCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:MBShareCollectionCellID];
    }
    return _myCollection;
}


@end
