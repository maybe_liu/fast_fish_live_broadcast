//
//  MBShareCollectionCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBShareCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitle;

@property(nonatomic, copy)NSString *topImage;
@property(nonatomic, copy)NSString *titleString;

@end

NS_ASSUME_NONNULL_END
