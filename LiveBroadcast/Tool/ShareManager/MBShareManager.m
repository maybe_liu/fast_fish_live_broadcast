//
//  MBShareManager.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShareManager.h"
#import "MBShareView.h"
#import <UMShare/UMShare.h>


@implementation MBShareManager
SingleM(MBShareManager)
/**
 弹出分享页面
 */
-(void)mb_initShareView{
    MBShareView  *shareView = [[MBShareView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight}}];
    shareView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//    [self.fromVC.view addSubview:shareView];
    [APP_delegate.window addSubview:shareView];
}

/**
 关闭分享页面
 */
-(void)mb_removeShareView{
    if (self.removeShareViewBlockHandler) {
        self.removeShareViewBlockHandler();
    }
}

/**
 单个分享方法
 @param type 分享平台
 */
-(void)mb_singleShareWithSharePlatform:(MBSharePlatformType)type{
    if (self.sharePlatformBlockHandler) {
        self.sharePlatformBlockHandler(type);
    }
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    UMShareWebpageObject *webObject;
    if ([self.shareImage hasPrefix:@"http://"] || [self.shareImage hasPrefix:@"https://"]) {
        webObject  = [UMShareWebpageObject shareObjectWithTitle:self.shareTitle descr:self.shareSubTitle thumImage:self.shareImage];
    }else {
         webObject = [UMShareWebpageObject shareObjectWithTitle:self.shareTitle descr:self.shareSubTitle thumImage:[UIImage imageNamed:self.shareImage]];
    }
    webObject.webpageUrl = self.shareURL;
    messageObject.shareObject = webObject;
    UMSocialPlatformType shareType;
    switch (type) {
        case MBSharePlatformTypeWeChat:{
            shareType = UMSocialPlatformType_WechatSession;
            [self shareWithType:shareType messageObject:messageObject];
        }break;
        case MBSharePlatformTypeFriendCircle:{
            shareType = UMSocialPlatformType_WechatTimeLine;
            [self shareWithType:shareType messageObject:messageObject];
        }break;
        case MBSharePlatformTypeQQ:{
            shareType = UMSocialPlatformType_QQ;
            [self shareWithType:shareType messageObject:messageObject];
        }break;
        case MBSharePlatformTypeQzone:{
            shareType = UMSocialPlatformType_Qzone;
            [self shareWithType:shareType messageObject:messageObject];
        }break;
        case MBSharePlatformTypeWeibo:{
            shareType = UMSocialPlatformType_Sina;
            [self shareWithType:shareType messageObject:messageObject];
        }break;
        case MBSharePlatformTypeSystemFriend:{
            [self shareSystemFriends];
        }break;
    }
}

-(void)shareWithType:(UMSocialPlatformType)shareType messageObject:(UMSocialMessageObject *)messageObject {
    [[UMSocialManager defaultManager] shareToPlatform:shareType messageObject:messageObject currentViewController:nil completion:^(id data, NSError *error) {
        if (error) {
            if (error.code == UMSocialPlatformErrorType_Cancel) {
                [MBProgressHUD showError:@"取消分享"];
            }else if (error.code == UMSocialPlatformErrorType_NotNetWork){
                 [MBProgressHUD showError:@"网络连接失败"];
            }else {
                [MBProgressHUD showError:@"分享失败"];
            }
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                [MBProgressHUD showSuccess:@"分享成功"];
                //                UMSocialShareResponse *resp = data;
                
                //分享结果消息
                //                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                //                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                //                [weakview removeFromSuperview];
            }else{
                //                UMSocialLogInfo(@"response data is %@",data);
            }
        }
        //        [self alertWithError:error];
    }];
}

-(void)shareSystemFriends {
    
}

@end
