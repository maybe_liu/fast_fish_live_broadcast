//
//  MBShareManager.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "single.h"
NS_ASSUME_NONNULL_BEGIN
/*分享平台种类*/
typedef NS_ENUM(NSInteger,MBSharePlatformType){
    MBSharePlatformTypeWeChat             = 0, //微信
    MBSharePlatformTypeFriendCircle       = 1 << 0, //朋友圈
    MBSharePlatformTypeTwitter            = 1 << 1, //twitter
    MBSharePlatformTypeFacebook           = 1 << 2, //facebook
    MBSharePlatformTypeTelegram           = 1 << 3,  //telgram
    MBSharePlatformTypeShareURL           = 1 << 4,  //分享链接
    MBSharePlatformTypeQQ                 = 1 << 5,  //QQ好友
    MBSharePlatformTypeQzone              = 1 << 6,  //QQ空间
    MBSharePlatformTypeWeibo              = 1 << 7,  //微博
    MBSharePlatformTypeSystemFriend       = 1 << 8  //系统好友
};

/**
 分享内容类型
 */
typedef NS_ENUM(NSInteger,MBShareContentType){
    MBShareContentTypeLink,                     //链接
    MBShareContentTypeImage,                     //图片
    MBShareContentTypeVideo                     //图片
};
/**
 分享面板 点击回调
 */
typedef void(^MBSharePlatformBlockHandler)(MBSharePlatformType type);

/**
 分享面板 取消回调
 */
typedef void(^MBRemoveShareViewBlockHandler)(void);

@interface MBShareManager : NSObject
SingleH(MBShareManager)

/**
 弹出分享页面
 */
-(void)mb_initShareView;



/**
 关闭分享页面
 */
-(void)mb_removeShareView;

/**
 单个分享方法
 @param type 分享平台
 */
-(void)mb_singleShareWithSharePlatform:(MBSharePlatformType)type;

@property(nonatomic,copy)NSString                     *shareURL;
@property(nonatomic,copy)NSString                     *shareTitle;
@property(nonatomic,copy)NSString                     *shareSubTitle;
@property(nonatomic,copy)NSString                     *shareImage;
@property(nonatomic, copy)MBSharePlatformBlockHandler   sharePlatformBlockHandler;
@property(nonatomic, copy)MBRemoveShareViewBlockHandler removeShareViewBlockHandler;
@property(nonatomic, assign)MBSharePlatformType         type;
@property(nonatomic, strong)UIViewController          *fromVC;

@end

NS_ASSUME_NONNULL_END
