//
//  MBLocation.h
//
//  Created by Maybe on 16/8/17.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "single.h"
typedef void (^ResultBlock)(NSDictionary *dic);
@interface MBLocation : NSObject<CLLocationManagerDelegate>
//(MBLocation);
SingleH(MBLocation);


@property(nonatomic,strong) CLLocationManager * locationManager;
@property(nonatomic,assign) double longitude; //经度
@property(nonatomic,assign) double latitude; //纬度
@property(nonatomic,copy) NSString * address ;  // 详细地址
@property(nonatomic,copy) NSString * province ;  // 省
@property(nonatomic,copy) NSString * city ; //城市名
@property(nonatomic,copy) NSString * district ;  // 地区
@property(nonatomic,copy) NSString * lastName ;  // 最后的详细地址



-(void)startLocation; // 开始定位
-(void)getCurrentLocation:(ResultBlock)block;
-(void)getCurrentAddress:(ResultBlock)block;
@end
