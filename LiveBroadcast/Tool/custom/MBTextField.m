//
//  MBTextField.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTextField.h"

//字母+数字
#define kLetterNum  @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
#define kEmail      @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.@"
//#define kPassword   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_~!@#$%^&*()+-="
#define kPassword   @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_~!@#$%^&*()+-=.,/?<>;:'"
@interface MBTextField() <UITextFieldDelegate>
//筛选条件
@property (nonatomic,copy) NSString *filter;
@end


@implementation MBTextField

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    //设置默认值
    self.rightPadding = 0;
    self.leftPadding = 0;
    self.limitedType = MBGeneralTextFieldTypeNomal;
    self.textAlignment = NSTextAlignmentLeft;
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    
    //设置边框和颜色
    self.layer.cornerRadius = 5;
    self.backgroundColor = [UIColor whiteColor];
    self.font = kFont(16);
    self.placeholderColor = Color_AAAAAA;
    self.textColor = Color_666666;
    self.placeholderFont = kFont(16);
    
    [self addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (CGRect)clearButtonRectForBounds:(CGRect)bounds {
    [super clearButtonRectForBounds:bounds];
    CGRect rect = CGRectMake(bounds.size.width - bounds.size.height, 0, bounds.size.height, bounds.size.height);
    return rect;
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL result = YES;
    //超过最大长度 并且不是取消键被点击了
    if ((textField.text.length >= self.maxLength) && self.maxLength && ![string isEqualToString:@""]) {
        return NO;
    }else {
        if (!self.filter) {
            result = YES;
        }else {
            //限制条件
            NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:self.filter] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""]; //按cs分离出数组,数组按@""分离出字符串
            result = [string isEqualToString:filtered];
            if (!result) {
                return NO;
            }
        }
    }
    
    return result;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    BOOL result = YES;
    
    return result;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason  API_AVAILABLE(ios(10.0)){
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    BOOL result = YES;
    
    return result;
}


//textField内容有变化会调用这个方法
- (void)textFieldDidChange:(UITextField *)textField{
    if(self.textFieldDidChange){
        self.textFieldDidChange(textField.text);
    }
}

#pragma mark - setter getter
- (void)setLimitedType:(MBGeneralTextFieldType)limitedType {
    _limitedType = limitedType;
    
    //根据Type选择键盘
    if (limitedType == MBGeneralTextFieldTypeNomal) {
        self.keyboardType = UIKeyboardTypeDefault;
        self.filter = nil;
    }else{  //限制输入这里使用自定义键盘
        self.keyboardType = UIKeyboardTypeASCIICapable;
        if (limitedType == MBGeneralTextFieldTypeNumber) {  //数字
            self.keyboardType = UIKeyboardTypeNumberPad;
            self.filter = nil;
        }else if(limitedType == MBGeneralTextFieldTypeNumberOrLetter){  //数字和字母
            self.filter = kLetterNum;
        }else if(limitedType == MBGeneralTextFieldTypeEmail){  //email
            self.filter = kEmail;
        }else if(limitedType == MBGeneralTextFieldTypePassword){ //密码 数字 字母 下划线组成
            self.filter = kPassword;
        }
    }
}

- (void)setLeftPadding:(CGFloat)leftPadding {
    _leftPadding = leftPadding;
    [self setValue:@(leftPadding) forKey:@"paddingLeft"];
}

- (void)setRightPadding:(CGFloat)rightPadding{
    _rightPadding = rightPadding;
    [self setValue:@(rightPadding) forKey:@"paddingRight"];
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
    [self setValue:_placeholderColor forKeyPath:@"_placeholderLabel.textColor"];
}

- (void)setCustomLeftView:(UIView *)customLeftView {
    _customLeftView = customLeftView;
    self.leftView = customLeftView;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)setCustomRightView:(UIView *)customRightView {
    _customRightView = customRightView;
    self.rightView = customRightView;
    self.rightViewMode = UITextFieldViewModeAlways;
}
-(void)setPlaceholderFont:(UIFont *)placeholderFont {
    _placeholderFont = placeholderFont;
    [self setValue:_placeholderFont forKeyPath:@"_placeholderLabel.font"];
}

//iOS11之后placeholder设置偏移后placeholder位置没有变化
- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    if (@available(iOS 11.0, *)) {
        //如果是左对齐 则+leftPadding
        //右对齐      则-rightPadding
        //中间对其    则pading设置为0
        CGFloat padding = 0;
        if(self.textAlignment == NSTextAlignmentRight){
            padding = -_rightPadding;
        }else if(self.textAlignment == NSTextAlignmentLeft){
            padding = _leftPadding;
        }
        CGRect rect = {{bounds.origin.x+padding,bounds.origin.y},bounds.size};
        return [super placeholderRectForBounds:rect];
    }
    return  [super placeholderRectForBounds:bounds];
}

-(void)setMTintColor:(UIColor *)mTintColor {
    _mTintColor = mTintColor;
    self.tintColor = _mTintColor;
}
-(void)setMBackgroundColor:(UIColor *)mBackgroundColor {
    _mBackgroundColor = mBackgroundColor;
    self.backgroundColor = _mBackgroundColor;
}
@end
