//
//  MBTextField.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, MBGeneralTextFieldType) {
    
    MBGeneralTextFieldTypeNomal = 0,
    MBGeneralTextFieldTypeNumber,           //数字
    MBGeneralTextFieldTypeNumberOrLetter,   //数字和字母
    MBGeneralTextFieldTypeEmail,            //数字 字母 和 特定字符( '.'  '@')
    MBGeneralTextFieldTypePassword,         //数字 字母 下划线
};
@interface MBTextField : UITextField


/**
 UPLimitedTextFieldType 根据type值不同 给出不同limited 默认UPLimitedTextFieldTypeNomal
 */
@property (nonatomic,assign) MBGeneralTextFieldType limitedType;

/**
 UPTextField内容发生改变block回调
 */
@property (nonatomic, copy) void (^textFieldDidChange)(NSString *text);

/**
 textField允许输入的最大长度 默认 0不限制
 */
@property (nonatomic,assign) NSInteger maxLength;

/**
 距离左边的间距  默认10
 */
@property (nonatomic,assign) CGFloat leftPadding;

/**
 距离右边的间距 默认 10
 */
@property (nonatomic,assign) CGFloat rightPadding;

/**
 给placeHolder设置颜色
 */
@property (nonatomic,strong) UIColor *placeholderColor;

/**
 textField -> leftView
 */
@property (nonatomic,strong) UIView *customLeftView;

/**
 textField -> rightView
 */
@property (nonatomic,strong) UIView *customRightView;

@property (nonatomic,strong) UIFont *placeholderFont;
/*光标颜色*/
@property (nonatomic,strong) UIColor *mTintColor;

/**
 背景颜色
 */
@property (nonatomic,strong) UIColor *mBackgroundColor;


@end

NS_ASSUME_NONNULL_END
