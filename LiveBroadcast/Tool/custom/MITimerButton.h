//
//  MITimerButton.h
//  BestMovement
//
//  Created by 肖剑 on 15/3/4.
//  Copyright (c) 2015年 MakingIt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MITimerButton : UIButton
@property (nonatomic, assign) NSInteger timeInterval;
@property (nonatomic,assign)BOOL isCountiue;        //是否让倒计时
- (void)timerButtonPressed;
@end
