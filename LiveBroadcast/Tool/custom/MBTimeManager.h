//
//  MBTimeManager.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBTimeManager : NSObject
+(instancetype)shateInstance;

/**
 获得（年－月－日）时间
 */
- (NSString *)getYearAndMouthInBoundary:(NSTimeInterval)timeInterval;

/**
 获得（月－日）时间
 */
- (NSString *)getMouthAndDay:(NSTimeInterval)timeInterval;

/**
 获得（年－月－日 **:**:**）时间
 参数为秒
 */
- (NSString *)getComleteTime:(NSTimeInterval)timeInterval;


/// 根据毫秒时间戳进行格式化  获得（年－月－日 **:**:**）时间
- (NSString *)getComleteFormatTime:(NSInteger)time ;

/**
 获得（年－月－日）当前时间
 */
- (NSString *)getTimeNow;

/**
 获得（年－月－日 **:**:**）时间
 */
- (NSString *)getNonSecComleteTime:(NSTimeInterval)timeInterval;


/**
 获得当前时间的时间戳(年－月－日 **:**:**)
 */
-(long) getCurrentTimestamp;

/**
 @return 返回当前月(年-月)
 */
-(NSString *)getYearMonth;

/**
 返回日
 */
-(NSInteger)getDayWithTime:(NSTimeInterval)timeInterval;


/**
 @return 返回当前的日期（年-月-日）
 */
-(NSString *)getCurrentYearMonthDay;



/**
 @return 返回（**年**月**日）
 */
-(NSString *)getSelectYearMonthDay:(NSDate *)date;

/**
 @return 返回（**-**-**）日期格式
 */
-(NSString *)getLineYearMonthDay:(NSDate *)date;

/**
 返回相隔的时间差
 @[24,19,16]  24小时19分钟16秒
 */
- (NSArray *)getCountDownTimeArray:(NSTimeInterval)timeInterval;

/**
 是否有剩余时间
 */
- (BOOL)hasCountDownTime:(NSTimeInterval)timeInterval;

/**
 @return 年/月/日/
 */
-(NSString *)formatterDateWithBiasYearMonthDay:(NSTimeInterval)timeInterval;

/**
 @return 小时/分钟
 */
-(NSString *)formatterDateWithHourMin:(NSTimeInterval)timeInterval;

///时间戳——字符串时间
- (NSString *)cStringFromTimestamp:(NSString *)timestamp;
///字符串时间——时间戳
- (NSString *)cTimestampFromString:(NSString *)theTime;
/*返回 年月日时分秒*/
-(NSString *)getKuaiqianOrderTime;
/**
 @return 返回当前的日期（月-日） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterMonthDay:(NSString *)timeString;
/**
 @return 返回当前的日期（月-日） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterHourMin:(NSString *)timeString;
/**
 @return 返回当前的日期（时：分：秒） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterHourMinSecond:(NSString *)timeString;

/*当前时间 往前days天的时间戳*/
-(NSInteger)getTimestampFromCurrentToSomeDays:(NSInteger)days;

/**
 @return 返回 YYYY-MM-DD HH-mm  传入时间，内部转化为时间戳
 */
-(NSString *)getFormatterYearMonthDayHourMin:(NSString *)timeString;
@end

NS_ASSUME_NONNULL_END
