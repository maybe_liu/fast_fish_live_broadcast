//
//  MBButton.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBButton.h"

@implementation MBButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted{}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    }
    
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonClick
{
    if (_miBtnBlock) {
        _miBtnBlock();
    }
}



@end
