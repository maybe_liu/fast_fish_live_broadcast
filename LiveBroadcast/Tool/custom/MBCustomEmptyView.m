//
//  MBCustomEmptyView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBCustomEmptyView.h"

@implementation MBCustomEmptyView
+ (instancetype)mb_emptyView{
    return [MBCustomEmptyView emptyViewWithImageStr:Defaultwusous titleStr:@"" detailStr:@""];
}


+ (instancetype)mb_emptyViewWithImage:(UIImage *)image{
//    return [MBCustomEmptyView emptyViewWithImageStr:Defaultwusous titleStr:@"暂无数据" detailStr:@""];
   return [MBCustomEmptyView emptyViewWithImage:image titleStr:@"暂无数据" detailStr:@""];
}

+ (instancetype)mb_emptyActionViewImage:(UIImage *)image Target:(id)target action:(SEL)action {
    return [MBCustomEmptyView emptyActionViewWithImageStr:Defaultwusous
                                             titleStr:@"无网络连接"
                                            detailStr:@"请检查你的网络连接是否正确!"
                                          btnTitleStr:@"重新加载"
                                               target:target
                                               action:action];
}
- (void)prepare{
    [super prepare];
    self.autoShowEmptyView = NO;
    self.titleLabTextColor = Color_666666;
    self.titleLabFont = kFont(14);
}
@end
