//
//  MBCustomButton.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 自定义button
 */
typedef NS_ENUM(NSInteger,MBImagePosition){
    MBImagePositionLeft = 0, //默认图片在左，文字在右
    MBImagePositionRight = 1, //图片在右，文字在左
    MBImagePositionTop = 2 , //图片在上，文字在下
    MBImagePositionBottom = 3, //图片在下，文字在上
};
@interface MBCustomButton : UIButton
/**
 间距
 */
@property(nonatomic,assign)CGFloat spacing;
@property(nonatomic,assign)MBImagePosition postion;
@end

NS_ASSUME_NONNULL_END
