//
//  UPCustomRefresh.h
//  UpexExchange
//
//  Created by Maybe on 2018/7/4.
//  Copyright © 2018年 upex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UPCustomRefresh : NSObject
typedef void(^UPRefreshComponentRefreshingBlock)(void);

typedef void(^UPFooterRefreshComponentRefreshingBlock)(void);


/**
 自定义下来刷新
 */
+(MJRefreshHeader *)getGifHeader:(UPRefreshComponentRefreshingBlock)block;

/*自定义加载更多*/
+(MJRefreshFooter *)up_getFooter:(UPFooterRefreshComponentRefreshingBlock)block;

@end
