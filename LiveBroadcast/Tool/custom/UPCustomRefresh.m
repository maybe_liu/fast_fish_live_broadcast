//
//  UPCustomRefresh.m
//  UpexExchange
//
//  Created by Maybe on 2018/7/4.
//  Copyright © 2018年 upex. All rights reserved.
//

#import "UPCustomRefresh.h"
#import <ImageIO/ImageIO.h>

@implementation UPCustomRefresh
+(MJRefreshHeader *)getGifHeader:(UPRefreshComponentRefreshingBlock)block{
    //        NSURL *fileUrl = [[NSBundle mainBundle] URLForResource:@"熊孩子" withExtension:@"gif"];
    //        //加载GIF图片
    //        CGImageSourceRef gifSource = CGImageSourceCreateWithURL((CFURLRef) fileUrl, NULL);
    //        //将GIF图片转换成对应的图片源
    //        size_t frameCout = CGImageSourceGetCount(gifSource);
    //        //获取其中图片源个数，即由多少帧图片组成
    //        NSMutableArray *frames = [[NSMutableArray alloc] init];
    //        //定义数组存储拆分出来的图片
    //        for (size_t i = 0; i < frameCout; i++) {
    //            CGImageRef imageRef = CGImageSourceCreateImageAtIndex(gifSource, i, NULL);
    //            //从GIF图片中取出源图片
    //            UIImage *imageName = [UIImage imageWithCGImage:imageRef];
    //            //将图片源转换成UIimageView能使用的图片源
    //            [frames addObject:imageName];
    //            //将图片加入数组中
    //            CGImageRelease(imageRef);
    //        }
    //        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
    //            block();
    //        }];
    //        // 隐藏时间
    //        header.lastUpdatedTimeLabel.hidden = YES;
    //        // 隐藏状态
    //        header.stateLabel.hidden = YES;
    //        // 设置普通状态的动画图片
    //        [header setImages:frames forState:MJRefreshStateIdle];
    //        // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    //        [header setImages:frames forState:MJRefreshStatePulling];
    //        // 设置正在刷新状态的动画图片
    //        [header setImages:frames forState:MJRefreshStateRefreshing];
    //        return header;
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        block();
    }];
    return header;
}
/*自定义加载更多*/
+(MJRefreshFooter *)up_getFooter:(UPFooterRefreshComponentRefreshingBlock)block {
  MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
      block();
    }];
    return footer;
}


@end
