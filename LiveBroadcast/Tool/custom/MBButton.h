//
//  MBButton.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^MIButtonClickBlock)();

@interface MBButton : UIButton

@property (nonatomic, copy)MIButtonClickBlock miBtnBlock;

@end

NS_ASSUME_NONNULL_END
