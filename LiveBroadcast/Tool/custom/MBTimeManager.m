//
//  MBTimeManager.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTimeManager.h"
@interface MBTimeManager()
@property (strong, nonatomic)NSDateFormatter *dateFormatter;

@end
@implementation MBTimeManager
+ (instancetype)shateInstance {
    __strong static MBTimeManager* shareInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareInstance = [[MBTimeManager alloc] init];
        shareInstance.dateFormatter = [[NSDateFormatter alloc]init];
    });
    return shareInstance;
}

/**
 获得（年－月－日）时间
 */
- (NSString *)getYearAndMouthInBoundary:(NSTimeInterval)timeInterval {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}

/**
 获得（月－日）时间
 */
- (NSString *)getMouthAndDay:(NSTimeInterval)timeInterval {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval/1000.0)];
    [self.dateFormatter setDateFormat:@"MM-dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}

/**
 获得（年－月－日）当前时间
 */
- (NSString *)getTimeNow {
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970]* 1000;
    return [[[MBTimeManager shateInstance] getYearAndMouthInBoundary:timeInterval] copy];
}

/**
 获得（年－月－日 **:**:**）时间
 */
- (NSString *)getComleteTime:(NSTimeInterval)timeInterval {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}

- (NSString *)getComleteFormatTime:(NSInteger)time {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(time/1000)];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}


/**
 获得（年－月－日 **:**）时间
 */
- (NSString *)getNonSecComleteTime:(NSTimeInterval)timeInterval {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}
/**
 获得当前时间的时间戳(年－月－日 **:**:**)
 */
-(long)getCurrentTimestamp
{
    NSDate * date = [NSDate date];
    return [date timeIntervalSince1970];
}
/**
 @return 返回当前月(年-月)
 */
-(NSString *)getYearMonth
{
    NSDate * date = [NSDate date];
    [self.dateFormatter setDateFormat:@"yyyy-MM"];
    NSString * dateStr = [self.dateFormatter stringFromDate:date];
    return dateStr;
}

/**
 @return 返回当前的日期（年-月-日）
 */
-(NSString *)getCurrentYearMonthDay
{
    NSDate * date = [NSDate date];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString * dateStr = [self.dateFormatter stringFromDate:date];
    return dateStr;
}

/**
 @return 返回（**年**月**日）
 */
-(NSString *)getSelectYearMonthDay:(NSDate *)date{
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return dateStr;
}

/**
 @return 返回（**-**-**）日期格式
 */
-(NSString *)getLineYearMonthDay:(NSDate *)date{
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return dateStr;
}


-(NSInteger)getDayWithTime:(NSTimeInterval)timeInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)/1000.0];
    [self.dateFormatter setDateFormat:@"dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    NSInteger backInteger = [dateStr integerValue];
    return backInteger;
}

/**
 返回相隔的时间差
 @[2,24,19]  2天24小时19分
 */
- (NSArray *)getCountDownTimeArray:(NSTimeInterval)timeInterval {
    //计算相差时间戳（精确到秒）
    long time = timeInterval/1000;
    long currentTimestamp = [[MBTimeManager shateInstance]getCurrentTimestamp];
    long deltaTime = time-currentTimestamp;
    
    //    NSInteger second = deltaTime%60;
    NSInteger minute = (deltaTime/60)%60;
    NSInteger hour = (deltaTime/60/60)%24;
    NSInteger day = deltaTime/60/60/24;
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:day],[NSNumber numberWithInteger:hour],[NSNumber numberWithInteger:minute], nil];
}

/**
 是否有剩余时间
 */
- (BOOL)hasCountDownTime:(NSTimeInterval)timeInterval {
    //计算相差时间戳（精确到秒）
    long time = timeInterval/1000;
    long currentTimestamp = [[MBTimeManager shateInstance]getCurrentTimestamp];
    long deltaTime = time-currentTimestamp;
    if (deltaTime > 0) {
        return YES;
    }
    return NO;
}

/**
 @return 年/月/日/
 */
-(NSString *)formatterDateWithBiasYearMonthDay:(NSTimeInterval)timeInterval{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)];
    [self.dateFormatter setDateFormat:@"yyyy/MM/dd"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}
/**
 @return 小时/分钟
 */
-(NSString *)formatterDateWithHourMin:(NSTimeInterval)timeInterval{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:(timeInterval)];
    [self.dateFormatter setDateFormat:@"HH:mm"];
    NSString *dateStr = [self.dateFormatter stringFromDate:date];
    return [dateStr copy];
}

//时间戳——字符串时间
- (NSString *)cStringFromTimestamp:(NSString *)timestamp {
    //时间戳转时间的方法
    NSDate *timeData = [NSDate dateWithTimeIntervalSince1970:[timestamp intValue]];
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy年MM月dd日 HH:mm"];
    NSString *strTime = [dateFormatter stringFromDate:timeData];
    return strTime;
}

//字符串时间——时间戳
- (NSString *)cTimestampFromString:(NSString *)theTime {
    //theTime __@"%04d-%02d-%02d %02d:%02d:00"
    //装换为时间戳
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate* dateTodo = [self.dateFormatter dateFromString:theTime];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[dateTodo timeIntervalSince1970]];
    
    return timeSp;
}
/*返回 年月日时分秒*/
-(NSString *)getKuaiqianOrderTime{
    NSDate * date = [NSDate date];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd- HH:mm:ss:SSS"];
    NSString * dateStr = [self.dateFormatter stringFromDate:date];
    return dateStr;
}
/**
 @return 返回当前的日期（月-日） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterMonthDay:(NSString *)timeString {
    NSString *timeTmp = [self cTimestampFromString:timeString];
    [self.dateFormatter setDateFormat:@"MM-dd"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([timeTmp doubleValue])];
    NSString *resultString = [self.dateFormatter stringFromDate:date];
    return resultString;
}
/**
 @return 返回当前的日期（月:日） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterHourMin:(NSString *)timeString {
    NSString *timeTmp = [self cTimestampFromString:timeString];
    [self.dateFormatter setDateFormat:@"HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([timeTmp doubleValue])];
    NSString *resultString = [self.dateFormatter stringFromDate:date];
    
    return resultString;
}
/**
 @return 返回当前的日期（时：分：秒） 传入格式化时间，内部转化为时间戳
 */
-(NSString *)getFormatterHourMinSecond:(NSString *)timeString {
    NSString *timeTmp = [self cTimestampFromString:timeString];
    [self.dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([timeTmp doubleValue])];
    NSString *resultString = [self.dateFormatter stringFromDate:date];
    
    return resultString;
}

/*当前时间 往前days天的时间戳*/
-(NSInteger)getTimestampFromCurrentToSomeDays:(NSInteger)days {
    
    NSInteger currentTimestamp = [[MBTimeManager shateInstance]getCurrentTimestamp];
    NSInteger timeTamp = currentTimestamp - days*24*60*60;
    return timeTamp*1000;
}
/**
 @return 返回 YYYY-MM-DD HH-mm  传入时间，内部转化为时间戳
 */
-(NSString *)getFormatterYearMonthDayHourMin:(NSString *)timeString {
    NSString *timeTmp = [self cTimestampFromString:timeString];
    [self.dateFormatter setDateFormat:@"yyyy-MM-dd- HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([timeTmp doubleValue])];
    NSString *resultString = [self.dateFormatter stringFromDate:date];
    return resultString;
}
@end
