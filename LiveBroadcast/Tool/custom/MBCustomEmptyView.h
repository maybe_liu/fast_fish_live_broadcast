//
//  MBCustomEmptyView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "LYEmptyView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBCustomEmptyView : LYEmptyView

/**
 通用空白页

 @return view
 */
+ (instancetype)mb_emptyView;

/**
 使用自定义占位图片

 @param image 使用传入占位图片
 @return emptyView
 */
+ (instancetype)mb_emptyViewWithImage:(UIImage *)image;

/**
 加入点击事件
 @param image 使用传入占位图片
 @param target 点击
 @param action 事件
 @return emptyView
 */
+ (instancetype)mb_emptyActionViewImage:(UIImage *)image Target:(id)target action:(SEL)action;

@end

NS_ASSUME_NONNULL_END
