//
//  UILabel+Baseline.m
//  QiCheng-IOS
//
//  Created by Aimee on 16/9/7.
//

#import "UILabel+Baseline.h"

@implementation UILabel (Baseline)
// 获取带有不同样式的文字内容
//stringArray 字符串数组
//attributeAttay 样式数组
- (void)attributedText:(NSArray*)stringArray attributeAttay:(NSArray *)attributeAttay{
    // 定义要显示的文字内容
    NSString *string = [stringArray componentsJoinedByString:@""];//拼接传入的字符串数组
    // 通过要显示的文字内容来创建一个带属性样式的字符串对象
    NSMutableAttributedString * result = [[NSMutableAttributedString alloc] initWithString:string];
    for(NSInteger i = 0; i < stringArray.count; i++){
    // 将某一范围内的字符串设置样式
        [result setAttributes:attributeAttay[i] range:[string rangeOfString:stringArray[i]]];
    }
    // 返回已经设置好了的带有样式的文字
    self.attributedText=[[NSAttributedString alloc] initWithAttributedString:result];
}
@end
