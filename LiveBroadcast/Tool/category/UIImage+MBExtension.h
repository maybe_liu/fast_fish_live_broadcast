//
//  UIImage+MBExtension.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (MBExtension)
//压缩图片到多少 KB 以下
//先压缩图片质量，如果已经小于指定大小，就可得到清晰的图片，否则再压缩图片尺寸
+ (UIImage *)compressImage:(UIImage *)image toKBByte:(NSUInteger)KBvalue;

/*多张图片上下拼接*/
+ (UIImage *)combineImage:(NSArray<UIImage *> *)images;
/*2张图片上下拼接*/
+ (UIImage *)combineImageUpImage:(UIImage *)upImage  DownImage:(UIImage *)downImage;
/*保存图片在相册 并获取图片*/
+(void)savePhotoalbumWithImage:(UIImage *)image complete:(void (^)(NSData* imageData))successBlock;

/*获取 相册视频路径以及文件*/
+(void)gainPhotoalbumVideo:(PHAsset *)videoAsset complete:(void(^)(NSString *videoPath,NSData *videoData))successBlock;
@end

NS_ASSUME_NONNULL_END
