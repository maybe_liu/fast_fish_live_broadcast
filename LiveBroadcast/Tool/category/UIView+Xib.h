//
//  UIView+Xib.h
//  YueHuaYueYou
//
//  Created by Azure on 2017/9/25.
//  Copyright © 2017年 中合农发. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Xib)

+ (instancetype)viewWithNibName:(NSString *)nibname;
+ (instancetype)nibView;

@end
