//
//  UIView+CurrentController.h
//  QiCheng-IOS
//
//  Created by Maybe on 16/9/13.
//  Copyright © 2016年 中合农发. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CurrentController)

/**
 当前view所在的控制器
 */
-(UIViewController *)getCurrentViewController;

/**
 
 当前windows下的控制器
 */
- (UIViewController *)getWindowController;

/**
 获取当前屏幕上present的控制器
 */
- (UIViewController *)getPresentedViewController;


@end
