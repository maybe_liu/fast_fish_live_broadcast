//
//  UILabel+Baseline.h
//  QiCheng-IOS
//
//  Created by Aimee on 16/9/7.
//

#import <UIKit/UIKit.h>

@interface UILabel (Baseline)
- (void)attributedText:(NSArray*)stringArray attributeAttay:(NSArray *)attributeAttay;
@end
