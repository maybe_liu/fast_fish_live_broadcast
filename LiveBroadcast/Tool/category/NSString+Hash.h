//
//  NSString+Hash.h
//  RuYuTravel
//
//  Created by dev08 on 2018/6/5.
//  Copyright © 2018年 maybe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Hash)
@property (nonatomic, copy, readonly) NSString *md5String;
@property (nonatomic, copy, readonly) NSString *sha1String;
@property (nonatomic, copy, readonly) NSString *sha256String;
@property (nonatomic, copy, readonly) NSString *sha512String;

-(NSString *)hmacSHA1StringWithKey:(NSString *)key;

-(NSString *)hmacSHA256StringWithKey:(NSString *)key;

-(NSString *)hmacSHA512StringWithKey:(NSString *)key;

@end
