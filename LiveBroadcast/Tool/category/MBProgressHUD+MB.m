//
//  MBProgressHUD+MB.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/18.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBProgressHUD+MB.h"

@implementation MBProgressHUD (MB)
#pragma mark 显示一条信息
+ (void)showMessage:(NSString *)message toView:(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self show:message icon:nil view:view];
    });

}

#pragma mark 显示带图片或者不带图片的信息
+ (void)show:(NSString *)text icon:(NSString *)icon view:(UIView *)view{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
    // 判断是否显示图片
    if (icon == nil) {
        hud.mode = MBProgressHUDModeText;
    }else{
        // 设置图片
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"MBProgressHUD.bundle/%@", icon]];
        img = img == nil ? [UIImage imageNamed:icon] : img;
        hud.customView = [[UIImageView alloc] initWithImage:img];
        // 再设置模式
        hud.mode = MBProgressHUDModeCustomView;
    }
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    // 指定时间之后再消失
    [hud hide:YES afterDelay:kMBHudShowDuration];
}

#pragma mark 显示成功信息
+ (void)showSuccess:(NSString *)success toView:(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self show:success icon:@"success.png" view:view];
    });
}

#pragma mark 显示错误信息
+ (void)showError:(NSString *)error toView:(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self show:error icon:@"error.png" view:view];
    });
}

#pragma mark 显示警告信息
+ (void)showWarning:(NSString *)Warning toView:(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self show:Warning icon:@"warn" view:view];
    });
}

#pragma mark 显示自定义图片信息
+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message toView:(UIView *)view{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self show:message icon:imageName view:view];
    });
}

#pragma mark 加载中
+ (MBProgressHUD *)showActivityMessage:(NSString*)message view:(UIView *)view{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    // 快速显示一个提示信息
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    // 细节文字
    //    hud.detailsLabelText = @"请耐心等待";
    // 再设置模式
    hud.mode = MBProgressHUDModeIndeterminate;
    
    // 隐藏时候从父控件中移除
    hud.removeFromSuperViewOnHide = YES;
    
    return hud;
}

+ (MBProgressHUD *)showProgressBarToView:(UIView *)view{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeDeterminate;
    hud.labelText = @"加载中...";
    return hud;
}



+ (void)showMessage:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showMessage:message toView:nil];
    });
}

+ (void)showSuccess:(NSString *)success{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showSuccess:success toView:nil];
    });
}

+ (void)showError:(NSString *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showError:error toView:nil];
    });
}

+ (void)showWarning:(NSString *)Warning{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showWarning:Warning toView:nil];
    });
}

+ (void)showMessageWithImageName:(NSString *)imageName message:(NSString *)message{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showMessageWithImageName:imageName message:message toView:nil];
    });
}

+ (MBProgressHUD *)showActivityMessage:(NSString*)message{
    
    return [self showActivityMessage:message view:nil];
}




+ (void)hideHUDForView:(UIView *)view{
    if (view == nil) view = [[UIApplication sharedApplication].windows lastObject];
    [self hideHUDForView:view animated:YES];
}

+ (void)hideHUD{
    [self hideHUDForView:nil];
}
@end
