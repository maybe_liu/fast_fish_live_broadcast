//
//  NSString+MBCommon.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (MBCommon)
//传入 秒  得到 xx:xx:xx
+(NSString *)getMMSSFromSS:(NSString *)totalTime;
/*非空判断  nil, @"", @"  ", @"\n" will Returns NO; otherwise Returns YES. */
- (BOOL)isNotBlank;
@end

NS_ASSUME_NONNULL_END
