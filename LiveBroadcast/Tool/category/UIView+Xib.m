//
//  UIView+Xib.m
//  YueHuaYueYou
//
//  Created by Azure on 2017/9/25.
//  Copyright © 2017年 中合农发. All rights reserved.
//

#import "UIView+Xib.h"

@implementation UIView (Xib)

+ (instancetype)viewWithNibName:(NSString *)nibname {
    UINib *nib = [UINib nibWithNibName:nibname bundle:nil];
    NSArray *objectArray = [nib instantiateWithOwner:nil options:nil];
    return [objectArray objectAtIndex:0];
}

+ (instancetype)nibView {
    return [self viewWithNibName:NSStringFromClass([self class])];
}

@end
