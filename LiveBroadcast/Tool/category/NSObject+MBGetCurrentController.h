//
//  NSObject+MBGetCurrentController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (MBGetCurrentController)

/**
 当前view所在的控制器
 */
//-(UIViewController *)getCurrentViewController;

/**
 
 当前windows下的控制器
 */
- (UIViewController *)getWindowController;

/**
 获取当前屏幕上present的控制器
 */
- (UIViewController *)getPresentedViewController;
@end

NS_ASSUME_NONNULL_END
