//
//  MBShortVideoConnmentListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoCommentReplyModel :NSObject
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * videoId;
@property (nonatomic , copy) NSString              * sendTime;
@property (nonatomic , copy) NSString              * atuserId;
@property (nonatomic , copy) NSString              * commentsId;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * comment;
@property (nonatomic , copy) NSString              * createTime;

@end

@interface MBShortVideoCommentModel :NSObject
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * sendTime;
@property (nonatomic , copy) NSString              * commentPraiseCount;
@property (nonatomic , copy) NSString              * commentsId;
@property (nonatomic , copy) NSString              * videoId;
@property (nonatomic , copy) NSString              * atuserId;
@property (nonatomic , copy) NSString              * comment;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * praiseJudge;
@property (nonatomic , copy) NSArray<MBShortVideoCommentReplyModel *>              * replyList;

@end

@interface MBShortVideoConnmentListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBShortVideoCommentModel *>              * commentsList;

@end

NS_ASSUME_NONNULL_END
