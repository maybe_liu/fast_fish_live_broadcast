//
//  MBShortVideoConnmentListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoConnmentListModel.h"

@implementation MBShortVideoCommentReplyModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBShortVideoCommentModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"replyList":[MBShortVideoCommentReplyModel class],
             };
}
@end

@implementation MBShortVideoConnmentListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"commentsList":[MBShortVideoCommentModel class],
             };
}
@end
