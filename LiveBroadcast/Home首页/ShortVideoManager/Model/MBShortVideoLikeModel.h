//
//  MBShortVideoLikeModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoLikeModel : MBBaseModel
@property (nonatomic , copy) NSString              * likesCount;

@end

NS_ASSUME_NONNULL_END
