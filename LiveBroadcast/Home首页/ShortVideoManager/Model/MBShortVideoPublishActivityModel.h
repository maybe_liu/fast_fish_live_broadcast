//
//  MBShortVideoPublishActivityModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PublishActivityTagModel :NSObject
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * labelImage;
@property (nonatomic , copy) NSString              * labelName;

@end


@interface MBShortVideoPublishActivityModel : MBBaseModel
@property (nonatomic , copy) NSArray<PublishActivityTagModel *>              * labelAlls;

@end


NS_ASSUME_NONNULL_END
