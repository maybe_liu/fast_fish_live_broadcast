//
//  MBShortVideoDetailModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoDetailModel : MBBaseModel
@property (nonatomic , copy) NSString              * coverUrl;
@property (nonatomic , copy) NSString              * allowState;
@property (nonatomic , copy) NSString              * issueTime;
@property (nonatomic , copy) NSString              * imgType;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * isCheat;
@property (nonatomic , copy) NSString              * pageView;
@property (nonatomic , copy) NSString              * clickType;
@property (nonatomic , copy) NSString              * videoUrl;
@property (nonatomic , copy) NSString              * whetherPraise;
@property (nonatomic , copy) NSString              * markState;
@property (nonatomic , copy) NSString              * issueId;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * privateOneself;
@property (nonatomic , copy) NSString              * transpond;
@property (nonatomic , copy) NSString              * clickPraise;
@property (nonatomic , copy) NSString              * videoHeadline;
@property (nonatomic , copy) NSString              * videoIntroduce;
@property (nonatomic , copy) NSString              * commentCount;
@property (nonatomic , copy) NSString              * yesOrNo;

@end

NS_ASSUME_NONNULL_END
