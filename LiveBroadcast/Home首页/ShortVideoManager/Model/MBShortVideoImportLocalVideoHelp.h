//
//  MBShortVideoImportLocalVideoHelp.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 导入本地视频的工具类
 */
@interface MBShortVideoImportLocalVideoHelp : NSObject
- (void)exportAssetList:(NSArray *)assets fromController:(UIViewController *)fromVC coverImage:(UIImage *)coverImage entity:(MBShortVideoEntityType)type;
@end

NS_ASSUME_NONNULL_END
