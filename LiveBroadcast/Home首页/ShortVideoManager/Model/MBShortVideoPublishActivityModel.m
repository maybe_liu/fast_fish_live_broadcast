//
//  MBShortVideoPublishActivityModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoPublishActivityModel.h"

@implementation PublishActivityTagModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}

@end
@implementation MBShortVideoPublishActivityModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"labelAlls":[PublishActivityTagModel class],
             };
}
@end
