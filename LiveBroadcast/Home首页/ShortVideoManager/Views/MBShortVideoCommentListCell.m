//
//  MBShortVideoCommentListCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCommentListCell.h"

@implementation MBShortVideoCommentListCell


-(void)setCommentModel:(MBShortVideoCommentReplyModel *)commentModel {
    _commentModel = commentModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_commentModel.headUrl] placeholderImage:nil];
    self.nameLabel.text = _commentModel.nickname;
    self.timeLabel.text = _commentModel.sendTime;
    self.commentLabel.text = _commentModel.comment;

}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.nameLabel.textColor = Color_333333;
    self.nameLabel.font = kFont(12);
    self.timeLabel.textColor = Color_999999;
    self.timeLabel.font = kFont(12);
    self.commentLabel.textColor = Color_333333;
    self.commentLabel.font = kFont(14);
    
    self.likeButton.postion = MBImagePositionTop;
    self.likeButton.spacing = 4.0f;
    [self.likeButton setTitleColor:Color_CCCCCC forState:UIControlStateNormal];
    self.likeButton.titleLabel.font = kFont(12);
    self.likeButton.hidden = YES;
    
    self.moreButton.postion = MBImagePositionRight;
    self.moreButton.spacing = 4.0f;
    [self.moreButton setTitleColor:[UIColor colorWithHexString:@"#2F91FF"] forState:UIControlStateNormal];
    self.moreButton.titleLabel.font = kFont(12);
    self.moreButton.hidden = YES;
    self.lineView.backgroundColor = Color_Line;
    self.showFloor = NO;
}

- (IBAction)likeButtonClick:(MBCustomButton *)sender {
//    if (self.likeBlock) {
//        self.likeBlock(self.commentModel);
//    }
}
- (IBAction)moreButtonClick:(MBCustomButton *)sender {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
