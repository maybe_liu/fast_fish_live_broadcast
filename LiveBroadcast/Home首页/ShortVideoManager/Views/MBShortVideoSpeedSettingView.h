//
//  MBShortVideoSpeedSettingView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TimeSelectViewDelegate <NSObject>
- (void)onVideoTimeEffectsClear;
- (void)onVideoTimeEffectsSpeed;
- (void)onVideoTimeEffectsBackPlay;
- (void)onVideoTimeEffectsRepeat;
@end

typedef void(^SpeedCancelBlockHandler)(void);
typedef void(^SpeedTureBlockHandler)(void);

@interface MBShortVideoSpeedSettingView : UIView
@property (weak, nonatomic) IBOutlet UIView *customTopContentView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet MBBaseCollectionView *mainCollection;
@property (weak, nonatomic) IBOutlet MBCustomButton *revocationButton;

@property (nonatomic, copy) SpeedCancelBlockHandler speedCancelBlock;
@property (nonatomic, copy) SpeedTureBlockHandler speedTureBlock;
@property(nonatomic,weak) id<TimeSelectViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
