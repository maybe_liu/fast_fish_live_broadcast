//
//  MBShortVideoFilterSetCollectionCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/6.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoFilterSetCollectionCell : UICollectionViewCell
@property(nonatomic, copy)NSString *imageName;
@property(nonatomic, copy)NSString *filterName;
@property(nonatomic, strong)NSDictionary *dataDict;

@end

NS_ASSUME_NONNULL_END
