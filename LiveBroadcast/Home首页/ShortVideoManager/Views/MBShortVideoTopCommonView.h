//
//  MBShortVideoTopCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^QuitBlockHandler)(void);
typedef void(^LightBlockHandler)(void);
typedef void(^ReverBlockHandler)(void);


@interface MBShortVideoTopCommonView : UIView
@property (weak, nonatomic) IBOutlet UIButton *quitButton;
@property (weak, nonatomic) IBOutlet UIButton *lightButton;
@property (weak, nonatomic) IBOutlet UIButton *reverButton;
@property (copy, nonatomic) QuitBlockHandler quitBlock;
@property (copy, nonatomic) LightBlockHandler lightBlock;
@property (copy, nonatomic) ReverBlockHandler reverBlock;

@end

NS_ASSUME_NONNULL_END
