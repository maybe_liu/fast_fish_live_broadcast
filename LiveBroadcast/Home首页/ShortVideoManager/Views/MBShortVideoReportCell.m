//
//  MBShortVideoReportCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoReportCell.h"

@implementation MBShortVideoReportCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    // Initialization code
}
-(void)initUI {
    self.lineView.backgroundColor = Color_Line;
    self.nameLabel.textColor = Color_333333;
    self.nameLabel.font = kFont(14);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
