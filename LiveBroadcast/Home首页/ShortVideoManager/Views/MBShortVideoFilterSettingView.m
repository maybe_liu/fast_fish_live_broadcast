//
//  MBShortVideoFilterSettingView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoFilterSettingView.h"
#import "MBShortVideoFilterSetCollectionCell.h"
typedef NS_ENUM(NSInteger,ShortVideoFilterType) {
    FilterType_None         = 0,
    FilterType_normal       ,   //标准
    FilterType_yinghong     ,   //樱红滤镜
    FilterType_yunshang     ,   //云裳滤镜
    FilterType_chunzhen     ,   //纯真滤镜
    FilterType_bailan       ,   //白兰滤镜
    FilterType_yuanqi       ,   //元气滤镜
    FilterType_chaotuo      ,   //超脱滤镜
    FilterType_xiangfen     ,   //香氛滤镜
    FilterType_white        ,   //美白滤镜
    FilterType_langman         ,   //浪漫滤镜
    FilterType_qingxin         ,   //清新滤镜
    FilterType_weimei         ,   //唯美滤镜
    FilterType_fennen         ,   //粉嫩滤镜
    FilterType_huaijiu         ,   //怀旧滤镜
    FilterType_landiao         ,   //蓝调滤镜
    FilterType_qingliang     ,   //清凉滤镜
    FilterType_rixi         ,   //日系滤镜
};

@interface MBShortVideoFilterSettingView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)NSArray *dataArray;
@property (nonatomic, strong)UICollectionViewFlowLayout * layout;

@end

static NSString * const MBShortVideoFilterSetCollectionCellID = @"MBShortVideoFilterSetCollectionCellID";
@implementation MBShortVideoFilterSettingView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.bottomTitleLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.bottomTitleLabel.font = kFont(16);
    self.mainCollection.collectionViewLayout = self.layout;
    self.mainCollection.showsVerticalScrollIndicator = NO;
    self.mainCollection.showsHorizontalScrollIndicator = NO;
    self.mainCollection.dataSource = self;
    self.mainCollection.delegate = self;
    self.mainCollection.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
    self.mainCollection.backgroundColor = [UIColor clearColor];
    [self.mainCollection registerClass:[MBShortVideoFilterSetCollectionCell class] forCellWithReuseIdentifier:MBShortVideoFilterSetCollectionCellID];
    [self.mainCollection reloadData];
}

- (IBAction)cancelButtonClick:(UIButton *)sender {
    [self.delegate onSetFilterWithImage:[UIImage imageNamed:@""]];
    if (self.filterCancelBlock) {
        self.filterCancelBlock();
    }
}

- (IBAction)tureButtonClick:(UIButton *)sender {
    if (self.filterTureBlock) {
        self.filterTureBlock();
    }
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBShortVideoFilterSetCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBShortVideoFilterSetCollectionCellID forIndexPath:indexPath];
    cell.dataDict = self.dataArray[indexPath.row];
    return cell;
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (indexPath.row == self.dataArray.count) {
        return;
    }else {
        [self setFilter:indexPath.row];
    }
}

- (void)setFilter:(NSInteger)index
{
    NSString* lookupFileName = @"";
    
    switch (index) {
        case FilterType_None:
            break;
        case FilterType_normal:
            lookupFileName = @"normal.png";
            break;
        case FilterType_yinghong:
            lookupFileName = @"yinghong.png";
            break;
        case FilterType_yunshang:
            lookupFileName = @"yunshang.png";
            break;
        case FilterType_chunzhen:
            lookupFileName = @"chunzhen.png";
            break;
        case FilterType_bailan:
            lookupFileName = @"bailan.png";
            break;
        case FilterType_yuanqi:
            lookupFileName = @"yuanqi.png";
            break;
        case FilterType_chaotuo:
            lookupFileName = @"chaotuo.png";
            break;
        case FilterType_xiangfen:
            lookupFileName = @"xiangfen.png";
            break;
        case FilterType_white:
            lookupFileName = @"white.png";
            break;
        case FilterType_langman:
            lookupFileName = @"langman.png";
            break;
        case FilterType_qingxin:
            lookupFileName = @"qingxin.png";
            break;
        case FilterType_weimei:
            lookupFileName = @"weimei.png";
            break;
        case FilterType_fennen:
            lookupFileName = @"fennen.png";
            break;
        case FilterType_huaijiu:
            lookupFileName = @"huaijiu.png";
            break;
        case FilterType_landiao:
            lookupFileName = @"landiao.png";
            break;
        case FilterType_qingliang:
            lookupFileName = @"qingliang.png";
            break;
        case FilterType_rixi:
            lookupFileName = @"rixi.png";
            break;
        default:
            break;
    }
    
    NSString * path = [[NSBundle mainBundle] pathForResource:@"FilterResource" ofType:@"bundle"];

    UIImage* image;
    if (path != nil && index != FilterType_None) {
        path = [path stringByAppendingPathComponent:lookupFileName];
        image = [UIImage imageWithContentsOfFile:path];

    } else {
        image = nil;
    }
    
    [self.delegate onSetFilterWithImage:image];
}


-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat cellWidth = 50;
        layout.minimumLineSpacing = 15;
        CGFloat cellHeight = 90;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}
-(NSArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = @[
                                              @{@"icon":@"orginal",@"title":@"正常"},
                                              @{@"icon":@"biaozhun",@"title":@"标准"},
                                              @{@"icon":@"yinghong",@"title":@"樱红"},
                                              @{@"icon":@"yunshang",@"title":@"云裳"},
                                              @{@"icon":@"chunzhen",@"title":@"纯真"},
                                              @{@"icon":@"bailan",@"title":@"白兰"},
                                              @{@"icon":@"yuanqi",@"title":@"元气"},
                                              @{@"icon":@"chaotuo",@"title":@"超脱"},
                                              @{@"icon":@"xiangfen",@"title":@"香氛"},
                                              @{@"icon":@"fwhite",@"title":@"美白"},
                                              @{@"icon":@"langman",@"title":@"浪漫"},
                                              @{@"icon":@"qingxin",@"title":@"清新"},
                                              @{@"icon":@"weimei",@"title":@"唯美"},
                                              @{@"icon":@"fennen",@"title":@"粉嫩"},
                                              @{@"icon":@"huaijiu",@"title":@"怀旧"},
                                              @{@"icon":@"landiao",@"title":@"蓝调"},
                                              @{@"icon":@"qingliang",@"title":@"清凉"},
                                              @{@"icon":@"rixi",@"title":@"日系"},
                       ];
    }
    return _dataArray;
}

@end
