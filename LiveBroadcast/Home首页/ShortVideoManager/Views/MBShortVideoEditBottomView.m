//
//  MBShortVideoEditBottomView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoEditBottomView.h"

@implementation MBShortVideoEditBottomView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.musicButton.postion =  MBImagePositionTop;
    self.musicButton.spacing = 5.0;
    self.filterButton.postion =  MBImagePositionTop;
    self.filterButton.spacing = 5.0;
    self.coverButton.postion =  MBImagePositionTop;
    self.coverButton.spacing = 5.0;
    self.movementButton.postion =  MBImagePositionTop;
    self.movementButton.spacing = 5.0;
    self.speedButton.postion =  MBImagePositionTop;
    self.speedButton.spacing = 5.0;
    self.charButton.postion =  MBImagePositionTop;
    self.charButton.spacing = 5.0;
    self.musicButton.titleLabel.font = kFont(12);
    self.filterButton.titleLabel.font = kFont(12);
    self.coverButton.titleLabel.font = kFont(12);
    self.movementButton.titleLabel.font = kFont(12);
    self.speedButton.titleLabel.font = kFont(12);
    self.charButton.titleLabel.font = kFont(12);
}
- (IBAction)musicButtonClick:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.musicButton.selected = !self.musicButton.selected;
    if ([self.delegate respondsToSelector:@selector(onMusicButtonClick:)]) {
        [self.delegate onMusicButtonClick:self.musicButton.selected];
    }
}
- (IBAction)filterButtonClick:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.filterButton.selected = YES;
    if ([self.delegate respondsToSelector:@selector(onFilterButtonClick:)]) {
        [self.delegate onFilterButtonClick:self.filterButton.selected];
    }
}
- (IBAction)coverButton:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.coverButton.selected = YES;
    if ([self.delegate respondsToSelector:@selector(onCoverButtonClick:)]) {
        [self.delegate onCoverButtonClick:self.coverButton.selected];
    }
}
- (IBAction)movementButtonClick:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.movementButton.selected = YES;
    if ([self.delegate respondsToSelector:@selector(onMovementButtonClick:)]) {
        [self.delegate onMovementButtonClick:self.movementButton.selected];
    }
}
- (IBAction)speedButtonClick:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.speedButton.selected = YES;
    if ([self.delegate respondsToSelector:@selector(onSpeedButtonClick:)]) {
        [self.delegate onSpeedButtonClick:self.speedButton.selected];
    }
}
- (IBAction)charButtonClick:(MBCustomButton *)sender {
    [self resetButtonNormal];
    self.charButton.selected = YES;
    if ([self.delegate respondsToSelector:@selector(onCharButtonClick:)]) {
        [self.delegate onCharButtonClick:self.charButton.selected];
    }
}

-(void)resetButtonNormal {
    self.musicButton.selected = NO;
    self.filterButton.selected = NO;
    self.coverButton.selected = NO;
    self.movementButton.selected = NO;
    self.speedButton.selected = NO;
    self.charButton.selected = NO;
}



@end
