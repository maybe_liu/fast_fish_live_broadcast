//
//  MBShortVideoCommentSectionHeadView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCommentSectionHeadView.h"

@interface MBShortVideoCommentSectionHeadView ()

@end

@implementation MBShortVideoCommentSectionHeadView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    [self.userButton setTitleColor:[UIColor colorWithHexString:@"#2F91FF"] forState:UIControlStateNormal];
    self.userButton.titleLabel.font = kFont(13);
    self.userTitleLabel.textColor = Color_333333;
    self.userTitleLabel.font = kFont(13);
    self.likeCountLabel.textColor = Color_333333;
    self.likeCountLabel.font = kFont(13);
    self.commentCountLabel.textColor = Color_333333;
    self.commentCountLabel.font = kFont(13);
}
- (IBAction)userButtonClick:(UIButton *)sender {
    if (self.jumpBlock) {
        self.jumpBlock();
    }
}
-(void)refreshUIWithData:(MBShortVideoDetailModel *)model {
    [self.userButton setTitle:[NSString stringWithFormat:@"%@%@",model.nickname,@":"] forState:UIControlStateNormal];
    self.userTitleLabel.text = model.videoHeadline;
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@%@",model.clickPraise ? model.clickPraise : @"0" ,@" 赞"];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@%@",model.commentCount ? model.commentCount : @"0", @" 评论"];
}
-(void)setModel:(MBShortVideoDetailModel *)model {
    _model = model;
    [self.userButton setTitle:[NSString stringWithFormat:@"%@%@",model.nickname,@":"] forState:UIControlStateNormal];
    self.userTitleLabel.text = model.videoHeadline;
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@%@",model.clickPraise ? model.clickPraise : @"0" ,@" 赞"];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@%@",model.commentCount ? model.commentCount : @"0", @" 评论"];
}
@end
