//
//  MBShortVideoTopCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoTopCommonView.h"

@implementation MBShortVideoTopCommonView

-(void)awakeFromNib {
    [super awakeFromNib];
}
- (IBAction)quitButtonClick:(UIButton *)sender {
    if (self.quitBlock) {
        self.quitBlock();
    }
}

- (IBAction)lightButtonClick:(UIButton *)sender {
    if (self.lightBlock) {
        self.lightBlock();
    }
}
- (IBAction)reverButtonClick:(UIButton *)sender {
    if (self.reverBlock) {
        self.reverBlock();
    }
}

@end
