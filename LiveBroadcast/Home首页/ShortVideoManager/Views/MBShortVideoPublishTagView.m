//
//  MBShortVideoPublishTagView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoPublishTagView.h"
#import "MBShortVideoPublishTagCollectionCell.h"

@interface MBShortVideoPublishTagView ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic, strong) UICollectionViewFlowLayout * layout;

@end

static NSInteger const cols = 4;
static CGFloat const margin = 20;
static CGFloat const CellHeight = 50;

static NSString * const MBShortVideoPublishTagCollectionCellID = @"MBShortVideoPublishTagCollectionCellID";

@implementation MBShortVideoPublishTagView

-(CGFloat)getCurrentViewHeight {
    NSInteger row = (NSInteger)ceilf(_dataArray.count / 4.0);
    CGFloat height = row * CellHeight + 55 + (row -1) * margin + 50;
    return height;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.topTitleLabel.font = kFont(14);
    self.topTitleLabel.textColor = Color_333333;
    self.mainCollectionView = ({
        MBBaseCollectionView *mainCollectionView = [[MBBaseCollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:self.layout];
        mainCollectionView.collectionViewLayout = self.layout;
        mainCollectionView.backgroundColor = Color_White;
        mainCollectionView.bounces = NO;
        mainCollectionView.showsVerticalScrollIndicator = NO;
        mainCollectionView.showsHorizontalScrollIndicator = NO;
        mainCollectionView.dataSource = self;
        mainCollectionView.delegate = self;
        mainCollectionView.scrollEnabled = NO;
//        mainCollectionView.allowsMultipleSelection = YES;
        [mainCollectionView registerClass:[MBShortVideoPublishTagCollectionCell class] forCellWithReuseIdentifier:MBShortVideoPublishTagCollectionCellID];
        [self.customContentView addSubview:mainCollectionView];
        [mainCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.customContentView);
        }];
        mainCollectionView;
    });

    
}


#pragma mark - setter
-(void)setDataArray:(NSArray *)dataArray {
    _dataArray = dataArray;
    [self.mainCollectionView reloadData];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBShortVideoPublishTagCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBShortVideoPublishTagCollectionCellID forIndexPath:indexPath];
    PublishActivityTagModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    PublishActivityTagModel *model = self.dataArray[indexPath.row];
    self.selectModel = model;
//    MBLog(@"++++++%@",collectionView.indexPathsForSelectedItems);
}


#pragma mark - getter

-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        CGFloat cellWidth = (kScreenWidth - 3*margin -30) / cols;
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = margin;
        CGFloat cellHeight = CellHeight;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}

@end
