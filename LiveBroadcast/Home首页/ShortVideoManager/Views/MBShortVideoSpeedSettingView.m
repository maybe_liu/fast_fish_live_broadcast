//
//  MBShortVideoSpeedSettingView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoSpeedSettingView.h"
#import "MBShortVideoMovementSettingCell.h"

@interface MBShortVideoSpeedSettingView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)NSArray *dataArray;
@property (nonatomic, strong)UICollectionViewFlowLayout * layout;


@end

static NSString * const MBShortVideoMovementSettingCellIDs = @"MBShortVideoMovementSettingCellIDs";

@implementation MBShortVideoSpeedSettingView

- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.speedCancelBlock) {
        self.speedCancelBlock();
    }
}

- (IBAction)tureButtonClick:(UIButton *)sender {
    if (self.speedTureBlock) {
        self.speedTureBlock();
    }
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self initUI];
}
-(void)initUI {
    self.bottomTitleLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.bottomTitleLabel.font = kFont(16);
    self.mainCollection.collectionViewLayout = self.layout;
    self.mainCollection.showsVerticalScrollIndicator = NO;
    self.mainCollection.showsHorizontalScrollIndicator = NO;
    self.mainCollection.dataSource = self;
    self.mainCollection.delegate = self;
    self.mainCollection.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
    self.mainCollection.backgroundColor = [UIColor clearColor];
    [self.mainCollection registerClass:[MBShortVideoMovementSettingCell class] forCellWithReuseIdentifier:MBShortVideoMovementSettingCellIDs];
    [self.mainCollection reloadData];
    self.revocationButton.postion = MBImagePositionTop;
    self.revocationButton.spacing = 6.0f;
    [self.revocationButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.revocationButton.titleLabel.font = kFont(12);
}
#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBShortVideoMovementSettingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBShortVideoMovementSettingCellIDs forIndexPath:indexPath];
    cell.dataDict = self.dataArray[indexPath.row];
    cell.topImageButton.userInteractionEnabled = NO;
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:{
            if ([self.delegate respondsToSelector:@selector(onVideoTimeEffectsClear)]) {
                [self.delegate onVideoTimeEffectsClear];
            }
        }break;
        case 1:{
            if ([self.delegate respondsToSelector:@selector(onVideoTimeEffectsBackPlay)]) {
                [self.delegate onVideoTimeEffectsBackPlay];
            }
        }break;
        case 2:{
            if ([self.delegate respondsToSelector:@selector(onVideoTimeEffectsRepeat)]) {
                [self.delegate onVideoTimeEffectsRepeat];
            }
        }break;
        case 3:{
            if ([self.delegate respondsToSelector:@selector(onVideoTimeEffectsSpeed)]) {
                [self.delegate onVideoTimeEffectsSpeed];
            }
        }break;
            
       
    }
}

- (IBAction)revocationButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(onVideoTimeEffectsClear)]) {
        [self.delegate onVideoTimeEffectsClear];
    }
}

-(NSArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = @[
                       @{@"icon":@"motion_time_normal",@"title":@"无"},
                       @{@"icon":@"motion_time_reverse",@"title":@"倒放"},
                       @{@"icon":@"motion_time_repeat",@"title":@"反复"},
                       @{@"icon":@"motion_time_slow",@"title":@"慢动作"},
                       ];
    }
    return _dataArray;
}
-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat cellWidth = 50;
        layout.minimumLineSpacing = 15;
        CGFloat cellHeight = 90;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}

@end
