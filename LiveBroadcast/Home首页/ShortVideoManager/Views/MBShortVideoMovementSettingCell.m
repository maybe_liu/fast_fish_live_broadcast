//
//  MBShortVideoMovementSettingCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoMovementSettingCell.h"

@implementation MBShortVideoMovementSettingCell

-(void)beginPress:(UIButton *)button {
    if (self.beginPressBlock) {
        self.beginPressBlock();
    }
}
-(void)endPress:(UIButton *)button{
    if (self.endPressBlock) {
        self.endPressBlock();
    }
}

-(void)setDataDict:(NSDictionary *)dataDict {
    _dataDict = dataDict;
    NSURL *parh = [[NSBundle mainBundle]URLForResource:dataDict[@"icon"] withExtension:@"gif"];
    YYImage * image = [YYImage imageWithContentsOfFile:parh.path];
    self.gifImageView.image = image;
    self.bottomNameLabel.text = _dataDict[@"title"];
    
//    [self.topImageButton setImage:[UIImage imageNamed:_dataDict[@"icon"]] forState:UIControlStateNormal];
//    [self.topImageButton setTitle:_dataDict[@"title"] forState:UIControlStateNormal];
}
-(void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    self.gifImageView.image = [UIImage imageNamed:_imageName];
//        [self.topImageButton setImage:[UIImage imageNamed:_imageName] forState:UIControlStateNormal];
}
-(void)setFilterName:(NSString *)filterName {
    _filterName = filterName;
    self.bottomNameLabel.text = _filterName;
//    [self.topImageButton setTitle:_filterName forState:UIControlStateNormal];

}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
//        [self addSubview:self.topImageButton];
        [self.contentView addSubview:self.gifImageView];
        [self.contentView addSubview:self.bottomNameLabel];
        [self.contentView addSubview:self.topImageButton];
        
        [self layoutChildViews];
    }
    return self;
}



-(void)layoutChildViews{
    MB_WeakSelf(self);
    [self.gifImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakself.contentView.mas_top).offset(5);
        make.centerX.equalTo(weakself.contentView.mas_centerX);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(50);
    }];
    [self.bottomNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakself.gifImageView.mas_bottom).offset(6);
        make.left.equalTo(weakself.gifImageView.mas_left);
        make.right.equalTo(weakself.gifImageView.mas_right);
    }];
    [self.topImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakself.contentView);
    }];
}
//-(MBCustomButton *)topImageButton {
//    if (_topImageButton == nil) {
//        _topImageButton = [MBCustomButton buttonWithType:UIButtonTypeCustom];
//        _topImageButton.postion = MBImagePositionTop;
//        _topImageButton.spacing = 6.0f;
//        _topImageButton.titleLabel.font = kFont(12);
//        [_topImageButton setTitleColor:Color_White forState:UIControlStateNormal];
//        [_topImageButton addTarget:self action:@selector(beginPress:) forControlEvents:UIControlEventTouchDown];
//        [_topImageButton addTarget:self action:@selector(endPress:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
//
//    }
//    return _topImageButton;
//}
//-(UIImageView *)topImageView{
//    if (_topImageView == nil) {
//        _topImageView = [[UIImageView alloc]init];
//        _topImageView.image = [UIImage imageNamed:@""];
//        _topImageView.layer.cornerRadius = 25;
//        _topImageView.layer.masksToBounds = YES;
//    }
//    return _topImageView;
//}

-(MBCustomButton *)topImageButton {
    if (_topImageButton == nil) {
        _topImageButton = [MBCustomButton buttonWithType:UIButtonTypeCustom];
        [_topImageButton setBackgroundColor:[UIColor clearColor]];
        [_topImageButton addTarget:self action:@selector(beginPress:) forControlEvents:UIControlEventTouchDown];
        [_topImageButton addTarget:self action:@selector(endPress:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    }
    return _topImageButton;
}
-(YYAnimatedImageView *)gifImageView {
    if (_gifImageView == nil) {
        _gifImageView = [[YYAnimatedImageView alloc]init];
        _gifImageView.autoPlayAnimatedImage = YES;
    }
    return _gifImageView;
}

-(UILabel *)bottomNameLabel{
    if (_bottomNameLabel == nil) {
        _bottomNameLabel = [[UILabel alloc]init];
        _bottomNameLabel.font = kFont(12);
        _bottomNameLabel.textColor = Color_White;
        [_bottomNameLabel sizeToFit];
        _bottomNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _bottomNameLabel;
}


@end
