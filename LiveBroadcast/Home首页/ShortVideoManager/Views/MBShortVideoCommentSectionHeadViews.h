//
//  MBShortVideoCommentSectionHeadViews.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoDetailModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^JumpToUserCenterBlock)(void);

@interface MBShortVideoCommentSectionHeadViews : UITableViewHeaderFooterView
@property (nonatomic, copy) JumpToUserCenterBlock jumpBlock;

//@property (weak, nonatomic) IBOutlet UIButton *userButton;
//@property (weak, nonatomic) IBOutlet UILabel *userTitleLabel;
//@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
//@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property(nonatomic, strong)MBShortVideoDetailModel *model;

@end

NS_ASSUME_NONNULL_END
