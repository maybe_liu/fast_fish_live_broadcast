//
//  MBShortVideoEditBottomView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol MBShortVideoEditBottomDelegate <NSObject>

-(void)onMusicButtonClick:(BOOL)isSelect; /*音乐*/
-(void)onFilterButtonClick:(BOOL)isSelect;/*滤镜*/
-(void)onCoverButtonClick:(BOOL)isSelect;/*封面*/
-(void)onMovementButtonClick:(BOOL)isSelect;/*动作*/
-(void)onSpeedButtonClick:(BOOL)isSelect;/*速度*/
-(void)onCharButtonClick:(BOOL)isSelect;/*文字*/


@end


@interface MBShortVideoEditBottomView : UIView
@property (weak, nonatomic) IBOutlet MBCustomButton *musicButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *filterButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *coverButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *movementButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *speedButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *charButton;

@property(nonatomic, weak)id<MBShortVideoEditBottomDelegate>delegate;
@end

NS_ASSUME_NONNULL_END
