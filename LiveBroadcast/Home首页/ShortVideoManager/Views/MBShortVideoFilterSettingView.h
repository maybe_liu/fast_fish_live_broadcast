//
//  MBShortVideoFilterSettingView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^FilterCancelBlockHandler)(void);
typedef void(^FilterTureBlockHandler)(void);

@protocol FilterSettingViewDelegate <NSObject>

- (void)onSetFilterWithImage:(UIImage*)image;
- (void)onSetBeautyDepth:(float)beautyDepth WhiteningDepth:(float)whiteningDepth;

@end

@interface MBShortVideoFilterSettingView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet MBBaseCollectionView *mainCollection;
@property (nonatomic, copy) FilterCancelBlockHandler filterCancelBlock;
@property (nonatomic, copy) FilterTureBlockHandler filterTureBlock;

@property (nonatomic, weak) id<FilterSettingViewDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
