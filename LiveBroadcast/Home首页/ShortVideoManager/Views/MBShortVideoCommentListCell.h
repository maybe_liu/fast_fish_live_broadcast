//
//  MBShortVideoCommentListCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoConnmentListModel.h"

NS_ASSUME_NONNULL_BEGIN

/*展开收起*/
typedef void(^CommonPakeup)(void);
/*刷新tableview*/
typedef void(^FloorUp)(void);

typedef void(^LikeBlockHandler)(MBShortVideoCommentModel *commentModel);

@interface MBShortVideoCommentListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet MBCustomButton *likeButton;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet MBCustomButton *moreButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (nonatomic, copy) CommonPakeup commonPakeup;
@property (nonatomic, copy) FloorUp floorUp;
@property (nonatomic, copy) LikeBlockHandler likeBlock;



@property (nonatomic,assign) BOOL showFloor;   //看错全部楼层

@property (nonatomic, strong)MBShortVideoCommentReplyModel *commentModel;

//有多少层展示多少层
- (void)setView;
//最多展示3层
- (void)FloorShow:(NSArray *)reply;
//楼层大于3 楼层折叠时
- (void)FloorFolding:(NSArray *)reply;



@end

NS_ASSUME_NONNULL_END
