//
//  MBShortVideoBottomCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShootBlockHandler)(void);
typedef void(^ShootLongBlockHandler)(void);
typedef void(^NextBlockHandler)(void);
typedef void(^PhotoBlockHandler)(void);
typedef void(^DeleteBlockHandler)(UIButton *button);



@interface MBShortVideoBottomCommonView : UIView
@property (weak, nonatomic) IBOutlet UIButton *shootButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *nextButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *photoButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (copy, nonatomic) ShootBlockHandler shootBlcok;
@property (copy, nonatomic) ShootLongBlockHandler longShootBlcok;
@property (copy, nonatomic) NextBlockHandler nextBlcok;
@property (copy, nonatomic) PhotoBlockHandler photoBlcok;
@property (copy, nonatomic) DeleteBlockHandler deleteBlcok;
@property (nonatomic, assign)MBShortVideoState videoState;
@property (nonatomic, assign)NSInteger recordTime;


@end

NS_ASSUME_NONNULL_END
