//
//  MBShortVideoMovementSettingCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^beginPressBlock)(void);
typedef void(^endPressBlock)(void);


@interface MBShortVideoMovementSettingCell : UICollectionViewCell
@property(nonatomic, copy)NSString *imageName;
@property(nonatomic, copy)NSString *filterName;
@property(nonatomic, strong)NSDictionary *dataDict;
@property(nonatomic, strong)MBCustomButton *topImageButton;
@property(nonatomic, strong)UILabel *bottomNameLabel;
@property(nonatomic, strong)YYAnimatedImageView *gifImageView;

@property(nonatomic, copy)beginPressBlock beginPressBlock;
@property(nonatomic, copy)endPressBlock endPressBlock;
//@property(nonatomic, )

@end

NS_ASSUME_NONNULL_END
