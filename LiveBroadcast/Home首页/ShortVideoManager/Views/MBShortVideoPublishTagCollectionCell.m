//
//  MBShortVideoPublishTagCollectionCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoPublishTagCollectionCell.h"

@interface MBShortVideoPublishTagCollectionCell ()
@property(nonatomic, strong)UILabel *customLabel;

@end

@implementation MBShortVideoPublishTagCollectionCell

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.customLabel = [[UILabel alloc]init];
    self.customLabel.font = kFont(14);
    self.customLabel.textAlignment = NSTextAlignmentCenter;
    self.customLabel.textColor = Color_333333;
    self.customLabel.backgroundColor = Color_F5F5F5;
    self.customLabel.layer.cornerRadius = 16.0f;
    self.customLabel.layer.masksToBounds = YES;
    [self.contentView addSubview:self.customLabel];
    [self.customLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(70, 32));
    }];
    
}
-(void)setModel:(PublishActivityTagModel *)model {
    _model = model;
    self.customLabel.text = _model.labelName;
}
-(void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.customLabel.textColor = Color_White;
        self.customLabel.backgroundColor = Color_FF8000;
    }else {
        self.customLabel.textColor = Color_333333;
        self.customLabel.backgroundColor = Color_F5F5F5;
    }
}

@end
