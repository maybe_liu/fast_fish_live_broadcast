//
//  MBShortVideoMovementSettingView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoMovementSettingView.h"
#import "MBShortVideoMovementSettingCell.h"

@interface MBShortVideoMovementSettingView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)NSArray *dataArray;
@property (nonatomic, strong)UICollectionViewFlowLayout * layout;


@end

static NSString * const MBShortVideoMovementSettingCellID = @"MBShortVideoMovementSettingCell";
@implementation MBShortVideoMovementSettingView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.bottomTitleLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.bottomTitleLabel.font = kFont(16);
    self.mainCollection.collectionViewLayout = self.layout;
    self.mainCollection.showsVerticalScrollIndicator = NO;
    self.mainCollection.showsHorizontalScrollIndicator = NO;
    self.mainCollection.dataSource = self;
    self.mainCollection.delegate = self;
    self.mainCollection.contentInset = UIEdgeInsetsMake(0, 20, 0, 20);
    self.mainCollection.backgroundColor = [UIColor clearColor];
    [self.mainCollection registerClass:[MBShortVideoMovementSettingCell class] forCellWithReuseIdentifier:MBShortVideoMovementSettingCellID];
    [self.mainCollection reloadData];
    self.revocationButton.postion = MBImagePositionTop;
    self.revocationButton.spacing = 6.0f;
    [self.revocationButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.revocationButton.titleLabel.font = kFont(12);
}

- (IBAction)cancelButtonClick:(UIButton *)sender {
//    [self.delegate onSetFilterWithImage:[UIImage imageNamed:@""]];
    if (self.movementCancelBlock) {
        self.movementCancelBlock();
    }
}

- (IBAction)tureButtonClick:(UIButton *)sender {
    if (self.movementTureBlock) {
        self.movementTureBlock();
    }
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBShortVideoMovementSettingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBShortVideoMovementSettingCellID forIndexPath:indexPath];
        cell.dataDict = self.dataArray[indexPath.row];
    MB_WeakSelf(self);
    cell.beginPressBlock = ^{
        [weakself beginPress:indexPath.row];
    };
    cell.endPressBlock = ^{
        [weakself endPress:indexPath.row];
    };
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
//    switch (indexPath.row) {
//        case 0:        [self.delegate onVideoEffectBeginClick:TXEffectType_ROCK_LIGHT];break;
//        case 1:        [self.delegate onVideoEffectBeginClick:TXEffectType_DARK_DRAEM];break;
//        case 2:        [self.delegate onVideoEffectBeginClick:TXEffectType_SOUL_OUT];break;
//        case 3:        [self.delegate onVideoEffectBeginClick:TXEffectType_SCREEN_SPLIT];break;
//        case 4:        [self.delegate onVideoEffectBeginClick:TXEffectType_WIN_SHADOW];break;
//        case 5:        [self.delegate onVideoEffectBeginClick:TXEffectType_GHOST_SHADOW];break;
//        case 6:        [self.delegate onVideoEffectBeginClick:TXEffectType_PHANTOM];break;
//        case 7:        [self.delegate onVideoEffectBeginClick:TXEffectType_GHOST];break;
//        case 8:        [self.delegate onVideoEffectBeginClick:TXEffectType_LIGHTNING];break;
//        case 9:        [self.delegate onVideoEffectBeginClick:TXEffectType_MIRROR];break;
//        case 10:       [self.delegate onVideoEffectBeginClick:TXEffectType_ILLUSION];break;
//    }
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
//    switch (indexPath.row) {
//        case 0:        [self.delegate onVideoEffectEndClick:TXEffectType_ROCK_LIGHT];break;
//        case 1:        [self.delegate onVideoEffectEndClick:TXEffectType_DARK_DRAEM];break;
//        case 2:        [self.delegate onVideoEffectEndClick:TXEffectType_SOUL_OUT];break;
//        case 3:        [self.delegate onVideoEffectEndClick:TXEffectType_SCREEN_SPLIT];break;
//        case 4:        [self.delegate onVideoEffectEndClick:TXEffectType_WIN_SHADOW];break;
//        case 5:        [self.delegate onVideoEffectEndClick:TXEffectType_GHOST_SHADOW];break;
//        case 6:        [self.delegate onVideoEffectEndClick:TXEffectType_PHANTOM];break;
//        case 7:        [self.delegate onVideoEffectEndClick:TXEffectType_GHOST];break;
//        case 8:        [self.delegate onVideoEffectEndClick:TXEffectType_LIGHTNING];break;
//        case 9:        [self.delegate onVideoEffectEndClick:TXEffectType_MIRROR];break;
//        case 10:       [self.delegate onVideoEffectEndClick:TXEffectType_ILLUSION];break;
//    }
}

//响应事件
-(void) beginPress:(NSInteger)tag {
//    CGFloat offset = _mainCollection.contentOffset.x;
//    if (offset < 0 || offset > _mainCollection.contentSize.width - _mainCollection.bounds.size.width) {
//        // 在回弹区域会触发button事件被cancel,导致收不到 TouchEnd 事件
//        return;
//    }
    TXEffectType type = (TXEffectType)tag;
    [self.delegate onVideoEffectBeginClick:type];
}

//响应事件
-(void) endPress:(NSInteger)tag {
    TXEffectType type = (TXEffectType)tag;
    [self.delegate onVideoEffectEndClick:type];
}
- (IBAction)revocationButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(onVideoEffectRevocation)]) {
        [self.delegate onVideoEffectRevocation];
    }
}



-(NSArray *)dataArray{
    if (_dataArray == nil) {
        _dataArray = @[
                       @{@"icon":@"motion_rock_light",@"title":@"动感光波"},
                       @{@"icon":@"motion_dark_dream",@"title":@"暗黑幻境"},
                       @{@"icon":@"motion_soul_out",@"title":@"灵魂出窍"},
                       @{@"icon":@"motion_split_screen",@"title":@"视频分裂"},
                       @{@"icon":@"motion_win_shaddow",@"title":@"百叶窗"},
                       @{@"icon":@"motion_illusion",@"title":@"鬼影"},
                       @{@"icon":@"motion_phantom_shaddow",@"title":@"幻影"},
                       @{@"icon":@"motion_ghost",@"title":@"幽灵"},
                       @{@"icon":@"motion_lightning",@"title":@"闪电"},
                       @{@"icon":@"motion_mirror",@"title":@"镜像"},
                       @{@"icon":@"motion_ghost_shaddow",@"title":@"幻觉"},
                       ];
    }
    return _dataArray;
}
-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat cellWidth = 50;
        layout.minimumLineSpacing = 15;
        CGFloat cellHeight = 90;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}


@end
