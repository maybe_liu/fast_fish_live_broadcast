//
//  MBShortVideoCommentListSectionView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoConnmentListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SectionClickBlockHandler)(MBShortVideoCommentModel *commentModel);

@interface MBShortVideoCommentListSectionView : UITableViewHeaderFooterView
@property (strong, nonatomic)  UIImageView *headImageView;
@property (strong, nonatomic)  UILabel *nameLabel;
@property (strong, nonatomic)  UILabel *timeLabel;
@property (strong, nonatomic)  MBCustomButton *likeButton;
@property (strong, nonatomic)  UILabel *commentLabel;
@property (strong, nonatomic)  MBCustomButton *moreButton;
@property (strong, nonatomic)  UIView *lineView;

@property (nonatomic, strong)MBShortVideoCommentModel *commentModel;
@property (nonatomic, copy) SectionClickBlockHandler sectionClickBlock;

@end

NS_ASSUME_NONNULL_END
