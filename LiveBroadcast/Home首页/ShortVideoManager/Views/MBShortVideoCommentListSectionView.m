//
//  MBShortVideoCommentListSectionView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCommentListSectionView.h"

@implementation MBShortVideoCommentListSectionView

-(void)setCommentModel:(MBShortVideoCommentModel *)commentModel {
    _commentModel = commentModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_commentModel.headUrl] placeholderImage:nil];
    self.nameLabel.text = _commentModel.nickname;
    self.timeLabel.text = _commentModel.sendTime;
    self.commentLabel.text = _commentModel.comment;
}


-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self initUI];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sectionClick)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

-(void)initUI {
    [self.contentView addSubview:self.headImageView];
    [self.contentView addSubview:self.nameLabel];
    [self.contentView addSubview:self.timeLabel];
    [self.contentView addSubview:self.commentLabel];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.moreButton];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    [self.headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(10);
        make.left.equalTo(self.contentView.mas_left).offset(12);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headImageView.mas_right).offset(8);
        make.top.equalTo(self.headImageView.mas_top).offset(4);
    }];
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel.mas_left);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
    }];
    [self.commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLabel.mas_left);
        make.top.equalTo(self.timeLabel.mas_bottom).offset(5);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.mas_centerX);
        make.top.equalTo(self.commentLabel.mas_bottom).offset(10);
    }];
}
-(void)sectionClick {
    if (self.sectionClickBlock) {
        self.sectionClickBlock(self.commentModel);
    }
}

- (void)moreButtonClick:(MBCustomButton *)sender {
    
}

-(UIImageView *)headImageView {
    if (_headImageView == nil) {
        _headImageView = [[UIImageView alloc]init];
        _headImageView.layer.cornerRadius = 20.0f;
        _headImageView.layer.masksToBounds = YES;
    }
    return _headImageView;
}

-(UILabel *)nameLabel {
    if (_nameLabel == nil) {
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.textColor = Color_333333;
        _nameLabel.font = kFont(12);
    }
    return _nameLabel;
}
-(UILabel *)timeLabel {
    if (_timeLabel == nil) {
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.textColor = Color_999999;
        _timeLabel.font = kFont(12);
    }
    return _timeLabel;
}

-(UILabel *)commentLabel {
    if (_commentLabel == nil) {
        _commentLabel = [[UILabel alloc]init];
        _commentLabel.textColor = Color_333333;
        _commentLabel.font = kFont(14);
    }
    return _commentLabel;
}
-(UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = Color_Line;
    }
    return _lineView;
}
-(MBCustomButton *)moreButton {
    if (_moreButton == nil) {
        _moreButton = [MBCustomButton buttonWithType:UIButtonTypeCustom];
        [_moreButton setTitle:@"展开更多评论" forState:UIControlStateNormal];
        _moreButton.postion = MBImagePositionRight;
        _moreButton.spacing = 4.0f;
        [_moreButton setTitleColor:[UIColor colorWithHexString:@"#2F91FF"] forState:UIControlStateNormal];
        _moreButton.titleLabel.font = kFont(12);
        _moreButton.hidden = YES;
        [_moreButton addTarget:self action:@selector(moreButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

@end
