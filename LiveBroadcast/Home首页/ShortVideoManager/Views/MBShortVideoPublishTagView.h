//
//  MBShortVideoPublishTagView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoPublishActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoPublishTagView : UIView
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (strong, nonatomic)  MBBaseCollectionView *mainCollectionView;
@property (nonatomic, strong)NSArray *dataArray;
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (nonatomic, strong) PublishActivityTagModel *selectModel;

-(CGFloat)getCurrentViewHeight;

//-(PublishActivityTagModel *)getSelectModel;
@end

NS_ASSUME_NONNULL_END
