//
//  MBShortVideoFilterSetCollectionCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/6.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoFilterSetCollectionCell.h"
@interface MBShortVideoFilterSetCollectionCell()
@property(nonatomic, strong)UIImageView *topImageView;
@property(nonatomic, strong)UILabel *bottomNameLabel;
@property(nonatomic, strong)UIImageView *selectImageView;
@end

@implementation MBShortVideoFilterSetCollectionCell

-(void)setDataDict:(NSDictionary *)dataDict {
    _dataDict = dataDict;
    self.topImageView.image = [UIImage imageNamed:_dataDict[@"icon"]];
    self.bottomNameLabel.text = _dataDict[@"title"];
}
-(void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    self.topImageView.image = [UIImage imageNamed:_imageName];
}
-(void)setFilterName:(NSString *)filterName {
    _filterName = filterName;
    self.bottomNameLabel.text = _filterName;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.topImageView];
        [self.contentView addSubview:self.bottomNameLabel];
        [self.contentView addSubview:self.selectImageView];
        [self layoutChildViews];
    }
    return self;
}

-(void)setSelected:(BOOL)selected {
    if (selected == YES) {
        self.selectImageView.hidden = NO;
    }else {
        self.selectImageView.hidden = YES;
    }
}


-(void)layoutChildViews{
    MB_WeakSelf(self);
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakself.contentView.mas_top).offset(0);
        make.centerX.equalTo(weakself.contentView.mas_centerX);
        make.height.mas_equalTo(50);
        make.width.mas_equalTo(50);
    }];
    [self.bottomNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakself.topImageView.mas_bottom).offset(6);
        make.left.equalTo(weakself.topImageView.mas_left);
        make.right.equalTo(weakself.topImageView.mas_right);
    }];
    [self.selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.topImageView);
    }];
}
-(UIImageView *)topImageView{
    if (_topImageView == nil) {
        _topImageView = [[UIImageView alloc]init];
        _topImageView.image = [UIImage imageNamed:@""];
        _topImageView.layer.cornerRadius = 25;
        _topImageView.layer.masksToBounds = YES;
    }
    return _topImageView;
}
-(UILabel *)bottomNameLabel{
    if (_bottomNameLabel == nil) {
        _bottomNameLabel = [[UILabel alloc]init];
        _bottomNameLabel.font = kFont(12);
        _bottomNameLabel.textColor = Color_White;
        [_bottomNameLabel sizeToFit];
        _bottomNameLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _bottomNameLabel;
}
-(UIImageView *)selectImageView {
    if (_selectImageView == nil) {
        _selectImageView = [[UIImageView alloc]init];
        _selectImageView.layer.cornerRadius = 25;
        _selectImageView.layer.masksToBounds = YES;
        _selectImageView.image = kImage(@"filter_selected");
        _selectImageView.hidden = YES;
    }
    return _selectImageView;
}
@end
