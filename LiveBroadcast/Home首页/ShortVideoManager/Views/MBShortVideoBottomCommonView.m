//
//  MBShortVideoBottomCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoBottomCommonView.h"

@implementation MBShortVideoBottomCommonView

-(void)setRecordTime:(NSInteger)recordTime {
    _recordTime = recordTime;
    self.timeLabel.text = [NSString stringWithFormat:@"%zd%@",_recordTime,@"s"];
}

-(void)setVideoState:(MBShortVideoState)videoState {
    switch (videoState) {
        case MBShortVideoStateIng:{
            [self.shootButton setImage:kImage(@"tran_ing") forState:UIControlStateNormal];
            self.timeLabel.hidden = NO;
            self.photoButton.hidden = YES;
            self.deleteButton.hidden = YES;
            self.nextButton.hidden = NO;
        } break;
        case MBShortVideoStatePause:{
            [self.shootButton setImage:kImage(@"shortVideo_pause") forState:UIControlStateNormal];
            self.timeLabel.hidden = NO;
            self.photoButton.hidden = YES;
            self.deleteButton.hidden = NO;
            self.nextButton.hidden = NO;
        } break;
        case MBShortVideoStateComplete:{
            [self.shootButton setImage:kImage(@"") forState:UIControlStateNormal];
        } break;
        case MBShortVideoStateStart:{
            [self.shootButton setImage:kImage(@"tran_start") forState:UIControlStateNormal];
            self.timeLabel.hidden = YES;
            self.photoButton.hidden = NO;
            self.deleteButton.hidden = YES;
            self.nextButton.hidden = YES;
        } break;
    }
}

-(void)awakeFromNib {
    [super awakeFromNib];
    self.deleteButton.hidden = YES;
    self.timeLabel.hidden = YES;
    self.nextButton.hidden = YES;
    self.photoButton.postion = MBImagePositionTop;
    self.photoButton.spacing = 8.0;
    self.photoButton.titleLabel.font = kMFont(12);

    self.deleteButton.postion = MBImagePositionTop;
    self.deleteButton.spacing = 8.0;
    self.deleteButton.titleLabel.font = kMFont(12);
    
    self.nextButton.postion = MBImagePositionTop;
    self.nextButton.spacing = 8.0;
    self.nextButton.titleLabel.font = kMFont(12);
    
    self.timeLabel.font = kMFont(14);
    self.timeLabel.textColor = Color_White;
    
}

//-(void)shootButtonLongClick:(UILongPressGestureRecognizer *)longGest  {
//    if (longGest.state == UIGestureRecognizerStateBegan) {
//        if (self.longShootBlcok) {
//            self.longShootBlcok();
//        }
//    }
//}

- (IBAction)shootButton:(UIButton *)sender {
    if (self.shootBlcok) {
        self.shootBlcok();
    }
}
- (IBAction)nextButtonClick:(MBCustomButton *)sender {
    if (self.nextBlcok) {
        self.nextBlcok();
    }
}
- (IBAction)photoButtonClick:(MBCustomButton *)sender {
    if (self.photoBlcok) {
        self.photoBlcok();
    }
}
- (IBAction)deleteButtonClick:(MBCustomButton *)sender {
    if (self.deleteBlcok) {
        self.deleteBlcok(sender);
    }
}

@end
