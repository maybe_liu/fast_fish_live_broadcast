//
//  MBShortVideoCoverSettingView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCoverSettingView.h"

@implementation MBShortVideoCoverSettingView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}
-(void)initUI {
    self.bottomTitleLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.bottomTitleLabel.font = kFont(16);
}

- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.coverCancelBlock) {
        self.coverCancelBlock();
    }
}

- (IBAction)tureButtonClick:(UIButton *)sender {
    if (self.coverTureBlock) {
        self.coverTureBlock();
    }
}


@end
