//
//  MBShortVideoCommentHeadView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MBShortVideoCommentHeadViewDelegate <NSObject>
- (void)attentionUserButtonClick; /*查看用户事件*/
- (void)shareButtonClick;  /*分享*/
- (void)likeButtonClick; /*点赞*/
- (void)attentionButtonClick;/*关注*/
- (void)popButtonClick; /*返回*/
- (void)reportButtonClick; /*举报*/
@end

@interface MBShortVideoCommentHeadView : UIView
@property (weak, nonatomic) IBOutlet MBTextField *wordField;
@property (weak, nonatomic) IBOutlet MBCustomButton *shareButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet MBCustomButton *likeButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *attentionButton;
@property (weak, nonatomic) IBOutlet UIView *attentionView;
@property (weak, nonatomic) IBOutlet UIButton *attentionUserButton;
@property (weak, nonatomic) IBOutlet UIButton *popButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *reportButton;

@property (nonatomic, weak)id<MBShortVideoCommentHeadViewDelegate>delegate;

@property (nonatomic, copy) NSString *likeCount;
@property (nonatomic, copy)NSString *defaultImageURL;

-(void)refreshUIWithDataWith:(MBShortVideoDetailModel *)model;

-(void)hiddenDefaultWhenHaveData;

-(void)hiddenAttention:(BOOL)hidden;
@end

NS_ASSUME_NONNULL_END
