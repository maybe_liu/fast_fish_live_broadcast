//
//  MBShortVideoMusicMixView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicCollectionCell.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^MusicCancelBlockHandler)(void);
typedef void(^MusicTureBlockHandler)(void);

@protocol MusicMixViewDelegate <NSObject>

- (void)onOpenLocalMusicList;                                                                               //打开本地音乐
- (void)onSetVideoVolume:(CGFloat)videoVolume musicVolume:(CGFloat)musicVolume;                             //音量设置
- (void)onSetBGMWithFileAsset:(AVAsset*)fileAsset startTime:(CGFloat)startTime endTime:(CGFloat)endTime; //音乐设置
/*关闭添加bgm view*/
- (void)removeMusicView;

/*添加bgm 完成*/
- (void)completeMusicView;

@end

@interface MBShortVideoMusicMixView : UIView
@property (weak, nonatomic) IBOutlet UILabel *originsoundLabel;
@property (weak, nonatomic) IBOutlet UISlider *originsoundSlider;
@property (weak, nonatomic) IBOutlet UILabel *bgmsoundLabel;
@property (weak, nonatomic) IBOutlet UISlider *bgmsoundSlider;
@property (weak, nonatomic) IBOutlet UIButton *selectBGMButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet MBBaseCollectionView *musicCollection;
@property (nonatomic, copy)MusicCancelBlockHandler musicCancelBlock;
@property (nonatomic, copy)MusicTureBlockHandler musicTureBlock;

@property (nonatomic, weak) id<MusicMixViewDelegate> delegate;

- (void)addMusicInfo:(MusicInfo*)musicInfo;

@end

NS_ASSUME_NONNULL_END
