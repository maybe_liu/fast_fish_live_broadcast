//
//  MBShortVideoPublishTagCollectionCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBShortVideoPublishActivityModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoPublishTagCollectionCell : UICollectionViewCell
@property (nonatomic, copy) NSString *titleString;
@property (nonatomic, strong)PublishActivityTagModel *model;
@end

NS_ASSUME_NONNULL_END
