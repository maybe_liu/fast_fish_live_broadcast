//
//  MBShortVideoCoverSettingView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^CoverCancelBlockHandler)(void);
typedef void(^CoverTureBlockHandler)(void);

@interface MBShortVideoCoverSettingView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;

@property (nonatomic, copy) CoverCancelBlockHandler coverCancelBlock;
@property (nonatomic, copy) CoverTureBlockHandler coverTureBlock;

@end

NS_ASSUME_NONNULL_END
