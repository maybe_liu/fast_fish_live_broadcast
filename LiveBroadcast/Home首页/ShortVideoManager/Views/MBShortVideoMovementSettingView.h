//
//  MBShortVideoMovementSettingView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol VideoEffectViewDelegate <NSObject>
- (void)onVideoEffectBeginClick:(TXEffectType)effectType;
- (void)onVideoEffectEndClick:(TXEffectType)effectType;
- (void)onVideoEffectRevocation;

@end

typedef void(^MovementCancelBlockHandler)(void);
typedef void(^MovementTureBlockHandler)(void);

@interface MBShortVideoMovementSettingView : UIView
@property (weak, nonatomic) IBOutlet UIView *customTopContentView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet MBBaseCollectionView *mainCollection;
@property (weak, nonatomic) IBOutlet MBCustomButton *revocationButton;

@property (nonatomic,weak) id <VideoEffectViewDelegate> delegate;
@property (nonatomic, copy) MovementCancelBlockHandler movementCancelBlock;
@property (nonatomic, copy) MovementTureBlockHandler movementTureBlock;
@end

NS_ASSUME_NONNULL_END
