//
//  MBShortVideoCommentSectionHeadViews.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCommentSectionHeadViews.h"

@interface MBShortVideoCommentSectionHeadViews ()

@property (strong, nonatomic)  UIButton *userButton;
@property (strong, nonatomic)  UILabel *userTitleLabel;
@property (strong, nonatomic)  UIImageView *userImageView;

@property (strong, nonatomic)  UILabel *likeCountLabel;
@property (strong, nonatomic)  UIImageView *likeImageView;

@property (strong, nonatomic)  UILabel *commentCountLabel;
@property (strong, nonatomic)  UIImageView *commentCountImageView;

@property (nonatomic, strong) UIView *lineView;
@end

@implementation MBShortVideoCommentSectionHeadViews

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self createUI];
    }
    return self;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)createUI {
    [self.contentView addSubview:self.userImageView];
    [self.contentView addSubview:self.userButton];
    [self.contentView addSubview:self.userTitleLabel];
    [self.contentView addSubview:self.likeImageView];
    [self.contentView addSubview:self.likeCountLabel];
    [self.contentView addSubview:self.commentCountImageView];
    [self.contentView addSubview:self.commentCountLabel];
    [self.contentView addSubview:self.lineView];
    
    [self.userImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top).offset(12);
        make.left.equalTo(self.contentView.mas_left).offset(12);
    }];
    [self.userButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImageView.mas_right).offset(12);
        make.centerY.equalTo(self.userImageView.mas_centerY);
    }];
    [self.userTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userButton.mas_right).offset(12);
        make.centerY.equalTo(self.userImageView.mas_centerY);
    }];
    [self.likeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userImageView.mas_bottom).offset(12);
        make.left.equalTo(self.userImageView.mas_left);
    }];
    [self.likeCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.likeImageView.mas_right).offset(12);
        make.centerY.equalTo(self.likeImageView.mas_centerY);
    }];
    [self.commentCountImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.likeImageView.mas_bottom).offset(12);
        make.left.equalTo(self.userImageView.mas_left);
    }];
    [self.commentCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.commentCountImageView.mas_right).offset(12);
        make.centerY.equalTo(self.commentCountImageView.mas_centerY);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
}

-(void)initUI {
    [self.userButton setTitleColor:[UIColor colorWithHexString:@"#2F91FF"] forState:UIControlStateNormal];
    self.userButton.titleLabel.font = kFont(13);
    self.userTitleLabel.textColor = Color_333333;
    self.userTitleLabel.font = kFont(13);
    self.likeCountLabel.textColor = Color_333333;
    self.likeCountLabel.font = kFont(13);
    self.commentCountLabel.textColor = Color_333333;
    self.commentCountLabel.font = kFont(13);
}
- (void)userButtonClick:(UIButton *)sender {
    if (self.jumpBlock) {
        self.jumpBlock();
    }
}
-(void)refreshUIWithData:(MBShortVideoDetailModel *)model {
    [self.userButton setTitle:[NSString stringWithFormat:@"%@%@",model.nickname,@":"] forState:UIControlStateNormal];
    self.userTitleLabel.text = model.videoHeadline;
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@%@",model.clickPraise ? model.clickPraise : @"0" ,@" 赞"];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@%@",model.commentCount ? model.commentCount : @"0", @" 评论"];
}
-(void)setModel:(MBShortVideoDetailModel *)model {
    _model = model;
    [self.userButton setTitle:[NSString stringWithFormat:@"%@%@",model.nickname,@":"] forState:UIControlStateNormal];
    self.userTitleLabel.text = model.videoHeadline;
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@%@",model.clickPraise ? model.clickPraise : @"0" ,@" 赞"];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@%@",model.commentCount ? model.commentCount : @"0", @" 评论"];
}


-(UIImageView *)userImageView {
    if (_userImageView == nil) {
        _userImageView = [[UIImageView alloc]init];
        _userImageView.image = kImage(@"bianji");
    }
    return _userImageView;
}
-(UIButton *)userButton {
    if (_userButton == nil) {
        _userButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_userButton addTarget:self action:@selector(userButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_userButton setTitleColor:[UIColor colorWithHexString:@"#2F91FF"] forState:UIControlStateNormal];
        _userButton.titleLabel.font = kFont(13);
    }
    return _userButton;
}
-(UILabel *)userTitleLabel {
    if (_userTitleLabel == nil) {
        _userTitleLabel = [[UILabel alloc]init];
        _userTitleLabel.textColor = Color_333333;
        _userTitleLabel.font = kFont(13);
    }
    return _userTitleLabel;
}
-(UIImageView *)likeImageView {
    if (_likeImageView == nil) {
        _likeImageView = [[UIImageView alloc]init];
        _likeImageView.image = kImage(@"xin");
    }
    return _likeImageView;
}

-(UILabel *)likeCountLabel {
    if (_likeCountLabel == nil) {
        _likeCountLabel = [[UILabel alloc]init];
        _likeCountLabel.textColor = Color_333333;
        _likeCountLabel.font = kFont(13);
    }
    return _likeCountLabel;
}
-(UIImageView *)commentCountImageView {
    if (_commentCountImageView == nil) {
        _commentCountImageView = [[UIImageView alloc]init];
        _commentCountImageView.image = kImage(@"pingl");
    }
    return _commentCountImageView;
}
-(UILabel *)commentCountLabel {
    if (_commentCountLabel == nil) {
        _commentCountLabel = [[UILabel alloc]init];
        _commentCountLabel.textColor = Color_333333;
        _commentCountLabel.font = kFont(13);
    }
    return _commentCountLabel;
}
-(UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = Color_Line;
        _lineView.hidden = YES;
    }
    return _lineView;
}
@end
