//
//  MBShortVideoCommentHeadView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoCommentHeadView.h"

@interface MBShortVideoCommentHeadView ()

@property (nonatomic, strong)UIImageView *defaultImageView; /*进入页面展示图，有数据则隐藏*/

@end

@implementation MBShortVideoCommentHeadView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)refreshUIWithDataWith:(MBShortVideoDetailModel *)model {
    [self.attentionUserButton sd_setImageWithURL:[NSURL URLWithString:model.headUrl] forState:UIControlStateNormal];

    if ([model.whetherPraise isEqualToString:@"0"]) {
        [self.likeButton setImage:kImage(@"shortvideo_unlike") forState:UIControlStateNormal];
    }else {
        [self.likeButton setImage:kImage(@"shortvideo_like") forState:UIControlStateNormal];
    }
    self.bottomTitleLabel.text = model.videoHeadline;
    if ([model.yesOrNo isEqualToString:@"1"]) {/*已关注 则隐藏*/
        [self hiddenAttention:YES];
    }else {
        [self hiddenAttention:NO];
    }
}

-(void)hiddenAttention:(BOOL)hidden {
    if (hidden == YES) {
        self.attentionButton.hidden = YES;
        [self.attentionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@40);
        }];
    }else {
        self.attentionButton.hidden = NO;
        [self.attentionView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@118);
        }];
    }
}

-(void)setLikeCount:(NSString *)likeCount {
    _likeCount = likeCount;
    [self.likeButton setTitle:likeCount forState:UIControlStateNormal];
}
-(void)setDefaultImageURL:(NSString *)defaultImageURL {
    _defaultImageURL = defaultImageURL;
    [self.defaultImageView sd_setImageWithURL:[NSURL URLWithString:_defaultImageURL] placeholderImage:nil];
}
-(void)hiddenDefaultWhenHaveData {
    self.defaultImageView.hidden = YES;
}

-(void)initUI {
    self.wordField.placeholderFont = kFont(14);
    self.wordField.placeholderColor = Color_White;
    self.wordField.mTintColor = Color_FF8000;
    self.wordField.mBackgroundColor = [UIColor clearColor];
    self.bottomTitleLabel.font = kFont(16);
    self.bottomTitleLabel.textColor = Color_White;
    self.shareButton.postion = MBImagePositionTop;
    self.shareButton.spacing = 7;
    self.shareButton.titleLabel.font = kFont(12);
    self.likeButton.postion = MBImagePositionTop;
    self.likeButton.spacing = 7;
    self.likeButton.titleLabel.font = kFont(12);
    self.attentionView.layer.cornerRadius = 20;
    self.attentionView.layer.masksToBounds = YES;
    self.attentionView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    self.attentionButton.titleLabel.font = kFont(14);
    [self.attentionButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.attentionUserButton.layer.cornerRadius = 20;
    self.attentionUserButton.layer.masksToBounds = YES;
}



#pragma mark - event
- (IBAction)shareButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(shareButtonClick)]) {
        [self.delegate shareButtonClick];
    }
}
- (IBAction)likeButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(likeButtonClick)]) {
        [self.delegate likeButtonClick];
    }
}
- (IBAction)attentionButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(attentionButtonClick)]) {
        [self.delegate attentionButtonClick];
    }
}
- (IBAction)attentionUserButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(attentionUserButtonClick)]) {
        [self.delegate attentionUserButtonClick];
    }
}
- (IBAction)popButtonClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(popButtonClick)]) {
        [self.delegate popButtonClick];
    }
}
- (IBAction)reportButtonClick:(MBCustomButton *)sender {
    if ([self.delegate respondsToSelector:@selector(reportButtonClick)]) {
        [self.delegate reportButtonClick];
    }
}
-(UIImageView *)defaultImageView {
    if (_defaultImageView == nil) {
        _defaultImageView = [[UIImageView alloc]init];
        _defaultImageView.hidden = YES ;
    }
    return _defaultImageView;
}

@end
