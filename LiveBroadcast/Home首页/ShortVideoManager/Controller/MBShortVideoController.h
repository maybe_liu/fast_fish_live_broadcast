//
//  MBShortVideoController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "MBShortVideoRecordConfig.h"

NS_ASSUME_NONNULL_BEGIN

/*背景音乐*/
@interface RecordMusicInfo : NSObject
@property (nonatomic, copy) NSString* filePath;
@property (nonatomic, copy) NSString* soneName;
@property (nonatomic, copy) NSString* singerName;
@property (nonatomic, assign) CGFloat duration;

@end
/**
 短视频 录制主控制器
 */
@interface MBShortVideoController : MBBaseController
@property (nonatomic, copy) void(^onRecordCompleted)(TXUGCRecordResult *result);
@property(nonatomic, assign)CGFloat MAX_RECORD_TIME; /*录制时间*/
@property(nonatomic, assign)MBShortVideoEntityType entityType;


-(instancetype)initWithConfigure:(MBShortVideoRecordConfig *)configure;


@end



NS_ASSUME_NONNULL_END
