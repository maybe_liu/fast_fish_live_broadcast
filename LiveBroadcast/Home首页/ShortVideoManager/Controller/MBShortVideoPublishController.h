//
//  MBShortVideoPublishController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 发布短视频 页面
 */
@interface MBShortVideoPublishController : MBBaseController
@property (nonatomic, copy)NSString *videoURL; /*腾讯云短视频url*/
@property (nonatomic, copy)NSString *videoDownloadURL; /*腾讯云短视频下载url*/
@property (nonatomic, strong)UIImage *coverImage; 
@property (nonatomic, strong)NSString *videoPath; /*本地视频URL*/

@property (nonatomic, strong)UIImage *publishImage; /*发布图片*/
@property (nonatomic, strong)UIImage *publishImageCoverImage; /*发布图片的封面图*/
@property(nonatomic, assign)MBShortVideoEntityType entityType;

@end

NS_ASSUME_NONNULL_END
