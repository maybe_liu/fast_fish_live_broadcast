//
//  MBShortVideoEditController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoEditController.h"
#import "VideoPreview.h"
#import "MBShortVideoEditBottomView.h"
#import "VideoCutView.h"
#import <TXLiteAVSDK.h>
#import <MediaPlayer/MPMediaPickerController.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "VideoRangeSlider.h"
#import "VideoRangeConst.h"
#import "FilterSettingView.h"
#import "MusicMixView.h"
#import "PasterAddView.h"
#import "TextAddView.h"
#import "TimeSelectView.h"
#import "EffectSelectView.h"
#import "MBShortVideoTextEditController.h"
#import "TXCVEFColorPalette.h"
#import "TransitionView.h"
#import "TXColor.h"
#import "MBShortVideoPasterController.h"
#import "UIView+Additions.h"
#import "MBShortVideoPreviewController.h"
#import "MBShortVideoMusicMixView.h"
#import "MBShortVideoPublishController.h"
#import "MBQiNiuUpTokenModel.h"
#import "MBShortVideoFilterSettingView.h"
#import "MBShortVideoMovementSettingView.h"
#import "MBShortVideoSpeedSettingView.h"
#import "MBShortVideoCoverSettingView.h"



static const int ImageSlideFPS = 30;
typedef  NS_ENUM(NSInteger,ActionType)
{
    ActionType_Save,
    ActionType_Publish,
    ActionType_Save_Publish,
};
typedef  NS_ENUM(NSInteger,TimeType)
{
    TimeType_Clear,
    TimeType_Back,
    TimeType_Repeat,
    TimeType_Speed,
};
typedef  NS_ENUM(NSInteger,VideoType)
{
    VideoType_Video,
    VideoType_Picture,
};


@interface MBShortVideoEditController ()<TXVideoGenerateListener,VideoPreviewDelegate,MBShortVideoEditBottomDelegate,TXVideoCustomProcessListener, FilterSettingViewDelegate, VideoCutViewDelegate, MusicMixViewDelegate, PasterAddViewDelegate, TextAddViewDelegate, VideoPasterViewControllerDelegate, VideoTextViewControllerDelegate,VideoEffectViewDelegate,TimeSelectViewDelegate,TransitionViewDelegate,MPMediaPickerControllerDelegate, UIActionSheetDelegate, UITabBarDelegate>
@property (strong,nonatomic) VideoPreview   *videoPreview;
@property (strong,nonatomic) TXVideoEditer  *ugcEdit;
@property (assign,nonatomic) CGFloat        duration;
@property (nonatomic, strong) UIButton      *leftNavButton;
@property (nonatomic, strong) UIButton      *rightNavButton;
@property (nonatomic, strong) MBShortVideoFilterSettingView   *filterView; /*滤镜*/
@property (nonatomic, strong) MBShortVideoMovementSettingView *movementView; /*动作*/
@property (nonatomic, strong) MBShortVideoSpeedSettingView    *speedView; /*速度*/
@property (nonatomic, strong) MBShortVideoCoverSettingView    *coverView; /*封面*/

@end

@implementation MBShortVideoEditController
{
    //裁剪时间
    CGFloat         _leftTime;
    CGFloat         _rightTime;
    
    NSMutableArray  *_cutPathList;
    NSString        *_videoOutputPath;
    NSString        *_gifOutputPath;
    ActionType      _actionType;
    
    //生成时的进度浮层
    UILabel*        _generationTitleLabel;
    UIView*         _generationView;
    UIProgressView* _generateProgressView;
    UIButton*       _generateCannelBtn;
    
    UILabel*        _cutTipsLabel;
    UIColor*        _barTintColor;
    
    MBShortVideoEditBottomView*       _bottomBar;     //底部栏
    UIView*             _accessoryView; //二级工具栏
    VideoCutView*       _videoCutView;  //裁剪
//    FilterSettingView*  _filterView;    //滤镜
//    MBShortVideoFilterSettingView  *_filterView; //滤镜
    MBShortVideoMusicMixView       *_musixMixView;  //混音
    
    PasterAddView*      _pasterView;         //贴图
    TextAddView*        _textView;           //字幕
    TimeSelectView*     _timeSelectView;     //时间特效栏
//    EffectSelectView*   _effectSelectView;   //动效选择
    TransitionView*     _transitionView;     //转场特效
    
    int                 _effectType;
    TimeType            _timeType;
    
    NSMutableArray<VideoTextInfo*>*   _videoTextInfos;   //保存己添加的字幕
    NSMutableArray<VideoPasterInfo*>* _videoPaterInfos;  //保存己添加的贴纸
    AVURLAsset*   _fileAsset;
    
    CGFloat _playTime;
    
    NSArray *_videoPreviewConstrains;
    MASViewAttribute *topGuide;
    MASViewAttribute *bottomGuide;
    MASViewAttribute *leftGuide;
    MASViewAttribute *rightGuide;
    CGFloat videoPreviewBarHeight;
    CGFloat accessoryToolHeight;
    
    BOOL  _isReverse;
    BOOL  _isBGMLoop;
    BOOL  _isFadeIn;
    BOOL  _isFadeOut;
//    UIImage * _coverImage;
}
-(instancetype)init
{
    self = [super init];
    if (self) {
        _cutPathList = [NSMutableArray array];
        _videoOutputPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"outputCut.mp4"];
        _gifOutputPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"outputCut.gif"];
        _videoTextInfos = [NSMutableArray new];
        _videoPaterInfos = [NSMutableArray new];
        _effectType = -1;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent  =  YES;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_videoCutView stopGetImageList];
    self.navigationController.navigationBar.translucent  =  NO;
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}
- (void)dealloc
{
    [_videoPreview removeNotification];
    if (self.removeVideoAfterFinish) {
        [[NSFileManager defaultManager] removeItemAtPath:_videoPath error:nil];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if (_videoAsset == nil && _videoPath != nil) {
        NSURL *avUrl = [NSURL fileURLWithPath:_videoPath];
        _videoAsset = [AVAsset assetWithURL:avUrl];
    }
    
    TXVideoInfo *videoMsg = [TXVideoInfoReader getVideoInfoWithAsset:_videoAsset];
    CGFloat duration = videoMsg.duration;
    _rightTime = duration;
    
    videoPreviewBarHeight = 100 * kScaleY;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.leftNavButton];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightNavButton];
    
    self.view.backgroundColor = Color_Bj;
    
    UIEdgeInsets insets = UIEdgeInsetsZero;
    id boundaryAttribute = self.view;
    
    topGuide    = self.view.mas_top;
    bottomGuide = self.view.mas_bottom;
    leftGuide   = self.view.mas_left;
    rightGuide  = self.view.mas_right;
    if (@available(iOS 11, *)) {
        boundaryAttribute = self.view.mas_safeAreaLayoutGuide;
        topGuide = self.view.mas_safeAreaLayoutGuideTop;
        bottomGuide = self.view.mas_safeAreaLayoutGuideBottom;
        leftGuide   = self.view.mas_safeAreaLayoutGuideLeft;
        rightGuide  = self.view.mas_safeAreaLayoutGuideRight;
        
        insets = [UIApplication sharedApplication].keyWindow.safeAreaInsets;
        insets.top = 0;
    }
    
    CGRect contentFrame = UIEdgeInsetsInsetRect(self.view.bounds, insets);
    
    CGFloat bottomToolHeight = 50;
    accessoryToolHeight = round([UIScreen mainScreen].bounds.size.height >= 667 ? 90 * kScaleY : 80 * kScaleY);
    accessoryToolHeight = 180; /*Maybe 添加*/
    
    _videoPreview = [[VideoPreview alloc] initWithFrame:self.view.frame coverImage:nil];
    _videoPreview.backgroundColor = [UIColor whiteColor];
    _videoPreview.delegate = self;
    [self.view addSubview:_videoPreview];
    
//    _videoPreviewConstrains = [_videoPreview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
//    }];
    
    _bottomBar = [MBShortVideoEditBottomView nibView];
    _bottomBar.frame = CGRectMake(0, kScreenHeight - 50 - 0 - kTabSafeBottomMargin, kScreenWidth, 50);
    _bottomBar.backgroundColor = [UIColor clearColor];
    _bottomBar.delegate = self;
    [self.view addSubview:_bottomBar];
    
    [_bottomBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(bottomToolHeight));
        make.left.equalTo(leftGuide);
        make.right.equalTo(rightGuide);
        make.bottom.equalTo(bottomGuide).offset(-20);
    }];
    
    CGRect accessoryFrame = contentFrame;
    accessoryFrame.origin.y = _bottomBar.top - accessoryToolHeight;
    accessoryFrame.size.height = accessoryToolHeight;
    _accessoryView = [[UIView alloc] initWithFrame:accessoryFrame];
    _accessoryView.clipsToBounds = YES;
    _accessoryView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    _accessoryView.hidden = YES;
//    _accessoryView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:_accessoryView];
    [_accessoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(accessoryToolHeight));
        make.left.equalTo(leftGuide);
        make.right.equalTo(rightGuide);
        make.bottom.equalTo(self.view.mas_bottom).offset(-kTabSafeBottomMargin);
    }];
    
    
    CGFloat selectViewHeight = [UIScreen mainScreen].bounds.size.height >= 667 ? 90 * kScaleY : 80 * kScaleY;
    _timeSelectView = [[TimeSelectView alloc] initWithFrame:CGRectMake(0, _bottomBar.top -  selectViewHeight, self.view.width, selectViewHeight)];
    _timeSelectView.delegate = self;
    _timeSelectView.hidden = (_videoAsset == nil);
    
    _transitionView = [[TransitionView alloc] initWithFrame:CGRectMake(0, _bottomBar.top -  selectViewHeight, self.view.width, selectViewHeight)];
    _transitionView.delegate = self;
    _transitionView.hidden = (_imageList == nil);
    
//    _effectSelectView = [[EffectSelectView alloc] initWithFrame:_timeSelectView.frame];
//    _effectSelectView.delegate = self;
    
    _cutTipsLabel = [[UILabel alloc] initWithFrame:_accessoryView.bounds];
    _cutTipsLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _cutTipsLabel.textAlignment = NSTextAlignmentCenter;
    _cutTipsLabel.text = @"请拖拽两侧滑块选择剪裁区域";
    _cutTipsLabel.textColor = [UIColor whiteColor];
    _cutTipsLabel.font = [UIFont systemFontOfSize:16];
//    [_accessoryView addSubview:_cutTipsLabel];
    
    TXPreviewParam *param = [[TXPreviewParam alloc] init];
    param.videoView = _videoPreview.renderView;
    param.renderMode = PREVIEW_RENDER_MODE_FILL_SCREEN;
    _ugcEdit = [[TXVideoEditer alloc] initWithPreview:param];
    _ugcEdit.generateDelegate = self;
    _ugcEdit.videoProcessDelegate = self;
    _ugcEdit.previewDelegate = _videoPreview;
    
    //    [_ugcEdit setVideoPath:_videoPath];
    //video
    if (_videoAsset != nil) {
        int result = [_ugcEdit setVideoAsset:_videoAsset];
        if (result != 0) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"视频读取失败"
                                                                message:result == -1 ? @"视频文件不存在" : @"暂不支持声道数大于2的视频编辑"
                                                               delegate:self
                                                      cancelButtonTitle:@"知道了"
                                                      otherButtonTitles:nil, nil];
            [alertView show];
        }
        [self initVideoCutView:VideoType_Video];
    }
    //image
    if (_imageList != nil) {
        [_ugcEdit setPictureList:_imageList fps:ImageSlideFPS];
        [self onVideoTransitionLefRightSlipping];
    }
    [self addWaterMark:videoMsg];
    _videoCutView.hidden = YES;
}
- (void)addWaterMark:(TXVideoInfo *)videoMsg {
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), ^{
        CGFloat videoWidth, videoHeight;
        if (videoMsg.angle / 90 % 2 == 0) {
            videoWidth = videoMsg.width;
            videoHeight = videoMsg.height;
        } else {
            videoWidth = videoMsg.height;
            videoHeight = videoMsg.width;
        }
        
        //图片编辑，视频宽高默认设置为720 * 1280
        if (videoWidth == 0 || videoHeight == 0) {
            videoWidth = 720;
            videoHeight = 1280;
        }
        
        UIImage *cloud = [UIImage imageNamed:@"tcloud_symbol"];
        CGFloat imageWidth;
        if (videoWidth > videoHeight) {
            imageWidth = 0.08*videoHeight;
        } else {
            imageWidth = 0.08*videoWidth;
        }
        CGFloat imageHeight = imageWidth / cloud.size.width * cloud.size.height;
        
        NSDictionary *textAttribute = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:imageHeight],
                                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        CGSize textSize = [@"快鱼直播" sizeWithAttributes:textAttribute];
        CGSize canvasSize = CGSizeMake(ceil(imageWidth + textSize.width), ceil(MAX(imageHeight,textSize.height) + imageWidth* 0.05));
        UIGraphicsBeginImageContext(canvasSize);
        [cloud drawInRect:CGRectMake(0, (canvasSize.height - imageHeight) / 2, imageWidth, imageHeight)];
        [@"快鱼直播" drawAtPoint:CGPointMake(imageWidth*1.05, (canvasSize.height - textSize.height) / 2)
             withAttributes:textAttribute];
        UIImage *waterimage = UIGraphicsGetImageFromCurrentImageContext(); //[UIImage imageNamed:@"watermark"];
        UIGraphicsEndImageContext();
        
        [_ugcEdit setWaterMark:waterimage normalizationFrame:CGRectMake(0.01, 0.01, canvasSize.width / videoWidth, 0)];
        
        UIImage *tailWaterimage = [UIImage imageNamed:@"tcloud_logo"];
        float w = 0.15;
        float x = (1.0 - w) / 2.0;
        float width = w * videoWidth;
        float height = width * tailWaterimage.size.height / tailWaterimage.size.width;
        float y = (videoHeight - height) / 2 / videoHeight;
        [_ugcEdit setTailWaterMark:tailWaterimage normalizationFrame:CGRectMake(x,y,w,0) duration:2];
    });
}

- (void)initVideoCutView:(VideoType)type
{
    CGRect frame = _accessoryView.frame;
    frame.origin.y -= videoPreviewBarHeight;
    frame.size.height = videoPreviewBarHeight;
    BOOL flag = NO;
    if (type == VideoType_Video) {
        if(_videoCutView) [_videoCutView removeFromSuperview];
        _videoCutView = [[VideoCutView alloc] initWithFrame:frame videoPath:_videoPath orVideoAsset:_videoAsset];
        _videoCutView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
        flag = YES;
        [self.view addSubview:_videoCutView];
    }else{
        if (_videoCutView) {
            [_videoCutView updateFrame:_duration - 1/ 30];
        }else{
            [_videoCutView removeFromSuperview];
            _videoCutView = [[VideoCutView alloc] initWithFrame:frame pictureList:_imageList duration:_duration fps:ImageSlideFPS];
            _videoCutView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
            flag = YES;
            [self.view addSubview:_videoCutView];
        }
    }
    if (flag) {
        [_videoCutView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(videoPreviewBarHeight));
            make.left.equalTo(leftGuide);
            make.right.equalTo(rightGuide);
            make.bottom.equalTo(_accessoryView.mas_top);
        }];
    }
    _videoCutView.delegate = self;
    [_videoCutView setCenterPanHidden:YES];
}

- (UIView*)generatingView
{
    /*用作生成时的提示浮层*/
    if (!_generationView) {
        _generationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height + 64)];
        _generationView.backgroundColor = UIColor.blackColor;
        _generationView.alpha = 0.9f;
        
        _generateProgressView = [UIProgressView new];
        _generateProgressView.center = CGPointMake(_generationView.width / 2, _generationView.height / 2);
        _generateProgressView.bounds = CGRectMake(0, 0, 225, 20);
        _generateProgressView.progressTintColor = TXColor.cyan;
        [_generateProgressView setTrackImage:[UIImage imageNamed:@"slide_bar_small"]];
        //_generateProgressView.trackTintColor = UIColor.whiteColor;
        //_generateProgressView.transform = CGAffineTransformMakeScale(1.0, 2.0);
        
        _generationTitleLabel = [UILabel new];
        _generationTitleLabel.font = [UIFont systemFontOfSize:14];
        _generationTitleLabel.text = @"视频生成中";
        _generationTitleLabel.textColor = UIColor.whiteColor;
        _generationTitleLabel.textAlignment = NSTextAlignmentCenter;
        _generationTitleLabel.frame = CGRectMake(0, _generateProgressView.y - 34, _generationView.width, 14);
        
        _generateCannelBtn = [UIButton new];
        [_generateCannelBtn setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
        _generateCannelBtn.frame = CGRectMake(_generateProgressView.right + 15, _generationTitleLabel.bottom + 10, 20, 20);
        [_generateCannelBtn addTarget:self action:@selector(onGenerateCancelBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [_generationView addSubview:_generationTitleLabel];
        [_generationView addSubview:_generateProgressView];
        [_generationView addSubview:_generateCannelBtn];
        [[[UIApplication sharedApplication] delegate].window addSubview:_generationView];
    }
    
    _generateProgressView.progress = 0.f;
    return _generationView;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_videoPreview.isPlaying) {
        [_videoPreview playVideo];
    }
}

- (void)goBack
{
    [self pause];
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

//保存
- (void)goSave
{
    [self pause];
    
    _actionType = ActionType_Save;
    
    if (_videoCutView.coloredCount > 0) {
        // 因为有特效时画面的变动会比较大，需要适当提高码率来保证清晰度
        int fps = 30;
        if (_imageList.count > 0) {
            fps = ImageSlideFPS;
        } else {
            fps = [TXVideoInfoReader getVideoInfoWithAsset:_videoAsset].fps;
        }
        [_ugcEdit setVideoBitrate:500 + 100 * fps];
    }
    [self onVideoPause];
    [_videoPreview setPlayBtn:NO];
    [self startGenerateWithTwoPassEnabled:NO];
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择压缩模式"
//                                                                             message:nil
//                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction *onePassAction = [UIAlertAction actionWithTitle:@"普通模式"
//                                                            style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * _Nonnull action) {
//                                                              [self startGenerateWithTwoPassEnabled:NO];
//                                                          }];
//    [alertController addAction:onePassAction];
//
//    if (_ugcEdit.supportsTwoPassEncoding) {
//
//        UIAlertAction *twoPassAction = [UIAlertAction actionWithTitle:@"质量优化模式"
//                                                                style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * _Nonnull action) {
//                                                                  [self startGenerateWithTwoPassEnabled:YES];
//                                                              }];
//        [alertController addAction:twoPassAction];
//    }
//
//    //视频生成GIF示例
//    if (_videoAsset != nil){
//        UIAlertAction *gifAction = [UIAlertAction actionWithTitle:@"原视频转换为gif"
//                                                            style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * _Nonnull action) {
//                                                              [self startGenerateGif];
//                                                          }];
//        [alertController addAction:gifAction];
//    }
//
//    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
//
//    [alertController addAction:cancelAction];
//    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)startGenerateWithTwoPassEnabled:(BOOL)twoPass
{
    [_videoPreview stopObservingAudioNotification];
    _generationView = [self generatingView];
    _generationView.hidden = NO;
    
    [_ugcEdit setCutFromTime:_leftTime toTime:_rightTime];
    [self checkVideoOutputPath];
    
    if (twoPass) {
        [_ugcEdit generateVideoWithTwoPass:VIDEO_COMPRESSED_720P videoOutputPath:_videoOutputPath];
    } else {
        [_ugcEdit generateVideo:VIDEO_COMPRESSED_720P videoOutputPath:_videoOutputPath];
    }
}

- (void)onGenerateCancelBtnClicked:(UIButton*)sender
{
    [_videoPreview startObservingAudioNotification];
    _generationView.hidden = YES;
    [_ugcEdit cancelGenerate];
}

- (void)pause
{
    [_ugcEdit pausePlay];
    [_videoPreview setPlayBtn:NO];
}

- (void)checkVideoOutputPath
{
    NSFileManager *manager = [[NSFileManager alloc] init];
    if ([manager fileExistsAtPath:_videoOutputPath]) {
        BOOL success =  [manager removeItemAtPath:_videoOutputPath error:nil];
        if (success) {
            NSLog(@"Already exist. Removed!");
        }
    }
}

#pragma mark - Lazy Loader
- (MBShortVideoMusicMixView *)musixMixView {
    if (_musixMixView == nil) {
//        _musixMixView = [[MusicMixView alloc] initWithFrame:CGRectMake(0, _videoPreview.bottom + 10 * kScaleY, self.view.width, _bottomBar.y - _videoPreview.bottom - 10 * kScaleY)];
        _musixMixView = [MBShortVideoMusicMixView nibView];
        _musixMixView.frame = (CGRect){{0,kScreenHeight - 180 - kTabSafeBottomMargin},{kScreenWidth, 180}};
        _musixMixView.delegate = self;
        _musixMixView.backgroundColor = [UIColor clearColor];
    }
    return _musixMixView;
}

-(MBShortVideoFilterSettingView *)filterView {
    if (_filterView == nil) {
        _filterView = [MBShortVideoFilterSettingView nibView];
        _filterView.frame = (CGRectMake(0, kScreenHeight - 180 - kTabSafeBottomMargin, kScreenWidth, 180));
        _filterView.delegate = self;
        _filterView.backgroundColor = [UIColor clearColor];
    }
    return _filterView;
}

-(MBShortVideoMovementSettingView *)movementView {
    if (_movementView == nil) {
        _movementView = [MBShortVideoMovementSettingView nibView];
        _movementView.frame = CGRectMake(0, kScreenHeight - 180 - kTabSafeBottomMargin, kScreenWidth, 180);
        _movementView.delegate = self;
        _movementView.backgroundColor = [UIColor clearColor];
    }
    return _movementView;
}
-(MBShortVideoSpeedSettingView *)speedView {
    if (_speedView == nil) {
        _speedView = [MBShortVideoSpeedSettingView nibView];
        _speedView.frame = CGRectMake(0, kScreenHeight - 180 - kTabSafeBottomMargin, kScreenWidth, 180);
        _speedView.delegate = self;
        _speedView.backgroundColor = [UIColor clearColor];
    }
    return _speedView;
}
-(MBShortVideoCoverSettingView *)coverView {
    if (_coverView == nil) {
        _coverView = [MBShortVideoCoverSettingView nibView];
        _coverView.frame = CGRectMake(0, kScreenHeight - 60 - kTabSafeBottomMargin, kScreenWidth, 60);
        _coverView.backgroundColor = [UIColor clearColor];
    }
    return _coverView;
}


- (PasterAddView *)pasterView {
    if (_pasterView == nil) {
        _pasterView = [[PasterAddView alloc] initWithFrame:CGRectMake(0, _videoPreview.bottom + 30 * kScaleY, self.view.width, _bottomBar.y - _videoPreview.bottom - 30 * kScaleY)];
        _pasterView.delegate = self;
    }
    return _pasterView;
}

- (TextAddView *)textView {
    if (_textView == nil) {
        _textView = [[TextAddView alloc] initWithFrame:CGRectMake(0, _videoPreview.bottom + 30 * kScaleY, self.view.width, _bottomBar.y - _videoPreview.bottom - 30 * kScaleY)];
        _textView.delegate = self;
    }
    return _textView;
}

#pragma mark - BottomTabBarDelegate
- (void)setAccessoryView:(UIView *)view withPreviewBarHidden:(BOOL)hidden withAccessoryShow:(BOOL)isShow accessoryHeight:(CGFloat)viewHeght
{
    BOOL changed = _videoCutView.hidden != hidden;
    _videoCutView.hidden = hidden;
    if (changed) {
        [_accessoryView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(viewHeght);
            make.left.equalTo(leftGuide);
            make.right.equalTo(rightGuide);
            make.bottom.equalTo(self.view.mas_bottom).offset(-kTabSafeBottomMargin);
        }];
        [self.view layoutIfNeeded];
    }
    [_accessoryView removeAllSubViews];
//    view.center = CGPointMake(CGRectGetMidX(_accessoryView.bounds), CGRectGetMidY(_accessoryView.bounds));
//    view.bottom = _accessoryView.bottom;
    
    [_accessoryView addSubview:view];
    [view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_accessoryView);
    }];
//    view.frame = _accessoryView.frame;
    if (isShow == YES) {
        _accessoryView.hidden = NO;
        _bottomBar.hidden = YES;
    }else {
        _accessoryView.hidden = YES;
        _bottomBar.hidden = NO;
    }
}
/*裁剪*/
- (void)onCutBtnClicked
{
    [self setAccessoryView:_cutTipsLabel withPreviewBarHidden:NO withAccessoryShow:YES accessoryHeight:180.0];
    [_videoCutView setEffectDeleteBtnHidden:YES];
    [_videoPreview setBGMControlBtnHidden:YES];
}
/*封面*/
-(void)onCoverButtonClick:(BOOL)isSelect {
    [self setAccessoryView:self.coverView withPreviewBarHidden:NO withAccessoryShow:isSelect accessoryHeight:60.0f];
    [_videoCutView refreshUIWithType:MBShortVideoRangeSliderTypeNone];
    
    [_videoCutView setEffectDeleteBtnHidden:YES];
    [_videoPreview setBGMControlBtnHidden:YES];
  
    MB_WeakSelf(self);
    _coverView.coverCancelBlock = ^{
//        _coverImage = nil;
        [weakself setAccessoryView:weakself.coverView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:60.0];
    };
    
    _coverView.coverTureBlock = ^{
        CGFloat selectTime = _videoCutView.currentTime;
        weakself.coverImage =  [TXVideoInfoReader getSampleImage:selectTime videoAsset:_videoAsset];
        [weakself setAccessoryView:weakself.coverView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:60.0];
    };
}

/*速度*/
-(void)onSpeedButtonClick:(BOOL)isSelect
{
//    [self setAccessoryView:_videoAsset == nil ? _transitionView : _timeSelectView withPreviewBarHidden:NO withAccessoryShow:isSelect accessoryHeight:180.0];
    [self setAccessoryView:self.speedView withPreviewBarHidden:NO withAccessoryShow:isSelect accessoryHeight:130.0f];
    [_videoCutView refreshUIWithType:MBShortVideoRangeSliderTypeNormal];

    [_videoCutView setEffectDeleteBtnHidden:YES];
    [_videoPreview setBGMControlBtnHidden:YES];
    MB_WeakSelf(self);
    _speedView.speedCancelBlock = ^{
        [weakself setAccessoryView:weakself.movementView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
    _speedView.speedTureBlock = ^{
        [weakself setAccessoryView:weakself.movementView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
}
/*动作*/
- (void)onMovementButtonClick:(BOOL)isSelect
{
    [self setAccessoryView:self.movementView withPreviewBarHidden:NO withAccessoryShow:isSelect accessoryHeight:130.0];
    [_videoCutView refreshUIWithType:MBShortVideoRangeSliderTypeNormal];

    [_videoCutView setEffectDeleteBtnHidden:NO];
    [_videoPreview setBGMControlBtnHidden:YES];
    MB_WeakSelf(self);
    _movementView.movementTureBlock = ^{
        [weakself setAccessoryView:weakself.movementView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
    _movementView.movementCancelBlock = ^{
        [weakself setAccessoryView:weakself.movementView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
}

/*滤镜delegate*/
- (void)onSetFilterWithImage:(UIImage *)image
{
    [_ugcEdit setFilter:image];
    [_videoPreview setBGMControlBtnHidden:YES];
}
/*滤镜*/
- (void)onFilterButtonClick:(BOOL)isSelect
{
    [self setAccessoryView:self.filterView withPreviewBarHidden:YES withAccessoryShow:isSelect accessoryHeight:180.0];
    _videoCutView.videoRangeSlider.hidden = NO;
    [_videoPreview setBGMControlBtnHidden:YES];
    MB_WeakSelf(self);
    _filterView.filterTureBlock = ^{
         [weakself setAccessoryView:weakself.filterView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
    _filterView.filterCancelBlock = ^{
         [weakself setAccessoryView:weakself.filterView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0];
    };
}
/*背景音乐*/
- (void)onMusicButtonClick:(BOOL)isSelect
{
    [self setAccessoryView:self.musixMixView withPreviewBarHidden:YES withAccessoryShow:isSelect accessoryHeight:180.0];
    _videoCutView.videoRangeSlider.hidden = NO;
    [_videoPreview setBGMControlBtnHidden:YES];
    MB_WeakSelf(self);
    _musixMixView.musicCancelBlock = ^{
        [weakself setAccessoryView:weakself.musixMixView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0f];
    };
    _musixMixView.musicTureBlock = ^{
        [weakself setAccessoryView:weakself.musixMixView withPreviewBarHidden:YES withAccessoryShow:NO accessoryHeight:180.0f];
    };
}

/*贴纸*/
- (void)onPasterBtnClicked
{
    [self setAccessoryView:self.pasterView withPreviewBarHidden:YES withAccessoryShow:YES accessoryHeight:180.0f];
    _videoCutView.videoRangeSlider.hidden = NO;
    [_videoPreview setBGMControlBtnHidden:YES];
}
/*文字*/
- (void)onCharButtonClick:(BOOL)isSelect
{
    [_videoPreview removeFromSuperview];
    [_videoPreview removeConstraints:_videoPreview.constraints];
    //己有添加字幕的话只操作本地裁剪时间内的
    NSMutableArray* inRangeVideoTexts = [NSMutableArray new];
    for (VideoTextInfo* info in _videoTextInfos) {
        if (info.startTime >= _rightTime || info.endTime <= _leftTime)
            continue;
        
        [inRangeVideoTexts addObject:info];
    }
    
    [_ugcEdit pausePlay];
    [_videoPreview setPlayBtn:NO];
    
    MBShortVideoTextEditController* vc = [[MBShortVideoTextEditController alloc] initWithVideoEditer:_ugcEdit previewView:_videoPreview startTime:_leftTime endTime:_rightTime videoTextInfos:inRangeVideoTexts];
    vc.delegate = self;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark VideoEffectViewDelegate

-(void)onVideoEffectRevocation {
    [_videoCutView onEffectDelete:^(VideoColorInfo *info) {
        if (info) {
            float time = _isReverse ? MAX(info.endPos, info.startPos) : MIN(info.endPos, info.startPos);
            [_videoCutView setPlayTime:time];
            _playTime = time;
        }
    }];
}

- (void)onVideoEffectBeginClick:(TXEffectType)effectType
{
    _effectType = effectType;
    UIColor *color = TXCVEFColorPaletteColorAtIndex((NSUInteger)effectType);
    const CGFloat alpha = 0.7;
    [_videoCutView startColoration:color alpha:alpha];
    [_ugcEdit startEffect:(TXEffectType)_effectType startTime:_playTime];
    if (!_isReverse) {
        [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.currentPos toTime:_videoCutView.videoRangeSlider.rightPos];
    }else{
        [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.leftPos toTime:_videoCutView.videoRangeSlider.currentPos];
    }
    [_videoPreview setPlayBtn:YES];
}

- (void)onVideoEffectEndClick:(TXEffectType)effectType
{
    if (_effectType != -1) {
        [_videoPreview setPlayBtn:NO];
        [_videoCutView stopColoration];
        [_ugcEdit stopEffect:effectType endTime:_playTime];
        [_ugcEdit pausePlay];
        _effectType = -1;
    }
}

#pragma mark TimeSelectViewDelegate
- (void)onVideoTimeEffectsClear
{
    _timeType = TimeType_Clear;
    _isReverse = NO;
    [_ugcEdit setReverse:_isReverse];
    [_ugcEdit setRepeatPlay:nil];
    [_ugcEdit setSpeedList:nil];
    [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.leftPos toTime:_videoCutView.videoRangeSlider.rightPos];
    
    [_videoPreview setPlayBtn:YES];
    [_videoCutView setCenterPanHidden:YES];
}
- (void)onVideoTimeEffectsBackPlay
{
    _timeType = TimeType_Back;
    _isReverse = YES;
    [_ugcEdit setReverse:_isReverse];
    [_ugcEdit setRepeatPlay:nil];
    [_ugcEdit setSpeedList:nil];
    [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.leftPos toTime:_videoCutView.videoRangeSlider.rightPos];
    
    [_videoPreview setPlayBtn:YES];
    [_videoCutView setCenterPanHidden:YES];
    _videoCutView.videoRangeSlider.hidden = NO;
}
- (void)onVideoTimeEffectsRepeat
{
    _timeType = TimeType_Repeat;
    _isReverse = NO;
    [_ugcEdit setReverse:_isReverse];
    [_ugcEdit setSpeedList:nil];
    TXRepeat *repeat = [[TXRepeat alloc] init];
    repeat.startTime = _leftTime + (_rightTime - _leftTime) / 5;
    repeat.endTime = repeat.startTime + 0.5;
    repeat.repeatTimes = 3;
    [_ugcEdit setRepeatPlay:@[repeat]];
    [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.leftPos toTime:_videoCutView.videoRangeSlider.rightPos];
    
    [_videoPreview setPlayBtn:YES];
    [_videoCutView setCenterPanHidden:NO];
    [_videoCutView setCenterPanFrame:repeat.startTime];
}

- (void)onVideoTimeEffectsSpeed
{
    _timeType = TimeType_Speed;
    _isReverse = NO;
    [_ugcEdit setReverse:_isReverse];
    [_ugcEdit setRepeatPlay:nil];
    TXSpeed *speed1 =[[TXSpeed alloc] init];
    speed1.startTime = _leftTime + (_rightTime - _leftTime) * 1.5 / 5;
    speed1.endTime = speed1.startTime + 0.5;
    speed1.speedLevel = SPEED_LEVEL_SLOW;
    TXSpeed *speed2 =[[TXSpeed alloc] init];
    speed2.startTime = speed1.endTime;
    speed2.endTime = speed2.startTime + 0.5;
    speed2.speedLevel = SPEED_LEVEL_SLOWEST;
    TXSpeed *speed3 =[[TXSpeed alloc] init];
    speed3.startTime = speed2.endTime;
    speed3.endTime = speed3.startTime + 0.5;
    speed3.speedLevel = SPEED_LEVEL_SLOW;
    [_ugcEdit setSpeedList:@[speed1,speed2,speed3]];
    [_ugcEdit startPlayFromTime:_videoCutView.videoRangeSlider.leftPos toTime:_videoCutView.videoRangeSlider.rightPos];
    
    [_videoPreview setPlayBtn:YES];
    [_videoCutView setCenterPanHidden:NO];
    [_videoCutView setCenterPanFrame:speed1.startTime];
}

#pragma mark TransitionViewDelegate
- (void)onVideoTransitionLefRightSlipping
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_LefRightSlipping duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

- (void)onVideoTransitionUpDownSlipping
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_UpDownSlipping duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

- (void)onVideoTransitionEnlarge
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_Enlarge duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

- (void)onVideoTransitionNarrow
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_Narrow duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

- (void)onVideoTransitionRotationalScaling
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_RotationalScaling duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

- (void)onVideoTransitionFadeinFadeout
{
    __weak __typeof(self) weakSelf = self;
    [_ugcEdit setPictureTransition:TXTransitionType_FadeinFadeout duration:^(CGFloat duration) {
        _duration = duration;
        _rightTime = duration;
        [weakSelf initVideoCutView:VideoType_Picture];
        [weakSelf.ugcEdit startPlayFromTime:0 toTime:weakSelf.duration];
        [weakSelf.videoPreview setPlayBtn:YES];
    }];
}

#pragma mark TXVideoGenerateListener
-(void) onGenerateProgress:(float)progress
{
    _generateProgressView.progress = progress;
}

-(void) onGenerateComplete:(TXGenerateResult *)result
{
    _generationView.hidden = YES;
    [_videoPreview startObservingAudioNotification];
    
    if (result.retCode == 0) {
//        TXVideoInfo *videoInfo = [TXVideoInfoReader getVideoInfo:_videoOutputPath];
//        MBShortVideoPreviewController* vc = [[MBShortVideoPreviewController alloc] initWithCoverImage:videoInfo.coverImage videoPath:_videoOutputPath renderMode:RENDER_MODE_FILL_EDGE showEditButton:NO];
        [self uploadShortVideo:_videoOutputPath];
        dispatch_async(dispatch_get_main_queue()
                       , ^{
                           [MBProgressHUD showActivityMessage:@"正在上传视频..."];

                       });
        
//        [self.navigationController pushViewController:vc animated:YES];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"视频生成失败"
                                                            message:[NSString stringWithFormat:@"错误码：%ld 错误信息：%@",(long)result.retCode,result.descMsg]
                                                           delegate:self
                                                  cancelButtonTitle:@"知道了"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
}

#pragma mark - 上传视频
-(void)uploadShortVideo:(NSString *)videoPath {
    NSDictionary *param = @{};
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kThird_Qiniu_Token setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBQiNiuUpTokenModel *upTokenModel = [MBQiNiuUpTokenModel mj_objectWithKeyValues:AJson];
        QNUploadManager *manager = [[QNUploadManager alloc]init];
        NSString *timeKey = [NSString stringWithFormat:@"%zd",[[MBTimeManager shateInstance]getCurrentTimestamp]];
        NSString *imageName = [NSString stringWithFormat:@"%@%@%@%@%@%@",@"TXVideo_",timeKey,@"_",@"userId=",filter([MBUserModel sharedMBUserModel].userInfo.userId),@".mp4"];
        [manager putFile:videoPath key:imageName token:upTokenModel.upToken complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
            if (info.statusCode == 200) {
                NSString *videoPath = [NSString stringWithFormat:@"%@",resp[@"key"]];
                MBShortVideoPublishController *vc = [[MBShortVideoPublishController alloc]init];
                vc.videoURL = videoPath;
                vc.videoDownloadURL = videoPath;
                vc.coverImage = weakself.coverImage;
//                vc.videoPath = weakself.videoPath;
                vc.videoPath = weakself.videoPath ? weakself.videoPath : _videoOutputPath;
                vc.entityType = weakself.entityType;
                [MBProgressHUD hideHUD];
                [MBProgressHUD showSuccess:@"上传成功"];
                [weakself.navigationController pushViewController:vc animated:YES];
            }
        } option:nil];
        
    } failure:^(id  _Nonnull AJson) {
        [MBProgressHUD showError:@"上传视频失败"];
    }];
}


#pragma mark VideoPreviewDelegate
- (void)onVideoPlay
{
    CGFloat currentPos = _videoCutView.videoRangeSlider.currentPos;
    if (currentPos < _leftTime || currentPos > _rightTime)
        currentPos = _leftTime;
    
    if(_isReverse && currentPos != 0){
        [_ugcEdit startPlayFromTime:0 toTime:currentPos];
    }
    else if(_videoCutView.videoRangeSlider.rightPos != 0){
        [_ugcEdit startPlayFromTime:currentPos toTime:_videoCutView.videoRangeSlider.rightPos];
    }
    else{
        [_ugcEdit startPlayFromTime:currentPos toTime:_rightTime];
    }
}

- (void)onVideoPause
{
    [_ugcEdit pausePlay];
}

- (void)onVideoResume
{
    //    [_ugcEdit resumePlay];
    [self onVideoPlay];
}

- (void)onVideoPlayProgress:(CGFloat)time
{
    _playTime = time;
    [_videoCutView setPlayTime:_playTime];
}

- (void)onVideoPlayFinished
{
    if (_effectType != -1) {
        [self onVideoEffectEndClick:_effectType];
    }else{
        [_ugcEdit startPlayFromTime:_leftTime toTime:_rightTime];
    }
}

- (void)onBGMLoop:(BOOL)isLoop
{
    _isBGMLoop = isLoop;
    [_ugcEdit setBGMLoop:_isBGMLoop];
}

- (void)onBGMFadeIn:(BOOL)isFadeIn
{
    _isFadeIn = isFadeIn;
    [_ugcEdit setBGMFadeInDuration:_isFadeIn ? 3 : 0 fadeOutDuration:_isFadeOut ? 3 : 0];
}

- (void)onBGMFadeOut:(BOOL)isFadeOut
{
    _isFadeOut = isFadeOut;
    [_ugcEdit setBGMFadeInDuration:_isFadeIn ? 3 : 0 fadeOutDuration:_isFadeOut ? 3 : 0];
}

- (void)onVideoEnterBackground
{
    [_ugcEdit pauseGenerate];
}

- (void)onVideoWillEnterForeground
{
    [_ugcEdit resumeGenerate];
}

#pragma mark TXVideoCustomProcessListener
- (GLuint)onPreProcessTexture:(GLuint)texture width:(CGFloat)width height:(CGFloat)height timestamp:(UInt64)timestamp
{
    static int i = 0;
    if (i++ % 100 == 0) {
        NSLog(@"onPreProcessTexture width:%f height:%f  timestamp:%f", width, height,timestamp/1000.0);
    }
    
    return texture;
}

- (void)onTextureDestoryed
{
    NSLog(@"onTextureDestoryed");
}

#pragma mark - VideoCutViewDelegate

/*播放*/
-(void)onVideoCutPlay:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        [sender setImage:kImage(@"shortvideo_Fill") forState:UIControlStateNormal];
        [_ugcEdit pausePlay];
    }else {
        [sender setImage:kImage(@"shortvideo_ pause") forState:UIControlStateNormal];
        [_ugcEdit resumePlay];
    }
}

//裁剪
- (void)onVideoLeftCutChanged:(VideoRangeSlider *)sender
{
    //[_ugcEdit pausePlay];
    [_videoPreview setPlayBtn:NO];
    [_ugcEdit previewAtTime:sender.leftPos];
}

- (void)onVideoRightCutChanged:(VideoRangeSlider *)sender
{
    [_videoPreview setPlayBtn:NO];
    [_ugcEdit previewAtTime:sender.rightPos];
}

- (void)onVideoCutChangedEnd:(VideoRangeSlider *)sender
{
    _leftTime = sender.leftPos;
    _rightTime = sender.rightPos;
    [_ugcEdit startPlayFromTime:sender.leftPos toTime:sender.rightPos];
    [_videoPreview setPlayBtn:YES];
}

- (void)onVideoCenterRepeatChanged:(VideoRangeSlider*)sender
{
    [_videoPreview setPlayBtn:NO];
    [_ugcEdit previewAtTime:sender.centerPos];
}

- (void)onVideoCenterRepeatEnd:(VideoRangeSlider*)sender;
{
    _leftTime = sender.leftPos;
    _rightTime = sender.rightPos;
    
    if (_timeType == TimeType_Repeat) {
        TXRepeat *repeat = [[TXRepeat alloc] init];
        repeat.startTime = sender.centerPos;
        repeat.endTime = sender.centerPos + 0.5;
        repeat.repeatTimes = 3;
        [_ugcEdit setRepeatPlay:@[repeat]];
        [_ugcEdit setSpeedList:nil];
    }
    else if (_timeType == TimeType_Speed) {
        TXSpeed *speed1 =[[TXSpeed alloc] init];
        speed1.startTime = sender.centerPos;
        speed1.endTime = speed1.startTime + 0.5;
        speed1.speedLevel = SPEED_LEVEL_SLOW;
        TXSpeed *speed2 =[[TXSpeed alloc] init];
        speed2.startTime = speed1.endTime;
        speed2.endTime = speed2.startTime + 0.5;
        speed2.speedLevel = SPEED_LEVEL_SLOWEST;
        TXSpeed *speed3 =[[TXSpeed alloc] init];
        speed3.startTime = speed2.endTime;
        speed3.endTime = speed3.startTime + 0.5;
        speed3.speedLevel = SPEED_LEVEL_SLOW;
        [_ugcEdit setSpeedList:@[speed1,speed2,speed3]];
        [_ugcEdit setRepeatPlay:nil];
    }
    
    if (_isReverse) {
        [_ugcEdit startPlayFromTime:sender.leftPos toTime:sender.centerPos + 1.5];
    }else{
        [_ugcEdit startPlayFromTime:sender.centerPos toTime:sender.rightPos];
    }
    [_videoPreview setPlayBtn:YES];
}

- (void)onVideoCutChange:(VideoRangeSlider *)sender seekToPos:(CGFloat)pos
{
    _playTime = pos;
    [_ugcEdit previewAtTime:_playTime];
    [_videoPreview setPlayBtn:NO];
}

//美颜
- (void)onSetBeautyDepth:(float)beautyDepth WhiteningDepth:(float)whiteningDepth
{
    [_ugcEdit setBeautyFilter:beautyDepth setWhiteningLevel:whiteningDepth];
}

- (void)onEffectDelete:(VideoColorInfo *)info
{
    if (info) {
        float time = _isReverse ? MAX(info.endPos, info.startPos) : MIN(info.endPos, info.startPos);
        [_videoCutView setPlayTime:time];
        _playTime = time;
    }
    [_ugcEdit deleteLastEffect];
    [_videoPreview setPlayBtn:NO];
}

#pragma mark - TextAddViewDelegate
//打开字幕操作viewcontroller
- (void)onAddTextBtnClicked
{
    [_videoPreview removeFromSuperview];
    
    //己有添加字幕的话只操作本地裁剪时间内的
    NSMutableArray* inRangeVideoTexts = [NSMutableArray new];
    for (VideoTextInfo* info in _videoTextInfos) {
        if (info.startTime >= _rightTime || info.endTime <= _leftTime)
            continue;
        
        [inRangeVideoTexts addObject:info];
    }
    
    [_ugcEdit pausePlay];
    [_videoPreview setPlayBtn:NO];
    
    MBShortVideoTextEditController* vc = [[MBShortVideoTextEditController alloc] initWithVideoEditer:_ugcEdit previewView:_videoPreview startTime:_leftTime endTime:_rightTime videoTextInfos:inRangeVideoTexts];
    vc.delegate = self;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)onSetVideoPasterInfosFinish:(NSArray<VideoPasterInfo*>*)videoPasterInfos
{
    //更新贴纸信息
//    [_videoPaterInfos removeAllObjects];
//    [_videoPaterInfos addObjectsFromArray:videoPasterInfos];
    
    _videoPreview.frame = CGRectMake(0, 0, self.view.width, 350 * kScaleY);
    _videoPreview.delegate = self;
    [_videoPreview setPlayBtnHidden:NO];
    [self.view insertSubview:_videoPreview atIndex:0];
    [_videoPreviewConstrains makeObjectsPerformSelector:@selector(install)];
    if (videoPasterInfos.count > 0) {
        [_textView setEdited:YES];
    }else {
        [_textView setEdited:NO];
    }
}

#pragma mark - VideoTextViewControllerDelegate
- (void)onSetVideoTextInfosFinish:(NSArray<VideoTextInfo *> *)videoTextInfos
{
    //更新文字信息
    //新增的
    for (VideoTextInfo* info in videoTextInfos) {
        if (![_videoTextInfos containsObject:info]) {
            [_videoTextInfos addObject:info];
        }
    }
    
    NSMutableArray* removedTexts = [NSMutableArray new];
    for (VideoTextInfo* info in _videoTextInfos) {
        //删除的
        NSUInteger index = [videoTextInfos indexOfObject:info];
        if ( index != NSNotFound) {
            continue;
        }
        
        if (info.startTime < _rightTime && info.endTime > _leftTime)
            [removedTexts addObject:info];
    }
    
    if (removedTexts.count > 0)
        [_videoTextInfos removeObjectsInArray:removedTexts];
    
    _videoPreview.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    _videoPreview.delegate = self;
    [_videoPreview setPlayBtnHidden:NO];
    [self.view insertSubview:_videoPreview atIndex:0];
    [_videoPreview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        
    }];
//    [_videoPreviewConstrains makeObjectsPerformSelector:@selector(install)];
    
    if (videoTextInfos.count > 0) {
        [_textView setEdited:YES];
    }
    else {
        [_textView setEdited:NO];
    }
}

#pragma mark - PasterAddViewDelegate
- (void)onAddPasterBtnClicked
{
    [_videoPreview removeFromSuperview];
    [_videoPreview removeConstraints:_videoPreview.constraints];
    //己有添加字幕的话只操作本地裁剪时间内的
    NSMutableArray* inRangeVideoPasters = [NSMutableArray new];
    for (VideoPasterInfo* info in _videoPaterInfos) {
        if (info.startTime >= _rightTime || info.endTime <= _leftTime)
            continue;
        
        [inRangeVideoPasters addObject:info];
    }
    
    [_ugcEdit pausePlay];
    [_videoPreview setPlayBtn:NO];
    
    MBShortVideoPasterController* vc = [[MBShortVideoPasterController alloc] initWithVideoEditer:_ugcEdit previewView:_videoPreview startTime:_leftTime endTime:_rightTime videoPasterInfos:inRangeVideoPasters];
    vc.delegate = self;
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - MPMediaPickerControllerDelegate
- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    NSArray *items = mediaItemCollection.items;
    MPMediaItem *songItem = [items objectAtIndex:0];
    
    NSURL *url = [songItem valueForProperty:MPMediaItemPropertyAssetURL];
    NSString* songName = [songItem valueForProperty: MPMediaItemPropertyTitle];
    NSString* authorName = [songItem valueForProperty:MPMediaItemPropertyArtist];
    NSNumber* duration = [songItem valueForKey:MPMediaItemPropertyPlaybackDuration];
    NSLog(@"MPMediaItemPropertyAssetURL = %@", url);
    
    MusicInfo* musicInfo = [MusicInfo new];
    musicInfo.duration = duration.floatValue;
    musicInfo.soneName = songName;
    musicInfo.singerName = authorName;
    
    if (mediaPicker.editing) {
        mediaPicker.editing = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self saveAssetURLToFile:musicInfo assetURL:url];
        });
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

//点击取消时回调
- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 将AssetURL(音乐)导出到app的文件夹并播放
- (void)saveAssetURLToFile:(MusicInfo*)musicInfo assetURL:(NSURL*)assetURL
{
    musicInfo.fileAsset = [AVAsset assetWithURL:assetURL];
    if (musicInfo.fileAsset != nil) {
        [_musixMixView addMusicInfo:musicInfo];
    }
}

#pragma mark - MusicMixViewDelegate


//打开本地系统音乐
- (void)onOpenLocalMusicList
{
    [self pause];
    
    MPMediaPickerController *mpc = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
    mpc.delegate = self;
    mpc.editing = YES;
    mpc.allowsPickingMultipleItems = NO;
    [self presentViewController:mpc animated:YES completion:nil];
}

//设音量效果
- (void)onSetVideoVolume:(CGFloat)videoVolume musicVolume:(CGFloat)musicVolume
{
    [_ugcEdit setVideoVolume:videoVolume];
    [_ugcEdit setBGMVolume:musicVolume];
}

- (void)onSetBGMWithFileAsset:(AVURLAsset*)fileAsset startTime:(CGFloat)startTime endTime:(CGFloat)endTime
{
    if (![_fileAsset isEqual:fileAsset]) {
        __weak __typeof(self) weakSelf = self;
        [_ugcEdit setBGMAsset:fileAsset result:^(int result) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (result == -1){
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"设置背景音乐失败"
                                                                        message:@"不支持当前格式的背景音乐!"
                                                                       delegate:weakSelf
                                                              cancelButtonTitle:@"知道了"
                                                              otherButtonTitles:nil, nil];
                    [alertView show];
                }else{
                    [weakSelf setBGMVolume:fileAsset startTime:startTime endTime:endTime];
                }
            });
        }];
    }else{
        [self setBGMVolume:fileAsset startTime:startTime endTime:endTime];
    }
}

-(void)setBGMVolume:(AVURLAsset *)fileAsset startTime:(CGFloat)startTime endTime:(CGFloat)endTime
{
    _fileAsset = fileAsset;
    [_ugcEdit setBGMStartTime:startTime endTime:endTime];
    [_ugcEdit setBGMFadeInDuration:_isFadeIn ? 3 : 0 fadeOutDuration:_isFadeOut ? 3 : 0];
    [_ugcEdit setBGMLoop:_isBGMLoop];
    [_ugcEdit startPlayFromTime:_leftTime toTime:_rightTime];
    
    if (_fileAsset == nil) [_ugcEdit setVideoVolume:1.f];
    [_videoPreview setPlayBtn:YES];
}

//生成gif图片
-(void)startGenerateGif{
    //创建CFURL对象
    /*
     CFURLCreateWithFileSystemPath(CFAllocatorRef allocator, CFStringRef filePath, CFURLPathStyle pathStyle, Boolean isDirectory)
     
     allocator : 分配器,通常使用kCFAllocatorDefault
     filePath : 路径
     pathStyle : 路径风格,我们就填写kCFURLPOSIXPathStyle 更多请打问号自己进去帮助看
     isDirectory : 一个布尔值,用于指定是否filePath被当作一个目录路径解决时相对路径组件
     */
    CFURLRef url = CFURLCreateWithFileSystemPath (
                                                  kCFAllocatorDefault,
                                                  (CFStringRef)_gifOutputPath,
                                                  kCFURLPOSIXPathStyle,
                                                  false);
    
    //通过一个url返回图像目标 kUTTypeGIF  CFStringRef
    __block int picCount = 20;
    NSMutableArray *picArr = [NSMutableArray arrayWithCapacity:picCount];
    [TXVideoInfoReader getSampleImages:picCount videoAsset:_videoAsset progress:^BOOL(int number, UIImage *image) {
        if (image == nil){
            picCount--;
        }else{
            [picArr addObject:image];
        }
        if (picArr.count >= picCount) {
            dispatch_async(dispatch_get_main_queue(), ^{
                CGImageDestinationRef destination = CGImageDestinationCreateWithURL(url, kUTTypeGIF, picArr.count, NULL);
                
                //设置gif的信息,播放间隔时间,基本数据,和delay时间
                NSDictionary *frameProperties = [NSDictionary
                                                 dictionaryWithObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:0.001f], (NSString *)kCGImagePropertyGIFDelayTime, nil]
                                                 forKey:(NSString *)kCGImagePropertyGIFDictionary];
                
                //设置gif信息
                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:2];
                
                [dict setObject:[NSNumber numberWithBool:YES] forKey:(NSString*)kCGImagePropertyGIFHasGlobalColorMap];
                
                [dict setObject:(NSString *)kCGImagePropertyColorModelRGB forKey:(NSString *)kCGImagePropertyColorModel];
                
                [dict setObject:[NSNumber numberWithInt:8] forKey:(NSString*)kCGImagePropertyDepth];
                
                [dict setObject:[NSNumber numberWithInt:0] forKey:(NSString *)kCGImagePropertyGIFLoopCount];
                
                NSDictionary *gifProperties = [NSDictionary dictionaryWithObject:dict
                                                                          forKey:(NSString *)kCGImagePropertyGIFDictionary];
                //合成gif
                CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)gifProperties);
                for (UIImage* dImg in picArr)
                {
                    CGImageDestinationAddImage(destination, dImg.CGImage, (__bridge CFDictionaryRef)frameProperties);
                }
                CGImageDestinationFinalize(destination);
                CFRelease(destination);
                NSData *data = [NSData dataWithContentsOfFile:_gifOutputPath];
                // 保存到本地相册
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"gif生成成功，已经保存到系统相册，请前往系统相册查看" message:nil delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
                    [alert show];
                }] ;
            });
        }
        return YES;
    }];
}


#pragma mark - getter
-(UIButton *)leftNavButton {
    if (_leftNavButton == nil) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:kImage(@"shortvideo_left") forState:UIControlStateNormal];
        button.frame = (CGRect){{0,0},{40,40}};
        button.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        [button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        _leftNavButton = button;
    }
    return _leftNavButton;
}

-(UIButton *)rightNavButton {
    if (_rightNavButton == nil) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(goSave) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:Color_FF8000];
        button.size = CGSizeMake(60, 30);
        [button setTitle:@"下一步" forState:UIControlStateNormal];
        [button setTitleColor:Color_White forState:UIControlStateNormal];
        button.titleLabel.font = kMFont(14);
        button.layer.cornerRadius = 15;
        button.layer.masksToBounds = YES;
        _rightNavButton = button;
    }
    return _rightNavButton;
}


@end
