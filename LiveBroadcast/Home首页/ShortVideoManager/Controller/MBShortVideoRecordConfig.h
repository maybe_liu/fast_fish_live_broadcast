//
//  MBShortVideoRecordConfig.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoRecordConfig : NSObject
@property(nonatomic,assign)TXVideoAspectRatio videoRatio;
@property(nonatomic,assign)TXVideoResolution videoResolution;
@property(nonatomic,assign)TXVideoQuality videoQuality;
@property(nonatomic,assign)TXVideoBeautyStyle videoBeautyStyle;
@property(nonatomic,assign)int bps;
@property(nonatomic,assign)int fps;
@property(nonatomic,assign)int gop;
@property(nonatomic,assign)BOOL enableAEC;
+ (instancetype)defaultConfigure;
@end

NS_ASSUME_NONNULL_END
