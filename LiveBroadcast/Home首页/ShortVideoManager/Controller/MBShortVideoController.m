//
//  MBShortVideoController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoController.h"
#import "VideoRecordProcessView.h"
#import "MBShortVideoTopCommonView.h"
#import "MBShortVideoBottomCommonView.h"
#import "MBShortVideoPreviewController.h"
#import "MBShortVideoEditController.h"
#import "MBShortVideoPublishController.h"
#import "MBShortVideoImportLocalVideoHelp.h"



#define BUTTON_RECORD_SIZE          75
#define BUTTON_CONTROL_SIZE         32
#define BUTTON_MASK_HEIGHT          170
#define BUTTON_PROGRESS_HEIGHT      3
#define BUTTON_SPEED_WIDTH          45
#define BUTTON_SPEED_HEIGHT         15
#define BUTTON_SPEED_INTERVAL       30
#define BUTTON_SPEED_COUNT          5
//#define MAX_RECORD_TIME             30
#define MIN_RECORD_TIME             1
#define RightSideButtonLabelColor   UIColor.whiteColor

@interface MBShortVideoController ()<TXUGCRecordListener,TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic, strong)MBShortVideoTopCommonView *topCommonView;
@property(nonatomic, strong)MBShortVideoBottomCommonView *bottomCommonView;

@end

@implementation MBShortVideoController{
    MBShortVideoRecordConfig*                 _videoConfig;
    BOOL                            _cameraFront;
    BOOL                            _cameraPreviewing;
    BOOL                            _videoRecording;
    BOOL                            _isPaused;
    BOOL                            _isFlash;
    
    UIButton *                      _btnRatio;
    UIButton *                      _btnRatio43;
    UIButton *                      _btnRatio11;
    UIButton *                      _btnRatio169;
    CGRect                          _btnRatioFrame;
    UIView *                        _bottomMask;
    UIView *                        _videoRecordView;
    UIButton *                      _btnDelete;
    UIButton *                      _btnStartRecord;
    UIButton *                      _btnFlash;
    UIButton *                      _btnCamera;
    UIButton *                      _btnBeauty;
#ifndef UGC_SMART
    UIButton *                      _btnMusic;
#endif
    UIButton *                      _btnTorch;
    UIButton *                      _btnDone;
    UILabel *                       _recordTimeLabel;
    UISegmentedControl *            _speedOptionControl;
    CGFloat                         _currentRecordTime;
    
//    BeautySettingPanel*             _vBeauty;
    
    BOOL                            _navigationBarHidden;
    BOOL                            _statusBarHidden;
    BOOL                            _appForeground;
    
    UIDeviceOrientation             _deviceOrientation;// 未初始化
    
    AVAsset*                        _BGMAsset;
    double                          _BGMDuration;
    
    VideoRecordProcessView *        _progressView;
#ifndef UGC_SMART
//    VideoRecordMusicView *          _musicView;
//    SpeedMode                       _speedMode;
#endif
    TXVideoAspectRatio              _aspectRatio;
    BOOL                            _isBackDelete;
    BOOL                            _bgmRecording;
    BOOL                            _bgmLoop;
    int                             _deleteCount;
    float                           _zoom;
    
    CGFloat                         _bgmBeginTime;
    BOOL                            _receiveBGMProgress;
    
    MBProgressHUD*                  _hub;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    MB_WeakSelf(self);
    self.onRecordCompleted = ^(TXUGCRecordResult * _Nonnull result) {
        weakself.bottomCommonView.videoState = MBShortVideoStateStart; /*重置底部区域*/
        MBShortVideoEditController *editVC = [[MBShortVideoEditController alloc]init];
        [editVC setVideoPath:result.videoPath];
        editVC.coverImage = result.coverImage;
        editVC.entityType = weakself.entityType;
        [weakself.navigationController pushViewController:editVC animated:YES];
    };
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self startCameraPreview];
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    // 禁用返回手势
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopCameraPreview];
    [self stopVideoRecord];
    [TXUGCRecord shareInstance].recordDelegate = nil;
    [[TXUGCRecord shareInstance].partsManager deleteAllParts];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

#pragma mark - NSNotificationHandler
-(void)onAudioSessionEvent:(NSNotification*)notification
{
    NSDictionary *info = notification.userInfo;
    AVAudioSessionInterruptionType type = [info[AVAudioSessionInterruptionTypeKey] unsignedIntegerValue];
    if (type == AVAudioSessionInterruptionTypeBegan) {
        // 在10.3及以上的系统上，分享跳其它app后再回来会收到AVAudioSessionInterruptionWasSuspendedKey的通知，不处理这个事件。
        if ([info objectForKey:@"AVAudioSessionInterruptionWasSuspendedKey"]) {
            return;
        }
        _appForeground = NO;
        if (!_isPaused && _videoRecording)
            [self onBtnRecordStartClicked];
        
    }else{
        AVAudioSessionInterruptionOptions options = [info[AVAudioSessionInterruptionOptionKey] unsignedIntegerValue];
        if (options == AVAudioSessionInterruptionOptionShouldResume) {
            _appForeground = YES;
        }
    }
}

- (void)onAppDidEnterBackGround:(UIApplication*)app
{
    _appForeground = NO;
    if (!_isPaused && _videoRecording){
        [self onBtnRecordStartClicked];
    }
    
//    if (!_vBeauty.hidden) {
//        [self onBtnBeautyClicked];
//    }
}

- (void)onAppWillEnterForeground:(UIApplication*)app
{
    _appForeground = YES;
    
}

- (void)onAppWillResignActive:(UIApplication*)app
{
    _appForeground = NO;
    if (!_isPaused && _videoRecording)
        [self onBtnRecordStartClicked];
    
//    if (!_vBeauty.hidden) {
//        [self onBtnBeautyClicked];
//    }
    
}
- (void)onAppDidBecomeActive:(UIApplication*)app
{
    _appForeground = YES;
    
}


-(void)createUI {
    _videoRecordView = [[UIView alloc] initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight}}];
    [self.view addSubview:_videoRecordView];
    [self.view addSubview:self.topCommonView];
    MB_WeakSelf(self);
    self.topCommonView.quitBlock = ^{
        [weakself popViewController];
    };
    self.topCommonView.lightBlock = ^{
        [weakself openOrCloseLight:YES];
    };
    self.topCommonView.reverBlock = ^{
        [weakself reverCamera];
    };
    
    [self.view addSubview:self.bottomCommonView];
    self.bottomCommonView.photoBlcok = ^{
        [weakself openAlbum];
    };
    
    self.bottomCommonView.deleteBlcok = ^(UIButton * _Nonnull button) {
        [weakself onBtnDeleteClicked];
        button.hidden = YES;
        weakself.bottomCommonView.videoState = MBShortVideoStateStart;
    };
    self.bottomCommonView.nextBlcok = ^{
        [weakself completeTran];
    };
    self.bottomCommonView.shootBlcok = ^{
//        weakself.MAX_RECORD_TIME = 30.0;
        [weakself startTran];
    };    
}
-(void)createCustomNav {
    self.navigationItem.title = @"";
}

/*退出d*/
-(void)popViewController {
    [self.navigationController popViewControllerAnimated:NO];
}
/*闪光灯*/
-(void)openOrCloseLight:(BOOL)open {
    _isFlash = !_isFlash;
//    _isFlash = YES;
   [[TXUGCRecord shareInstance] toggleTorch:_isFlash];
}
/**
 反转摄像头
 */
-(void)reverCamera {
    _cameraFront = !_cameraFront;
    [[TXUGCRecord shareInstance] switchCamera:_cameraFront];
}

/**
 开始、 恢复 录制
 */
-(void)startTran {
    [self onBtnRecordStartClicked];
}

/**
 完成录制
 */
-(void)completeTran{
    if (!_videoRecording)
        return;
    [self stopVideoRecord];
}
/**
 打开相册
 */
-(void)openAlbum {
    MB_WeakSelf(self);
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.isSelectOriginalPhoto = NO;
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowTakePicture = NO;
    imagePickerVc.allowCrop = NO;
    imagePickerVc.showSelectedIndex = YES;
    imagePickerVc.minImagesCount = 1;
    imagePickerVc.maxImagesCount = 9;
    imagePickerVc.alwaysEnableDoneBtn = NO;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        UIImage *resultImage = [UIImage combineImage:photos];
        [UIImage savePhotoalbumWithImage:resultImage complete:^(NSData * _Nonnull imageData) {
            UIImage *resultImage = [UIImage imageWithData:imageData];
            MBShortVideoPublishController *vc = [[MBShortVideoPublishController alloc]init];
            vc.publishImage = resultImage;
            vc.publishImageCoverImage = photos.firstObject;
            vc.entityType = weakself.entityType;
            [weakself.navigationController pushViewController:vc animated:YES];
        }];
    }];
    [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
//        [UIImage gainPhotoalbumVideo:asset complete:^(NSString * _Nonnull videoPath, NSData * _Nonnull videoData) {
//            MBShortVideoEditController *editVC = [[MBShortVideoEditController alloc]init];
//            [editVC setVideoPath:videoPath];
//            editVC.coverImage = coverImage;
//            [weakself.navigationController pushViewController:editVC animated:YES];
//        }];
//        [weakself exportAssetList:@[asset]];
        MBShortVideoImportLocalVideoHelp *help = [[MBShortVideoImportLocalVideoHelp alloc]init];
//        [help exportAssetList:@[asset] fromController:self];
        [help exportAssetList:@[asset] fromController:self coverImage:coverImage entity:weakself.entityType];
        
    }];
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}


-(instancetype)initWithConfigure:(MBShortVideoRecordConfig *)configure {
    self = [super init];
    if (self)
    {
        _videoConfig = configure;
        _cameraFront = YES;
        _cameraPreviewing = NO;
        _videoRecording = NO;
        _bgmRecording = NO;
        _bgmLoop = YES;
        _receiveBGMProgress = YES;
#ifndef UGC_SMART
//        _speedMode = SpeedMode_Standard;
#endif
        _zoom        = 1.0;
        _bgmBeginTime = 0;
        _currentRecordTime = 0;
        _appForeground = YES;
        
#ifndef UGC_SMART
//        _speedMode = SpeedMode_Standard;
#endif
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onAudioSessionEvent:)
                                                     name:AVAudioSessionInterruptionNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    }
    return self;
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark - event
/*播放*/
-(void)onBtnFlashClicked
{
    if (_isFlash) {
        [_btnFlash setImage:[UIImage imageNamed:@"closeFlash"] forState:UIControlStateNormal];
        [_btnFlash setImage:[UIImage imageNamed:@"closeFlash_hover"] forState:UIControlStateHighlighted];
    }else{
        [_btnFlash setImage:[UIImage imageNamed:@"openFlash"] forState:UIControlStateNormal];
        [_btnFlash setImage:[UIImage imageNamed:@"openFlash_hover"] forState:UIControlStateHighlighted];
    }
    _isFlash = !_isFlash;
    [[TXUGCRecord shareInstance] toggleTorch:_isFlash];
}
/*删除*/
-(void)onBtnDeleteClicked
{
    if (_videoRecording && !_isPaused) {
        [self onBtnRecordStartClicked];
    }
    if (0 == _deleteCount) {
//        [_progressView prepareDeletePart];
        [_progressView comfirmDeletePart];
        [[TXUGCRecord shareInstance].partsManager deleteLastPart];
        _isBackDelete = YES;
    }else{
        [_progressView comfirmDeletePart];
        [[TXUGCRecord shareInstance].partsManager deleteLastPart];
        _isBackDelete = YES;
#ifndef UGC_SMART
        if ([TXUGCRecord shareInstance].partsManager.getVideoPathList.count ==0) {
            [[TXUGCRecord shareInstance] setRecordSpeed:VIDEO_RECORD_SPEED_NOMAL];
        }
#endif
    }
    if (2 == ++ _deleteCount) {
        _deleteCount = 0;
    }
}
/*开始录制*/
-(void)onBtnRecordStartClicked
{
    if (!_videoRecording)
    {
        [self startVideoRecord];
    }
    else
    {
        if (_isPaused) {
#ifndef UGC_SMART
//            [self setSpeedRate];
            
            if (_bgmRecording) {
//                [self resumeBGM];
            }else{
//                [self playBGM:_bgmBeginTime];
                _bgmRecording = YES;
            }
#endif
            [[TXUGCRecord shareInstance] resumeRecord];
            
            self.bottomCommonView.videoState = MBShortVideoStateIng;
            
            if (_deleteCount == 1) {
                [_progressView cancelDelete];
                _deleteCount = 0;
            }
#ifndef UGC_SMART_btnStartRecord
            _speedOptionControl.hidden = YES;
#endif
            _isPaused = NO;
        }
        else {
            _btnDelete.enabled = NO;
            [[TXUGCRecord shareInstance] pauseRecord:^{
                _btnDelete.enabled = YES;
            }];
            [_progressView pause];
            _isPaused = YES;
            self.bottomCommonView.videoState = MBShortVideoStatePause;
#ifndef UGC_SMART
//            [self pauseBGM];
            _speedOptionControl.hidden = NO;
#endif
        }
    }
}
/*完成录制*/
- (void)onBtnDoneClicked
{
    if (!_videoRecording)
        return;
    
    [self stopVideoRecord];
}

-(void)startCameraPreview
{
    
    if (_cameraPreviewing == NO)
    {
        //简单设置
        //        TXUGCSimpleConfig * param = [[TXUGCSimpleConfig alloc] init];
        //        param.videoQuality = VIDEO_QUALITY_MEDIUM;
        //        [[TXUGCRecord shareInstance] startCameraSimple:param preview:_videoRecordView];
        //自定义设置
        
        TXUGCCustomConfig * param = [[TXUGCCustomConfig alloc] init];
        param.videoResolution =  _videoConfig.videoResolution;
        param.videoFPS = _videoConfig.fps;
        param.videoBitratePIN = _videoConfig.bps;
        param.GOP = _videoConfig.gop;
        param.enableAEC = _videoConfig.enableAEC;
        param.minDuration = MIN_RECORD_TIME;
        param.maxDuration = self.MAX_RECORD_TIME;
        [[TXUGCRecord shareInstance] startCameraCustom:param preview:_videoRecordView];
        [[TXUGCRecord shareInstance] setAspectRatio:_aspectRatio];
        [[TXUGCRecord shareInstance] setBeautyStyle:0 beautyLevel:5 whitenessLevel:7 ruddinessLevel:5];
        
        //[[TXUGCRecord shareInstance] setZoom:2.5];
#ifndef UGC_SMART
        [TXUGCRecord shareInstance].videoProcessDelegate = self;
        
        
        CGFloat videoWidth, videoHeight;
        switch (param.videoResolution) {
            case VIDEO_RESOLUTION_360_640:
                videoWidth = 360;
                videoHeight = 640;
                break;
            case VIDEO_RESOLUTION_540_960:
                videoWidth = 540;
                videoHeight = 960;
                break;
            case VIDEO_RESOLUTION_720_1280:
                videoWidth = 720;
                videoHeight = 1280;
                break;
            case VIDEO_RESOLUTION_1080_1920:
                videoWidth = 1080;
                videoHeight = 1920;
                break;
        }
        UIImage *cloud = [UIImage imageNamed:@"tcloud_symbol"];
        CGFloat imageWidth;
        if (videoWidth > videoHeight) {
            imageWidth = 0.08*videoHeight;
        } else {
            imageWidth = 0.08*videoWidth;
        }
        CGFloat imageHeight = imageWidth / cloud.size.width * cloud.size.height;
        
        NSDictionary *textAttribute = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:imageHeight],
                                        NSForegroundColorAttributeName:[UIColor whiteColor]};
        CGSize textSize = [@"快鱼直播" sizeWithAttributes:textAttribute];
        CGSize canvasSize = CGSizeMake(ceil(imageWidth + textSize.width), ceil(MAX(imageHeight,textSize.height) + imageWidth* 0.05));
        UIGraphicsBeginImageContext(canvasSize);
        [cloud drawInRect:CGRectMake(0, (canvasSize.height - imageHeight) / 2, imageWidth, imageHeight)];
        [@"快鱼直播" drawAtPoint:CGPointMake(imageWidth*1.05, (canvasSize.height - textSize.height) / 2)
             withAttributes:textAttribute];
        UIImage *waterimage = UIGraphicsGetImageFromCurrentImageContext(); //[UIImage imageNamed:@"watermark"];
        UIGraphicsEndImageContext();
        
        [[TXUGCRecord shareInstance] setWaterMark:waterimage normalizationFrame:CGRectMake(0.01, 0.01, canvasSize.width / videoWidth, 0)];
#endif
//        [_vBeauty resetValues];
        _cameraPreviewing = YES;
    }
}
/*结束录制*/
-(void)stopCameraPreview
{
    if (_cameraPreviewing == YES)
    {
        [[TXUGCRecord shareInstance] stopCameraPreview];
#ifndef UGC_SMART
        [TXUGCRecord shareInstance].videoProcessDelegate = nil;
#endif
        _cameraPreviewing = NO;
    }
}
/*开始录制*/
-(void)startVideoRecord
{
    [self refreshRecordTime:0];
    [self startCameraPreview];
#ifndef UGC_SMART
//    [self setSpeedRate];
#endif
    [TXUGCRecord shareInstance].recordDelegate = self;
    int result = [[TXUGCRecord shareInstance] startRecord];
    //自定义目录
    //    int result = [[TXUGCRecord shareInstance] startRecord:[NSTemporaryDirectory() stringByAppendingPathComponent:@"outRecord.mp4"] videoPartsFolder:NSTemporaryDirectory()coverPath:[NSTemporaryDirectory() stringByAppendingPathComponent:@"outRecord.jpg"]];
    if(0 != result)
    {
        if(-3 == result) [self alert:@"启动录制失败" msg:@"请检查摄像头权限是否打开"];
        else if(-4 == result) [self alert:@"启动录制失败" msg:@"请检查麦克风权限是否打开"];
        else if(-5 == result) [self alert:@"启动录制失败" msg:@"licence 验证失败"];
    }else{
#ifndef UGC_SMART
        //如果设置了BGM，播放BGM
//        [self playBGM:_bgmBeginTime];
#endif
        
        //初始化录制状态
        _bgmRecording = YES;
        _videoRecording = YES;
        _isPaused = NO;
        
        //录制过程中不能切换分辨率
        _btnRatio169.enabled = NO;
        _btnRatio43.enabled = NO;
        _btnRatio11.enabled = NO;
        
#ifndef UGC_SMART
        _speedOptionControl.hidden = YES;
#endif
        self.bottomCommonView.videoState = MBShortVideoStateIng;
    }
}

-(void)alert:(NSString *)title msg:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)stopVideoRecord
{
    [[TXUGCRecord shareInstance] stopRecord];
    [self resetVideoUI];
}

-(void)resetVideoUI{
    _isPaused = NO;
    _videoRecording = NO;
}

-(void)onBtnCameraClicked
{
    _cameraFront = !_cameraFront;
    [[TXUGCRecord shareInstance] switchCamera:_cameraFront];
    if (_cameraFront) {
        [_btnFlash setImage:[UIImage imageNamed:@"openFlash_disable"] forState:UIControlStateNormal];
        _btnFlash.enabled = NO;
    }else{
        if (_isFlash) {
            [_btnFlash setImage:[UIImage imageNamed:@"openFlash"] forState:UIControlStateNormal];
            [_btnFlash setImage:[UIImage imageNamed:@"openFlash_hover"] forState:UIControlStateHighlighted];
        }else{
            [_btnFlash setImage:[UIImage imageNamed:@"closeFlash"] forState:UIControlStateNormal];
            [_btnFlash setImage:[UIImage imageNamed:@"closeFlash_hover"] forState:UIControlStateHighlighted];
        }
        _btnFlash.enabled = YES;
    }
    [[TXUGCRecord shareInstance] toggleTorch:_isFlash];
}

#pragma mark ---- VideoRecordListener ----
-(void) onRecordProgress:(NSInteger)milliSecond{
    [self refreshRecordTime: milliSecond / 1000.0];
}
-(void) onRecordComplete:(TXUGCRecordResult*)result;
{
    MB_WeakSelf(self);
    _appForeground = YES;
    if (_appForeground)
    {
        
        TX_Enum_Type_RenderMode renderMode = /*_aspectRatio == VIDEO_ASPECT_RATIO_9_16 ? RENDER_MODE_FILL_SCREEN :*/ RENDER_MODE_FILL_EDGE;
        if (result.retCode == UGC_RECORD_RESULT_OK) {
            if (self.onRecordCompleted) {
                self.onRecordCompleted(result);
            }
            [self stopCameraPreview];
        }
        else if(result.retCode == UGC_RECORD_RESULT_OK_BEYOND_MAXDURATION){
            if (self.onRecordCompleted) {
                self.onRecordCompleted(result);
            }
            [self stopCameraPreview];
//            [self stopVideoRecord]; /*这里打开会重复触发完成回调 onRecordComplete*/
        }
        else if(result.retCode == UGC_RECORD_RESULT_OK_INTERRUPT){
            [MBProgressHUD showMessage:@"录制被打断"];
        }
        else if(result.retCode == UGC_RECORD_RESULT_OK_UNREACH_MINDURATION){
//            [MBProgressHUD showMessage:@"至少要录够5秒"];
            if (self.onRecordCompleted) {
                self.onRecordCompleted(result);
            }
        }
        else if(result.retCode == UGC_RECORD_RESULT_FAILED){
            [MBProgressHUD showMessage:@"视频录制失败"];

        }
    }
    //分片不再使用的时候请主动删除，否则分片会一直存在本地，导致内存占用越来越大，下次startRecord时候，SDK也会默认加载当前分片
    [[TXUGCRecord shareInstance].partsManager deleteAllParts];
#ifndef UGC_SMART
//    [self resetBGM];
#endif
    [self refreshRecordTime:0];
}

#pragma mark ---- Video Beauty UI ----
-(void)initBeautyUI{
//    NSUInteger controlHeight = [BeautySettingPanel getHeight];
//    _vBeauty = [[BeautySettingPanel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - controlHeight, self.view.frame.size.width, controlHeight)];
//    _vBeauty.hidden = YES;
//    _vBeauty.delegate = self;
//    _vBeauty.pituDelegate = self;
//    [self.view addSubview:_vBeauty];
}

-(void)setMAX_RECORD_TIME:(CGFloat)MAX_RECORD_TIME {
    _MAX_RECORD_TIME = MAX_RECORD_TIME;
}

-(void)refreshRecordTime:(float)second
{
    _currentRecordTime = second;
    [_progressView update:_currentRecordTime / self.MAX_RECORD_TIME];
//    NSInteger min = (int)_currentRecordTime / 60;
    NSInteger sec = (int)_currentRecordTime % 60;
    self.bottomCommonView.recordTime = sec;
//    [_recordTimeLabel setText:[NSString stringWithFormat:@"%02ld:%02ld", min, sec]];
//    [_recordTimeLabel sizeToFit];
}

-(MBShortVideoTopCommonView *)topCommonView {
    if (_topCommonView == nil) {
        _topCommonView = [MBShortVideoTopCommonView nibView];
        _topCommonView.frame = (CGRect){{0,0},{kScreenWidth,120}};
        _topCommonView.autoresizingMask = NO;
        _topCommonView.backgroundColor = [UIColor clearColor];
    }
    return _topCommonView;
}
-(MBShortVideoBottomCommonView *)bottomCommonView {
    if (_bottomCommonView == nil) {
        _bottomCommonView = [MBShortVideoBottomCommonView nibView];
        _bottomCommonView.frame = (CGRect){{0,kScreenHeight - kTabSafeBottomMargin - 130},{kScreenWidth,130}};
        _bottomCommonView.autoresizingMask = NO;
        _bottomCommonView.backgroundColor = [UIColor clearColor];
    }
    return _bottomCommonView;
}
@end
