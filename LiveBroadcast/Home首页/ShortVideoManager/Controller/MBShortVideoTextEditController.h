//
//  MBShortVideoTextEditController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN
@class TXVideoEditer;
@class VideoPreview;
@class VideoTextFiled;

/**
 字幕添加操作操作器
 */

@interface VideoTextInfo : NSObject
@property (nonatomic, strong) VideoTextFiled* textField;
@property (nonatomic, assign) CGFloat startTime; //in seconds
@property (nonatomic, assign) CGFloat endTime;
@end


@protocol VideoTextViewControllerDelegate <NSObject>
//返回
- (void)onSetVideoTextInfosFinish:(NSArray<VideoTextInfo*>*)videoTextInfos;

@end

@interface MBShortVideoTextEditController : MBBaseController
@property (nonatomic, weak) id<VideoTextViewControllerDelegate> delegate;

- (id)initWithVideoEditer:(TXVideoEditer*)videoEditer previewView:(VideoPreview*)previewView startTime:(CGFloat)startTime endTime:(CGFloat)endTime videoTextInfos:(NSArray<VideoTextInfo*>*)videoTextInfos;


@end

NS_ASSUME_NONNULL_END
