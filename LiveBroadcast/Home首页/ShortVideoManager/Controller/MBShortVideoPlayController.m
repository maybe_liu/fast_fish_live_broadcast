//
//  MBShortVideoPlayController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoPlayController.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "MBShortVideoDetailModel.h"
#import "MBShortVideoLikeModel.h"
#import "MBShareManager.h"
#import "MBShareCommonModel.h"
#import "MBUserDetailController.h"
#import "MBKeyBoardInputField.h"
#import "MBShortVideoCommentListCell.h"
#import "MBShortVideoCommentHeadView.h"
#import "MBShortVideoReportController.h"
#import "MBShortVideoConnmentListModel.h"
#import "MBShortVideoCommentSectionHeadView.h"
#import "MBShortVideoCommentSectionHeadViews.h"
#import "MBShortVideoCommentListSectionView.h"
#import <ViewDeck/ViewDeck.h>
#import "LiveGiftShowCustom.h"
#import "ThumbView.h"

@interface MBShortVideoPlayController ()<LiveGiftShowCustomDelegate,TXVodPlayListener,UITableViewDelegate,UITableViewDataSource,MBShortVideoCommentHeadViewDelegate,IIViewDeckControllerDelegate>
@property (nonatomic, strong) UIButton      *leftNavButton;
@property (nonatomic, strong) MBShortVideoDetailModel *videoDetailModel;
@property (nonatomic, strong) MBKeyBoardInputField *inputField;
@property (nonatomic, strong) MBBaseTableview      *mainTableView;
@property (nonatomic, strong) MBShortVideoCommentHeadView *headView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (strong, nonatomic)  MBTextField *wordField;
@property (nonatomic,copy)NSString *commentId;
@property (nonatomic,copy)NSString *commentUserId;
@property (nonatomic ,weak) LiveGiftShowCustom * customGiftShow;

@end


   static NSString *const MBShortVideoCommentListCellID = @"MBShortVideoCommentListCellID";
   static NSString *const MBShortVideoCommentSectionHeadViewsID = @"MBShortVideoCommentSectionHeadViewsID";
   static NSString *const MBShortVideoCommentListSectionViewID = @"MBShortVideoCommentListSectionViewID";

@implementation MBShortVideoPlayController{
    UIView *    mVideoContainer;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initCustomNav];
    [self initUI];
    [self sendVideoDetailRequest];
    [self sendVideoCommentListRequest];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //初始化礼物弹幕视图
    [self customGiftShow];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.viewDeckController.delegate = self;
    self.navigationController.navigationBar.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    [UIApplication sharedApplication].statusBarHidden = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [UIApplication sharedApplication].statusBarHidden = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
}
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    if ([_txLivePlayer isPlaying]) {
//        [_txLivePlayer stopPlay];
//    }
}
-(void)dealloc {
    if ([_txLivePlayer isPlaying]) {
        [_txLivePlayer stopPlay];
    }
}
/**
 获取视频详情
 */
-(void)sendVideoDetailRequest {
    MB_WeakSelf(self);
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.videoID forKey:@"videoId"];
    [param setValue:self.videoType forKey:@"type"];
//    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
//        [param setValue:[MBUserModel sharedMBUserModel].userInfo.userId forKey:@"userId"];
//    }
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [MBHttpRequset requestWithUrl:kLive_shortVideo_videoDetail setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShortVideoDetailModel *model = [MBShortVideoDetailModel  mj_objectWithKeyValues:AJson];
        weakself.videoDetailModel = model;
        [weakself refreshHeadViewUIWith:model];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

/** 点赞 */
-(void)sendVideoLikeRequest {
    MB_WeakSelf(self);
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.videoID forKey:@"videoId"];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [MBHttpRequset requestWithUrl:kLive_shortVideo_like setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShortVideoLikeModel *model = [MBShortVideoLikeModel mj_objectWithKeyValues:AJson];
        [weakself refreshLikeCount:model.likesCount];
        [MBProgressHUD showSuccess:@"点赞成功"];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

/**获取评论*/
-(void)sendRefreshVideoCommentListRequest {
    MB_WeakSelf(self);
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.videoID forKey:@"videoId"];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [param setValue:@(1) forKey:@"pageNo"];
    [self.dataArray removeAllObjects];
    [MBHttpRequset requestWithUrl:kLive_shortVideo_commentsList setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShortVideoConnmentListModel *model = [MBShortVideoConnmentListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray =  model.commentsList;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView.mj_footer endRefreshing];
    } failure:^(id  _Nonnull AJson) {
        [weakself.mainTableView.mj_footer endRefreshing];
    }];
}

/**获取评论*/
-(void)sendVideoCommentListRequest {
    MB_WeakSelf(self);
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.videoID forKey:@"videoId"];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];

    [MBHttpRequset requestWithUrl:kLive_shortVideo_commentsList setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShortVideoConnmentListModel *model = [MBShortVideoConnmentListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray =  model.commentsList;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView.mj_footer endRefreshing];
    } failure:^(id  _Nonnull AJson) {
        [weakself.mainTableView.mj_footer endRefreshing];
    }];
}
/**刷新评论*/
-(void)refreshCommentListWithCommentID:(NSString *)commentID {
    MB_WeakSelf(self);
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.videoID forKey:@"videoId"];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    
    [MBHttpRequset requestWithUrl:kLive_shortVideo_commentsList setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShortVideoConnmentListModel *model = [MBShortVideoConnmentListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray =  model.commentsList;
        
        for (NSInteger i = 0; i < self.dataArray.count; i++) {
            MBShortVideoCommentModel *tmpModel = self.dataArray[i];
            if (![tmpModel.ID isEqualToString:commentID]) {
                continue;
            }else {
                for (MBShortVideoCommentModel *aModel in tmpArray) {
                    if ([aModel.ID isEqualToString:commentID]) {
                        [self.dataArray replaceObjectAtIndex:i withObject:aModel];
                        break;
                    }
                }
                
            }
        }
       
        
//        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView.mj_footer endRefreshing];
    } failure:^(id  _Nonnull AJson) {
        [weakself.mainTableView.mj_footer endRefreshing];
    }];
}

/** 分享 */
-(void)sendShareInfoRequest {
    NSDictionary *param = @{
                            @"id":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"type":@(4)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_CommonShareApi setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBShareCommonModel *model = [MBShareCommonModel mj_objectWithKeyValues:AJson];
        [[MBShareManager shareMBShareManager] mb_initShareView];
        [MBShareManager shareMBShareManager].shareTitle = weakself.videoDetailModel.videoHeadline;
        [MBShareManager shareMBShareManager].shareSubTitle = weakself.videoDetailModel.videoIntroduce;
        [MBShareManager shareMBShareManager].shareURL = model.url;
        [MBShareManager shareMBShareManager].shareImage = weakself.videoDetailModel.coverUrl;
        [MBShareManager shareMBShareManager].sharePlatformBlockHandler = ^(MBSharePlatformType type) {
            switch (type) {
                case MBSharePlatformTypeSystemFriend:{
//                    [weakself showSystemFriend];
                }break;
            }
        };
        
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*举报*/
-(void)sendReportVideoRequest {
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"videoId":self.videoID,
                            @"reportContent":@"违法违规"
                            };
//    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_shortvideo_report setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"举报成功"];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*加入黑名单*/
-(void)sendJoinBlackListRequest {
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"blackUserId":self.videoDetailModel.issueId,
                            };
    //    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_shortvideo_pullblacklist setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"拉入黑名单成功"];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*关注*/
-(void)sendAttentionRequest:(void(^)(BOOL success))success {
    MB_WeakSelf(self);
    NSDictionary *param = @{
                           @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                           @"befansId":filter(self.videoDetailModel.issueId),
                           @"type":@"5"
                           };
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_setFocusUs setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        success(YES);
    } failure:^(id  _Nonnull AJson) {
        success(NO);
    }];
}
/*评论作者*/
-(void)sendCommentRequest:(NSString *)commentString {
    MB_WeakSelf(self);
    NSDictionary *param = @{
                            @"videoId":self.videoID,
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"comments":commentString
                            };
    [MBHttpRequset requestWithUrl:kLive_shortvideo_sendComment setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"评论成功"];
        [weakself sendRefreshVideoCommentListRequest];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*评论 某个评论*/
-(void)sendCommentReque:(NSString *)commentString commentId:(NSString *)commentId commentUser:(NSString *)commentUserID {
    MB_WeakSelf(self);
    NSDictionary *param = @{
                            @"videoId":self.videoID,
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"comments":commentString,
                            @"atuserId":commentUserID,
                            @"commentsId":commentId,
                            };
    [MBHttpRequset requestWithUrl:kLive_shortvideo_sendComment setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"评论成功"];
        [weakself refreshCommentListWithCommentID:commentId];
//        [weakself sendVideoCommentListRequest];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

/*点赞评论*/
-(void)sendLikeCommentRequest:(NSString *)commentId {
    MB_WeakSelf(self);
    NSDictionary *param = @{
                            @"videoId":self.videoID,
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"commentsId":commentId
                            };
    [MBHttpRequset requestWithUrl:kLive_shortvideo_likecomment setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"点赞成功"];
        [weakself.mainTableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

-(void)initCustomNav {
    self.navigationItem.title = @"";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.leftNavButton];
    self.pageNo = 1;
}
-(void)initUI {
    MB_WeakSelf(self);
    [self.view addSubview:self.mainTableView];
    [self.view addSubview:self.inputField];
    [self.view addSubview:self.wordField];

    self.mainTableView.tableHeaderView = self.headView;
    self.headView.defaultImageURL = self.coverImage;
    self.mainTableView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo ++;
        [weakself sendVideoCommentListRequest];
    }];
    self.inputField.wordFieldBlock = ^(NSString * _Nonnull wordString,MBwordFieldType type) {
        weakself.inputField.type = MBwordFieldTypeAudience;
        if (type == MBwordFieldTypeAudience) {
             [weakself sendCommentRequest:wordString];
        }else if(type == MBwordFieldTypeAnchor){
            [weakself sendCommentReque:wordString commentId:weakself.commentId commentUser:weakself.commentUserId];
        }
        weakself.inputField.mainField.text = @"";
        [weakself.inputField.mainField resignFirstResponder];
    };
    if ([self.entityType isEqualToString:@"2"]) {
        _mbImageView = ({
            UIImageView *imageView = [[UIImageView alloc]init];
//            imageView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight - kTabSafeBottomMargin}};
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView;
        });
        _mbScrollView = ({
            MBBaseScrollView *scrollView = [[MBBaseScrollView alloc]init];
            scrollView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight - kTabSafeBottomMargin}};
            scrollView.bounces = NO;
            scrollView;
        });
        [self.headView insertSubview:_mbScrollView atIndex:0];
//        [self.view addSubview:_mbScrollView];
        [_mbScrollView addSubview:_mbImageView];
//        self.mainTableView.backgroundColor = [UIColor clearColor];
        
        [_mbImageView sd_setImageWithURL:[NSURL URLWithString:self.videoPath] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [weakself.headView hiddenDefaultWhenHaveData];
            CGSize imageSize = image.size;
            CGFloat scale = kScreenWidth/imageSize.width;
            CGFloat height = imageSize.height *scale;
            _mbScrollView.contentSize = CGSizeMake(kScreenWidth, height);
            _mbImageView.frame = (CGRect){{0,0},{kScreenWidth,height}};
            weakself.headView.frame = (CGRect){{0,0},{kScreenWidth,height}};
        }];
    }else {
        CGRect VideoFrame = self.view.bounds;
        mVideoContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
        [self.view insertSubview:mVideoContainer atIndex:0];
//        mVideoContainer.center = self.view.center;
        _txLivePlayer = [[TXVodPlayer alloc] init];
        //    _txLivePlayer.vodDelegate = self;
        _txLivePlayer.loop = YES;
        [_txLivePlayer setupVideoWidget:self.headView insertIndex:0];
        int result = [_txLivePlayer startPlay:self.videoPath];
        if( result != 0)
        {
            NSLog(@"播放器启动失败");
        }
    }
}

-(void)refreshHeadViewUIWith:(MBShortVideoDetailModel *)model {
    [self.headView refreshUIWithDataWith:model];
}

-(void)refreshLikeCount:(NSString *)likeCount {
    self.headView.likeCount = likeCount;
}




#pragma mark - event
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    ThumbView* heart = [[ThumbView alloc]initWithFrame:CGRectMake(0, 0, 36, 36)];
    [self.view addSubview:heart];
    CGPoint fountainSource = CGPointMake(kScreenWidth - 90, self.view.bounds.size.height - 36/2.0 - 10);
    heart.center = fountainSource;
    [heart thumbINView:self.view];
    
    // button点击动画
    CAKeyframeAnimation *btnAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    btnAnimation.values = @[@(1.0),@(0.7),@(0.5),@(0.3),@(0.5),@(0.7),@(1.0), @(1.2), @(1.4), @(1.2), @(1.0)];
    btnAnimation.keyTimes = @[@(0.0),@(0.1),@(0.2),@(0.3),@(0.4),@(0.5),@(0.6),@(0.7),@(0.8),@(0.9),@(1.0)];
    btnAnimation.calculationMode = kCAAnimationLinear;
    btnAnimation.duration = 0.3;
    [self.view.layer addAnimation:btnAnimation forKey:@"SHOW"];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count + 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }else {
        MBShortVideoCommentModel *model = self.dataArray[section - 1];
        return model.replyList.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBShortVideoCommentListCell *cell = [tableView dequeueReusableCellWithIdentifier:MBShortVideoCommentListCellID];
    if (cell == nil) {
        cell = [[MBShortVideoCommentListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBShortVideoCommentListCellID];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MBShortVideoCommentModel *model = self.dataArray[indexPath.section - 1];
    cell.commentModel = model.replyList[indexPath.row];
    MB_WeakSelf(self);
    cell.likeBlock = ^(MBShortVideoCommentModel * _Nonnull commentModel) {
        [weakself sendLikeCommentRequest:commentModel.commentsId];
    };
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MB_WeakSelf(self);
    if (section == 0) {
        MBShortVideoCommentSectionHeadViews *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:MBShortVideoCommentSectionHeadViewsID];
        if (view == nil) {
            view = [[MBShortVideoCommentSectionHeadViews alloc]initWithReuseIdentifier:MBShortVideoCommentSectionHeadViewsID];
        }
        //    view.size = (CGSize){kScreenWidth,90};
        view.jumpBlock = ^{
            MBUserDetailController *vc = [[MBUserDetailController alloc]init];
            vc.userId = weakself.videoDetailModel.issueId;
            [weakself.navigationController pushViewController:vc animated:YES];
        };
        view.model = self.videoDetailModel;
//        view.contentView.backgroundColor = Color_Red;
        return view;
    }else {
        MBShortVideoCommentListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:MBShortVideoCommentListSectionViewID];
        if (view == nil) {
            view = [[MBShortVideoCommentListSectionView alloc]initWithReuseIdentifier:MBShortVideoCommentListSectionViewID];
        }
        view.commentModel = self.dataArray[section - 1];
        view.sectionClickBlock = ^(MBShortVideoCommentModel * _Nonnull commentModel) {
            weakself.inputField.type = MBwordFieldTypeAnchor;
            if ([commentModel.userId isNotBlank] && [commentModel.ID isNotBlank]) {
                weakself.commentId = commentModel.ID;
                weakself.commentUserId = commentModel.userId;
                [weakself.wordField becomeFirstResponder];
            }
        };
        return view;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 90.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section != self.dataArray.count) {
        return 0.01;
    }else { /*最后一组*/
        return 30;
    }
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    scrollView.contentOffset = offset;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    self.inputField.type = MBwordFieldTypeAnchor;
//    MBShortVideoCommentModel *model = self.dataArray[indexPath.row];
//    if ([model.userId isNotBlank] && [model.ID isNotBlank]) {
//        self.commentId = model.ID;
//        self.commentUserId = model.userId;
//        [self.wordField becomeFirstResponder];
//    }
}


#pragma mark - MBShortVideoCommentHeadViewDelegate
- (void)attentionUserButtonClick{
    MBUserDetailController *vc = [[MBUserDetailController alloc]init];
    vc.userId = self.videoDetailModel.issueId;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)shareButtonClick{
    [self sendShareInfoRequest];
}
- (void)likeButtonClick{
     [self sendVideoLikeRequest];
}
- (void)attentionButtonClick{
    MB_WeakSelf(self);
    [self sendAttentionRequest:^(BOOL success) {
        if (success == YES) {
            [weakself.headView hiddenAttention:YES];
        }
    }];
}
- (void)popButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)reportButtonClick{
    MB_WeakSelf(self);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *reportAction = [UIAlertAction actionWithTitle:@"举报" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        MBShortVideoReportController *vc = [[MBShortVideoReportController alloc]init];
        vc.videoID = weakself.videoID;
        [weakself.navigationController pushViewController:vc animated:YES];
    }];
    UIAlertAction *pullBlacklistAction = [UIAlertAction actionWithTitle:@"加入黑名单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakself sendJoinBlackListRequest];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:reportAction];
    [alert addAction:pullBlacklistAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - NSNotification
/*键盘展开*/
- (void)keyBoardWillShow:(NSNotification *) note {
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘高度
    CGRect keyBoardBounds  = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyBoardHeight = keyBoardBounds.size.height;
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    CGRect begin = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGFloat currenKeyBoardHeight = [[userInfo objectForKey:UIKeyboardBoundsUserInfoKey] CGRectValue].size.height;
    if (begin.size.height > 0 && (begin.origin.y - keyBoardBounds.origin.y > 0)) {
        keyBoardHeight = currenKeyBoardHeight;
        MBLog(@"键盘高度%@ +++ %lf",NSStringFromCGRect(keyBoardBounds),keyBoardHeight);

        // 定义好动作
        void (^animation)(void) = ^void(void) {
            self.inputField.transform = CGAffineTransformMakeTranslation(0, -keyBoardHeight - 100);
            [self.inputField.mainField becomeFirstResponder];
        };
        if (animationTime > 0) {
            [UIView animateWithDuration:animationTime animations:animation];
        } else {
            animation();
        }
    }
}
/*键盘收起*/
- (void)keyBoardWillHide:(NSNotification *) note {
    // 获取用户信息
    NSDictionary *userInfo = [NSDictionary dictionaryWithDictionary:note.userInfo];
    // 获取键盘动画时间
    CGFloat animationTime  = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    // 定义好动作
    void (^animation)(void) = ^void(void) {
        self.inputField.transform = CGAffineTransformIdentity;
    };
    if (animationTime > 0) {
        [UIView animateWithDuration:animationTime animations:animation];
        [self.inputField.mainField resignFirstResponder];
        
    } else {
        animation();
    }
}

#pragma mark - getter
-(UIButton *)leftNavButton {
    if (_leftNavButton == nil) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:kImage(@"shortvideo_left") forState:UIControlStateNormal];
        button.frame = (CGRect){{0,0},{40,40}};
        button.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        [button addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        _leftNavButton = button;
    }
    return _leftNavButton;
}
-(MBKeyBoardInputField *)inputField {
    if (_inputField == nil) {
        _inputField = [MBKeyBoardInputField nibView];
        _inputField.frame = CGRectMake(0, kScreenHeight + 40, kScreenWidth, 40);
    }
    return _inputField;
}
-(MBBaseTableview *)mainTableView {
    if (_mainTableView == nil) {
        _mainTableView = [[MBBaseTableview alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight}} style:UITableViewStyleGrouped];
        _mainTableView.backgroundColor = Color_White;
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
//        _mainTableView.estimatedRowHeight = 100.0f;
        _mainTableView.rowHeight = 100.0f;

        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBShortVideoCommentListCell class]) bundle:nil] forCellReuseIdentifier:MBShortVideoCommentListCellID];
        
        [_mainTableView registerClass:[MBShortVideoCommentSectionHeadViews class] forHeaderFooterViewReuseIdentifier:MBShortVideoCommentSectionHeadViewsID];
        [_mainTableView registerClass:[MBShortVideoCommentListSectionView class] forHeaderFooterViewReuseIdentifier:MBShortVideoCommentListSectionViewID];
    }
    return _mainTableView;
}
-(MBShortVideoCommentHeadView *)headView {
    if (_headView == nil) {
        _headView = [MBShortVideoCommentHeadView nibView];
        _headView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight}};
        _headView.delegate = self;
//        _headView.backgroundColor = [UIColor clearColor];
    }
    return _headView;
}
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
-(MBTextField *)wordField {
    if (_wordField == nil) {
        _wordField = [[MBTextField alloc]init];
        _wordField.frame = (CGRect){{12,kScreenHeight - 25 - kTabSafeBottomMargin},{kScreenWidth - 24,26}};
        _wordField.placeholderFont = kFont(14);
        _wordField.placeholderColor = Color_White;
        _wordField.mTintColor = Color_FF8000;
        _wordField.mBackgroundColor = [UIColor clearColor];
        _wordField.placeholder = @"说点什么...";
    }
    return _wordField;
}

- (BOOL)viewDeckController:(IIViewDeckController *)viewDeckController willOpenSide:(IIViewDeckSide)side
{
    //IIViewDeckSideRight
    //IIViewDeckSideRight
    if (side == IIViewDeckSideRight) {
        MBUserDetailController *vc = [[MBUserDetailController alloc]init];
        vc.userId = self.videoDetailModel.issueId;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    return NO;
}
/*
 礼物视图支持很多配置属性，开发者按需选择。
 */
- (LiveGiftShowCustom *)customGiftShow{
    if (!_customGiftShow) {
        _customGiftShow = [LiveGiftShowCustom addToView:self.view];
        _customGiftShow.addMode = LiveGiftAddModeAdd;
        [_customGiftShow setMaxGiftCount:3];
        [_customGiftShow setShowMode:LiveGiftShowModeFromTopToBottom];
        [_customGiftShow setAppearModel:LiveGiftAppearModeLeft];
        [_customGiftShow setHiddenModel:LiveGiftHiddenModeNone];
        [_customGiftShow enableInterfaceDebug:YES];
        _customGiftShow.delegate = self;
    }
    return _customGiftShow;
}

@end
