//
//  MBShortVideoReportController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoReportController.h"
#import "MBShortVideoReportCell.h"

@interface MBShortVideoReportController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSArray *dataArray;
@property (nonatomic, copy)NSString *reportString;
@end

static NSString *const MBShortVideoReportCellID = @"MBShortVideoReportCellID";
@implementation MBShortVideoReportController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
}

-(void)createUI {
    [self.reportButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.reportButton.titleLabel.font = kMFont(16);
    [self.reportButton setBackgroundColor:Color_FF8000];
    self.reportButton.layer.cornerRadius = 22.0f;
    self.reportButton.layer.masksToBounds = YES;
    
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 50.0f;
    self.mainTableView.scrollEnabled = NO;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBShortVideoReportCell class]) bundle:nil] forCellReuseIdentifier:MBShortVideoReportCellID];
    self.mainTableView.showsVerticalScrollIndicator = NO;
}

-(void)createCustomNav {
    self.navigationItem.title = @"举报";
}
/*举报*/
-(void)sendReportVideoRequest {
    MB_WeakSelf(self);
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"videoId":self.videoID,
                            @"reportContent":@"违法违规"
                            };
    [MBHttpRequset requestWithUrl:kLive_shortvideo_report setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"举报成功"];
        [weakself.navigationController popViewControllerAnimated:YES];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
- (IBAction)reportButtonClick:(UIButton *)sender {
    if ([self.reportString isNotBlank]) {
        [self sendReportVideoRequest];
    }else {
        [MBProgressHUD showError:@"请选择举报的分类"];
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
    //    return 10;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBShortVideoReportCell *cell = [tableView dequeueReusableCellWithIdentifier:MBShortVideoReportCellID];
    if (cell == nil) {
        cell = [[MBShortVideoReportCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBShortVideoReportCellID];
    }
    cell.nameLabel.text = self.dataArray[indexPath.row];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.reportString = self.dataArray[indexPath.row];
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = ({
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = Color_Line;
        view.size = (CGSize){kScreenWidth,50};
        view;
    });
    UILabel *label = ({
        UILabel *label = [[UILabel alloc]init];
        label.text = @"请选择举报分类，分类越准，处理越快";
        label.textColor = Color_333333;
        label.font = kFont(14);
        label;
    });
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(view.mas_left).offset(12);
        make.centerY.equalTo(view.mas_centerY);
    }];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}

-(NSArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = @[@"违法违规",@"色情低俗",@"垃圾广告、买假冒伪劣产品",@"内容不适合孩子观看",@"未成年不适当行为",@"标题党、封面党、骗点击",@"作品令人反感我不喜欢"];
    }
    return _dataArray;
}
@end
