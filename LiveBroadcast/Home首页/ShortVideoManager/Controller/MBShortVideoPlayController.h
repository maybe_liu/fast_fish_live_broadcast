//
//  MBShortVideoPlayController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 短视频 播放控制器
 */
@interface MBShortVideoPlayController : MBBaseController{
    TXVodPlayer *      _txLivePlayer;
    TXVodPlayConfig   *_config;
    UIImageView       *_mbImageView;
    MBBaseScrollView  *_mbScrollView;
}

@property (nonatomic, copy)NSString *videoPath;
@property (nonatomic, copy)NSString *videoID;
@property (nonatomic, copy)NSString *videoType;
@property (nonatomic, copy)NSString *coverImage;

@property (nonatomic, copy)NSString *entityType; /*视频 or 图片*/


//@property (weak, nonatomic) IBOutlet MBTextField *wordField;
//@property (weak, nonatomic) IBOutlet MBCustomButton *shareButton;
//@property (weak, nonatomic) IBOutlet UILabel *bottomTitleButton;
//@property (weak, nonatomic) IBOutlet MBCustomButton *commentButton;
//@property (weak, nonatomic) IBOutlet MBCustomButton *likeButton;
//@property (weak, nonatomic) IBOutlet MBCustomButton *attentionButton;
//@property (weak, nonatomic) IBOutlet UIView *attentionView;
//@property (weak, nonatomic) IBOutlet UIButton *attentionUserButton;
//@property (weak, nonatomic) IBOutlet UIButton *popButton;
//@property (weak, nonatomic) IBOutlet MBCustomButton *reportButton;



@end

NS_ASSUME_NONNULL_END
