//
//  MBShortVideoEditController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 短视频 编辑页面
 */
@interface MBShortVideoEditController : MBBaseController

@property (strong,nonatomic) NSString *videoPath;

@property (strong,nonatomic) AVAsset  *videoAsset;

@property (strong,nonatomic) NSArray  *imageList;

@property (assign, nonatomic) BOOL removeVideoAfterFinish;

@property (strong,nonatomic) UIImage  *coverImage;

@property(nonatomic, assign)MBShortVideoEntityType entityType;

@end

NS_ASSUME_NONNULL_END
