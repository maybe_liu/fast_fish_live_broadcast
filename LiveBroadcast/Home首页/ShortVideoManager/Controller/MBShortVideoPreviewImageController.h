//
//  MBShortVideoPreviewImageController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoPreviewImageController : MBBaseController

@property (nonatomic, strong)UIImage *previweImage;

@end

NS_ASSUME_NONNULL_END
