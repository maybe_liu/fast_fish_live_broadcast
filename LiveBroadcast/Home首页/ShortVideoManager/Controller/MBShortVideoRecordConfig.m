//
//  MBShortVideoRecordConfig.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoRecordConfig.h"

@implementation MBShortVideoRecordConfig
-(instancetype)init
{
    self = [super init];
    if (self) {
        _videoResolution = VIDEO_RESOLUTION_720_1280;
        _videoRatio = VIDEO_ASPECT_RATIO_9_16;
        _videoQuality = VIDEO_QUALITY_HIGH;
        _videoBeautyStyle = VIDOE_BEAUTY_STYLE_SMOOTH;
        _bps = 9600;
        _fps = 30;
        _gop = 3;
        _enableAEC = YES;
    }
    return self;
}
+ (instancetype)defaultConfigure
{
    return [[MBShortVideoRecordConfig alloc] init];
}

@end
