//
//  MBShortVideoReportController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/3.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShortVideoReportController : MBBaseController
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (weak, nonatomic) IBOutlet UIButton *reportButton;
@property (nonatomic, copy) NSString * videoID;
@end

NS_ASSUME_NONNULL_END
