//
//  MBShortVideoPreviewController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import <TXLiteAVSDK.h>
NS_ASSUME_NONNULL_BEGIN

/**
 短视频预览vc
 */
@interface MBShortVideoPreviewController : MBBaseController
@property (copy, nonatomic) void(^onTapEdit)(MBShortVideoPreviewController *previewController);
@property (readonly, nonatomic) NSString *videoPath;

- (instancetype)initWithCoverImage:(UIImage *)coverImage
                         videoPath:(NSString*)videoPath
                        renderMode:(TX_Enum_Type_RenderMode)renderMode
                    showEditButton:(BOOL)showEditButton;
@end

NS_ASSUME_NONNULL_END
