//
//  MBShortVideoPreviewImageController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBShortVideoPreviewImageController.h"

@interface MBShortVideoPreviewImageController ()

@end

@implementation MBShortVideoPreviewImageController {
    UIImageView       *_mbImageView;
    MBBaseScrollView  *_mbScrollView;
    UIButton          *_leftNavButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mbImageView = ({
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView;
    });
    _mbScrollView = ({
        MBBaseScrollView *scrollView = [[MBBaseScrollView alloc]init];
        scrollView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight - kTabSafeBottomMargin}};
        scrollView.bounces = NO;
        scrollView;
    });
    _leftNavButton = ({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:kImage(@"shortvideo_left") forState:UIControlStateNormal];
        button.frame = (CGRect){{10,kNavStatusHeight},{40,40}};
        [button addTarget:self action:@selector(popNav) forControlEvents:UIControlEventTouchUpInside];
        button;
    });
    
    [self.view insertSubview:_mbScrollView atIndex:0];
    [_mbScrollView addSubview:_mbImageView];
    [self.view addSubview:_leftNavButton];
    
    CGSize imageSize = self.previweImage.size;
    CGFloat scale = kScreenWidth/imageSize.width;
    CGFloat height = imageSize.height *scale - kNavStatusHeight;
    _mbScrollView.contentSize = CGSizeMake(kScreenWidth, height);
    _mbImageView.frame = (CGRect){{0,0},{kScreenWidth,height}};
    _mbImageView.image = self.previweImage;
}

-(void)popNav {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
