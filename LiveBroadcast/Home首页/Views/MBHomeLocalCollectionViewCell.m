//
//  MBHomeLocalCollectionViewCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLocalCollectionViewCell.h"

@implementation MBHomeLocalCollectionViewCell
-(void)setModel:(MBHomeLocalModel *)model {
    _model = model;
    [self.topBjImageView sd_setImageWithURL:[NSURL URLWithString:_model.coverUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.userNameLabel.text = _model.nickname;
//    if ([_model.type isEqualToString:@"1"]) {
//        [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
//    }else {
//        [self.topCommonButton setImage:kImage(@"home_pic") forState:UIControlStateNormal];
//    }
    [self.localRangeButton setTitle:_model.clickPraise forState:UIControlStateNormal];
}

-(void)setIdleAllmodel:(MBIdleAllModel *)idleAllmodel {
    _idleAllmodel = idleAllmodel;
    [self.topBjImageView sd_setImageWithURL:[NSURL URLWithString:_idleAllmodel.coverUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_idleAllmodel.headUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.userNameLabel.text = _idleAllmodel.nickname;
    [self.localRangeButton setTitle:_model.clickPraise forState:UIControlStateNormal];

//    if ([_idleAllmodel.type isEqualToString:@"1"]) {
//        [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
//    }else {
//        [self.topCommonButton setImage:kImage(@"home_pic") forState:UIControlStateNormal];
//    }
    self.alphaLabel.text = _idleAllmodel.videoHeadline;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.firstLineHeadIndent = 8.0f;
    NSDictionary *attributeDic = @{NSParagraphStyleAttributeName : paragraphStyle};
    [self.alphaLabel attributedText:@[self.alphaLabel.text] attributeAttay:@[attributeDic]];
//    [self.localRangeButton setTitle:[NSString stringWithFormat:@"%.2f%@",_model.distance,@"km"] forState:UIControlStateNormal];
}
-(void)setIdleLocalmodel:(MBIdleLocalModel *)idleLocalmodel {
    _idleLocalmodel = idleLocalmodel;
    [self.topBjImageView sd_setImageWithURL:[NSURL URLWithString:_idleLocalmodel.coverUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_idleLocalmodel.headUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.userNameLabel.text = _idleLocalmodel.nickname;
//    if ([_idleLocalmodel.type isEqualToString:@"1"]) {
//        [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
//    }else {
//        [self.topCommonButton setImage:kImage(@"home_pic") forState:UIControlStateNormal];
//    }
    self.alphaLabel.text = _idleLocalmodel.videoHeadline;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.firstLineHeadIndent = 8.0f;
    NSDictionary *attributeDic = @{NSParagraphStyleAttributeName : paragraphStyle};
    [self.alphaLabel attributedText:@[self.alphaLabel.text] attributeAttay:@[attributeDic]];
    
//    [self.localRangeButton setTitle:[NSString stringWithFormat:@"%.2f%@",_idleLocalmodel.distance,@"km"] forState:UIControlStateNormal];
    [self.localRangeButton setTitle:_model.clickPraise forState:UIControlStateNormal];

}


- (void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}

-(void)createUI {
    self.topBjImageView.backgroundColor = Color_Bj;
    self.topBjImageView.image = kImage(@"wuzuopqit");
    [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
    self.alphaLabel.textColor = Color_White;
    self.alphaLabel.font = kFont(14);
    self.alphaLabel.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.3];
    
    
    self.userIcon.layer.cornerRadius = 15.0f;
    self.userIcon.layer.masksToBounds = YES;
    self.userNameLabel.font = kFont(12);
    self.userNameLabel.textColor = Color_333333;
    self.bottomContentView.backgroundColor = Color_White;
    
    self.localRangeButton.spacing = 3.0f;
    self.localRangeButton.postion = MBImagePositionLeft;
    [self.localRangeButton setTitleColor:Color_999999 forState:UIControlStateNormal];
    self.localRangeButton.titleLabel.font = kFont(12);
    self.localRangeButton.userInteractionEnabled = NO;
}

@end
