//
//  MBHomeWaterfallCollectionViewCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeWaterfallCollectionViewCell.h"

@interface MBHomeWaterfallCollectionViewCell()

@end
@implementation MBHomeWaterfallCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}

-(void)createUI {
    self.topBjImageView.backgroundColor = Color_Bj;
    self.topBjImageView.image = kImage(@"wuzuopqit");
    [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
    self.alphaLabel.textColor = Color_White;
    self.alphaLabel.font = kFont(14);
    self.alphaLabel.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.3];
    self.userIcon.layer.cornerRadius = 15.0f;
    self.userIcon.layer.masksToBounds = YES;
    self.userNameLabel.font = kFont(12);
    self.userNameLabel.textColor = Color_333333;
    self.timeLabel.font = kFont(12);
    self.timeLabel.textColor = Color_999999;
    self.bottomContentView.backgroundColor = Color_White;
}

@end
