//
//  MBHomeLocalHeadView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLocalHeadView.h"

@implementation MBHomeLocalHeadView

-(void)awakeFromNib {
    [super awakeFromNib];
    self.locationLabel.textColor = Color_666666;
    self.locationLabel.font = kFont(12);
}
-(void)setLocationString:(NSString *)locationString {
    _locationString = locationString;
    self.locationLabel.text = _locationString;
}

@end
