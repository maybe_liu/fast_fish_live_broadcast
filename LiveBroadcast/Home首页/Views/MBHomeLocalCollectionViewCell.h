//
//  MBHomeLocalCollectionViewCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBHomeLocalListModel.h"
#import "MBIdleAllListModel.h"
#import "MBIdleLocalListModel.h"



NS_ASSUME_NONNULL_BEGIN

@interface MBHomeLocalCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topBjImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomContentView;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet MBCustomButton *localRangeButton;

@property (weak, nonatomic) IBOutlet UIButton *topCommonButton;
@property (weak, nonatomic) IBOutlet UILabel *alphaLabel;
@property (nonatomic, strong)MBHomeLocalModel *model;
@property (nonatomic, strong)MBIdleAllModel *idleAllmodel;
@property (nonatomic, strong)MBIdleLocalModel *idleLocalmodel;


@end

NS_ASSUME_NONNULL_END
