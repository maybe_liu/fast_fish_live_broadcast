//
//  MBHomeLocalHeadViews.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBHomeLocalHeadViews : UICollectionReusableView
@property (nonatomic, copy)NSString *locationString;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end

NS_ASSUME_NONNULL_END
