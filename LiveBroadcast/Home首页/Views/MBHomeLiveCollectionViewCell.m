//
//  MBHomeLiveCollectionViewCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLiveCollectionViewCell.h"

@implementation MBHomeLiveCollectionViewCell

-(void)setModel:(MBHomeLiveModel *)model {
    _model = model;
    [self.topBjImageView sd_setImageWithURL:[NSURL URLWithString:_model.coverUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.userNameLabel.text = _model.nickname;
    [self.viewCountButton setTitle:_model.praiseCount forState:UIControlStateNormal];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}

-(void)createUI {
    self.topBjImageView.backgroundColor = Color_Bj;
    self.topBjImageView.image = kImage(@"wuzuopqit");
//    [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
    self.alphaLabel.textColor = Color_White;
    self.alphaLabel.font = kFont(14);
    self.alphaLabel.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.3];
    self.userIcon.layer.cornerRadius = 15.0f;
    self.userIcon.layer.masksToBounds = YES;
    self.userNameLabel.font = kFont(12);
    self.userNameLabel.textColor = Color_333333;
    self.bottomContentView.backgroundColor = Color_White;
    self.viewCountButton.spacing = 3.0f;
    self.viewCountButton.postion = MBImagePositionLeft;
    [self.viewCountButton setTitleColor:Color_999999 forState:UIControlStateNormal];
    self.viewCountButton.titleLabel.font = kFont(12);
    self.viewCountButton.userInteractionEnabled = NO;
}

@end
