//
//  MBHomeAttentionCollectionViewCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBHomeAttentionListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBHomeAttentionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topBjImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomContentView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *topCommonButton;
@property (weak, nonatomic) IBOutlet UILabel *alphaLabel;
@property (nonatomic, strong)MBHomeAttentionModel *model;
@property (weak, nonatomic) IBOutlet MBCustomButton *loveButton;

@end

NS_ASSUME_NONNULL_END
