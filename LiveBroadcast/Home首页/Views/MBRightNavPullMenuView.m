//
//  MBRightNavPullMenuView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBRightNavPullMenuView.h"
#import "MBShortVideoController.h"
#import "MBLiveMainController.h"


@implementation MBRightNavPullMenuView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}
-(void)createUI {
    self.shortVideoButton.postion = MBImagePositionLeft;
    self.shortVideoButton.spacing = 10;
    self.shortVideoButton.titleLabel.font = kFont(14);
    [self.shortVideoButton setTitleColor:Color_333333 forState:UIControlStateNormal];
    [self.shortVideoButton setBackgroundColor:Color_White];
    
    self.liveButton.postion = MBImagePositionLeft;
    self.liveButton.spacing = 10;
    self.liveButton.titleLabel.font = kFont(14);
    [self.liveButton setTitleColor:Color_333333 forState:UIControlStateNormal];
    [self.liveButton setBackgroundColor:Color_White];

    self.lineView.backgroundColor = Color_Line;
    self.customContentView.layer.cornerRadius = 4.0f;
    self.customContentView.layer.borderColor = Color_Line.CGColor;
    self.customContentView.layer.borderWidth = 0.5;
    self.customContentView.layer.masksToBounds = YES;
    self.topSpacing.constant = kNavBarHeight + 4;
    [self setNeedsLayout];
}
- (IBAction)shortVideoButton:(MBCustomButton *)sender {
    if (self.shortVideoBlock) {
        self.shortVideoBlock();
    }
    [self removeFromSuperview];
}
- (IBAction)liveButtonClick:(MBCustomButton *)sender {
    if (self.liveBlock) {
        self.liveBlock();
    }
    [self removeFromSuperview];
}

@end
