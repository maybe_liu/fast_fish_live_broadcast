//
//  MBHomeFindCollectionViewCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeFindCollectionViewCell.h"

@implementation MBHomeFindCollectionViewCell

-(void)setModel:(MBHomeFindModel *)model {
    _model = model;
    [self.topBjImageView sd_setImageWithURL:[NSURL URLWithString:_model.coverUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:[UIImage imageNamed:@""]];
    self.userNameLabel.text = _model.nickname;
    if ([_model.type isEqualToString:@"1"]) {
        [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
    }else {
        [self.topCommonButton setImage:kImage(@"home_pic") forState:UIControlStateNormal];
    }
    [self.loveButton setTitle:_model.clickPraise forState:UIControlStateNormal];
    if ([_model.clickType isEqualToString:@"1"]) {
        /*已点赞*/
        [self.loveButton setTitleColor:Color_FF5656 forState:UIControlStateNormal];
        [self.loveButton setImage:kImage(@"home_like") forState:UIControlStateNormal];
    }else if ([_model.clickType isEqualToString:@"2"]){
        /*未点赞*/
        [self.loveButton setTitleColor:Color_999999 forState:UIControlStateNormal];
        [self.loveButton setImage:kImage(@"home_like") forState:UIControlStateNormal];
//        [self.loveButton setImage:kImage(@"home_unlike") forState:UIControlStateNormal];

    }
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}

-(void)createUI {
    self.topBjImageView.backgroundColor = Color_Bj;
    self.topBjImageView.image = kImage(@"wuzuopqit");
    [self.topCommonButton setImage:kImage(@"duanship") forState:UIControlStateNormal];
    self.alphaLabel.textColor = Color_White;
    self.alphaLabel.font = kFont(14);
    self.alphaLabel.backgroundColor = [[UIColor colorWithHexString:@"#000000"] colorWithAlphaComponent:0.3];
    self.userIcon.layer.cornerRadius = 15.0f;
    self.userIcon.layer.masksToBounds = YES;
    self.userNameLabel.font = kFont(12);
    self.userNameLabel.textColor = Color_333333;
    self.bottomContentView.backgroundColor = Color_White;
    self.loveButton.spacing = 3.0f;
    self.loveButton.postion = MBImagePositionLeft;
    [self.loveButton setTitleColor:Color_999999 forState:UIControlStateNormal];
    self.loveButton.titleLabel.font = kFont(12);
}
- (IBAction)loveButtonClick:(MBCustomButton *)sender {
    [MBProgressHUD showSuccess:@"关注成功"];
}


@end
