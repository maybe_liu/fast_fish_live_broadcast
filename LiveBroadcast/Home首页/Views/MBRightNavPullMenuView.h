//
//  MBRightNavPullMenuView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShortVideoBlockHandler)(void);
typedef void(^LiveBlockHandler)(void);

@interface MBRightNavPullMenuView : UIView

@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpacing;
@property (weak, nonatomic) IBOutlet MBCustomButton *shortVideoButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *liveButton;
@property (copy, nonatomic) ShortVideoBlockHandler shortVideoBlock;
@property (copy, nonatomic) LiveBlockHandler       liveBlock;

@end

NS_ASSUME_NONNULL_END
