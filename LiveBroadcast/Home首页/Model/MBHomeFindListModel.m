//
//  MBHomeFindListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeFindListModel.h"
@implementation MBHomeFindModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBHomeFindListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"list":[MBHomeFindModel class],
             };
}
@end
