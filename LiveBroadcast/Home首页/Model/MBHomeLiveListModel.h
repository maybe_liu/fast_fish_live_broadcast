//
//  MBHomeLiveListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBHomeLiveModel : MBBaseModel
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * clickType;
@property (nonatomic , copy) NSString              * videoUrl;
@property (nonatomic , assign) CGFloat                high;
@property (nonatomic , copy) NSString              * clickPraise;
@property (nonatomic , copy) NSString              * issueTime;
@property (nonatomic , copy) NSString              * longitude;
@property (nonatomic , copy) NSString              * latitude;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * buyHeadline;
@property (nonatomic , copy) NSString              * type2;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , assign) CGFloat                wide;
@property (nonatomic , copy) NSString              * coverUrl;
@property (nonatomic , copy) NSString              * isCheat;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * pullUrl; /*播放地址*/
@property (nonatomic , copy) NSString              * viewCount; /*观看人数*/
@property (nonatomic , copy) NSString              * streamId; /*房间号*/
@property (nonatomic , copy) NSString              * roomId; /*房间号*/
@property (nonatomic , copy) NSString              * praiseCount; /*点赞数*/


@end

@interface MBHomeLiveListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBHomeLiveModel *>              * liveInfoList;
@end


NS_ASSUME_NONNULL_END
