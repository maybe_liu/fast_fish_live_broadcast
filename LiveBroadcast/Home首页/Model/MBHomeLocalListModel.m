//
//  MBHomeLocalListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLocalListModel.h"
@implementation MBHomeLocalModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBHomeLocalListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"localList":[MBHomeLocalModel class],
             };
}
@end
