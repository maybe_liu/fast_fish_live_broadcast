//
//  MBHomeAttentionListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/22.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeAttentionListModel.h"

@implementation MBHomeAttentionModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBHomeAttentionListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"userVideos":[MBHomeAttentionModel class],
             };
}
@end
