//
//  MBLiveMainRemoveAudienceController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainRemoveAudienceController.h"
#import "MBLiveMainRemoveAudienceCell.h"
#import "MBLiveMainRemoveAudienceListModel.h"

@interface MBLiveMainRemoveAudienceController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@property (nonatomic, strong)MBLiveMainRemoveAudienceListModel *listModel;

@end
static NSString *const MBLiveMainRemoveAudienceCellID = @"MBLiveMainRemoveAudienceCellID";

@implementation MBLiveMainRemoveAudienceController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    [self loadData];
}
/*获取*/
-(void)loadData {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_liveRemoveAudienceListApi setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        weakself.listModel = [MBLiveMainRemoveAudienceListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:weakself.listModel.list];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView ly_endLoading];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
-(void)createCustomNav{
    self.navigationItem.title = @"踢人记录";
    self.view.backgroundColor = Color_White;
}
-(void)createUI {
    self.mainTableView.backgroundColor = self.view.backgroundColor;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 80.0f;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainRemoveAudienceCell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainRemoveAudienceCellID];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.mainTableView.ly_emptyView = emptyView;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainRemoveAudienceCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainRemoveAudienceCellID];
    if (cell == nil) {
        cell = [[MBLiveMainRemoveAudienceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainRemoveAudienceCellID];
    }
    MBLiveMainRemoveAudienceModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


@end
