//
//  MBLiveMainBlackListController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainBlackListController.h"
#import "MBLiveMainBlackListCell.h"
#import "MBLiveMainBlackAudienceListModel.h"

@interface MBLiveMainBlackListController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;

@end

static NSString *const MBLiveMainBlackListCellID = @"MBLiveMainBlackListCell.h";
@implementation MBLiveMainBlackListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    [self loadData];
}
/*获取*/
-(void)loadData {
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_BlacklistList setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBLiveMainBlackAudienceListModel *model = [MBLiveMainBlackAudienceListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.blacklistList];
        [weakself.mainTableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

-(void)cancelBlack:(MBLiveMainBlackAudienceModel *)model {
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"blacklistId":model.ID
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_blacklistRemove setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showMessage:@"移除黑名单成功"];
        [weakself loadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}


-(void)createCustomNav{
    self.navigationItem.title = @"拉黑记录";
    self.view.backgroundColor = Color_White;
}
-(void)createUI {
    self.mainTableView.backgroundColor = self.view.backgroundColor;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 80.0f;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainBlackListCell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainBlackListCellID];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainBlackListCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainBlackListCellID];
    if (cell == nil) {
        cell = [[MBLiveMainBlackListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainBlackListCellID];
    }
    cell.model = self.dataArray[indexPath.row];
    MB_WeakSelf(self);
    cell.cancelBlock = ^(MBLiveMainBlackAudienceModel * _Nonnull model) {
        [weakself cancelBlack:model];
    };
    return cell;
}
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


@end
