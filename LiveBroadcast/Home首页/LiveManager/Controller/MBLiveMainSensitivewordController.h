//
//  MBLiveMainSensitivewordController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 敏感词设置
 */
@interface MBLiveMainSensitivewordController : MBBaseController
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@property (weak, nonatomic) IBOutlet MBBaseTableview *mainTableView;

@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *sensitiveContentView;
@property (weak, nonatomic) IBOutlet MBTextField *wordField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (nonatomic, copy)NSString *liveID;


@end

NS_ASSUME_NONNULL_END
