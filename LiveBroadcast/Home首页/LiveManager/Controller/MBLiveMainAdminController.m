//
//  MBLiveMainAdminController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainAdminController.h"
#import "MBLiveMainAddAdminController.h"
#import "MBLiveMainAdminCell.h"


@interface MBLiveMainAdminController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, strong)UIButton *rightNavButton;
@property (nonatomic, strong)NSMutableArray *dataArray;

@end

static NSString *const MBLiveMainAdminCellID = @"MBLiveMainAdminCellID";
@implementation MBLiveMainAdminController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
}
-(void)createCustomNav{
    self.navigationItem.title = @"管理员设置";
}


-(void)createUI {
    self.topTitleLabel.textColor = Color_666666;
    self.topTitleLabel.font = kFont(12);
    self.mainTableView.backgroundColor = self.view.backgroundColor;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 80.0f;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainAdminCell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainAdminCellID];
    self.mainTableView.showsVerticalScrollIndicator = NO;
}

-(void)rightNavButtonClick{
    MBLiveMainAddAdminController *vc = [[MBLiveMainAddAdminController alloc]init];
    vc.liveID = self.liveID;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainAdminCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainAdminCellID];
    if (cell == nil) {
        cell = [[MBLiveMainAdminCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainAdminCellID];
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


#pragma mark - getter and setter
-(UIButton *)rightNavButton {
    if (_rightNavButton == nil) {
        _rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightNavButton setTitle:@"添加管理员" forState:UIControlStateNormal];
        [_rightNavButton setTitleColor:Color_FF8000 forState:UIControlStateNormal];
        _rightNavButton.titleLabel.font = kFont(16);
        [_rightNavButton addTarget:self action:@selector(rightNavButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightNavButton;
}

-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
