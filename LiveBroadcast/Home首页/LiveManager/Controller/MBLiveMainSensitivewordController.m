//
//  MBLiveMainSensitivewordController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainSensitivewordController.h"
#import "MBLiveMainSensitivewordCell.h"

@interface MBLiveMainSensitivewordController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

static NSString *const  MBLiveMainSensitivewordCellID = @"MBLiveMainSensitivewordCellID";
@implementation MBLiveMainSensitivewordController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
}

-(void)addSensitive {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"sensitiveWords":self.wordField.text,
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_liveAddSensitiveWord setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        if (![weakself.dataArray containsObject:self.wordField.text]) {
            [weakself.dataArray addObject:self.wordField.text];
            [weakself.mainTableView reloadData];
        }
        
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

-(void)deleteSensitive:(NSString *)deleteString {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"sensitiveWords":deleteString,
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_liveDeleteSensitiveWord setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        if ([weakself.dataArray containsObject:deleteString]) {
            [weakself.dataArray removeObject:deleteString];
            [weakself.mainTableView reloadData];
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
}


-(void)createUI {
    self.topContentView.backgroundColor = [UIColor clearColor];
    self.sensitiveContentView.backgroundColor = Color_White;
    self.topTitleLabel.textColor = Color_666666;
    self.topTitleLabel.font = kFont(14);
    self.wordField.maxLength = 6;
    self.wordField.mTintColor = Color_FF8000;
    self.wordField.limitedType = MBGeneralTextFieldTypeNomal;
    self.wordField.placeholderFont = kFont(14);
    self.wordField.placeholderColor = Color_AAAAAA;
    self.wordField.font = kFont(16);
    self.wordField.textColor = Color_333333;
    [self.addButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.addButton.titleLabel.font = kFont(14);
    [self.addButton setBackgroundColor:Color_FF8000];
    self.addButton.layer.cornerRadius = 16.0f;
    self.addButton.layer.masksToBounds = YES;
    self.mainTableView.backgroundColor = self.view.backgroundColor;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 50.0f;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [self.mainTableView registerClass:[MBLiveMainSensitivewordCell class] forCellReuseIdentifier:MBLiveMainSensitivewordCellID];
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainSensitivewordCell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainSensitivewordCellID];
    self.mainTableView.showsVerticalScrollIndicator = NO;

}
-(void)createCustomNav{
    self.navigationItem.title = @"敏感词设置";
}

#pragma mark - event

- (IBAction)addButtonClick:(UIButton *)sender {
    if ([self.wordField.text isNotBlank]) {
        [self addSensitive];
    }else {
        [MBProgressHUD showMessage:@"请输入敏感词"];
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainSensitivewordCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainSensitivewordCellID];
    if (cell == nil) {
        cell = [[MBLiveMainSensitivewordCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainSensitivewordCellID];
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.wordString = self.dataArray[indexPath.row];
    MB_WeakSelf(self);
    cell.deleteBlock = ^(NSString * _Nonnull wordString) {
        [weakself deleteSensitive:wordString];
    };
    return cell;
}
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
