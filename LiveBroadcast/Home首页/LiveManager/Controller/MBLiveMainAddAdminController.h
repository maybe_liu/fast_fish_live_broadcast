//
//  MBLiveMainAddAdminController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveMainAddAdminController : MBBaseController
@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/
@property (weak, nonatomic) IBOutlet UIView *topSearchView;
@property (weak, nonatomic) IBOutlet MBTextField *searchField;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@end

NS_ASSUME_NONNULL_END
