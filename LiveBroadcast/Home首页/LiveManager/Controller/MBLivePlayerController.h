//
//  MBLivePlayerController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/14.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/*观众端 拉流*/
@interface MBLivePlayerController : MBBaseController
@property (nonatomic, copy)NSString *rtmpString; /*拉流地址*/
@property (nonatomic, assign) BOOL isLivePlay;
@property (nonatomic, assign) BOOL isRealtime;
@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/
@property (nonatomic, copy)    NSString *         roomID; /*群聊ID*/
@property (nonatomic, copy)    NSString *         streamID; /*群聊ID*/

@end

NS_ASSUME_NONNULL_END
