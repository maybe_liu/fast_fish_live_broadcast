//
//  MBLiveRemoveAndBlackController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MBLiveRemoveAndBlackDelegate <NSObject>

- (void)mbLiveRemoveAndBlackRemoveAudience:(NSString*)audienceID;
- (void)mbLiveRemoveAndBlackBlackAudience:(NSString*)audienceID;

@end

@interface MBLiveRemoveAndBlackController : MBBaseController

@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/
@property (weak, nonatomic) IBOutlet MBBaseTableview *mainTableView;

@property (nonatomic, weak) id <MBLiveRemoveAndBlackDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
