//
//  MBStartLiveController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN


/**
 发起直播
 */
@interface MBStartLiveController : MBBaseController
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet MBTextField *topTitleField;
@property (weak, nonatomic) IBOutlet UIView *topLine;
@property (weak, nonatomic) IBOutlet UILabel *liveCoverLabel;
@property (weak, nonatomic) IBOutlet UIButton *selecPictureButton;
@property (weak, nonatomic) IBOutlet UIView *bottomContentView;
@property (weak, nonatomic) IBOutlet UIButton *startLiveButton;

@end

NS_ASSUME_NONNULL_END
