//
//  MBSystemFriendController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSystemFriendController.h"
#import "MBSystemFriendCell.h"
#import "MBSystemFriendListModel.h"
#import "MBLiveMainController.h"
@interface MBSystemFriendController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@property (nonatomic, assign)NSInteger pageNo;
@property (nonatomic, strong)UIButton *rightNavButton;
@property (nonatomic, strong)NSArray *selectArray;

@end
static NSString *const MBSystemFriendCellID = @"MBSystemFriendCellID";
@implementation MBSystemFriendController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    [self loadSystemFriendList];
    [self createRightNavButton];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
-(void)createCustomNav{
    self.pageNo = 1;
    self.navigationItem.title = @"选择好友";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightNavButton];
}
/*获取好友列表*/
-(void)loadSystemFriendList {
    NSDictionary *param = @{
                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                            @"pageNo":@(self.pageNo)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_getFriendsList setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBSystemFriendListModel *model = [MBSystemFriendListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.friendsList];
        [weakself.mainTableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
-(void)rightNavButtonClick{
    NSMutableArray *tmpArray = [NSMutableArray array];
    for (NSIndexPath *indexPath in self.selectArray) {
        MBSystemFriendModel *model = self.dataArray[indexPath.row];
        NSString *idString = model.ID;
        [tmpArray addObject:idString];
    }
    NSString *resultString = [tmpArray componentsJoinedByString:@","];
    if (self.selectIDsBlock) {
        self.selectIDsBlock(resultString);
    }
    
    if (self.fromType == MBSystemFriendVCFromTypeLive) {
        if ([self.rightNavButton.currentTitle isEqualToString:@"完成"]) {
            [self.rightNavButton setTitle:@"分享" forState:UIControlStateNormal];
        }else {
            [MBProgressHUD showSuccess:@"分享好友成功"];
            for (UIViewController *vc in self.navigationController.viewControllers) {
                if ([vc isKindOfClass:[MBLiveMainController class]]) {
                    [self.navigationController popToViewController:vc animated:YES];
                }
            }
        }
    }else if(self.fromType == MBSystemFriendVCFromTypeShortVideo){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)createUI {
    self.topSearchView.backgroundColor = Color_Bj;
    self.topSearchView.layer.cornerRadius = 2.0f;
    self.topSearchView.layer.masksToBounds = YES;
    self.searchField.mTintColor = Color_FF8000;
    self.searchField.placeholderFont = kFont(14);
    self.searchField.placeholderColor = Color_666666;
    self.searchField.textColor = Color_333333;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 80.0f;
    self.mainTableView.allowsMultipleSelection = YES;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBSystemFriendCell class]) bundle:nil] forCellReuseIdentifier:MBSystemFriendCellID];
    self.mainTableView.showsVerticalScrollIndicator = NO;
}

-(void)createRightNavButton {
    if (self.fromType == MBSystemFriendVCFromTypeLive) {
        [self.rightNavButton setTitle:@"完成" forState:UIControlStateNormal];
    }else if (self.fromType == MBSystemFriendVCFromTypeShortVideo){
        [self.rightNavButton setTitle:@"确定" forState:UIControlStateNormal];
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
//    return 10;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBSystemFriendCell *cell = [tableView dequeueReusableCellWithIdentifier:MBSystemFriendCellID];
    if (cell == nil) {
        cell = [[MBSystemFriendCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBSystemFriendCellID];
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MBSystemFriendModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 获取单选选中项
    NSIndexPath *indexPath1 = tableView.indexPathForSelectedRow;
    // 获取全选选中项
//    NSArray *indexPaths2 = tableView.indexPathsForSelectedRows;
    self.selectArray = tableView.indexPathsForSelectedRows;
    MBLog(@"---%@",self.selectArray);
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectArray = tableView.indexPathsForSelectedRows;
    MBLog(@"-++++%@",self.selectArray);
}

#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
-(UIButton *)rightNavButton {
    if (_rightNavButton == nil) {
        _rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightNavButton setTitle:@"完成" forState:UIControlStateNormal];
        [_rightNavButton setTitleColor:Color_FF8000 forState:UIControlStateNormal];
        _rightNavButton.titleLabel.font = kFont(16);
        [_rightNavButton addTarget:self action:@selector(rightNavButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightNavButton;
}
@end
