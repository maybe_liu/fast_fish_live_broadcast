//
//  MBLiveRemoveAndBlackController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveRemoveAndBlackController.h"
#import "MBLiveRemoveAndBlackTableViewCell.h"
#import "MBLiveViewerModel.h"

@interface MBLiveRemoveAndBlackController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;
@property (nonatomic, strong)NSArray *liveViewerDataArray;

@end

 static NSString *const MBLiveRemoveAndBlackTableViewCellID = @"MBLiveRemoveAndBlackTableViewCellID";
@implementation MBLiveRemoveAndBlackController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    [self getLiveViewer];
}
-(void)createCustomNav{
    self.navigationItem.title = @"当前观众";
}
/*获取观众*/
-(void)getLiveViewer {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_getLiveViewer setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        weakself.liveViewerDataArray = [MBLiveViewerModel mj_objectArrayWithKeyValuesArray:AJson];
        [weakself.mainTableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*踢人*/
-(void)removeAudience:(NSString *)audienceIDString {
    NSDictionary *param = @{
                                   @"liveId":self.liveID,
                                   @"userId":audienceIDString
                                   };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_liveRemoveAudience setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"已成功踢出"];
        [weakself getLiveViewer];
        if ([self.delegate respondsToSelector:@selector(mbLiveRemoveAndBlackRemoveAudience:)]) {
            [self.delegate mbLiveRemoveAndBlackRemoveAudience:audienceIDString];
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
/*拉黑*/
-(void)blackAudience:(NSString *)audienceIDString {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                            @"blackUserId":audienceIDString
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_black setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [MBProgressHUD showSuccess:@"已成功拉黑"];
        [weakself getLiveViewer];
        if ([self.delegate respondsToSelector:@selector(mbLiveRemoveAndBlackBlackAudience:)]) {
            [self.delegate mbLiveRemoveAndBlackBlackAudience:audienceIDString];
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
}


-(void)createUI {
    self.mainTableView.backgroundColor = self.view.backgroundColor;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 90.0f;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveRemoveAndBlackTableViewCell class]) bundle:nil] forCellReuseIdentifier:MBLiveRemoveAndBlackTableViewCellID];
//    self.mainTableView.showsVerticalScrollIndicator = NO;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.liveViewerDataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveRemoveAndBlackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveRemoveAndBlackTableViewCellID];
    if (cell == nil) {
        cell = [[MBLiveRemoveAndBlackTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveRemoveAndBlackTableViewCellID];
    }
    cell.model = self.liveViewerDataArray[indexPath.row];
    MB_WeakSelf(self);
    cell.removeBlock = ^(MBLiveViewerModel * _Nonnull model) {
        [weakself removeAudience:model.ID];
    };
    cell.blackBlock = ^(MBLiveViewerModel * _Nonnull model) {
        [weakself blackAudience:model.ID];
    };
    return cell;
}


#pragma mark - getter and setter

-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
-(NSArray *)liveViewerDataArray {
    if (_liveViewerDataArray == nil) {
        _liveViewerDataArray = [NSMutableArray array];
    }
    return _liveViewerDataArray;
}

@end
