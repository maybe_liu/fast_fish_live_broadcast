//
//  MBLiveMainBlackListController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

/**
 主播 拉黑记录
 */
@interface MBLiveMainBlackListController : MBBaseController
@property (weak, nonatomic) IBOutlet MBBaseTableview *mainTableView;
@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/

@end

NS_ASSUME_NONNULL_END
