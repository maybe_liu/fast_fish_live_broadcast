//
//  MBLiveMainAddAdminController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainAddAdminController.h"
#import "MBLiveMainAddAdminCell.h"

@interface MBLiveMainAddAdminController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSMutableArray *dataArray;

@end
static NSString *const MBLiveMainAddAdminCellID = @"MBLiveMainAddAdminCellID";

@implementation MBLiveMainAddAdminController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
}
-(void)createCustomNav{
    self.navigationItem.title = @"添加管理员";
}
/*获取观众*/
-(void)getLiveViewer {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_getLiveViewer setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
//        weakself.liveViewerDataArray = [MBLiveViewerModel mj_objectArrayWithKeyValuesArray:AJson];
//        weakself.topCommonView.audienceDataArray = weakself.liveViewerDataArray;
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

-(void)createUI {
    self.topSearchView.backgroundColor = Color_Bj;
    self.topSearchView.layer.cornerRadius = 2.0f;
    self.topSearchView.layer.masksToBounds = YES;
    self.searchField.mTintColor = Color_FF8000;
    self.searchField.placeholderFont = kFont(14);
    self.searchField.placeholderColor = Color_666666;
    self.searchField.textColor = Color_333333;
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainAddAdminCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainAddAdminCellID];
    if (cell == nil) {
        cell = [[MBLiveMainAddAdminCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainAddAdminCellID];
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}


#pragma mark - getter and setter

-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


@end
