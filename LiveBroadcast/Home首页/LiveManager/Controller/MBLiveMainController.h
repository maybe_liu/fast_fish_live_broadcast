//
//  MBLiveMainController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
//#import "LiveRoom.h"

NS_ASSUME_NONNULL_BEGIN

/**
 主播端 推流主控制器
 */
@interface MBLiveMainController : MBBaseController

@property (nonatomic, copy)NSString *rtmpString; /*推流地址*/
@property (nonatomic, assign)BOOL isPreviewing; /*是否预览*/
//@property (nonatomic, weak)    LiveRoom*          liveRoom;
@property (nonatomic, copy)    NSString*          roomName;
@property (nonatomic, copy)    NSString*          userID;
@property (nonatomic, copy)    NSString*          userName;
@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/
@property (nonatomic, copy)    NSString *         streamID; /*主播ID*/

@end

NS_ASSUME_NONNULL_END
