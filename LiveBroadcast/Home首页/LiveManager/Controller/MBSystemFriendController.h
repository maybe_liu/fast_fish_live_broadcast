//
//  MBSystemFriendController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN
/*用户角色*/
typedef NS_ENUM(NSInteger,MBSystemFriendVCFromType){
    MBSystemFriendVCFromTypeLive                      = 0, /*直播选择好友*/
    MBSystemFriendVCFromTypeShortVideo                = 1 /* 短视频分享好友*/
};

typedef void(^SystemFriendSelectBlockHandler)(NSString *selectIDs);

@interface MBSystemFriendController : MBBaseController
@property (nonatomic, copy)    NSString *         liveID; /*主播ID*/
@property (weak, nonatomic) IBOutlet UIView *topSearchView;
@property (weak, nonatomic) IBOutlet MBTextField *searchField;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic, assign)MBSystemFriendVCFromType fromType;
@property (nonatomic, copy) SystemFriendSelectBlockHandler selectIDsBlock;

@end

NS_ASSUME_NONNULL_END
