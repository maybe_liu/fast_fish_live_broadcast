//
//  MBSystemFriendListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSystemFriendListModel.h"
@implementation MBSystemFriendModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}

@end
@implementation MBSystemFriendListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"friendsList":[MBSystemFriendModel class],
             };
}
@end
