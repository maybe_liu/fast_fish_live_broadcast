//
//  MBLiveGiftListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBLiveGiftModel :NSObject
@property (nonatomic , copy) NSString              * giftPrice;
@property (nonatomic , copy) NSString              * giftUrl;
@property (nonatomic , copy) NSString              * giftType;
@property (nonatomic , copy) NSString              * giftInfo;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * shelfState;
@property (nonatomic , copy) NSString              * rows;
@property (nonatomic , copy) NSString              * page;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * giftname;
@property (nonatomic , assign) CGFloat              time;

@end

@interface MBLiveGiftListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBLiveGiftModel *>              * gifts;

@end

NS_ASSUME_NONNULL_END
