//
//  MBLiveMainBlackAudienceListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainBlackAudienceListModel.h"
@implementation MBLiveMainBlackAudienceModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end

@implementation MBLiveMainBlackAudienceListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"blacklistList":[MBLiveMainBlackAudienceModel class],
             };
}
@end
