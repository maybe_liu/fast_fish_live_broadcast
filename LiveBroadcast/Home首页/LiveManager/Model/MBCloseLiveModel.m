//
//  MBCloseLiveModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/6.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBCloseLiveModel.h"

@implementation CloseLiveUserInfo

@end
@implementation MBCloseLiveModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"userInfo":[CloseLiveUserInfo class],
             };
}

@end
