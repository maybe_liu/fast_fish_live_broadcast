//
//  MBOpenLiveModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBOpenLiveModel : MBBaseModel
@property (nonatomic , copy) NSString              * roomId;
@property (nonatomic , copy) NSString              * liveId;
@property (nonatomic , copy) NSString              * pushUrl;
@property (nonatomic , copy) NSString              * privateMapKey;
@property (nonatomic , copy) NSString              * streamId;
@end

NS_ASSUME_NONNULL_END
