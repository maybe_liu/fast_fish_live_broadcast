//
//  MBLiveMianGetGiftListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/18.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface GiftRecordList : MBBaseModel
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * giftCount;
@property (nonatomic , copy) NSString              * giftInfo;
@property (nonatomic , copy) NSString              * giftname;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;


@end

@interface MBLiveMianGetGiftListModel : MBBaseModel
@property (nonatomic , copy) NSArray<GiftRecordList *>     * giftRecordList;

@end

NS_ASSUME_NONNULL_END
