//
//  MBShowLiveGiftModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/6.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBShowLiveGiftModel : NSObject

@property(nonatomic, strong) NSString *type;/**< 礼物类型 */
@property(nonatomic, strong) NSString *name;/**< 礼物的名称*/
@property(nonatomic, strong) NSString *picUrl;/**< 右侧礼物图片url */
@property(nonatomic, strong) NSString *rewardMsg;/**< 礼物配置的语句 */

@end

NS_ASSUME_NONNULL_END
