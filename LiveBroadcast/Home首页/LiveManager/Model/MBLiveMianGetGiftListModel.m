//
//  MBLiveMianGetGiftListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/18.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMianGetGiftListModel.h"
@implementation GiftRecordList

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBLiveMianGetGiftListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"giftRecordList":[GiftRecordList class],
             };
}

@end
