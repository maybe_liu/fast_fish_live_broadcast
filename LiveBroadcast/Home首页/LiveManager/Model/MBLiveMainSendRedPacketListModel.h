//
//  MBLiveMainSendRedPacketListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface LiveRedpacketList : MBBaseModel
@property (nonatomic , copy) NSString              * redMoney;
@property (nonatomic , copy) NSString              * createTime;
@property (nonatomic , copy) NSString              * batchNum;
@property (nonatomic , copy) NSString              * state;
@property (nonatomic , copy) NSString              * redId;
@end
@interface MBLiveMainSendRedPacketListModel : MBBaseModel
@property (nonatomic , copy) NSArray<LiveRedpacketList *>              * redpacketList;

@end

NS_ASSUME_NONNULL_END
