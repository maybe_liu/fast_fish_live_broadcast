//
//  MBLiveGiftListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveGiftListModel.h"
@implementation MBLiveGiftModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBLiveGiftListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"gifts":[MBLiveGiftModel class],
             };
}
@end
