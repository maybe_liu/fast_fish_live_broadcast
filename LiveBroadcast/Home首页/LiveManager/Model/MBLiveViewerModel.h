//
//  MBLiveViewerModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface MBLiveViewerModel : MBBaseModel
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * total;
@property (nonatomic , copy) NSString              * userId;

@end

NS_ASSUME_NONNULL_END
