//
//  MBLiveInfoModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/29.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveInfoModel.h"
@implementation UserInfoModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}

@end
@implementation liveViewerModel

@end
@implementation liveInfoModel

@end
@implementation liveCommentsModel

@end
@implementation MBLiveInfoModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"liveComments":[liveCommentsModel class],
             @"liveViewer":[liveViewerModel class],
             };
}
@end
