//
//  MBLiveRedpacketInfoModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveRedpacketInfoModel : MBBaseModel
@property (nonatomic , copy) NSString              * ifRedPacket;
@property (nonatomic , copy) NSString              * batchNum;

@end

NS_ASSUME_NONNULL_END
