//
//  MBLiveMainRemoveAudienceListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainRemoveAudienceListModel.h"
@implementation MBLiveMainRemoveAudienceModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBLiveMainRemoveAudienceListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"list":[MBLiveMainRemoveAudienceModel class],
             };
}
@end
