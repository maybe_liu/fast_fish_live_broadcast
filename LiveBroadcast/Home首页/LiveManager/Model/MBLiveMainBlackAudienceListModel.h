//
//  MBLiveMainBlackAudienceListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBLiveMainBlackAudienceModel : MBBaseModel
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;

@end

@interface MBLiveMainBlackAudienceListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBLiveMainBlackAudienceModel *>              * blacklistList;

@end

NS_ASSUME_NONNULL_END
