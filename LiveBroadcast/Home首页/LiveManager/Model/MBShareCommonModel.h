//
//  MBShareCommonModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBShareCommonModel : MBBaseModel
@property (nonatomic , copy) NSString              * url;

@end

NS_ASSUME_NONNULL_END
