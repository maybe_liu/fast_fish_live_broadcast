//
//  MBCloseLiveModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/6.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CloseLiveUserInfo :NSObject
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * userId;

@end

@interface MBCloseLiveModel : MBBaseModel
@property (nonatomic , copy) NSString              * viewerCount;
@property (nonatomic , copy) NSString              * praiseCount;
@property (nonatomic , strong) CloseLiveUserInfo              * userInfo;
@property (nonatomic , assign) NSInteger               liveTime;

@end

NS_ASSUME_NONNULL_END
