//
//  MBLiveMainSendRedPacketListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainSendRedPacketListModel.h"
@implementation LiveRedpacketList

@end
@implementation MBLiveMainSendRedPacketListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"redpacketList":[LiveRedpacketList class],
             };
}

@end
