//
//  MBSystemFriendListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBSystemFriendModel : MBBaseModel
@property (nonatomic , copy) NSString              * fansCount;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * worksCount;

@end
@interface MBSystemFriendListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBSystemFriendModel *>              * friendsList;

@end

NS_ASSUME_NONNULL_END
