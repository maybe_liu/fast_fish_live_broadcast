//
//  MBLiveInfoModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/29.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface UserInfoModel : MBBaseModel
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * ID;

@end

@interface liveViewerModel : MBBaseModel
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * userId;

@end
@interface liveInfoModel : MBBaseModel
@property (nonatomic , copy) NSString              * comments;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * userId;

@end
@interface liveCommentsModel : MBBaseModel
@property (nonatomic , copy) NSString              * comments;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * userId;

@end
@interface MBLiveInfoModel : MBBaseModel
@property(nonatomic, copy) NSString * clickPraise; //直播点赞数
@property (nonatomic , copy) NSArray<liveCommentsModel *>              * liveComments;
@property (nonatomic, strong) liveInfoModel                    *liveInfo;
@property (nonatomic , copy) NSArray<liveViewerModel *>              * liveViewer;
@property (nonatomic, strong) UserInfoModel                    *userInfo;


@end

NS_ASSUME_NONNULL_END
