//
//  MBQiNiuUpTokenModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBQiNiuUpTokenModel : MBBaseModel
@property (nonatomic , copy) NSString              *upToken;

@end

NS_ASSUME_NONNULL_END
