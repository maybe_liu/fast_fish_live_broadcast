//
//  MBLiveMainRemoveAudienceListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBLiveMainRemoveAudienceModel : MBBaseModel
@property (nonatomic , assign) NSInteger             createTime;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * nickname;
@end
@interface MBLiveMainRemoveAudienceListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBLiveMainRemoveAudienceModel *>              * list;

@end

NS_ASSUME_NONNULL_END
