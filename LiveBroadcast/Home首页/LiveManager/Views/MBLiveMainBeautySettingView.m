//
//  MBLiveMainBeautySettingView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainBeautySettingView.h"


@implementation MBLiveMainBeautySettingView{
    CGFloat _skinValue; //美白值
    CGFloat _beautyValue;//美颜值
}

-(void)awakeFromNib {
    [super awakeFromNib];
    self.topTitleLabel.textColor = Color_FF8000;
    self.topTitleLabel.font = kMFont(16);
    [self.tureButton setTitleColor:Color_FF8000 forState:UIControlStateNormal];
    self.tureButton.titleLabel.font = kFont(14);
    self.skinLabel.textColor = Color_333333;
    self.skinLabel.font = kFont(13);
    self.beautyLabel.textColor = Color_333333;
    self.beautyLabel.font = kFont(13);
    self.skinSlider.minimumTrackTintColor = Color_FF8000;
    self.skinSlider.thumbTintColor = Color_FF8000;
    self.skinSlider.maximumValue = 9;
    self.skinSlider.minimumValue = 0;
    
    self.beautySlider.minimumTrackTintColor = Color_FF8000;
    self.beautySlider.thumbTintColor = Color_FF8000;
    self.beautySlider.maximumValue = 9;
    self.beautySlider.minimumValue = 0;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:LocalSkinValueKey]) {
        NSNumber *value = [[NSUserDefaults standardUserDefaults] objectForKey:LocalSkinValueKey];
        [self.skinSlider setValue: [value floatValue] animated:NO];
        _skinValue = value.floatValue;
    }else {
        _skinValue = 5.0;
        [[NSUserDefaults standardUserDefaults] setObject:@(5.0) forKey:LocalSkinValueKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:LocalBeautyValueKey]) {
        NSNumber *value = [[NSUserDefaults standardUserDefaults] objectForKey:LocalBeautyValueKey];
        [self.beautySlider setValue: [value floatValue] animated:NO];
        _beautyValue = value.floatValue;
    }else {
        _beautyValue = 5.0;
        [[NSUserDefaults standardUserDefaults] setObject:@(5.0) forKey:LocalBeautyValueKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (IBAction)skinChange:(UISlider *)sender {
    float value = sender.value;
    if (sender.tag == 10000) { /*美白*/
        _skinValue = value;
    }else { /*美颜*/
        _beautyValue = value;
    }
}
- (IBAction)tureButtonClick:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(_skinValue) forKey:LocalSkinValueKey];
    [[NSUserDefaults standardUserDefaults] setObject:@(_beautyValue) forKey:LocalBeautyValueKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (self.beautySetBlock) {
        self.beautySetBlock(_skinValue, _beautyValue);
    }
}

@end
