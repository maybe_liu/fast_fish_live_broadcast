//
//  MBLiveMainSensitivewordCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainSensitivewordCell.h"

@implementation MBLiveMainSensitivewordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];

    }
    return self;
}
-(void)initUI {
    self.wordLabel.textColor = Color_333333;
    self.wordLabel.font = kFont(14);
}

-(void)setWordString:(NSString *)wordString {
    _wordString = wordString;
    self.wordLabel.text = _wordString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)removeButtonClick:(UIButton *)sender {
    if (self.deleteBlock) {
        self.deleteBlock(self.wordString);
    }
}

@end
