//
//  MBSystemFriendCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBSystemFriendListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBSystemFriendCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *namaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectImageView;
@property (nonatomic, strong)MBSystemFriendModel *model;
@end

NS_ASSUME_NONNULL_END
