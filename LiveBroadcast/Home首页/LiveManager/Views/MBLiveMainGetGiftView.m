//
//  MBLiveMainGetGiftView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainGetGiftView.h"
#import "MBLiveMainGetGiftCell.h"
#import "MBLiveMianGetGiftListModel.h"


static NSString *const MBLiveMainGetGiftCellID = @"MBLiveMainGetGiftCellID";
@interface MBLiveMainGetGiftView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)MBBaseTableview *mainTableView;
@property (nonatomic, strong)UIView *tableHeadView;
@property (nonatomic, strong)UIButton *cancelButton;
@property (nonatomic, assign)NSInteger pageNo;
@property (nonatomic, strong)NSMutableArray *dataArray;

@end

@implementation MBLiveMainGetGiftView

/**
 获取礼物列表
 */
-(void)loadMoreData {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"pageNo":@(self.pageNo)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_getListGiftRecord setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBLiveMianGetGiftListModel *model = [MBLiveMianGetGiftListModel mj_objectWithKeyValues:AJson];
        [weakself.dataArray addObjectsFromArray:model.giftRecordList];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView.mj_footer endRefreshing];
    } failure:^(id  _Nonnull AJson) {
        [weakself.mainTableView.mj_footer endRefreshing];
    }];
}

-(void)refreshData {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"pageNo":@(self.pageNo)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_getListGiftRecord setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        [weakself.dataArray  removeAllObjects];
        MBLiveMianGetGiftListModel *model = [MBLiveMianGetGiftListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.giftRecordList];
        [weakself.mainTableView reloadData];
        [weakself.mainTableView.mj_header endRefreshing];
    } failure:^(id  _Nonnull AJson) {
        [weakself.mainTableView.mj_header endRefreshing];
    }];
}

-(void)setLiveID:(NSString *)liveID {
    _liveID = liveID;
    [self refreshData];
}


-(instancetype)initWithFrame:(CGRect)frame {
    if (self == [super initWithFrame:frame]) {
        [self addSubview:self.mainTableView];
        self.mainTableView.tableHeaderView = self.tableHeadView;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.mainTableView.bounds byRoundingCorners:UIRectCornerTopLeft |UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.mainTableView.bounds;
        maskLayer.path = maskPath.CGPath;
        self.mainTableView.layer.mask = maskLayer;
        self.pageNo = 1;
        MB_WeakSelf(self);
        self.mainTableView.mj_footer = [UPCustomRefresh up_getFooter:^{
            weakself.pageNo++;
            [weakself loadMoreData];
        }];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeView)];
        [self addGestureRecognizer:tap];
        
    }
    return self;
}

#pragma mark - evnet
-(void)cancelClick {
    if (self.removeBlock) {
        self.removeBlock();
    }
}
-(void)removeView {
    if (self.removeBlock) {
        self.removeBlock();
    }
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBLiveMainGetGiftCell *cell = [tableView dequeueReusableCellWithIdentifier:MBLiveMainGetGiftCellID];
    if (cell == nil) {
        cell = [[MBLiveMainGetGiftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBLiveMainGetGiftCellID];
    }
    GiftRecordList *model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma mark - getter and setter
-(MBBaseTableview *)mainTableView {
    if (_mainTableView == nil) {
        _mainTableView = [[MBBaseTableview alloc]initWithFrame:CGRectMake(0, kScreenHeight - 317 -kTabSafeBottomMargin, kScreenWidth, 317) style:UITableViewStyleGrouped];
        _mainTableView.delegate = self;
        _mainTableView.dataSource = self;
        _mainTableView.rowHeight = 50.0;
        _mainTableView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
        _mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainGetGiftCell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainGetGiftCellID];
        _mainTableView.showsVerticalScrollIndicator = NO;

    }
    return _mainTableView;
}
-(UIView *)tableHeadView {
    if (_tableHeadView == nil) {
        _tableHeadView = [[UIView alloc]init];
        _tableHeadView.frame = (CGRect){{0,0},{kScreenWidth,50}};
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:kImage(@"tuichu4") forState:UIControlStateNormal];
        [button addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
        [_tableHeadView addSubview:button];
        [button mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tableHeadView.mas_top).offset(8);
            make.right.equalTo(_tableHeadView.mas_right).offset(0);
            make.size.mas_equalTo(CGSizeMake(36, 36));
        }];
        self.cancelButton = button;
        
        UILabel *label = [[UILabel alloc]init];
        label.text = @"我收到的礼物";
        label.textColor = Color_EEEEEE;
        label.font = kFont(14);
        [_tableHeadView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_tableHeadView.mas_top).offset(20);
            make.left.equalTo(_tableHeadView.mas_left).offset(12);
        }];
    }
    return _tableHeadView;
}

-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
