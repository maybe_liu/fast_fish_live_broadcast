//
//  MBSystemFriendCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/23.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSystemFriendCell.h"

@implementation MBSystemFriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.selectImageView.image = selected ? kImage(@"share_chose") : kImage(@"share_unchose");
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

-(void)initUI {
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.namaLabel.textColor = Color_333333;
    self.namaLabel.font = kFont(14);
}
-(void)setModel:(MBSystemFriendModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:kImage(@"")];
    self.namaLabel.text = _model.nickname;
}

@end
