//
//  MBLiveGiftAnimationView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveGiftAnimationView.h"

@implementation MBLiveGiftAnimationView

-(void)showGiftAnimationView {
//    [self.giftImageView s];
    [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:@""] placeholderImage:nil];
}

-(instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.giftImageView];
        self.giftImageView.frame = frame;
    }
    return self;
}
-(YYAnimatedImageView *)giftImageView {
    if (_giftImageView == nil) {
        _giftImageView = [[YYAnimatedImageView alloc]init];
    }
    return _giftImageView;
}



@end
