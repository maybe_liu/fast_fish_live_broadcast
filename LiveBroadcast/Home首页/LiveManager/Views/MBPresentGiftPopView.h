//
//  MBPresentGiftPopView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveGiftListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^CancelBlock)(void);
typedef void(^JumpRechargeBlock)(void);
typedef void(^CommitButtonBlock)(MBLiveGiftModel *,NSString * giftCount);
typedef void(^GiftCountBlock)(void);


@interface MBPresentGiftPopView : UIView
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollectionView;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *coinIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *jumpRechargeButton;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *commitButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *giftCountButton;
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (nonatomic, strong)NSMutableArray *dataArray;
@property (nonatomic, copy) CancelBlock cancelBlock;
@property (nonatomic, copy) JumpRechargeBlock jumpRechargeBlock;
@property (nonatomic, copy) CommitButtonBlock commitBlock;
@property (nonatomic, copy) GiftCountBlock giftCountBlock;
@property (weak, nonatomic) IBOutlet UIPageControl *mainPageController;


@property (nonatomic, copy) NSString *balanceString;



@end

NS_ASSUME_NONNULL_END
