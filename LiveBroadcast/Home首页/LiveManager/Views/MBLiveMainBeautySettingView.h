//
//  MBLiveMainBeautySettingView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^TureSetBlockHander)(float skinValue, float beautyValue);

@interface MBLiveMainBeautySettingView : UIView
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *skinLabel;
@property (weak, nonatomic) IBOutlet UISlider *skinSlider;
@property (weak, nonatomic) IBOutlet UILabel *beautyLabel;
@property (weak, nonatomic) IBOutlet UISlider *beautySlider;
@property (copy, nonatomic) TureSetBlockHander beautySetBlock;

@end

NS_ASSUME_NONNULL_END
