//
//  MBLiveOpenRedPacketResultView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CancelBlockHandler)(void);

@interface MBLiveOpenRedPacketResultView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIView *customView;

@property (nonatomic, copy) CancelBlockHandler cancelBlock;

@property (nonatomic, copy) NSString *moneyString;

@end

NS_ASSUME_NONNULL_END
