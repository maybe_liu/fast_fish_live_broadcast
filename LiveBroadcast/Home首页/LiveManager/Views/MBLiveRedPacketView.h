//
//  MBLiveRedPacketView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CancelBlockHandler)(void);
typedef void(^OpenRedPacketBlockHandler)(void);

@interface MBLiveRedPacketView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *redPacketImageView;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *customView;
@property (nonatomic, copy) CancelBlockHandler cancelBlock;
@property (nonatomic, copy) OpenRedPacketBlockHandler openBlock;

@end

NS_ASSUME_NONNULL_END
