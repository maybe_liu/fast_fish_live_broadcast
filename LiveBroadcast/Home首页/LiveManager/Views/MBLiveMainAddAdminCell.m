//
//  MBLiveMainAddAdminCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainAddAdminCell.h"

@implementation MBLiveMainAddAdminCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
-(void)initUI {
    self.headImageView.layer.cornerRadius = 11.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.font = kFont(14);
    self.lineView.backgroundColor = Color_Line;
    [self.addButton setTitleColor:Color_FF8000 forState:UIControlStateNormal];
    self.addButton.titleLabel.font = kFont(9);
    self.addButton.layer.cornerRadius = 15.0f;
    self.addButton.layer.masksToBounds = YES;
    self.addButton.layer.borderColor = Color_FF8000.CGColor;
    self.addButton.layer.borderWidth = 0.5;
}
- (IBAction)addButtonClick:(UIButton *)sender {
    if (self.addBlock) {
        self.addBlock();
    }
}

@end
