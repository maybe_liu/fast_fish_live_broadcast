//
//  MBLiveMainGetGiftCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainGetGiftCell.h"

@implementation MBLiveMainGetGiftCell

-(void)setModel:(GiftRecordList *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:kImage(@"")];
    self.nameLabel.text = _model.nickname;
    self.timeLabel.text = _model.createTime;
    [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:_model.giftInfo] placeholderImage:kImage(@"")];
    self.giftCountLabel.text = [NSString stringWithFormat:@"x %@",_model.giftCount];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.nameLabel.font = kFont(14);
    self.nameLabel.textColor = Color_DDDDDD;
    self.timeLabel.font = kFont(12);
    self.timeLabel.textColor = Color_AAAAAA;
    self.giftCountLabel.font = kMFont(14);
    self.giftCountLabel.textColor = Color_White;
    self.giftImageView.image = kImage(@"gifticon_666");
    self.giftImageView.layer.cornerRadius = 20.0f;
    self.giftImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
