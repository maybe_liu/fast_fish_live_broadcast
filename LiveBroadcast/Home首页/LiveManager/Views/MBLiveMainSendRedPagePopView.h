//
//  MBLiveMainSendRedPagePopView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef void(^cancelBlockHander)(void);

/**
 主播发红包
 */
@interface MBLiveMainSendRedPagePopView : UIView
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (weak, nonatomic) IBOutlet UIView *redPageContentView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet MBTextField *redPageTotleField;
@property (weak, nonatomic) IBOutlet UIView *firstLine;
@property (weak, nonatomic) IBOutlet UIView *secondLine;
@property (weak, nonatomic) IBOutlet UILabel *redPageNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *redPageNumberUnitLabel;
@property (weak, nonatomic) IBOutlet MBTextField *redPageNumberVauleField;
@property (weak, nonatomic) IBOutlet UIButton *tureButton;
@property (copy, nonatomic) cancelBlockHander cancelBlock;
@property (nonatomic, copy)NSString *liveID;

@end

NS_ASSUME_NONNULL_END
