//
//  MBPresentGiftPopCollectionCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveGiftListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBPresentGiftPopCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *giftMoneyLabel;
@property (nonatomic,strong) MBLiveGiftModel *model;
@end

NS_ASSUME_NONNULL_END
