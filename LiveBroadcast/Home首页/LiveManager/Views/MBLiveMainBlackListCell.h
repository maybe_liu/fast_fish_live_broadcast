//
//  MBLiveMainBlackListCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveMainBlackAudienceListModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^CancelBlockHander)(MBLiveMainBlackAudienceModel *model);
@interface MBLiveMainBlackListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (copy, nonatomic) CancelBlockHander cancelBlock;
@property (nonatomic, strong)MBLiveMainBlackAudienceModel *model;

@end

NS_ASSUME_NONNULL_END
