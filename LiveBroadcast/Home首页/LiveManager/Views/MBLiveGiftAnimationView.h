//
//  MBLiveGiftAnimationView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveGiftAnimationView : UIView
@property(nonatomic, strong)YYAnimatedImageView *giftImageView;
//@property (weak, nonatomic) IBOutlet YYAnimatedImageView *giftImageView;

@end

NS_ASSUME_NONNULL_END
