//
//  MBLiveRemoveAndBlackTableViewCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveViewerModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^RemoveBlockHander)(MBLiveViewerModel *model);
typedef void(^BlackBlockHander)(MBLiveViewerModel *model);


@interface MBLiveRemoveAndBlackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *namaLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIButton *blackButton;
@property (nonatomic, strong)MBLiveViewerModel *model;
@property (nonatomic, copy)RemoveBlockHander removeBlock;
@property (nonatomic, copy)BlackBlockHander blackBlock;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

NS_ASSUME_NONNULL_END
