//
//  MBLiveMessageTableView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveMessageTableViewcell.h"

NS_ASSUME_NONNULL_BEGIN

/**
 直播消息列表
 */
@interface MBLiveMessageTableView : UITableView<UITableViewDelegate, UITableViewDataSource>
// 给消息列表发送一条消息用于展示
- (void)appendMsg:(LiveRoomMsgModel *)msgModel;

@end

NS_ASSUME_NONNULL_END
