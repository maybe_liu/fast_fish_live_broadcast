//
//  MBLiveMainSendRedPagePopView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainSendRedPagePopView.h"
#import "MBLiveMainSendRedPacketListModel.h"

@implementation MBLiveMainSendRedPagePopView

/**
 获取礼物列表
 */
-(void)sendRedPacket {
    NSDictionary *param = @{
                            @"liveId":self.liveID,
                            @"redTotal":self.redPageTotleField.text,
                            @"redCount":self.redPageNumberVauleField.text
                            };
//    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_live_liveSendRedPacket setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
//        MBLiveMainSendRedPacketListModel *model = [MBLiveMainSendRedPacketListModel mj_objectWithKeyValues:AJson];
        [MBProgressHUD showMessage:@"发送红包成功"];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self initUI];
}

-(void)awakeFromNib {
    [super awakeFromNib];
}
-(void)initUI {
    self.customContentView.backgroundColor = [UIColor clearColor];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.redPageContentView.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(4, 4)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.redPageContentView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.redPageContentView.layer.mask = maskLayer;
    self.topTitleLabel.font = kFont(12);
    self.topTitleLabel.textColor = Color_333333;
    self.redPageTotleField.placeholderFont = kBFont(40);
    self.redPageTotleField.font = kBFont(40);
    self.redPageTotleField.placeholderColor = Color_CCCCCC;
    self.redPageTotleField.textColor = Color_333333;
    self.redPageTotleField.mTintColor = Color_FF8000;
    self.redPageTotleField.limitedType = MBGeneralTextFieldTypeNumber;
    self.firstLine.backgroundColor = Color_Line;
    self.secondLine.backgroundColor = Color_Line;
    self.redPageNumberLabel.font = kFont(14);
    self.redPageNumberLabel.textColor = Color_333333;
    self.redPageNumberUnitLabel.font = kFont(14);
    self.redPageNumberUnitLabel.textColor = Color_333333;
    self.redPageNumberVauleField.placeholderColor = Color_AAAAAA;
    self.redPageNumberVauleField.placeholderFont = kFont(14);
    self.redPageNumberVauleField.font = kFont(14);
    self.redPageNumberVauleField.textColor = Color_333333;
    self.redPageNumberVauleField.limitedType = MBGeneralTextFieldTypeNumber;
    [self.tureButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.tureButton.titleLabel.font = kFont(16);
    [self.tureButton setBackgroundColor:Color_FF8000];
    self.tureButton.layer.cornerRadius = 22.0f;
    self.tureButton.layer.masksToBounds = YES;
    
}

- (IBAction)tureButtonClick:(UIButton *)sender {
    if (![self.redPageTotleField.text isNotBlank] || [self.redPageNumberVauleField.text isEqualToString:@"0"]) {
        [MBProgressHUD showMessage:@"请输入红包金额"];
        return;
    }
    if (![self.redPageNumberVauleField.text isNotBlank] || [self.redPageNumberVauleField.text isEqualToString:@"0"]) {
        [MBProgressHUD showMessage:@"请输入红包个数"];
        return;
    }
    [self sendRedPacket];
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

@end
