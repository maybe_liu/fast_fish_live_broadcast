//
//  MBLiveOpenRedPacketResultView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveOpenRedPacketResultView.h"

@implementation MBLiveOpenRedPacketResultView

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.customView.backgroundColor = [UIColor clearColor];
    self.topTitleLabel.textColor = [UIColor colorWithHexString:@"#D02A2A"];
    self.topTitleLabel.font = kFont(18);
}
- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
-(void)setMoneyString:(NSString *)moneyString {
    _moneyString = moneyString;
    NSDictionary *big = @{NSFontAttributeName:[UIFont fontWithName:@"ArialMT" size: 70],NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#D02A2A"]};
    NSDictionary *normal = @{NSFontAttributeName:kMFont(18),NSForegroundColorAttributeName:[UIColor colorWithHexString:@"#D02A2A"]};
    if ([_moneyString isNotBlank]) {
        [self.moneyLabel attributedText:@[_moneyString,@"鱼币"] attributeAttay:@[big,normal]];
    }
}

@end
