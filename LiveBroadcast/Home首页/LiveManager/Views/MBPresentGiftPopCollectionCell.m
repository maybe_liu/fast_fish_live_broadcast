//
//  MBPresentGiftPopCollectionCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPresentGiftPopCollectionCell.h"

@implementation MBPresentGiftPopCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.giftNameLabel.textColor = Color_White;
    self.giftNameLabel.font = kFont(12);
    self.giftMoneyLabel.textColor = Color_666666;
    self.giftMoneyLabel.font = kFont(10);
}
-(void)setModel:(MBLiveGiftModel *)model {
    _model = model;
    [self.topImageView sd_setImageWithURL:[NSURL URLWithString:_model.giftInfo] placeholderImage:nil];
    self.giftNameLabel.text = _model.giftname;
    self.giftMoneyLabel.text = [NSString stringWithFormat:@"%@%@",_model.giftPrice,@"鱼币"];
}

-(void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.layer.borderColor = Color_FF8000.CGColor;
        self.layer.borderWidth = 1.0f;
    }else {
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.layer.borderWidth = 1.0f;
    }
}

@end
