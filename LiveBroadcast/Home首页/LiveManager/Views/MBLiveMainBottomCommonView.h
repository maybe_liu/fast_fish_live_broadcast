//
//  MBLiveMainBottomCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^cameraButtonBlockHandler)(void);
typedef void(^giftButtonBlockHandler)(void);
typedef void(^moreButtonBlockHandler)(void);

@interface MBLiveMainBottomCommonView : UIView
@property (nonatomic, copy)cameraButtonBlockHandler cameraBlock;
@property (nonatomic, copy)giftButtonBlockHandler giftBlock;
@property (nonatomic, copy)moreButtonBlockHandler moreBlock;



@end

NS_ASSUME_NONNULL_END
