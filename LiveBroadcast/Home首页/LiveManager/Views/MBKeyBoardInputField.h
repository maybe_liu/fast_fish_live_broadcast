//
//  MBKeyBoardInputField.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^WordFieldBlockHandler)(NSString *wordString,NSInteger commentType);


/*默认评论主播*/
typedef NS_ENUM(NSInteger,MBwordFieldType){
    MBwordFieldTypeAudience                = 0, /*评论主播*/
    MBwordFieldTypeAnchor                  = 1 /*评论观众*/
};

@interface MBKeyBoardInputField : UIView
@property (weak, nonatomic) IBOutlet MBTextField *mainField;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (copy, nonatomic) WordFieldBlockHandler wordFieldBlock;
@property (nonatomic, assign)MBwordFieldType type;
@end

NS_ASSUME_NONNULL_END
