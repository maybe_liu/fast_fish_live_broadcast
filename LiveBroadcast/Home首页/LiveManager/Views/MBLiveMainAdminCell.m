//
//  MBLiveMainAdminCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainAdminCell.h"

@implementation MBLiveMainAdminCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
-(void)initUI {
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.font = kFont(14);
    self.lineView.backgroundColor = Color_Line;
}

@end
