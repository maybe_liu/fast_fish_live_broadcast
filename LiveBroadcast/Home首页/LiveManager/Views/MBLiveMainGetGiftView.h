//
//  MBLiveMainGetGiftView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 收到的礼物
 */

typedef void(^RemoveBlockHandler)(void);
@interface MBLiveMainGetGiftView : UIView
@property (nonatomic, copy)RemoveBlockHandler removeBlock;
@property (nonatomic, copy)NSString *liveID;
@end

NS_ASSUME_NONNULL_END
