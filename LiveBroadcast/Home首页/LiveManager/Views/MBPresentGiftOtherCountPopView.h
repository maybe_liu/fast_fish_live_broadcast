//
//  MBPresentGiftOtherCountPopView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveGiftListModel.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^GiftSelectCountBlock)(NSInteger giftCount);

@interface MBPresentGiftOtherCountPopView : UIView
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (weak, nonatomic) IBOutlet UIView *topContentView;
@property (weak, nonatomic) IBOutlet UILabel *topTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UIView *countView;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet MBTextField *countField;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *commitButton;
@property (nonatomic, copy) GiftSelectCountBlock giftSelectCountBlock;
@property(nonatomic, strong) MBLiveGiftModel *selectModel;

@end

NS_ASSUME_NONNULL_END
