//
//  MBLiveMainTopAudienceCollectionCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveViewerModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveMainTopAudienceCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *centerHeadImageView;
@property (nonatomic, strong)MBLiveViewerModel *audienceModel;

@end

NS_ASSUME_NONNULL_END
