//
//  MBLiveFinshPopView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/21.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCloseLiveModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^gotoHomeMainVCBlockHandler)(void);
typedef void(^gotoUserVCBlockHandler)(void);


@interface MBLiveFinshPopView : UIView

@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (weak, nonatomic) IBOutlet UIImageView *topIcon;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet MBCustomButton *attentionButton;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchCountValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveDurationValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveDurationLabel;
@property (weak, nonatomic) IBOutlet UIButton *gotoUserHomePageButton;
@property (weak, nonatomic) IBOutlet UIButton *gotoHomeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gotoHomeButtonTopConstraint;

@property (nonatomic, assign)MBUserPart currentPart;//当前用户角色
@property (nonatomic, strong)MBCloseLiveModel *anchorCloseModel;
@property (weak, nonatomic) IBOutlet UIView *firstVerticalLine;
@property (weak, nonatomic) IBOutlet UIView *secondVerticalLine;

@property (copy, nonatomic)gotoHomeMainVCBlockHandler gotoHomeMainVCBlock;
@property (copy, nonatomic)gotoUserVCBlockHandler     gotoUserVCBlock;

@end

NS_ASSUME_NONNULL_END
