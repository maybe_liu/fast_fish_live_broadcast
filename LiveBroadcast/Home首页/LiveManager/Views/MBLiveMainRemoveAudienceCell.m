//
//  MBLiveMainRemoveAudienceCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainRemoveAudienceCell.h"

@implementation MBLiveMainRemoveAudienceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(MBLiveMainRemoveAudienceModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_model.headUrl] placeholderImage:kImage(@"")];
    self.nameLabel.text = _model.nickname;
    NSString *timeString = [[MBTimeManager shateInstance] getNonSecComleteTime:model.createTime/1000];
    self.timeLabel.text = [NSString stringWithFormat:@"%@%@",@"踢人时间：",timeString];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}



-(void)initUI {
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.font = kFont(14);
    self.timeLabel.textColor = Color_666666;
    self.timeLabel.font = kFont(12);
    self.lineView.backgroundColor = Color_Line;
}

@end
