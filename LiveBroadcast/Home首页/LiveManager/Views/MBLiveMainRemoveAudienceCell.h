//
//  MBLiveMainRemoveAudienceCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveMainRemoveAudienceListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveMainRemoveAudienceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (nonatomic, strong)MBLiveMainRemoveAudienceModel *model;
@end

NS_ASSUME_NONNULL_END
