//
//  MBLiveRedPacketView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveRedPacketView.h"

@implementation MBLiveRedPacketView


-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.headImageView.layer.cornerRadius = 23.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.layer.borderColor = Color_White.CGColor;
    self.headImageView.layer.borderWidth = 1.0f;
    self.nameLabel.textColor = [Color_White colorWithAlphaComponent:0.6];
    self.nameLabel.font = kFont(12);
    self.customView.backgroundColor = [UIColor clearColor];
    self.redPacketImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openRedPacket)];
    [self.redPacketImageView addGestureRecognizer:tap];
}
- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
-(void)openRedPacket {
    if (self.openBlock) {
        self.openBlock();
    }
}


@end
