//
//  MBPresentGiftOtherCountPopView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPresentGiftOtherCountPopView.h"

@interface MBPresentGiftOtherCountPopView ()
@property (nonatomic, assign)NSInteger giftCount;

@end

@implementation MBPresentGiftOtherCountPopView

-(void)setSelectModel:(MBLiveGiftModel *)selectModel {
    _selectModel = selectModel;
    [self.giftImageView sd_setImageWithURL:[NSURL URLWithString:_selectModel.giftInfo]];
    self.giftNameLabel.text = _selectModel.giftname;
}
-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.customContentView.backgroundColor = [UIColor clearColor];
    self.topContentView.backgroundColor = Color_White;
    self.topContentView.layer.cornerRadius = 4.0f;
    self.topContentView.layer.masksToBounds = YES;
    self.topTitleLabel.textColor = Color_333333;
    self.topTitleLabel.font = kMFont(16);
    self.giftNameLabel.textColor = Color_333333;
    self.giftNameLabel.font = kFont(14);
    self.countView.layer.borderColor = Color_Line.CGColor;
    self.countView.layer.borderWidth = 0.5;
    self.countField.limitedType = MBGeneralTextFieldTypeNumber;
    self.countField.textAlignment = NSTextAlignmentCenter;
    [self.commitButton setBackgroundColor:Color_FF8000];
    [self.commitButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.commitButton.titleLabel.font = kFont(16);
    self.commitButton.layer.cornerRadius = 22.0f;
    self.commitButton.layer.masksToBounds = YES;
    MB_WeakSelf(self);
    self.countField.textFieldDidChange = ^(NSString * _Nonnull text) {
        weakself.giftCount = text.integerValue;
    };
}

- (IBAction)minusButtonClick:(id)sender {
    if (self.giftCount < 0) {
        return;
    }
    self.giftCount--;
    self.countField.text = [NSString stringWithFormat:@"%zd",self.giftCount];
}
- (IBAction)plusButtonClick:(UIButton *)sender {
    self.giftCount++;
    self.countField.text = [NSString stringWithFormat:@"%zd",self.giftCount];
}
- (IBAction)commitButtonClick:(UIButton *)sender {
    if (self.giftSelectCountBlock) {
        self.giftSelectCountBlock(self.giftCount);
    }
}
- (IBAction)cancelButtonClick:(UIButton *)sender {
    [self removeFromSuperview];
}

@end
