//
//  MBLiveMainMoreCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CancelButtonBlockHandler)(void);
typedef void(^RedPacketBlockHander)(void);
typedef void(^BeautyBlockHander)(void);
typedef void(^SettingBlockHander)(void);
typedef void(^ShareBlockHander)(void);
typedef void(^NotificationBlockHander)(void);

@interface MBLiveMainMoreCommonView : UIView
@property (weak, nonatomic) IBOutlet UIView *customContentView;
@property (weak, nonatomic) IBOutlet MBCustomButton *redPageButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *beautyButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *settingButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *notificationButton;
@property (weak, nonatomic) IBOutlet MBCustomButton *shareButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (copy, nonatomic) CancelButtonBlockHandler cancelBlock;
@property (copy, nonatomic) RedPacketBlockHander redPacketBlock;
@property (copy, nonatomic) BeautyBlockHander beautyBlock;
@property (copy, nonatomic) SettingBlockHander settingBlock;
@property (copy, nonatomic) ShareBlockHander shareBlock;
@property (copy, nonatomic) NotificationBlockHander notificationBlock;

@property (nonatomic, copy)NSString *liveID;

@end

NS_ASSUME_NONNULL_END
