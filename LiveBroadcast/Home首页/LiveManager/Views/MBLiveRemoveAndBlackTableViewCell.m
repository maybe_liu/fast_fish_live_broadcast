//
//  MBLiveRemoveAndBlackTableViewCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveRemoveAndBlackTableViewCell.h"

@implementation MBLiveRemoveAndBlackTableViewCell

-(void)setModel:(MBLiveViewerModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_model.headUrl]];
    self.namaLabel.text = _model.nickname;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.namaLabel.textColor = Color_333333;
    self.namaLabel.font = kFont(14);
    [self.removeButton setBackgroundColor:Color_FF8000];
    [self.removeButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.removeButton.layer.cornerRadius = 15.0f;
    self.removeButton.layer.masksToBounds = YES;
    self.removeButton.titleLabel.font = kFont(14);
    
    [self.blackButton setBackgroundColor:Color_FF8000];
    [self.blackButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.blackButton.layer.cornerRadius = 15.0f;
    self.blackButton.layer.masksToBounds = YES;
    self.blackButton.titleLabel.font = kFont(14);
    self.lineView.backgroundColor = Color_Line;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)removeButtonClick:(UIButton *)sender {
    if (self.removeBlock) {
        self.removeBlock(self.model);
    }
}
- (IBAction)blackButtonClick:(UIButton *)sender {
    if (self.blackBlock) {
        self.blackBlock(self.model);
    }
}

@end
