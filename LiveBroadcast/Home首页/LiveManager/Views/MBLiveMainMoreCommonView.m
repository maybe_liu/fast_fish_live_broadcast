//
//  MBLiveMainMoreCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainMoreCommonView.h"

@implementation MBLiveMainMoreCommonView

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeTap)];
    [self addGestureRecognizer:tap];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    [self initUI];
}

-(void)initUI {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.customContentView.bounds byRoundingCorners:UIRectCornerTopLeft |UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.customContentView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.customContentView.layer.mask = maskLayer;
    self.customContentView.backgroundColor = [Color_White colorWithAlphaComponent:0.94];
    [self.redPageButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    self.redPageButton.titleLabel.font = kFont(12);
    self.redPageButton.postion = MBImagePositionTop;
    self.redPageButton.spacing = 14.0f;
    
    [self.beautyButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    self.beautyButton.titleLabel.font = kFont(12);
    self.beautyButton.postion = MBImagePositionTop;
    self.beautyButton.spacing = 14.0f;
    
    [self.settingButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    self.settingButton.titleLabel.font = kFont(12);
    self.settingButton.postion = MBImagePositionTop;
    self.settingButton.spacing = 14.0f;
    
    [self.notificationButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    self.notificationButton.titleLabel.font = kFont(12);
    self.notificationButton.postion = MBImagePositionTop;
    self.notificationButton.spacing = 14.0f;
    
    [self.shareButton setTitleColor:Color_666666 forState:UIControlStateNormal];
    self.shareButton.titleLabel.font = kFont(12);
    self.shareButton.postion = MBImagePositionTop;
    self.shareButton.spacing = 14.0f;
    
    self.lineView.backgroundColor = Color_EEEEEE;
    [self.cancelButton setTitleColor:Color_333333 forState:UIControlStateNormal];
    self.cancelButton.titleLabel.font = kFont(14);
}

- (IBAction)redPageButtonClick:(MBButton *)sender {
    if (self.redPacketBlock) {
        self.redPacketBlock();
    }
}
- (IBAction)beautyButtonClick:(MBButton *)sender {
    if (self.beautyBlock) {
        self.beautyBlock();
    }
}
- (IBAction)settingButtonClick:(MBButton *)sender {
    if (self.settingBlock) {
        self.settingBlock();
    }
}
- (IBAction)notificationButtonClick:(MBButton *)sender {
    if(self.notificationBlock){
        self.notificationBlock();
    }
}
- (IBAction)shareButtonClick:(MBButton *)sender {
    if (self.shareBlock) {
        self.shareBlock();
    }
}
- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

-(void)removeTap {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
@end
