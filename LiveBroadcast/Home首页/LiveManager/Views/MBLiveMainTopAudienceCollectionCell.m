//
//  MBLiveMainTopAudienceCollectionCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainTopAudienceCollectionCell.h"

@implementation MBLiveMainTopAudienceCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.centerHeadImageView.layer.cornerRadius = 15.0f;
    self.centerHeadImageView.layer.masksToBounds = YES;
}
-(void)setAudienceModel:(MBLiveViewerModel *)audienceModel {
    _audienceModel = audienceModel;
    [self.centerHeadImageView sd_setImageWithURL:[NSURL URLWithString:_audienceModel.headUrl]];
}

@end
