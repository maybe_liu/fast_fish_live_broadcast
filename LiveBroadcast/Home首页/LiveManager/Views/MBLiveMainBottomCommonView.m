//
//  MBLiveMainBottomCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainBottomCommonView.h"

@implementation MBLiveMainBottomCommonView

-(void)awakeFromNib {
    [super awakeFromNib];
}
/*相机*/
- (IBAction)cameraButtonClick:(UIButton *)sender {
    if (self.cameraBlock) {
        self.cameraBlock();
    }
}
/*礼物*/
- (IBAction)giftButtonClick:(UIButton *)sender {
    if (self.giftBlock) {
        self.giftBlock();
    }
}
/*更多*/
- (IBAction)moreButtonClick:(UIButton *)sender {
    if (self.moreBlock) {
        self.moreBlock();
    }
}




@end
