//
//  MBLiveMainGetGiftCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveMianGetGiftListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBLiveMainGetGiftCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *giftCountLabel;
@property (nonatomic, strong)GiftRecordList *model;
@end

NS_ASSUME_NONNULL_END
