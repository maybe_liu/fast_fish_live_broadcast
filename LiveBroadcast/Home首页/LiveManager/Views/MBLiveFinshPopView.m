//
//  MBLiveFinshPopView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/21.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveFinshPopView.h"

@implementation MBLiveFinshPopView

-(void)setAnchorCloseModel:(MBCloseLiveModel *)anchorCloseModel {
    _anchorCloseModel = anchorCloseModel;
    [self.topIcon sd_setImageWithURL:[NSURL URLWithString:_anchorCloseModel.userInfo.headUrl] placeholderImage:kImage(@"")];
    self.userName.text = _anchorCloseModel.userInfo.nickname;
    self.watchCountValueLabel.text = _anchorCloseModel.viewerCount;
    self.likeCountValueLabel.text = ([_anchorCloseModel.praiseCount isNotBlank] ? _anchorCloseModel.praiseCount : @"0");
    self.liveDurationValueLabel.text = [NSString getMMSSFromSS:[NSString stringWithFormat:@"%zd",_anchorCloseModel.liveTime/1000]];
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self createUI];
}

-(void)createUI {
    self.customContentView.backgroundColor = [UIColor clearColor];
    self.topIcon.layer.cornerRadius = 45.0f;
    self.topIcon.layer.masksToBounds = YES;
    self.topIcon.layer.borderWidth = 1.0f;
    self.topIcon.layer.borderColor = Color_White.CGColor;
    self.userName.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.userName.font = kFont(14);
    [self.attentionButton setBackgroundColor:Color_FF8000];
    [self.attentionButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.attentionButton.titleLabel.font = kFont(12);
    
    self.attentionButton.layer.cornerRadius = 16.0f;
    self.attentionButton.layer.masksToBounds = YES;
    
    self.finishLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.finishLabel.font = kFont(24);
    
    self.watchCountValueLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.watchCountValueLabel.font = kFont(15);
    
    self.watchCountLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.watchCountLabel.font = kFont(12);
    
    self.likeCountValueLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.likeCountValueLabel.font = kFont(15);
    
    self.likeCountLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.likeCountLabel.font = kFont(12);
    
    self.liveDurationValueLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.liveDurationValueLabel.font = kFont(15);
    
    self.liveDurationLabel.textColor = [Color_White colorWithAlphaComponent:0.8];
    self.liveDurationLabel.font = kFont(12);
    
    [self.gotoUserHomePageButton setBackgroundColor:Color_FF8000];
    self.gotoUserHomePageButton.layer.cornerRadius = 22.0f;
    self.gotoUserHomePageButton.layer.masksToBounds = YES;
    [self.gotoUserHomePageButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.gotoUserHomePageButton.titleLabel.font = kFont(16);
    
    [self.gotoHomeButton setBackgroundColor:[UIColor clearColor]];
    self.gotoHomeButton.layer.cornerRadius = 22.0f;
    self.gotoHomeButton.layer.masksToBounds = YES;
    self.gotoHomeButton.layer.borderWidth = 1.0f;
    self.gotoHomeButton.layer.borderColor = Color_White.CGColor;
    
    [self.gotoHomeButton setTitleColor:Color_White forState:UIControlStateNormal];
    self.gotoHomeButton.titleLabel.font = kFont(16);
    self.firstVerticalLine.backgroundColor = [Color_White colorWithAlphaComponent:0.2];
    self.secondVerticalLine.backgroundColor = [Color_White colorWithAlphaComponent:0.2];

}

-(void)setCurrentPart:(MBUserPart)currentPart {
    _currentPart = currentPart;
    if (_currentPart == MBUserPartAnchor) {
        self.attentionButton.hidden = YES;
        self.gotoUserHomePageButton.hidden = YES;
        self.gotoHomeButtonTopConstraint.constant = 50.f;
        [self.customContentView layoutIfNeeded];
    }
}

- (IBAction)attentionButtonClick:(UIButton *)sender {
}
- (IBAction)goUserHomeButtonClick:(UIButton *)sender {
    if (self.gotoUserVCBlock) {
        self.gotoUserVCBlock();
    }
}
- (IBAction)goHomeButtonClick:(UIButton *)sender {
//    MBBaseTabbarController *tab = (MBBaseTabbarController *)APP_delegate.window.rootViewController;
//    [tab updateCurrentIndex:0];
    if (self.gotoHomeMainVCBlock) {
        self.gotoHomeMainVCBlock();
    }
}


@end
