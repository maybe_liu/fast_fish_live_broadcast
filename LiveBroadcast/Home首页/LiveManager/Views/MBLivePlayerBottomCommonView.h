//
//  MBLivePlayerBottomCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/29.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^GiftBlock)(void);
typedef void(^LikeBlock)(UIButton *sender);
typedef void(^ShareBlock)(void);
typedef void(^SendMessageBlock)(NSString *message);


@interface MBLivePlayerBottomCommonView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet MBTextField *wordField;
@property (weak, nonatomic) IBOutlet UIButton *giftButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (nonatomic, copy) GiftBlock giftBlock;
@property (nonatomic, copy) LikeBlock likeBlock;
@property (nonatomic, copy) ShareBlock shareBlock;
@property (nonatomic, copy) SendMessageBlock sendMessageBlock;


-(void)setLikeOrUnLike:(BOOL)isLike;
@end

NS_ASSUME_NONNULL_END
