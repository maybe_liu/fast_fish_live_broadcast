//
//  MBPresentGiftCountPopView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPresentGiftCountPopView.h"

@implementation MBPresentGiftCountPopViewTableViewcell

-(void)setLeftString:(NSString *)leftString {
    _leftString = leftString;
    self.leftLabel.text = _leftString;
}
-(void)setRightString:(NSString *)rightString {
    _rightString = rightString;
    self.rightLabel.text = _rightString;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
-(void)initUI {
    [self.contentView addSubview:self.leftLabel];
    [self.contentView addSubview:self.rightLabel];
    [self.contentView addSubview:self.lineView];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    [self.leftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(12);
    }];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView.mas_left).offset(80);
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.height.mas_offset(0.5);
        make.left.equalTo(self.contentView.mas_left).offset(12);
        make.right.equalTo(self.contentView.mas_right);
    }];
}

-(UILabel *)leftLabel {
    if (_leftLabel == nil) {
        _leftLabel = [[UILabel alloc]init];
        _leftLabel.textColor = Color_FF8000;
        _leftLabel.font = kFont(12);
    }
    return _leftLabel;
}
-(UILabel *)rightLabel {
    if (_rightLabel == nil) {
        _rightLabel = [[UILabel alloc]init];
        _rightLabel.textColor = Color_999999;
        _rightLabel.font = kFont(12);
    }
    return _rightLabel;
}
-(UIView *)lineView {
    if (_lineView == nil) {
        _lineView = [[UIView alloc]init];
        _lineView.backgroundColor = Color_Line;
    }
    return _lineView;
}

@end

@interface MBPresentGiftCountPopView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong)NSArray *dataArray;
@property (nonatomic, strong)NSArray *rightArray;


@end

static NSString *const MBPresentGiftCountPopViewTableViewcellID = @"MBPresentGiftCountPopViewTableViewcellID";

@implementation MBPresentGiftCountPopView


-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeTap)];
//    [self addGestureRecognizer:tap];
}
-(void)removeTap {
    [self removeFromSuperview];
}
-(void)initUI {
    self.mainTableView.backgroundColor = Color_White;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = 40.0f;
    self.mainTableView.scrollEnabled = NO;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainTableView registerClass:[MBPresentGiftCountPopViewTableViewcell class] forCellReuseIdentifier:MBPresentGiftCountPopViewTableViewcellID];
//    [self.mainTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MBPresentGiftCountPopViewTableViewcell class]) bundle:nil] forCellReuseIdentifier:MBLiveMainSensitivewordCellID];
    self.mainTableView.showsVerticalScrollIndicator = NO;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBPresentGiftCountPopViewTableViewcell *cell = [tableView dequeueReusableCellWithIdentifier:MBPresentGiftCountPopViewTableViewcellID];
    if (cell == nil) {
        cell = [[MBPresentGiftCountPopViewTableViewcell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MBPresentGiftCountPopViewTableViewcellID];
    }
    cell.leftString = self.dataArray[indexPath.row];
    cell.rightString = self.rightArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.dataArray.count - 1) {
        if (self.selectGiftOtherCountBlock) {
            self.selectGiftOtherCountBlock();
        }
    }
    NSString *giftCount = self.dataArray[indexPath.row];
    if (self.selectGiftCountBlock) {
        self.selectGiftCountBlock(giftCount);
    }
}

-(NSArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = @[@"1314",@"520",@"188",@"66",@"30",@"10",@"1",@"1"];
    }
    return _dataArray;
}
-(NSArray *)rightArray {
    if (_rightArray == nil) {
        _rightArray = @[@"一生一世",@"我爱你",@"要抱抱",@"一切顺利",@"像你",@"十全十美",@"一心一意",@"其他数量"];
    }
    return _rightArray;
}
@end
