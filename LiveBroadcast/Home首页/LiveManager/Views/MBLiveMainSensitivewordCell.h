//
//  MBLiveMainSensitivewordCell.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^DeleteBlockHandler)(NSString *wordString);

@interface MBLiveMainSensitivewordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UILabel *wordLabel;
@property (nonatomic, copy) DeleteBlockHandler deleteBlock;
@property (nonatomic, copy) NSString *wordString;
@end

NS_ASSUME_NONNULL_END
