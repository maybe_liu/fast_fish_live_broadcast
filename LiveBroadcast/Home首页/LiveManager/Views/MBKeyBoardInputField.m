//
//  MBKeyBoardInputField.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBKeyBoardInputField.h"

@implementation MBKeyBoardInputField

-(void)awakeFromNib {
    [super awakeFromNib];
    self.mainField.placeholderFont = kFont(14);
    self.mainField.placeholderColor = Color_AAAAAA;
    self.mainField.font = kFont(16);
    self.mainField.textColor = Color_333333;
    self.mainField.mTintColor = Color_FF8000;
    self.sendButton.layer.cornerRadius = 15.0f;
    self.sendButton.layer.masksToBounds = YES;
    self.sendButton.titleLabel.font = kFont(14);
    [self.sendButton setTitleColor:Color_White forState:UIControlStateNormal];
    [self.sendButton setBackgroundColor:Color_FF8000];
    self.type = MBwordFieldTypeAudience;
}

- (IBAction)sendButtonClick:(UIButton *)sender {
    if ([self.mainField.text isNotBlank]) {
        if (self.wordFieldBlock) {
            self.wordFieldBlock(self.mainField.text,self.type);
        }
    }
}
-(void)setType:(MBwordFieldType)type {
    _type = type;
}

@end
