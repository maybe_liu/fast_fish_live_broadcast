//
//  MBLivePlayerBottomCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/29.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLivePlayerBottomCommonView.h"

@implementation MBLivePlayerBottomCommonView

-(void)setLikeOrUnLike:(BOOL)isLike{
    if (isLike == YES) {
        [self.likeButton setImage:kImage(@"livePlay_like") forState:UIControlStateNormal];
    }else {
        [self.likeButton setImage:kImage(@"livePlay_unlike") forState:UIControlStateNormal];
    }
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
}

-(void)initUI {
    self.wordField.placeholderFont = kFont(14);
    self.wordField.placeholderColor = [Color_White colorWithAlphaComponent:0.5];
    self.wordField.returnKeyType = UIReturnKeySend;
    self.wordField.delegate = self;
    [self.wordField addTarget:self action:@selector(fieldEndChange) forControlEvents:UIControlEventEditingDidEnd];
    
    self.wordField.font = kFont(14);
    self.wordField.textColor = Color_333333;
    self.wordField.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1];
    self.wordField.layer.cornerRadius = 20.0f;
    self.wordField.layer.masksToBounds = YES;
}

- (IBAction)giftButtonClick:(UIButton *)sender {
    if (self.giftBlock) {
        self.giftBlock();
    }
}
- (IBAction)likeButtonClick:(UIButton *)sender {
    if (self.likeBlock) {
        self.likeBlock(sender);
    }
}
- (IBAction)shareButtonClick:(UIButton *)sender {
    if (self.shareBlock) {
        self.shareBlock();
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {    
    [textField resignFirstResponder];//取消第一响应者
    return YES;
}
-(void)fieldEndChange {
    if (self.sendMessageBlock) {
        self.sendMessageBlock(self.wordField.text);
        self.wordField.text = @"";
    }
}

@end
