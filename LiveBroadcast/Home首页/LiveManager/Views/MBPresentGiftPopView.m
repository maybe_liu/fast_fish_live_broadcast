//
//  MBPresentGiftPopView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/11.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPresentGiftPopView.h"
#import "MBPresentGiftPopCollectionCell.h"
#import "MBPresentGiftCountPopView.h"
#import "MBPresentGiftOtherCountPopView.h"

@interface MBPresentGiftPopView ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic, strong) UICollectionViewFlowLayout * layout;
@property(nonatomic, strong) MBLiveGiftModel *selectModel;
@property(nonatomic, strong) MBPresentGiftCountPopView *giftCountView;
@property(nonatomic, copy)NSString *selectGiftCount;
@property(nonatomic, strong)MBPresentGiftOtherCountPopView *otherGiftCountView;
@end

static NSInteger const cols = 4;
static CGFloat const margin = 20;
static CGFloat const CellHeight = 90;

static NSString * const MBPresentGiftPopCollectionCellID = @"MBPresentGiftPopCollectionCellID";
@implementation MBPresentGiftPopView

#pragma mark - event

-(void)setBalanceString:(NSString *)balanceString {
    _balanceString = balanceString;
    self.balanceLabel.text = _balanceString;
}

- (IBAction)cancelButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
-(void)removeTap {
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}
- (IBAction)jumpRechargeButtonClick:(UIButton *)sender {
    if (self.jumpRechargeBlock) {
        self.jumpRechargeBlock();
    }
}
- (IBAction)commitButtonClick:(UIButton *)sender {
    if (!self.selectModel) {
        [MBProgressHUD showMessage:@"请选择礼物"];
        return;
    }else if ([self.selectGiftCount isEqualToString:@""]){
        [MBProgressHUD showMessage:@"请选择数量"];
        return;
    }else {
        if (self.commitBlock) {
            self.commitBlock(self.selectModel,self.selectGiftCount);
        }
    }
}
- (IBAction)giftCountButtonClick:(UIButton *)sender {
//    if (self.giftCountBlock) {
//        self.giftCountBlock();
//    }
    if (!self.selectModel) {
        [MBProgressHUD showMessage:@"请选择礼物"];
        return;
    }
    MBPresentGiftCountPopView *giftCountView  = [MBPresentGiftCountPopView nibView];
    giftCountView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight}};
    giftCountView.backgroundColor = [UIColor clearColor];
    [self addSubview:giftCountView];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeGiftCountView:)];
//    [giftCountView addGestureRecognizer:tap];
    
    MB_WeakSelf(giftCountView);
    MB_WeakSelf(self);
    giftCountView.selectGiftCountBlock = ^(NSString *giftCountPopCount) {
        [weakgiftCountView removeFromSuperview];
        [weakself.giftCountButton setTitle:giftCountPopCount forState:UIControlStateNormal];
        weakself.selectGiftCount = giftCountPopCount;
    };
    giftCountView.selectGiftOtherCountBlock = ^{
        [weakgiftCountView removeFromSuperview];
        [weakself addSubview:weakself.otherGiftCountView];
        weakself.otherGiftCountView.selectModel = weakself.selectModel;
        weakself.otherGiftCountView.giftSelectCountBlock = ^(NSInteger giftCount) {
            [weakself.otherGiftCountView removeFromSuperview];
            [weakself.giftCountButton setTitle:[NSString stringWithFormat:@"%zd",giftCount] forState:UIControlStateNormal];
            weakself.selectGiftCount = [NSString stringWithFormat:@"%zd",giftCount];
        };
    };
}

-(void)removeGiftCountView:(UITapGestureRecognizer *)tap{
    MBPresentGiftCountPopView *view = (MBPresentGiftCountPopView *)tap.view;
    [view removeFromSuperview];
}


-(void)awakeFromNib {
    [super awakeFromNib];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeTap)];
//    [self addGestureRecognizer:tap];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    [self initUI];
}

-(void)initUI {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.customContentView.bounds byRoundingCorners:UIRectCornerTopLeft |UIRectCornerTopRight cornerRadii:CGSizeMake(10, 10)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.customContentView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.customContentView.layer.mask = maskLayer;
    self.customContentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.9];
    self.topTitleLabel.textColor = Color_EEEEEE;
    self.topTitleLabel.font = kFont(14);
    self.mainCollectionView.collectionViewLayout = self.layout;
    self.mainCollectionView.backgroundColor = [UIColor clearColor];
    self.mainCollectionView.bounces = NO;
    self.mainCollectionView.showsVerticalScrollIndicator = NO;
    self.mainCollectionView.showsHorizontalScrollIndicator = NO;
    self.mainCollectionView.dataSource = self;
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.pagingEnabled = YES;
    [self.mainCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MBPresentGiftPopCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:MBPresentGiftPopCollectionCellID];
    self.lineView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    [self.jumpRechargeButton setBackgroundColor:[UIColor greenColor]];
    [self.commitButton setBackgroundColor:Color_FF8000];
    self.giftCountButton.postion = MBImagePositionTop;
    self.giftCountButton.spacing = 3.0f;
    [self.giftCountButton setBackgroundColor:Color_666666];
    self.selectGiftCount = @"1";
    self.balanceLabel.textColor = Color_White;
    self.balanceLabel.font = kFont(14);
}

#pragma mark - setter
//-(void)setDataArray:(NSArray *)dataArray {
//    _dataArray = dataArray;
//    [self.mainCollectionView reloadData];
//}
-(void)setDataArray:(NSMutableArray *)dataArray {
    _dataArray = dataArray;
    int pageCount = ceil(_dataArray.count/8.0);
    self.mainPageController.numberOfPages = (NSInteger)pageCount;
    self.mainPageController.hidesForSinglePage = YES;
    [self.mainCollectionView reloadData];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBPresentGiftPopCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBPresentGiftPopCollectionCellID forIndexPath:indexPath];
    MBLiveGiftModel *model = self.dataArray[indexPath.row];
    cell.model = model;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    //    PublishActivityTagModel *model = self.dataArray[indexPath.row];
    MBLiveGiftModel *model = self.dataArray[indexPath.row];
    self.selectModel = model;
    MBLog(@"++++++%@",collectionView.indexPathsForSelectedItems);
}

-(void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    
}
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    //1.根据偏移量判断一下应该显示第几个item
    CGFloat offSetX = targetContentOffset->x;
    MBLog(@"偏移量+++%f",offSetX);
    CGFloat itemWidth = 80;
    //item的宽度+行间距 = 页码的宽度
    NSInteger pageWidth = itemWidth + 10;
    //根据偏移量计算是第几页
    NSInteger pageNum = (offSetX+pageWidth/2)/pageWidth;
    self.mainPageController.currentPage = pageNum;
}


#pragma mark - getter
-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        CGFloat cellWidth = (kScreenWidth - 3*margin -30) / cols;
        layout.minimumLineSpacing = 20;
        layout.minimumInteritemSpacing = margin;
        CGFloat cellHeight = CellHeight;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _layout = layout;
    }
    return _layout;
}
-(MBPresentGiftOtherCountPopView *)otherGiftCountView {
    if (_otherGiftCountView == nil) {
        _otherGiftCountView = [MBPresentGiftOtherCountPopView nibView];
        _otherGiftCountView.frame = (CGRect){{0,0},{kScreenWidth,kScreenHeight}};
        _otherGiftCountView.backgroundColor = [UIColor clearColor];
    }
    return _otherGiftCountView;
}
@end
