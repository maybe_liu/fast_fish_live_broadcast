//
//  MBLiveMainBlackListCell.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainBlackListCell.h"

@implementation MBLiveMainBlackListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    // Initialization code
}
- (IBAction)cancelBlackButtonClick:(UIButton *)sender {
    if (self.cancelBlock) {
        self.cancelBlock(self.model);
    }
}

-(void)setModel:(MBLiveMainBlackAudienceModel *)model {
    _model = model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:_model.headUrl]];
    self.nameLabel.text = _model.nickname;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
-(void)initUI {
    self.headImageView.layer.cornerRadius = 20.0f;
    self.headImageView.layer.masksToBounds = YES;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.font = kFont(14);
    self.timeLabel.textColor = Color_666666;
    self.timeLabel.font = kFont(12);
    self.timeLabel.hidden = YES;
    self.lineView.backgroundColor = Color_Line;
    [self.cancelButton setTitleColor:Color_White forState:UIControlStateNormal];
    [self.cancelButton setBackgroundColor:Color_AAAAAA];
    self.cancelButton.titleLabel.font = kFont(12);
    self.cancelButton.layer.cornerRadius = 15.0f;
    self.cancelButton.layer.masksToBounds = YES;
}

@end
