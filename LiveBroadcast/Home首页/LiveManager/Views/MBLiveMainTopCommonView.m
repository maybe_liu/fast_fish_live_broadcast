//
//  MBLiveMainTopCommonView.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBLiveMainTopCommonView.h"
#import "MBLiveViewerModel.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "MBLiveMainTopAudienceCollectionCell.h"

@interface MBLiveMainTopCommonView ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong)UICollectionViewFlowLayout * layout;


@end


static NSString * const MBLiveMainTopAudienceCollectionCellID = @"MBLiveMainTopAudienceCollectionCellID";

@implementation MBLiveMainTopCommonView

-(void)setAudienceDataArray:(NSMutableArray *)audienceDataArray {
    _audienceDataArray = audienceDataArray;
    [self.mainCollection reloadData];
    [self.audienceCountButton setTitle:[NSString stringWithFormat:@"%zd",_audienceDataArray.count] forState:UIControlStateNormal];
}

-(void)jumpUserCenter {
    if ([self.liveInfoModel.userInfo.ID isNotBlank]) {
        if (self.jumpBlock) {
            self.jumpBlock(self.liveInfoModel.userInfo.ID);
        }
    }
}

-(void)setLiveInfoModel:(MBLiveInfoModel *)liveInfoModel {
    _liveInfoModel = liveInfoModel;
    
    [self.liverHeadImageView sd_setImageWithURL:[NSURL URLWithString:_liveInfoModel.userInfo.headUrl] placeholderImage:kImage(@"")];
    self.liverNameLabel.text = _liveInfoModel.userInfo.nickname;
    [self.liverLoveCountButton setTitle:_liveInfoModel.clickPraise forState:UIControlStateNormal];
}

-(void)awakeFromNib {
    [super awakeFromNib];
}
-(void)layoutSubviews {
    [super layoutSubviews];
    [self initUI];
}

-(void)initUI {
    self.liverInfoView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    self.liverInfoView.layer.cornerRadius = 20.0f;
    self.liverInfoView.layer.masksToBounds = YES;
    self.liverHeadImageView.layer.cornerRadius = 20.0f;
    self.liverHeadImageView.layer.masksToBounds = YES;
    self.liverNameLabel.textColor = Color_White;
    self.liverNameLabel.font = kFont(12);
    self.liverLoveCountButton.spacing = 3.0f;
    [self.liverLoveCountButton setTitleColor:Color_EEEEEE forState:UIControlStateNormal];
    self.liverLoveCountButton.titleLabel.font = kFont(9);
    self.liverLoveCountButton.userInteractionEnabled = NO;
    self.mainCollection.collectionViewLayout = self.layout;
    self.mainCollection.showsVerticalScrollIndicator = NO;
    self.mainCollection.showsHorizontalScrollIndicator = NO;
    self.mainCollection.dataSource = self;
    self.mainCollection.delegate = self;
    self.mainCollection.backgroundColor = [UIColor clearColor];
    [self.mainCollection registerNib:[UINib nibWithNibName:NSStringFromClass([MBLiveMainTopAudienceCollectionCell class]) bundle:nil] forCellWithReuseIdentifier:MBLiveMainTopAudienceCollectionCellID];
    
    self.audienceCountButton.postion = MBImagePositionLeft;
    self.audienceCountButton.spacing = 2.0f;
    [self.audienceCountButton setTitleColor:Color_White forState:UIControlStateNormal];
    [self.audienceCountButton setBackgroundColor: [[UIColor blackColor] colorWithAlphaComponent:0.3]];
    self.audienceCountButton.titleLabel.font = kFont(9);
    self.audienceCountButton.layer.cornerRadius = 10.0f;
    self.audienceCountButton.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpUserCenter)];
    [self.liverInfoView addGestureRecognizer:tap];
}
#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.audienceDataArray.count;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    MBLiveViewerModel *audienceModel = self.audienceDataArray[indexPath.row];
    if (self.jumpBlock) {
        self.jumpBlock(audienceModel.ID);
    }
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBLiveMainTopAudienceCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBLiveMainTopAudienceCollectionCellID forIndexPath:indexPath];
    cell.audienceModel = self.audienceDataArray[indexPath.row];
    return cell;
}
-(void)jumpRemoveAndBlackClick {
    if (self.jumpRemoveAndBlackBlock) {
        self.jumpRemoveAndBlackBlock();
    }
}


- (IBAction)quiteButtonClick:(UIButton *)sender {
    if (self.quitBlock) {
        self.quitBlock();
    }
}
- (IBAction)audienceCountButtonClick:(MBCustomButton *)sender {
    if (self.jumpRemoveAndBlackBlock) {
        self.jumpRemoveAndBlackBlock();
    }
}

-(UICollectionViewFlowLayout *)layout
{
    if (_layout == nil) {
        UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat cellWidth = 50;
        layout.minimumLineSpacing = 0;
        CGFloat cellHeight = 50;
        layout.itemSize = CGSizeMake(cellWidth, cellHeight);
        _layout = layout;
    }
    return _layout;
}


@end
