//
//  MBPresentGiftCountPopView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/4/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBPresentGiftCountPopViewTableViewcell:UITableViewCell

@property(nonatomic, strong)UILabel *leftLabel;
@property(nonatomic, strong)UILabel *rightLabel;
@property(nonatomic, strong)UIView *lineView;
@property(nonatomic, copy)NSString *leftString;
@property(nonatomic, copy)NSString *rightString;


@end

typedef void(^SelectGiftCountBlock)(NSString *);
typedef void(^SelectGiftOtherCountBlock)(void);

@interface MBPresentGiftCountPopView : UIView
@property (weak, nonatomic) IBOutlet MBBaseTableview *mainTableView;
@property (nonatomic, copy) SelectGiftCountBlock selectGiftCountBlock;
@property (nonatomic, copy) SelectGiftOtherCountBlock selectGiftOtherCountBlock;

@end

NS_ASSUME_NONNULL_END
