//
//  MBLiveMainTopCommonView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLiveInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^QuitButtonBlockHandler)(void);
typedef void(^JumpCenterBlockHandler)(NSString *userID);
typedef void(^JumpRemoveAndBlackBlockHandler)(void);

@interface MBLiveMainTopCommonView : UIView
@property (weak, nonatomic) IBOutlet UIView *liverInfoView;
@property (weak, nonatomic) IBOutlet UIImageView *liverHeadImageView;
@property (weak, nonatomic) IBOutlet UILabel *liverNameLabel;
@property (weak, nonatomic) IBOutlet MBCustomButton *liverLoveCountButton;

@property (weak, nonatomic) IBOutlet MBCustomButton *audienceCountButton;
@property (weak, nonatomic) IBOutlet UICollectionView *mainCollection;

@property (weak, nonatomic) IBOutlet UIButton *quiteLiveButton;


@property (copy, nonatomic) QuitButtonBlockHandler quitBlock;
@property (nonatomic, strong)NSMutableArray *audienceDataArray;
@property (nonatomic, strong)MBLiveInfoModel *liveInfoModel;
@property (copy, nonatomic) JumpCenterBlockHandler jumpBlock;
@property (copy, nonatomic) JumpRemoveAndBlackBlockHandler jumpRemoveAndBlackBlock;


@end

NS_ASSUME_NONNULL_END
