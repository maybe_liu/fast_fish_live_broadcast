//
//  MBHomeLocalController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLocalController.h"
#import "MBHomeLocalCollectionViewCell.h"
#import "XRWaterfallLayout.h"
#import "MBHomeLocalListModel.h"
#import "MBLocation.h"
#import "MBShortVideoPlayController.h"
@interface MBHomeLocalController ()<UICollectionViewDataSource,XRWaterfallLayoutDelegate,UICollectionViewDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;
@end

static NSString * const MBHomeLocalCollectionViewCellID = @"MBHomeLocalCollectionViewCellID";
static NSString * const MBHomeLocalHeadViewsID = @"MBHomeLocalHeadViewsID";
static CGFloat const  HeadViewHeight = 32;

@implementation MBHomeLocalController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = RandomColor;
    [self createUI];
    [self createCustomNav];
    [self layoutChildViews];
}
-(void)zj_viewWillAppearForIndex:(NSInteger)index {
    [self loadData];
    MB_WeakSelf(self);
    [[MBLocation shareMBLocation] getCurrentAddress:^(NSDictionary *dic) {
        MBLog(@"%@",dic);
        [weakself loadData];
    }];
}

-(void)loadData {
//    NSDictionary *param = @{
//                            @"userId":@"1",
//                            @"pageNo":@(self.pageNo),
//                            @"city":@"广州",
//                            @"longitude":@"113.4133",
//                            @"latitude":@"23.137518",
//                            };
    NSMutableDictionary *param  = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    if ([MBLocation shareMBLocation].city.length > 0) {
        [param setValue:[MBLocation shareMBLocation].city forKey:@"city"];
        [param setValue:@([MBLocation shareMBLocation].longitude) forKey:@"longitude"];
        [param setValue:@([MBLocation shareMBLocation].latitude) forKey:@"latitude"];
    }
    MB_WeakSelf(self);

    [MBHttpRequset requestWithUrl:kTradeid_home_getLocalData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeLocalListModel *model = [MBHomeLocalListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.localList];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    }];
}

-(void)loadMoreData {
    NSMutableDictionary *param  = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    if ([MBLocation shareMBLocation].city.length > 0) {
        [param setValue:[MBLocation shareMBLocation].city forKey:@"city"];
        [param setValue:@([MBLocation shareMBLocation].longitude) forKey:@"longitude"];
        [param setValue:@([MBLocation shareMBLocation].latitude) forKey:@"latitude"];
    }
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getLocalData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeLocalListModel *model = [MBHomeLocalListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray = model.localList;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    }];
}

-(void)createUI {
    
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
    layout.delegate = self;
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeLocalCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBHomeLocalCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    MB_WeakSelf(self);
    self.collectionView.mj_header = [UPCustomRefresh getGifHeader:^{
        weakself.pageNo = 1;
        [weakself loadData];
    }];
    self.collectionView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo++;
        [weakself loadMoreData];
    }];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.collectionView.ly_emptyView = emptyView;
}

-(void)layoutChildViews {
//    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.height.mas_equalTo(HeadViewHeight);
//        make.top.left.right.equalTo(self.view);
//    }];
}

#pragma mark - event
-(void)ReLocation {
    
}

#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeLocalCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBHomeLocalCollectionViewCellID forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];

    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBHomeLocalModel *model = self.dataArray[indexPath.row];
    CGFloat scale = itemWidth/model.wide;
    CGFloat height = model.high *scale;
    return height;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeLocalModel *model = self.dataArray[indexPath.row];
    if ([model.type isEqualToString:@"1"]) {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"1";
        vc.coverImage = model.coverUrl;
        [self.navigationController pushViewController:vc animated:YES];
        
    }else {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"2";
        vc.coverImage = model.coverUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)createCustomNav {
    self.pageNo = 1;
}


#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
