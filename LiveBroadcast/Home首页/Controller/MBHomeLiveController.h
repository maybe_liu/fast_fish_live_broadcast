//
//  MBHomeLiveController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "ZJScrollPageViewDelegate.h"

NS_ASSUME_NONNULL_BEGIN

/**
 首页 直播主控制器
 */
@interface MBHomeLiveController : MBBaseController<ZJScrollPageViewChildVcDelegate>

@end

NS_ASSUME_NONNULL_END
