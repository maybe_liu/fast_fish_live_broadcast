//
//  MBHomeWaterfallController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "JXCategoryListContainerView.h"
NS_ASSUME_NONNULL_BEGIN

/**
 首页流控制器
 */
@interface MBHomeWaterfallController : MBBaseController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
