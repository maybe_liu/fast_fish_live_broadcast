//
//  MBHomeLiveController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeLiveController.h"
#import "XRWaterfallLayout.h"
#import "MBHomeLiveCollectionViewCell.h"
#import "MBHomeLiveListModel.h"
#import "MBLivePlayerController.h"

@interface MBHomeLiveController ()<UICollectionViewDataSource,UICollectionViewDelegate,XRWaterfallLayoutDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) BOOL isLogin;

@end

static NSString * const MBHomeLiveCollectionViewCellID = @"MBHomeLiveCollectionViewCellID";

@implementation MBHomeLiveController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = RandomColor;
    [self createUI];
    [self createCustomNav];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoginState:) name:MBIsLoginNotification object:nil];
}


#pragma mark - NSNotification
-(void)LoginState:(NSNotification *)noti {
    if ([[noti.userInfo objectForKey:@"isFlag"] isEqualToString:MBLoginFLag]) {
        self.isLogin = YES;
    }else {
        self.isLogin = NO;
    }
}

-(void)zj_viewWillAppearForIndex:(NSInteger)index {
    [self loadData];
}

-(void)loadData {
    [self.collectionView ly_startLoading];

//    NSDictionary *param = @{
//                            @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
//                            @"pageNo":@(self.pageNo)
//                            };
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getLiveData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeLiveListModel *model = [MBHomeLiveListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.liveInfoList];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    }];
}

-(void)loadMoreData {
    [self.collectionView ly_startLoading];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getLiveData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeLiveListModel *model = [MBHomeLiveListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray = model.liveInfoList;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    }];
}

-(void)createUI {
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
    layout.delegate = self;
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
//    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeLiveCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBHomeLiveCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    MB_WeakSelf(self);
    self.collectionView.mj_header = [UPCustomRefresh getGifHeader:^{
        weakself.pageNo = 1;
        [weakself loadData];
    }];
    self.collectionView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo++;
        [weakself loadMoreData];
    }];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.collectionView.ly_emptyView = emptyView;
    self.collectionView.ly_emptyView.autoShowEmptyView = NO;
}

#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeLiveCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBHomeLiveCollectionViewCellID forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBHomeLiveModel *model = self.dataArray[indexPath.row];
    CGFloat scale = itemWidth/model.wide;
    CGFloat height = model.high *scale;
    return height;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isLogin) {
        MBHomeLiveModel *model = self.dataArray[indexPath.row];
        MBLivePlayerController *vc = [[MBLivePlayerController alloc]init];
        vc.rtmpString = model.pullUrl;
        vc.isLivePlay = YES;
        vc.liveID = model.ID;
        vc.streamID = model.streamId;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        MBLoginController *loginVC = [[MBLoginController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
}


-(void)createCustomNav {
    self.pageNo = 1;
}

#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
