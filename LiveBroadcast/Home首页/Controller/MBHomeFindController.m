//
//  MBHomeFindController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeFindController.h"
#import "MBHomeFindCollectionViewCell.h"
#import "XRWaterfallLayout.h"
#import "MBHomeFindListModel.h"
#import "MBShortVideoPlayController.h"

@interface MBHomeFindController ()<UICollectionViewDataSource,UICollectionViewDelegate,XRWaterfallLayoutDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end

static NSString * const MBHomeFindCollectionViewCellID = @"MBHomeFindCollectionViewCellID";

@implementation MBHomeFindController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = RandomColor;
    [self createUI];
    [self createCustomNav];
}

-(void)zj_viewWillAppearForIndex:(NSInteger)index {
    [self loadData];
}

-(void)loadData {
    NSDictionary *param = @{
//                            @"userId":@"1",
                            @"pageNo":@(self.pageNo)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getFoundData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeFindListModel *model = [MBHomeFindListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.list];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    }];
}

-(void)loadMoreData {
    NSDictionary *param = @{
//                            @"userId":@"1",
                            @"pageNo":@(self.pageNo)
                            };
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getFoundData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeFindListModel *model = [MBHomeFindListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray = model.list;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_footer endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_footer endRefreshing];
        [weakself.collectionView ly_endLoading];

    }];
}
-(void)createUI {
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
    //    [layout setColumnSpacing:5 rowSpacing:0 sectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    layout.delegate = self;
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
//    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeFindCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBHomeFindCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    MB_WeakSelf(self);
    self.collectionView.mj_header = [UPCustomRefresh getGifHeader:^{
        weakself.pageNo = 1;
        [weakself loadData];
    }];
    self.collectionView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo++;
        [weakself loadMoreData];
    }];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.collectionView.ly_emptyView = emptyView;
}

#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeFindCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBHomeFindCollectionViewCellID forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBHomeFindModel *model = self.dataArray[indexPath.row];
    CGFloat scale = itemWidth/model.wide;
    CGFloat height = model.high *scale;
    return height;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeFindModel *model = self.dataArray[indexPath.row];
    if ([model.type isEqualToString:@"1"]) {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"1";
        vc.coverImage = model.coverUrl;

        [self.navigationController pushViewController:vc animated:YES];

    }else {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"2";
        vc.coverImage = model.coverUrl;

        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)createCustomNav {
    self.pageNo = 1;
}

#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
@end
