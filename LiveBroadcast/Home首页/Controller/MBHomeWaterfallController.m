//
//  MBHomeWaterfallController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeWaterfallController.h"
#import "MBHomeWaterfallCollectionViewCell.h"
#import "XRWaterfallLayout.h"

@interface MBHomeWaterfallController ()<UICollectionViewDataSource,XRWaterfallLayoutDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@end

static NSString * const MBHomeWaterfallCollectionViewCellID = @"MBHomeWaterfallCollectionViewCellID";
@implementation MBHomeWaterfallController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = RandomColor;
    [self createUI];
    [self createCustomNav];
}

-(void)createUI {
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
//    [layout setColumnSpacing:5 rowSpacing:0 sectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    layout.delegate = self;
//    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeWaterfallCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBHomeWaterfallCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
}

#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 100;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeWaterfallCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBHomeWaterfallCollectionViewCellID forIndexPath:indexPath];
    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBLog(@"宽度%lf",itemWidth);
    return 218.0f;
}

-(void)createCustomNav {
    
}



- (UIView *)listView {
    return self.view;
}
//可选使用，列表显示的时候调用
- (void)listDidAppear {
    
}

//可选使用，列表消失的时候调用
- (void)listDidDisappear {
    
}
@end
