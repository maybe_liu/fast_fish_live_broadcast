//
//  MBHomeAttentionController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHomeAttentionController.h"
#import "MBHomeAttentionCollectionViewCell.h"
#import "XRWaterfallLayout.h"
#import "MBHomeAttentionListModel.h"
#import "MBShortVideoPlayController.h"
@interface MBHomeAttentionController ()<UICollectionViewDataSource,UICollectionViewDelegate,XRWaterfallLayoutDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, assign) BOOL isLogin;
@end

static NSString * const MBHomeAttentionCollectionViewCellID = @"MBHomeAttentionCollectionViewCellID";

@implementation MBHomeAttentionController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = RandomColor;
    [self createUI];
    [self createCustomNav];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoginState:) name:MBIsLoginNotification object:nil];
}
-(void)zj_viewWillAppearForIndex:(NSInteger)index {
    [self loadData];
    [self checkISLogin];
}

-(void)checkISLogin {
    if (!self.isLogin) {
        MBLoginController *loginVC = [[MBLoginController alloc]init];
        [self presentViewController:loginVC animated:YES completion:nil];
    }
}

#pragma mark - NSNotification
-(void)LoginState:(NSNotification *)noti {
    if ([[noti.userInfo objectForKey:@"isFlag"] isEqualToString:MBLoginFLag]) {
        self.isLogin = YES;
    }else {
        self.isLogin = NO;
    }
}




-(void)loadData {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getFocusData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeAttentionListModel *model = [MBHomeAttentionListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.userVideos];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    }];
}

-(void)loadMoreData {
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    if ([[MBUserModel sharedMBUserModel].userInfo.userId isNotBlank]) {
        [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    }
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kTradeid_home_getFocusData setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBHomeAttentionListModel *model = [MBHomeAttentionListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray = model.userVideos;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_footer endRefreshing];
        [weakself.collectionView ly_endLoading];

    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_footer endRefreshing];
        [weakself.collectionView ly_endLoading];
    }];
}

-(void)createUI {
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
    //    [layout setColumnSpacing:5 rowSpacing:0 sectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    layout.delegate = self;
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
//    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeAttentionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBHomeAttentionCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    MB_WeakSelf(self);
    self.collectionView.mj_header = [UPCustomRefresh getGifHeader:^{
        weakself.pageNo = 1;
        [weakself loadData];
    }];
    self.collectionView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo++;
        [weakself loadMoreData];
    }];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.collectionView.ly_emptyView = emptyView;
}

#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeAttentionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBHomeAttentionCollectionViewCellID forIndexPath:indexPath];
    cell.model = self.dataArray[indexPath.row];
    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBHomeAttentionModel *model = self.dataArray[indexPath.row];
    CGFloat scale = itemWidth/model.wide;
    CGFloat height = model.high *scale;
    return height;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeAttentionModel *model = self.dataArray[indexPath.row];
    if ([model.type isEqualToString:@"1"]) {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"1";
        vc.coverImage = model.coverUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }else {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"2";
        vc.coverImage = model.coverUrl;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)createCustomNav {
    self.pageNo = 1;
}

#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

@end
