//
//  MBHomeFindController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "ZJScrollPageViewDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBHomeFindController : MBBaseController<ZJScrollPageViewChildVcDelegate>

@end

NS_ASSUME_NONNULL_END
