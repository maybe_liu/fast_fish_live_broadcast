//
//  MBSTextView.h
//  MakingIt
//
//  Created by Booles on 14/11/6.
//  Copyright (c) 2014年 MakingIt. All rights reserved.

#import <UIKit/UIKit.h>

@protocol MITextViewDelegate;

@interface MITextView : UITextView{
    NSString *placeholder;
    UIColor *placeholderColor;
    
@private
    UILabel *placeHolderLabel;
}

@property (nonatomic, retain) UILabel *placeHolderLabel;
@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@property(nonatomic,unsafe_unretained) id<MITextViewDelegate> textViewDelegate;
@end

//@protocol MBTextViewDelegate <NSObject>
//
//@optional
//- (void)textViewDidCancelEditing:(MBSTextView *)textField; //点击键盘上的“取消”按钮。
//- (void)textViewDidFinsihEditing:(MBSTextView *)textField; //点击键盘框上的"确认"按钮。
//
//@end