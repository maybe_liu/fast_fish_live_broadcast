//
//  MBActivityCollectionCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/27.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBActivityCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UIButton *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIButton *likeLbl;
@property (weak, nonatomic) IBOutlet UIButton *lookLbl;
@property (weak, nonatomic) IBOutlet UIImageView *flagImg;

@end

NS_ASSUME_NONNULL_END
