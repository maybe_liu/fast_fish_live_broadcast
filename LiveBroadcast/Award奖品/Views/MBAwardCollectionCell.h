//
//  MBAwardCollectionCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBAwardCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@end

NS_ASSUME_NONNULL_END
