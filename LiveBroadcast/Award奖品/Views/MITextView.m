//
//  MBSTextView.m
//  MakingIt
//
//  Created by Booles on 14/11/6.
//  Copyright (c) 2014年 MakingIt. All rights reserved.

#import "MITextView.h"

@implementation MITextView
@synthesize placeHolderLabel;
@synthesize placeholder;
@synthesize placeholderColor;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setPlaceholder:@""];
    [self setPlaceholderColor:[UIColor lightGrayColor]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    self.returnKeyType = UIReturnKeyDone;
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setPlaceholder:@""];
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
        
//        self.inputAccessoryView = [[MBAccessoryView alloc] initWithDelegate:self];

//        CGSize size=[@" " sizeWithFont:[UIFont systemFontOfSize:12]];
//        [self setFont:[UIFont systemFontOfSize:20]];
//        CGSize size2=[@"uuuuuuuuuupppppppppp" sizeWithFont:[UIFont systemFontOfSize:20]];
//        NSLog(@"%f",self.contentOffset.x);
//        [self setContentOffset:CGPointMake(0, 0)];
//        
//        [self setBackgroundColor:[UIColor greenColor]];
//        
//        UILabel *biaoLbl=[[UILabel alloc] initWithFrame:CGRectMake(size.width+size2.width, 0, 2, frame.size.height)];
//        [biaoLbl setBackgroundColor:[UIColor blackColor]];
//        [self addSubview:biaoLbl];
    }
    return self;
}

- (void)textChanged:(NSNotification *)notification
{
    if([[self placeholder] length] == 0)
    {
        return;
    }
    
    if([[self text] length] == 0)
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    else
    {
        [[self viewWithTag:999] setAlpha:0];
    }
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
}

- (void)drawRect:(CGRect)rect
{
    if( [[self placeholder] length] > 0 )
    {
        if ( placeHolderLabel == nil )
        {
            placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,8,self.bounds.size.width - 20,0)];
            placeHolderLabel.lineBreakMode = UILineBreakModeWordWrap;
            placeHolderLabel.numberOfLines = 0;
            placeHolderLabel.font = [UIFont systemFontOfSize:14];
            placeHolderLabel.backgroundColor = [UIColor clearColor];
            placeHolderLabel.textColor = self.placeholderColor;
            placeHolderLabel.alpha = 0;
            placeHolderLabel.tag = 999;
            [self addSubview:placeHolderLabel];
        }
        
        placeHolderLabel.text = self.placeholder;
        [placeHolderLabel sizeToFit];
        [self sendSubviewToBack:placeHolderLabel];
    }
    
    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    
    [super drawRect:rect];
}

@end
