//
//  MBAwardMainController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBAwardMainController.h"
#import "MBAwardCollectionCell.h"
#import "MBAWardDetailController.h"
#import "MBProvideController.h"
#import "SDCycleScrollView.h"
@interface MBAwardMainController ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,SDCycleScrollViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *view;
    UICollectionView *_collectionView;
    NSArray *labels;
    UILabel *activityLbl;
    UILabel *activityDetail;
    
    UIImageView *topView;
}
@property(nonatomic, strong)UIView *rightIMButton;
@property (nonatomic,strong)NSMutableArray *imagesURLStrings;
@property (nonatomic, weak)SDCycleScrollView *cycleScrollView;
@end

@implementation MBAwardMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightIMButton];
    // 初始化控件
    [self initView];
    labels = [NSArray array];
//    NSDictionary *dict = @{
//                           @""
//
//                           };
}

- (NSMutableArray *)imagesURLStrings
{
    if (_imagesURLStrings == nil) {
        _imagesURLStrings = [NSMutableArray array];
    }
    return _imagesURLStrings;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MBHttpRequset requestWithUrl:kTradeid_activity_getActivityLabel setParams:@{} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self->labels = AJson[@"labelAllList"];
        self->_collectionView.height = ((IPHONE_SCREEN_WIDTH - 20)/2) * (self->labels.count/2 + self->labels.count%2) + ((self->labels.count/2) + self->labels.count%2 - 1)* 20;
        self->_scrollView.contentSize = CGSizeMake(0, 400 + self->_collectionView.height);
        [self->_collectionView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    [MBHttpRequset requestWithUrl:kTradeid_activity_getActivityRule setParams:@{} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSArray *arr = AJson[@"ruleList"];
        if (arr.count != 0) {
            NSDictionary *dict = [arr objectAtIndex:0];
            self->activityLbl.text = dict[@"ruleTitle"];
            self->activityDetail.text = dict[@"ruleContent"];
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    [MBHttpRequset requestWithUrl:kTradeid_activity_activityBannerList setParams:@{} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSArray *arr = AJson[@"list"];
        NSLog(@"kTradeid_activity_getActivityBannerByLabelId = %@",arr);
        if (arr.count != 0) {
            for (int i = 0; i < arr.count; i ++) {
                NSDictionary *dict = arr[i];
                [self.imagesURLStrings addObject:dict[@"image"]];
            }
            self.cycleScrollView.imageURLStringsGroup = self.imagesURLStrings;
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

- (UIView *)rightIMButton
{
    if (_rightIMButton == nil) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(10, 0, 80, 30);
        MBButton *shareBtn = [MBButton buttonWithType:UIButtonTypeCustom];
        shareBtn.frame = CGRectMake(10, 0, 80, 30);
        [shareBtn setTitle:@"提供奖品" forState:UIControlStateNormal];
        [shareBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateNormal];
        [view addSubview:shareBtn];
        
        shareBtn.miBtnBlock = ^(){
            MBProvideController *provideVC = [[MBProvideController alloc]init];
            [self.navigationController pushViewController:provideVC animated:YES];
        };
        
        _rightIMButton = view;
    }
    return _rightIMButton;
}


// 初始化控件
- (void)initView
{
    _scrollView = [[MBBaseScrollView alloc] initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT)];
    _scrollView.backgroundColor = self.view.backgroundColor;
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    self.view = _scrollView;
    // 用户顶部图
//    topView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,375,180)];
//    topView.contentMode = UIViewContentModeScaleAspectFill;
//    topView.clipsToBounds = YES;
//    topView.userInteractionEnabled=YES;
//    //初始化一个手势
//    UITapGestureRecognizer *tapGesturRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topViewClick)];
//    //为图片添加手势
//    [topView addGestureRecognizer:tapGesturRecognizer];
//    [_scrollView addSubview:topView];
    
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 180) delegate:self placeholderImage:[UIImage imageNamed:@"bg"]];
    [_scrollView addSubview:cycleScrollView];
    self.cycleScrollView = cycleScrollView;
    
    // centerView
    UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(0, cycleScrollView.bottom, IPHONE_SCREEN_WIDTH, 130)];
    centerView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:centerView];
    
    activityLbl = [[UILabel alloc]init];
    activityLbl.frame = CGRectMake(20, 10, IPHONE_SCREEN_WIDTH - 40, 20);
    activityLbl.text = @"活动规则";
    activityLbl.font = [UIFont systemFontOfSize:16.0f];
    [centerView addSubview:activityLbl];
    
    activityDetail = [[UILabel alloc]init];
    activityDetail.frame = CGRectMake(20, activityLbl.bottom + 10, IPHONE_SCREEN_WIDTH - 40, 90);
    activityDetail.text = @"短视频！短视频！短视频！重要的事情还得说三遍。如果你不知道短视频是什么就请绕道看看别的招聘信息吧！懂短视频运营玩法的来，懂短视频编导还能研发的...";
    activityDetail.font = [UIFont systemFontOfSize:14.0f];
    activityDetail.numberOfLines = 0;
    activityLbl.textColor = kColorRGBA(51, 51, 51, 1);
    [centerView addSubview:activityDetail];
    
    UILabel *activity = [[UILabel alloc]init];
    activity.frame = CGRectMake(0, centerView.bottom + 20, IPHONE_SCREEN_WIDTH, 40);
    activity.text = @"   活动";
    activity.backgroundColor = [UIColor whiteColor];
    activity.font = [UIFont systemFontOfSize:16.0f];
    [_scrollView addSubview:activity];
    
    // 作品
    UICollectionViewFlowLayout *fl = [[UICollectionViewFlowLayout alloc]init];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, activity.bottom, IPHONE_SCREEN_WIDTH, 0) collectionViewLayout:fl];
    _collectionView.delegate = self;
    
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = activity.backgroundColor;
    _collectionView.scrollEnabled = NO;
    [_scrollView addSubview: _collectionView];

    //布局
    fl.minimumInteritemSpacing = 0;
    fl.minimumLineSpacing = 5;
    [_collectionView registerNib:[UINib nibWithNibName:@"MBAwardCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"AWARDCONLECTIONCELL"];
    //
    //
}

- (void)topViewClick
{
    NSLog(@"头部点击事件");
}




//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
////        NSLog(@"  x %f   y  %f ",scrollView.contentOffset.x,scrollView.contentOffset.y);
//    if (scrollView.contentOffset.y  >= 200) {
//        _collectionView.scrollEnabled = YES;
//    }else{
//        _collectionView.scrollEnabled = NO;
//    }
//}


#pragma mark - UICollectionView delegate & dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return labels.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((IPHONE_SCREEN_WIDTH - 20)/2, (IPHONE_SCREEN_WIDTH - 20)/2);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBAwardCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AWARDCONLECTIONCELL" forIndexPath:indexPath];
    //    NSArray *arr = self.worksD[@"worksList"];
    NSDictionary *dict = labels[indexPath.row];
    [cell.imageV sd_setImageWithURL:[NSURL URLWithString:dict[@"labelImage"]] placeholderImage:[UIImage imageNamed:@"bg.png"]];
    cell.titleLbl.text = dict[@"labelName"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MBAWardDetailController *awardDetailVC = [[MBAWardDetailController alloc]init];
    NSDictionary *dict = labels[indexPath.row];
    awardDetailVC.title = [NSString stringWithFormat:@"%@活动专区",dict[@"labelName"]];
    awardDetailVC.dictDatas = dict;
    [self.navigationController pushViewController:awardDetailVC animated:YES];
}
@end
