//
//  MBSelectCategoryController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSelectCategoryController.h"

@interface MBSelectCategoryController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,strong)NSIndexPath *indexPath;

@property (nonatomic,strong)NSArray *datas;

@end

@implementation MBSelectCategoryController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.datas = [NSArray array];
    self.title = @"选择奖品类别";
    // Do any additional setup after loading the view from its nib.
    
    [MBHttpRequset requestWithUrl:kTradeid_activity_getActivityLabel setParams:@{} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.datas = AJson[@"labelAllList"];
        self.tableView.height = self.datas.count * 60;
        [self.tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"MBSelectCategoryControllerCELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idetifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dict = self.datas[indexPath.row];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:dict[@"labelName"] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    cell.textLabel.attributedText = string;
//    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"labelImage"]]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.indexPath != indexPath) {
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:self.indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;//(cell.accessoryType == UITableViewCellAccessoryCheckmark)?UITableViewCellAccessoryNone:UITableViewCellAccessoryCheckmark;
    self.indexPath = indexPath;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_SelectCategoryVCSelectBlock) {
        _SelectCategoryVCSelectBlock(self.datas[self.indexPath.row]);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
