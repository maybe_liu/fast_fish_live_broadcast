//
//  MBActivityDetailsController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/27.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBActivityDetailsController : MBBaseController

@property (nonatomic,strong)NSDictionary *acitivityData;

@end

NS_ASSUME_NONNULL_END
