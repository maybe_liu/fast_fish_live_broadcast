//
//  MBAWardDetailController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/27.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBAWardDetailController.h"
#import "MBTaskMainCell.h"
#import "MBActivityDetailsController.h"
#import "SDCycleScrollView.h"
@interface MBAWardDetailController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,SDCycleScrollViewDelegate>
{
    UITableView *_tableView;
    UIView *view;
}
@property (nonatomic,strong)NSArray *activityDatas;
@property (nonatomic,strong)NSMutableArray *imagesURLStrings;

@end

@implementation MBAWardDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 50)];
    [self.view addSubview:view];
    
    self.activityDatas = [NSArray array];
    
//    UIImageView *topView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,375,180)];
////    topView.contentMode = UIViewContentModeCenter;
//    topView.clipsToBounds = YES;
//    [topView sd_setImageWithURL:[NSURL URLWithString:self.dictDatas[@"labelImage"]] placeholderImage:[UIImage imageNamed:@"bg.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//        UIImage *image1=[image stretchableImageWithLeftCapWidth:image.size.width*0.001 topCapHeight:image.size.height*0.001];
//        topView.image = image1;
//    }];
//    [view addSubview:topView];
    
//    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 180) delegate:self placeholderImage:[UIImage imageNamed:@"bg"]];
//    cycleScrollView.imageURLStringsGroup = @[self.dictDatas[@"labelImage"]];
//    [view addSubview:cycleScrollView];
    
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"活动";
//    label.frame = CGRectMake(10, cycleScrollView.bottom + 20, IPHONE_SCREEN_WIDTH - 20, 30);
    label.frame = CGRectMake(10,  10, IPHONE_SCREEN_WIDTH - 20, 30);
    [view addSubview:label];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(view.left, 0, IPHONE_SCREEN_WIDTH, self.view.height - 44) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    NSDictionary *dict = @{@"labelId":[NSString stringWithFormat:@"%@",self.dictDatas[@"id"]],@"pageNo":@"1"};
    [MBHttpRequset requestWithUrl:kTradeid_activity_getActivityList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.activityDatas = AJson[@"ActivityList"];
        [self->_tableView reloadData];
        if (self.activityDatas.count == 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"暂无活动" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:YES];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
}


#pragma mark - Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.activityDatas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"TASKMAINCELL";
    MBTaskMainCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBTaskMainCell" owner:nil options:nil];
        cell = [nibs lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.timeLbl.textAlignment = NSTextAlignmentRight;
        cell.titileLbl.font = [UIFont systemFontOfSize:14.0f];
        cell.detailLbl.font = [UIFont systemFontOfSize:14.0f];
    }
    NSDictionary *dict = self.activityDatas[indexPath.row];
    [cell.imagV sd_setImageWithURL:[NSURL URLWithString:dict[@"coverUrl"]] placeholderImage:[UIImage imageNamed:@"bg"]];
//    cell.titileLbl.text = dict[@"activityTitle"];
//    cell.detailLbl.text = dict[@"activityContent"];
    cell.titileLbl.text = [NSString stringWithFormat:@"活动时间： %@ - %@",[self getTime:dict[@"starttime"]],[self getTime:dict[@"endtime"]]];
    cell.detailLbl.text = [NSString stringWithFormat:@"已参与人数： %@",dict[@"joinCount"]];
    if ([dict[@"activityState"] isEqualToString:@"1"]) {
        cell.timeLbl.text = @"进行中";
        cell.timeLbl.textColor = kColorRGBA(255, 128, 0, 1);
    }else{
        cell.timeLbl.text = @"已结束";
        cell.timeLbl.textColor = cell.detailLbl.textColor;
    }
    
    
    return cell;
}

- (NSString *)getTime:(NSString *)time
{
    NSTimeInterval interval    =[time doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = self.activityDatas[indexPath.row];
    [MBHttpRequset requestWithUrl:kTradeid_activity_getActivityData setParams:@{@"activityId":dict[@"id"]} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBActivityDetailsController *actDetailVC = [[MBActivityDetailsController alloc]init];
        actDetailVC.acitivityData = AJson;
        [self.navigationController pushViewController:actDetailVC animated:YES];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

@end
