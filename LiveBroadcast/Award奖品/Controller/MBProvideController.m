//
//  MBProvideController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/31.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBProvideController.h"
#import "MITextView.h"
#import "MBSelectCategoryController.h"
@interface MBProvideController ()
{
    NSDictionary *_dict;
}
@property (weak, nonatomic) IBOutlet MITextView *textV;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTf;
@property (weak, nonatomic) IBOutlet UITextField *giftTF;
@property (weak, nonatomic) IBOutlet UITextField *selectTF;

@end

@implementation MBProvideController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.textV.placeholder = @"请输入奖品描述";
}
- (IBAction)selectCategory {
    MBSelectCategoryController *scVC = [[MBSelectCategoryController alloc]init];
    scVC.SelectCategoryVCSelectBlock = ^(NSDictionary *dict){
        self->_dict = dict;
        self.selectTF.text = self->_dict[@"labelName"];
    };
    [self.navigationController pushViewController:scVC animated:YES];
}

- (IBAction)commit {
    if ([MBUserModel sharedMBUserModel].userInfo.userId != nil) {
        if (self.nameTF.text.length == 0) {
            [MBProgressHUD showError:@"请输入您的姓名"];
            return;
        }
        if (self.phoneTf.text.length != 11) {
            [MBProgressHUD showError:@"请输入正确的联系电话"];
            return;
        }
        if (self.giftTF.text.length == 0) {
            [MBProgressHUD showError:@"请输入奖品名词"];
            return;
        }
        if (self.selectTF.text.length == 0) {
            [MBProgressHUD showError:@"请选择奖品所属类别"];
            return;
        }
        if (self.textV.text.length == 0) {
            [MBProgressHUD showError:@"请输入奖品描述"];
            return;
        }
        NSDictionary *dict = @{
                               @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                               @"name":self.nameTF.text,
                               @"phone":self.phoneTf.text,
                               @"prizeName":self.giftTF.text,
                               @"prizeIntroduce":self.textV.text,
                               @"prizeType":_dict[@"id"]
                               };
        [MBHttpRequset requestWithUrl:kTradeid_activity_offerPrize setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(id  _Nonnull AJson) {
            
        }];
    }else{// 请登录
        
    }

}
@end
