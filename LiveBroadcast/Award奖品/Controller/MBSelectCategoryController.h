//
//  MBSelectCategoryController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/1.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^MBSelectCategoryControllerSelect)(NSDictionary *);

@interface MBSelectCategoryController : MBBaseController

@property (nonatomic, copy)MBSelectCategoryControllerSelect SelectCategoryVCSelectBlock;

@end

NS_ASSUME_NONNULL_END
