//
//  MBBaseNavigationController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseNavigationController.h"

@interface MBBaseNavigationController ()<UINavigationControllerDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,weak) UIViewController *currentShowVC;

@end

@implementation MBBaseNavigationController

+(void)initialize
{
    //设置导航栏背景
    UINavigationBar * bar = [UINavigationBar appearance];
    [bar setBarTintColor:Color_White];
    bar.translucent = NO;
    //设置导航栏中间的标题
    NSMutableDictionary * attrs = [NSMutableDictionary dictionary];
    //导航栏字体H1大小，
    attrs[NSFontAttributeName] = kMFont(18);
    attrs[NSForegroundColorAttributeName] = Color_333333;
    [bar setTitleTextAttributes:attrs];
}
-(void)viewDidLoad {
    [super viewDidLoad];
    self.interactivePopGestureRecognizer.delegate = self;
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
- (instancetype)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.interactivePopGestureRecognizer.delegate = (id)self;
        self.delegate = self;
    }
    return self;
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count == 1)
        self.currentShowVC = Nil;
    else
        self.currentShowVC = viewController;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // 删除系统自带的tabBarButton
    for (UIView *tabBar in self.tabBarController.tabBar.subviews) {
        if ([tabBar isKindOfClass:[UIControl class]]) {
            [tabBar removeFromSuperview];
        }
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        return (self.currentShowVC == self.topViewController);
    }
    return YES;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if(self.viewControllers.count >0 ){
        viewController.hidesBottomBarWhenPushed = YES;
        //设置左上角按钮
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"nav_left"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"nav_left"] forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
//        [button sizeToFit];
        button.frame = (CGRect){{0,0},{40,40}};
        button.contentHorizontalAlignment =  UIControlContentHorizontalAlignmentLeft;
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:button];
        self.interactivePopGestureRecognizer.enabled = NO;
        
    }
    [super pushViewController:viewController animated:animated];
}

-(void)back {
    [self popViewControllerAnimated:YES];
}

@end
