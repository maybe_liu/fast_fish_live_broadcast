//
//  MBBaseController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import <ViewDeck/ViewDeck.h>

@interface MBBaseController ()<IIViewDeckControllerDelegate>

@end

@implementation MBBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewDeckController.delegate = self;
    self.view.backgroundColor = kColorRGBA(245, 245, 245, 1.0f);
    self.automaticallyAdjustsScrollViewInsets = NO;
    for (UIView *view in self.navigationController.navigationBar.subviews.firstObject.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]] && view.height < 1)
        {
            view.hidden = YES;
        }
    }
}
/*主要用来做数据埋点*/
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 数据归档
- (void)archiveDatas:(NSObject *)datas
{
    //沙盒ducument目录
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //完整的文件路径
    NSString *path = [docPath stringByAppendingPathComponent:@"userModel.archive"];
    
    //将数据归档到path文件路径里面
    BOOL success = [NSKeyedArchiver archiveRootObject:datas toFile:path];
    if(success == YES)NSLog(@"归档成功");
}

- (id)unArchiveDatas
{
    //沙盒ducument目录
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //完整的文件路径
    NSString *path = [docPath stringByAppendingPathComponent:@"userModel.archive"];
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)viewDeckController:(IIViewDeckController *)viewDeckController willOpenSide:(IIViewDeckSide)side
{
    //IIViewDeckSideRight
    if (side == IIViewDeckSideRight) {
        return NO;
    }
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

@end
