//
//  MBBaseController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBBaseController : UIViewController

// 数据归档
- (void)archiveDatas:(id)datas;

// 反归档
- (id)unArchiveDatas;

@end

NS_ASSUME_NONNULL_END
