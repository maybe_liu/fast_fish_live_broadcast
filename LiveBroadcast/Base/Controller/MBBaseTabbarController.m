//
//  MBBaseTabbarController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseTabbarController.h"
#import "MBTabBar.h"
#import "MBBaseNavigationController.h"
#import "MBIdleMainController.h"
#import "MBTaskMainController.h"
#import "MBHomeMainController.h"
#import "MBAwardMainController.h"

@interface MBBaseTabbarController ()<MBTabbarDelegate>
/**自定义的tabBar*/
@property (nonatomic, strong, readwrite) MBTabBar *customTabBar;
@end

@implementation MBBaseTabbarController

-(instancetype) init {
    if (self = [super init]) {
        /*
         必须先创建设置tabbar
         才能再添加TabBarButtonItem
         */
        [self setupTabBar];
        [self setupAllChildViewController];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupTabBar];
    [self.view setNeedsDisplay];
    [self.view layoutIfNeeded];
    for (UIView * view in self.tabBar.subviews){
        if (![view isKindOfClass:[MBTabBar class]]) {
            [view removeFromSuperview];
        }
    }
}
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    self.customTabBar.delegate = nil;
//    [self.customTabBar removeFromSuperview];
//    self.customTabBar = nil;
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
//    for (UIView * view in self.tabBar.subviews){
//        if (![view isKindOfClass:[MBTabBar class]]) {
//            [view removeFromSuperview];
//        }
//    }
}

- (void)setupTabBar {
    self.customTabBar.frame = self.tabBar.bounds;
    [self.tabBar addSubview:self.customTabBar];
    self.customTabBar.delegate = self;
    /*删除顶部线条*/
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setBackgroundImage:img];
    [self.tabBar setShadowImage:img];
}

-(void)setupAllChildViewController {
    MBHomeMainController *homeVC = [[MBHomeMainController alloc]init];
    MBAwardMainController *terminiVC = [[MBAwardMainController alloc]init];
    MBTaskMainController *customVC = [[MBTaskMainController alloc]init];
    MBIdleMainController *visaVC = [[MBIdleMainController alloc]init];

    [self setupChildViewController:homeVC title:@"首页" image:@"icon1" selectedImage:@"icon1_cur"];
    [self setupChildViewController:terminiVC title:@"奖品" image:@"icon2" selectedImage:@"icon2_cur"];
    [self setupChildViewController:customVC title:@"任务" image:@"icon3" selectedImage:@"icon3_cur"];
    [self setupChildViewController:visaVC title:@"闲置" image:@"icon5" selectedImage:@"icon5_cur"];
}

- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title image:(NSString *)image  selectedImage:(NSString *)selectedImage
{
    childVc.title = title;
    childVc.tabBarItem.image = [UIImage imageNamed:image];
    childVc.tabBarItem.selectedImage = [UIImage imageNamed:selectedImage];
    //使用nav包装控制器
    MBBaseNavigationController *nav = [[MBBaseNavigationController alloc] initWithRootViewController:childVc];
    [self addChildViewController:nav];
    
    [self.customTabBar addTabBarButtonWithItem:childVc.tabBarItem];
}
- (void)updateCurrentIndex:(NSInteger)index {
    MBBaseNavigationController *navVC = (MBBaseNavigationController *)self.selectedViewController;
    if (navVC.viewControllers.count > 1) {
        [navVC popToRootViewControllerAnimated:NO];
    }
    
    self.selectedIndex = index;
    self.customTabBar.selectedIndex = index;
}

#pragma - MBTabbarDelegate
- (void)tabBar:(MBTabBar *)tabBar didSelectButtonForm:(NSInteger)from To:(NSInteger)to {
//    CATransition* animation = [CATransition animation];
//    [animation setDuration:0.1f];
//    [animation setType:kCATransitionFade];
//    [animation setSubtype:kCATransitionFromRight];
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
//    [[self.view layer] addAnimation:animation forKey:@"switchView"];
    self.selectedIndex = to;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = Color_White;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - getter
-(MBTabBar *)customTabBar {
    if (_customTabBar == nil) {
        _customTabBar = [[MBTabBar alloc]init];
        _customTabBar.backgroundColor = Color_White;
    }
    return _customTabBar;
}

@end
