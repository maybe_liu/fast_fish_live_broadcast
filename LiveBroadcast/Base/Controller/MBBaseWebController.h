//
//  MBBaseWebController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBBaseWebController : MBBaseController
@property(nonatomic, copy)NSString *webURL;
@property(nonatomic, copy)NSString *webNavTitle;
@end

NS_ASSUME_NONNULL_END
