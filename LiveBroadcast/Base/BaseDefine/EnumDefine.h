//
//  EnumDefine.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#ifndef EnumDefine_h
#define EnumDefine_h

/*用户角色*/
typedef NS_ENUM(NSInteger,MBUserPart){
    MBUserPartAudience                = 0, /*观众*/
    MBUserPartAnchor                  = 1 /*主播*/
};

typedef NS_ENUM(NSInteger,MBShortVideoState){
    MBShortVideoStateIng             = 0, /*正在录制*/
    MBShortVideoStatePause           = 1, /*暂停录制*/
    MBShortVideoStateComplete        = 2, /*完成录制*/
    MBShortVideoStateStart           = 3, /*未开始录制*/
};

/**短视频类型*/
typedef NS_ENUM(NSInteger,MBShortVideoType) {
    MBShortVideoTypeImage            = 0, /*图片*/
    MBShortVideoTypeVideo            = 1, /*视频*/
};

/**短视频类型*/
typedef NS_ENUM(NSInteger,MBShortVideoEntityType) {
    MBShortVideoEntityTypeNormal            = 0, /*普通视频*/
    MBShortVideoEntityTypeIdle              = 1, /*二手视频*/
    MBShortVideoEntityTypeActivity           = 2, /*活动视频*/
};

#endif /* EnumDefine_h */
