//
//  UIDefine.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#ifndef UIDefine_h
#define UIDefine_h

#define APP_delegate  ((AppDelegate *)[[UIApplication sharedApplication] delegate])


#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//系统版本
#define IOS7  (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) && ([[UIDevice currentDevice].systemVersion floatValue] < 8.0))
#define IOS8  (([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) && ([[UIDevice currentDevice].systemVersion floatValue] < 9.0))
#define IOS9  (([[UIDevice currentDevice].systemVersion floatValue] >= 9.0) && ([[UIDevice currentDevice].systemVersion floatValue] < 10.0))
#define IOS10  (([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) && ([[UIDevice currentDevice].systemVersion floatValue] < 11.0))



#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height

/*状态栏高度*/
#define kNavStatusHeight     [[UIApplication sharedApplication] statusBarFrame].size.height
/*导航栏高度*/
#define kNavBarHeight        44.0f
/*状态栏 + 导航栏*/
#define kNavHeight           (kNavStatusHeight + kNavBarHeight)

#define kChange(x) *(x * kScreenWidth/375.0f)
#define kScaleX [UIScreen mainScreen].bounds.size.width / 375
#define kScaleY [UIScreen mainScreen].bounds.size.height / 667



#define IS_IPHONE_4_OR_LESS (IS_IPHONE && kScreenHeight < 568.0)
#define IS_IPHONE_5         (IS_IPHONE && kScreenHeight == 568.0)
#define IS_IPHONE_6         (IS_IPHONE && kScreenHeight == 667.0)
#define IS_IPHONE_6P        (IS_IPHONE && kScreenHeight == 736.0)
#define IS_IPHONE_X         (IS_IPHONE && (kScreenHeight == 812.0 || kScreenHeight == 896))

/* tabbar 高度*/
#define kTabHeight           (IS_IPHONE_X ? 83.0 : 49.0)

/* tabbar 安全区域底部间隙*/
#define kTabSafeBottomMargin (IS_IPHONE_X ? 34.f : 0.f)

// 定义颜色
#define RGBCOLOR(r, g, b)                                                      \
[UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:1]
#define RGBACOLOR(r, g, b, a)                                                  \
[UIColor colorWithRed:(r) / 255.0                                            \
green:(g) / 255.0                                            \
blue:(b) / 255.0                                            \
alpha:(a)]

#define RandomColor [UIColor colorWithRed:((float)arc4random_uniform(256) / 255.0) green:((float)arc4random_uniform(256) / 255.0) blue:((float)arc4random_uniform(256) / 255.0) alpha:1.0];
/*颜色*/
#define Color_Black [UIColor colorWithHexString:@"#1a1a1a"]
#define Color_DrakBlack [UIColor colorWithHexString:@"#000000"]
#define Color_Gray [UIColor colorWithHexString:@"#9b9b9b"]
#define Color_Line [UIColor colorWithHexString:@"#eeeeee"]
#define Color_White [UIColor colorWithHexString:@"#ffffff"]
#define Color_Red [UIColor colorWithHexString:@"#ff3939"]
#define Color_Bj [UIColor colorWithHexString:@"#ffffff"]
#define Color_333333 [UIColor colorWithHexString:@"#333333"]
#define Color_999999 [UIColor colorWithHexString:@"#999999"]
#define Color_666666 [UIColor colorWithHexString:@"#666666"]
#define Color_FF8000 [UIColor colorWithHexString:@"#FF8000"]
#define Color_F8F8F8 [UIColor colorWithHexString:@"#F8F8F8"]
#define Color_FF5656 [UIColor colorWithHexString:@"#FF5656"]
#define Color_AAAAAA [UIColor colorWithHexString:@"#AAAAAA"]
#define Color_F5F5F5 [UIColor colorWithHexString:@"#F5F5F5"]
#define Color_EEEEEE [UIColor colorWithHexString:@"#EEEEEE"]
#define Color_DDDDDD [UIColor colorWithHexString:@"#DDDDDD"]
#define Color_CCCCCC [UIColor colorWithHexString:@"#CCCCCC"]

/*字体*/
#define FONT(a) (IOS7 || IOS8)?[UIFont systemFontOfSize:(a/2)]:[UIFont fontWithName:@"PingFangSC-Ultralight" size:(a/2)]

#define kMFont(a) [UIFont fontWithName:@"PingFangSC-Medium" size:a]
#define kFont(a) [UIFont fontWithName:@"PingFangSC-Regular" size:a]
#define kBFont(a) [UIFont fontWithName:@"Arial-BoldMT" size:a]

/*像素点*/
#define H(a) a/2

#define kImage(a) [UIImage imageNamed:a]


/**
 *  日志输出
 */
#ifdef DEBUG
#define MBLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define MBLog(...)

#endif
/**
 弱引用/强引用
 */
#define MB_WeakSelf(type)  __weak typeof(type) weak##type = type
#define MB_StrongSelf(type)  __strong typeof(type) type = weak##type

#endif /* UIDefine_h */
