//
//  StaticDefine.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#ifndef StaticDefine_h
#define StaticDefine_h

static NSString *const SeverSuccessCode = @"0000";
/*短视频使用*/
static NSString *const TencentLicenseURL = @"http://license.vod2.myqcloud.com/license/v1/57642d256f1850dd6217eee6e7c6b72f/TXUgcSDK.licence";
/*短视频使用*/
static NSString *const TencentKey = @"28617bed4c39a1258678bf3777f5c864";

static NSString *const LocalLiveId = @"LocalLiveId";
/*保存 美颜设置 美白值*/
static NSString *const LocalSkinValueKey = @"LocalSkinValueKey";
/*保存 美颜设置 美颜值*/
static NSString *const LocalBeautyValueKey = @"LocalBeautyValueKey";
/*一些占位图片*/
static NSString *const Defaultwubagua = @"wubagua";
static NSString *const Defaultwuguanzhu = @"wuguanzhu";
static NSString *const Defaultwusixin = @"wusixin";
static NSString *const Defaultwusous = @"wushuju";
static NSString *const Defaultwuxiaoxi = @"wuxiaoxi";
static NSString *const Defaultwuxihuan = @"wuxihuan";
static NSString *const Defaultwuzuop = @"wuzuop";
static NSString *const Defaultwuzuopqit = @"wuzuopqit";

/*分享使用的一些key*/
static NSString *UMAppKey = @"5bf26792f1f5562d4300034d";
static NSString *UMWeChatKey = @"wx21ba06be64b0ebc2";
static NSString *UMWeChatSecret = @"3baf1193c85774b3fd9d18447d76cab0";
static NSString *UMQQKey = @"C9dX5RxBmroYbBmS";
static NSString *UMQQAppID = @"1107904957";
static NSString *UMWeiboAppID = @"2087596088";
static NSString *UMWeiboSecret = @"291fd1a0306b1cd3ae5c5bb27083b2bc";

/*腾讯云通讯 appid*/
static int TencentChatAppid = 1400153190;
static NSString *TencentChatAccountType = @"36862";

static NSString * const CancelLoginNotificationKey = @"CancelLoginNotificationKey";

#endif /* StaticDefine_h */

