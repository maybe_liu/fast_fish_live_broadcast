//
//  MBHttpRequset.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
NS_ASSUME_NONNULL_BEGIN

@interface MBHttpRequset : NSObject

+ (AFHTTPSessionManager *)sharedInstance;

+ (void)requestWithUrl:(NSString *)urlName setParams:(NSDictionary *)params isShowProgressView:(BOOL)showProgressView success:(void (^)(id AJson))successBlock failure:(void (^)(id AJson))failureBlock;
+ (void)mb_requestWithUrl:(NSString *)urlName setParams:(NSDictionary *)params isShowProgressView:(BOOL)showProgressView success:(void (^)(id AJson))successBlock failure:(void (^)(id AJson))failureBlock;
@end

NS_ASSUME_NONNULL_END
