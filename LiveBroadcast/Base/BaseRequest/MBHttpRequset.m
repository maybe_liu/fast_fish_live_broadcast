//
//  MBHttpRequset.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHttpRequset.h"

@implementation MBHttpRequset

+ (AFHTTPSessionManager *)sharedInstance
{
    static dispatch_once_t once;
    static AFHTTPSessionManager *_manager;
    dispatch_once(&once, ^{
        _manager= [AFHTTPSessionManager manager];
    });
    return _manager;
}

+ (void)requestWithUrl:(NSString *)urlName setParams:(NSDictionary *)params isShowProgressView:(BOOL)showProgressView success:(void (^)(id _Nonnull))successBlock failure:(void (^)(id _Nonnull))failureBlock
{
    //http://[IP]:[端口]/videolive/common/sendSms
    //http://120.79.155.171:8080/videolive/common/sendSms
    NSString *url = [NSString stringWithFormat:@"%@%@",kMainUrl,urlName];
    
    AFHTTPSessionManager *manager = [MBHttpRequset sharedInstance];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content－Type"];

    
    [manager POST:url
              parameters:params
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
//                     NSLog(@"responseObject =============== %@",dictionary);
                     MBLog(@"++++++%@",urlName);
                     if ([dictionary[@"code"] isEqualToString:@"0000"] && dictionary[@"result"] != nil) {
                         successBlock(dictionary[@"result"]);
                     }else {
                         MBLog(@"++++++++%@%@%@",urlName,@"这个接口报错了",dictionary);
                         if (dictionary[@"message"] != nil && showProgressView){
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 [MBProgressHUD showMessage:dictionary[@"message"]];
                             });
//                             [MBProgressHUD showMessage:dictionary[@"message"]];
                         }
                     }
                 }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     NSLog(@"error =============== %@",error);
                     failureBlock(error);
                 }];
}
+ (void)mb_requestWithUrl:(NSString *)urlName setParams:(NSDictionary *)params isShowProgressView:(BOOL)showProgressView success:(void (^)(id AJson))successBlock failure:(void (^)(id AJson))failureBlock {
    NSString *url = [NSString stringWithFormat:@"%@%@",kMainUrl,urlName];
    
    AFHTTPSessionManager *manager = [MBHttpRequset sharedInstance];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    manager.requestSerializer=[AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content－Type"];
    
    
    [manager POST:url
       parameters:params
         progress:nil
          success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              NSDictionary *dictionary =[NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
              successBlock(dictionary);
//              if ([dictionary[@"code"] isEqualToString:@"0000"] && dictionary[@"result"] != nil) {
//                  successBlock(dictionary[@"result"]);
//              }
//              if (dictionary[@"message"] != nil && showProgressView)
//                  [MBProgressHUD showMessage:dictionary[@"message"]];
//              MBLog(@"++++++++%@%@%@",urlName,@"这个接口报错了",dictionary);
          }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              NSLog(@"error =============== %@",error);
              failureBlock(error);
          }];
}

@end
