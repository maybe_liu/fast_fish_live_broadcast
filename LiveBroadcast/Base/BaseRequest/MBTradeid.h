//
//  MBTradeid.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#ifndef MBTradeid_h
#define MBTradeid_h

// 通知
#define MBIsLoginNotification @"MBISLOGINNOTIFICATION"
#define MBLoginFLag @"MBLOGINFLAG"
#define MBLogoutFlag @"MBLOGOUTFLAG"


//#define kTradeid_public_checkTime @"/videolive-webapi/common/checkTime"
//#define kTradeid_public_checkTime @"/videolive/common/checkTime"

// #define kMainURL @"120.79.155.171:9080"
#define kMainUrl @"http://120.79.155.171:9080"
//#define kMainUrl @"http://192.168.0.128:8082"

/*
 公共
 **/
// 定时检测结束时间
#define kTradeid_public_checkTime @"/videolive-webapi/common/checkTime"
// 获取七牛云客户端上传凭证 #define kTradeid_public_checkTime @""
#define kTradeid_public_getUpToken @"/videolive-webapi/common/getUpToken"
// 第三方分享链接
#define kTradeid_public_share @"/videolive-webapi/common/share"


/*
 登录注册
 **/
// 用户注册接口
#define kTradeid_login_regist @"/videolive-webapi/userCenter/regist"
// 活动或招募邀请注册（第三方链接分享注册）
#define kTradeid_login_inviteRegist @"/videolive-webapi/userCenter/inviteRegist"
// 用户登录接口
#define kTradeid_login_login @"/videolive-webapi/userCenter/login"
// 发送短信
#define kTradeid_login_sendSms @"/videolive-webapi/common/sendSms"
// 微信用户登录
#define kTradeid_login_loginByWechat @"/videolive-webapi/userCenter/loginByWechat"
// 获取用户协议
#define kTradeid_login_getAgreementData @"/videolive-webapi/userCenter/getAgreementData"

/*
 首页
 **/
// 获取发现页面列表
#define kTradeid_home_getFoundData @"/videolive-webapi/video/getFoundData"
// 获取同城页面列表
#define kTradeid_home_getLocalData @"/videolive-webapi/video/getLocalData"
// 获取关注页面列表
#define kTradeid_home_getFocusData @"/videolive-webapi/video/getFocusData"
// 获取直播页面列表
#define kTradeid_home_getLiveData @"/videolive-webapi/live/getLiveData"

//判断用户是否达到直播条件
#define kHome_live_limits @"/videolive-webapi/live/getOpenLiveState"

//获取七牛云客户端上传凭证
#define kThird_Qiniu_Token @"/videolive-webapi/common/getUpToken"
/*主播开启直播*/
#define kLive_live_open @"/videolive-webapi/live/openLive"
/*主播关闭直播*/
#define kLive_live_close @"/videolive-webapi/live/closeLive"
/*获取直播观看人数*/
#define kLive_live_getLiveViewerCount @"/videolive-webapi/live/getLiveViewerCount"
/*获取直播观众*/
#define kLive_live_getLiveViewer @"/videolive-webapi/live/getLiveViewer"
/*获取直播礼物列表*/
#define kLive_live_getListGiftRecord @"/videolive-webapi/live/listGiftRecord"
/*获取直播礼物列表*/
#define kLive_live_getTopUpList @"/videolive-webapi/userCenter/getTopUpList"
/*获取我的账单里的提取列表*/
#define kLive_live_getExtractCashList @"/videolive-webapi/userCenter/getExtractCashList"
/*获取我的账单里的兑换列表*/
#define kLive_live_getExchangeList @"/videolive-webapi/userCenter/getExchangeList"
/*获取收到礼物的本日排行列表*/
#define kLive_live_receiveGiftThisDay @"/videolive-webapi/userCenter/receiveGiftThisDay"
/*获取收到礼物的本周排行列表*/
#define kLive_live_receiveGiftWeeks @"/videolive-webapi/userCenter/receiveGiftWeeks"
/*获取用户送出礼物的记录*/
#define kLive_live_userSendGiftList @"/videolive-webapi/userCenter/userSendGiftList"
/*获取账户收到的红包记录*/
#define kLive_live_receivedRedPacketRecord @"/videolive-webapi/userCenter/receivedRedPacketRecord"
/*获取账户发出的红包记录*/
#define kLive_live_emitRedPacketRecord @"/videolive-webapi/userCenter/emitRedPacketRecord"
/*主播发送红包*/
#define kLive_live_liveSendRedPacket @"/videolive-webapi/live/sendRedPacket"
/*主播添加敏感字*/
#define kLive_live_liveAddSensitiveWord @"/videolive-webapi/live/liveAddSensitiveWord"
/*主播删除敏感字*/
#define kLive_live_liveDeleteSensitiveWord @"/videolive-webapi/live/deleteSensitiveWord"
/*拉入黑名单*/
#define kLive_live_livePullBlackListApi @"/videolive-webapi/video/pullBlacklist"


/*获取主播踢人记录*/
#define kLive_live_liveRemoveAudienceListApi @"/videolive-webapi/live/liveKickingRecordList"
/*主播踢人*/
#define kLive_live_liveRemoveAudience @"/videolive-webapi/live/liveKickingRecord"

/*主播通知粉丝*/
#define kLive_live_liveNotificationFansApi @"/videolive-webapi/live/liveInformFans"
/*分享*/
#define kLive_live_CommonShareApi @"/videolive-webapi/common/share"
/*获取好友列表*/
#define kLive_getFriendsList @"/videolive-webapi/userCenter/getFriendsList"

/*获取直播详情*/
#define kLive_getLiveInfo @"/videolive-webapi/live/getLiveInfo"
/*观众退出直播间*/
#define kLive_live_audienceClose @"/videolive-webapi/live/removeUserInfoFromRedis"
/*直播点赞*/
#define kLive_livePlay_like @"/videolive-webapi/live/setPraiseToRedis"

/*查询红包信息*/
#define kLive_livePlay_haveRedpacket @"/videolive-webapi/live/ifRedPacket"
/*抢红包*/
#define kLive_livePlay_openRedpacket @"/videolive-webapi/live/robRedPacket"
/*直播拉黑*/
#define kLive_live_black @"/videolive-webapi/live/insertBlackListFromLive"

/*发布短视频*/
#define kLive_shortVideo_publish @"/videolive-webapi/video/setVideo"

/** 短视频详情 */
#define kLive_shortVideo_videoDetail @"/videolive-webapi/video/getVideoDetails"

/*短视频点赞*/
#define kLive_shortVideo_like @"/videolive-webapi/video/clickVideoPraise"
/*短视频评论列表*/
#define kLive_shortVideo_commentsList @"/videolive-webapi/video/getVideoCommentsList"
/*礼物效果图列表*/
#define kLive_live_giftPictureList @"/videolive-webapi/userCenter/getGiftList"
/*送主播礼物*/
#define kLive_live_sendGift @"/videolive-webapi/live/giveGift"
/*举报短视频*/
#define kLive_shortvideo_report @"/videolive-webapi/video/reportVideo"
/*拉黑短视频*/
#define kLive_shortvideo_pullblacklist @"/videolive-webapi/video/pullBlacklist"
/*获取发布视频标签列表*/
#define kLive_shortvideo_video @"/videolive-webapi/video/sendVideoLabelList"
/*短视频发表评论*/
#define kLive_shortvideo_sendComment @"/videolive-webapi/video/setVideoComments"
/*短视频点赞评论*/
#define kLive_shortvideo_likecomment @"/videolive-webapi/video/clickCommentsPraise"

/*二手视频所有页数据*/
#define kLive_secondHand_all @"/videolive-webapi/video/getSecondHandData"
/*二手视频同城页数据*/
#define kLive_secondHand_local @"/videolive-webapi/video/getSecondHandLocalData"
/*获取用户免责声明状态*/
#define kLive_secondHand_userLiabilityState @"/videolive-webapi/userCenter/userLiabilityState"
/*获取用户免责声明状态*/
#define kLive_secondHand_saveUserLiabilityState @"/videolive-webapi/userCenter/setUserLiabilityState"


/*
 短视频
 **/
#define kTradeid_video_ @""

/*
 用户中心
 **/
#define kTradeid_userCenter_setVideoLabel @"/videolive-webapi/userCenter/setVideoLabel"
// 搜索功能
#define kTradeid_userCenter_searchList @"/videolive-webapi/userCenter/searchList"
// 获取个人主页列表
#define kTradeid_userCenter_getPersonalData @"/videolive-webapi/userCenter/getPersonalData"
// 关注功能
#define kTradeid_userCenter_setFocusUs @"/videolive-webapi/video/setFocusUs"
// 获取个人主页分类
#define kTradeid_userCenter_getPersonalClassify @"/videolive-webapi/userCenter/getPersonalClassify"
// 变更手机号码
#define kTradeid_userCenter_changeNumber @"/videolive-webapi/userCenter/changeNumber"
// 获取黑名单
#define kTradeid_userCenter_BlacklistList @"/videolive-webapi/userCenter/getBlacklistList"
// 从黑名单中移除
#define kTradeid_userCenter_blacklistRemove @"/videolive-webapi/userCenter/blacklistRemove"
// 获取我的账户列表
#define kTradeid_userCenter_getAccountData @"/videolive-webapi/userCenter/getAccountData"
// 人民币充值鱼币功能
#define kTradeid_userCenter_rmbBuyfishCoins @"/videolive-webapi/userCenter/rmbBuyfishCoins"

// 获取用户的消息列表
#define kTradeid_userCenter_getMessages @"/videolive-webapi/userCenter/getMessages"
// 获取用户的八卦列表
#define kTradeid_userCenter_getGossipList @"/videolive-webapi/userCenter/getGossipList"
// 个人中心上传头像或背景图
#define kTradeid_userCenter_setHeadOrBackGroundUrl @"/videolive-webapi/userCenter/setHeadOrBackGroundUrl"
// 充值鱼币退款功能
#define kTradeid_userCenter_fishrefundRMB @"/videolive-webapi/userCenter/fishrefundRMB"
// 提现
#define kTradeid_userCenter_extractCash @"/videolive-webapi/userCenter/extractCash"
// 黄钻兑换鱼币
#define kTradeid_userCenter_yellowExchangeFish @"/videolive-webapi/userCenter/yellowExchangeFish"
// 购买热门头条功能
#define kTradeid_userCenter_buyHotHeadlines @"/videolive-webapi/userCenter/buyHotHeadlines"

/*
 支付
 **/
// 支付宝
#define kTradeid_pay_alipayRequest @"/videolive-webapi/pay/alipayRequest"

/*
 活动
 **/
// 获取活动列表
#define kTradeid_activity_getActivityList @"/videolive-webapi/activity/getActivityList"
// 获取活动banner列表
#define kTradeid_activity_activityBannerList @"/videolive-webapi/activity/activityBannerList"
// 根据活动标签获取不同活动类型banner列表
#define kTradeid_activity_getActivityBannerByLabelId @"/videolive-webapi/activity/getActivityBannerByLabelId"
// 获取活动页面活动规则
#define kTradeid_activity_getActivityRule @"/videolive-webapi/activity/getActivityRule"
// 活动标签
#define kTradeid_activity_getActivityLabel @"/videolive-webapi/activity/getActivityLabel"
// 获取活动列表
#define kTradeid_activity_getActivityList @"/videolive-webapi/activity/getActivityList"
// 获取活动详情
#define kTradeid_activity_getActivityData @"/videolive-webapi/activity/getActivityData"
// 获取参赛视频列表
#define kTradeid_activity_getActivityJoinList @"/videolive-webapi/activity/getActivityJoinList"
// 判断是否满足活动报名条件
#define kTradeid_activity_participateActivity @"/videolive-webapi/activity/participateActivity"
// 判断是否满足活动报名条件
#define kTradeid_activity_offerPrize @"/videolive-webapi/activity/offerPrize"

/*
 招募
 **/
// 获取招募banner列表
#define kTradeid_recruit_recruitBannerList @"/videolive-webapi/recruit/recruitBannerList"

// 获取招募列表
#define kTradeid_recruit_getRecruitList @"/videolive-webapi/recruit/getRecruitList"

// 获取招募详情
#define kTradeid_recruit_getRecruitData @"/videolive-webapi/recruit/getRecruitData"

// 判断是否满足招募报名条件
#define kTradeid_recruit_participate @"/videolive-webapi/recruit/participate"

//保存招募报名信息
#define kTradeid_recruit_participateData @"/videolive-webapi/recruit/participateData"

// 填写招募申请表
#define kTradeid_recruit_application @"/videolive-webapi/recruit/application"

/*
 二手
 **/
#define kTradeid_ldle_ @""
/*
 设置
 **/
// 隐私设置开关修改
#define kTradeid_setting_setUpSecretSwitch @"/videolive-webapi/userCenter/setUpSecretSwitch"
// 获取隐私设置开关状态
#define kTradeid_setting_getUpSecretSwitch @"/videolive-webapi/userCenter/getUpSecretSwitch"


#endif /* MBTradeid_h */
