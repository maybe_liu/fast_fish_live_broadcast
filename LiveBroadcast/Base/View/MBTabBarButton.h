//
//  MBTabBarButton.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBTabBarButton : UIButton
@property (nonatomic, strong, readwrite) UITabBarItem *item;

@end

NS_ASSUME_NONNULL_END
