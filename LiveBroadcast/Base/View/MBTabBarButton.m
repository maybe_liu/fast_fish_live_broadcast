//
//  MBTabBarButton.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTabBarButton.h"

@implementation MBTabBarButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font          = kMFont(15);
        [self setTitleColor:Color_999999  forState:UIControlStateNormal];
        [self setTitleColor:Color_333333 forState:UIControlStateSelected];
        
//        NSAttributedString *attributedString = [self attributedTitleForState:UIControlStateNormal];
//        NSDictionary *normalDict = @{NSFontAttributeName:kMFont(15),NSForegroundColorAttributeName:Color_999999};
//        NSAttributedString *normalAtt = [[NSAttributedString alloc]initWithString:self.currentTitle attributes:normalDict];;
//
//        NSDictionary *selectDict = @{NSFontAttributeName:kMFont(17),NSForegroundColorAttributeName:Color_333333};
//        NSAttributedString *selectAtt = [[NSAttributedString alloc]initWithString:self.currentTitle attributes:selectDict];;
//        [self setAttributedTitle:normalAtt forState:UIControlStateNormal];
//        [self setAttributedTitle:selectAtt forState:UIControlStateSelected];
    }
    return self;
}

/**
 *  重写这两个方法来自定义按钮的内部子控件布局
 */
- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageW = 0;
    CGFloat imageH = 0;
    imageW = 22;
    imageH = 22;
    CGFloat maxY   = contentRect.size.height;
    return CGRectMake((contentRect.size.width-imageW)/2.0, 7, imageW, imageH);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleW = contentRect.size.width;
    CGFloat titleH = contentRect.size.height;
    CGFloat titleX = 0;
    CGFloat titleY = contentRect.size.height;
    return CGRectMake(titleX, 0, titleW, titleH);
}

/*只要重写这个方法就可以避免按住以后按钮变灰*/
-(void)setHighlighted:(BOOL)highlighted{}

-(void)setItem:(UITabBarItem *)item {
    _item = item;
    [self setTitle:item.title forState:UIControlStateNormal];
    [self setTitle:item.title forState:UIControlStateSelected];
    [self setImage:item.image forState:UIControlStateNormal];
    [self setImage:item.selectedImage forState:UIControlStateSelected];
}


@end
