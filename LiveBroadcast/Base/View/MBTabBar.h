//
//  MBTabBar.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class MBTabBar;

@protocol MBTabbarDelegate <NSObject>
@optional
-(void)tabBar:(MBTabBar *)tabBar didSelectButtonForm:(NSInteger)from To:(NSInteger)to;
@end
@interface MBTabBar : UIView

@property(nonatomic, strong)NSMutableArray *tabbarButtons;
@property(nonatomic, weak)id<MBTabbarDelegate>delegate;
@property(nonatomic, assign)NSInteger selectedIndex;

- (void)addTabBarButtonWithItem:(UITabBarItem *)item;
- (void)updateTabBarButtonWithItem:(UITabBarItem *)item index:(NSInteger)index;
@end

NS_ASSUME_NONNULL_END
