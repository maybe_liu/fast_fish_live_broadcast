//
//  MBTabBar.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTabBar.h"
#import "MBTabBarButton.h"

@interface MBTabBar()
/**辅助保证只有一个按钮属于被选中状态*/
@property (nonatomic, weak, readwrite) MBTabBarButton *selectedButton;


@end

@implementation MBTabBar

- (void)addTabBarButtonWithItem:(UITabBarItem *)item
{
    //创建button
    MBTabBarButton *button = [[MBTabBarButton alloc] init];
    [self addSubview:button];
    [self.tabbarButtons addObject:button];
    
    //绑定数据
    button.item = item;
    
    //添加监听
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchDown];
    
    //默认选中第1个按钮
    if(self.tabbarButtons.count == 1)
    {
        [self buttonClick:button];
    }
}

- (void)updateTabBarButtonWithItem:(UITabBarItem *)item index:(NSInteger)index {
    if (self.tabbarButtons.count >= index) {
        return;
    }
    
    MBTabBarButton *button = (MBTabBarButton *)self.tabbarButtons[index];
    button.item = item;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat tabbarH = self.frame.size.height;
    CGFloat tabbarW = self.frame.size.width;
    CGFloat buttonY = 1;
    CGFloat buttonW = tabbarW / self.subviews.count;
    CGFloat buttonH = tabbarH;
    for(int i=0; i<self.tabbarButtons.count; i++) {
        MBTabBarButton *button = self.tabbarButtons[i];
        CGFloat buttonX = i * buttonW +2;
        
        button.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        button.tag = i;  //绑定tag便于控制器跳转
        
    }
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    
    MBTabBarButton *button = self.tabbarButtons[_selectedIndex];
    self.selectedButton.selected = NO;
    button.selected = YES;
    self.selectedButton = button;
}

#pragma mark 代理事件

- (void)buttonClick:(MBTabBarButton *)button {
    if ([self.delegate respondsToSelector:@selector(tabBar:didSelectButtonForm:To:)]) {
        [self.delegate tabBar:self didSelectButtonForm:self.selectedButton.tag To:button.tag];
    }
    
    self.selectedButton.selected = NO;
    button.selected = YES;
    self.selectedButton = button;
    button.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.5 delay:0 options:0 animations: ^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
            button.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
            button.transform = CGAffineTransformMakeScale(0.8, 0.8);
        }];
        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
            button.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    } completion:nil];
}


#pragma - getter
- (NSMutableArray *)tabbarButtons {
    if(_tabbarButtons == nil)
    {
        _tabbarButtons = [NSMutableArray array];
    }
    return _tabbarButtons;
}


@end
