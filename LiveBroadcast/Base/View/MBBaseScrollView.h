//
//  MBBaseScrollView.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBBaseScrollView : UIScrollView
@property (retain, nonatomic, readonly) UIImageView *verticalIndicator;
@property (retain, nonatomic, readonly) UIImageView *horizontalIndicator;

- (void)scrollToTopAnimated:(BOOL)animation;
- (void)scrollToBottomAnimated:(BOOL)animation;

//子视图位置变化时调整contentSize。
- (void)contentSizeToFit;
@end

NS_ASSUME_NONNULL_END
