//
//  MBIdleAllController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/30.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBIdleAllController.h"
#import "MBHomeLocalCollectionViewCell.h"
#import "XRWaterfallLayout.h"
#import "MBLocation.h"
#import "MBIdleAllListModel.h"
#import "MBShortVideoPlayController.h"

@interface MBIdleAllController ()<UICollectionViewDataSource,UICollectionViewDelegate,XRWaterfallLayoutDelegate>
@property (nonatomic, strong) MBBaseCollectionView *collectionView;
@property (nonatomic, assign) NSInteger  pageNo;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end
static NSString * const MBIdleAllCollectionViewCellID = @"MBIdleAllCollectionViewCellID";

@implementation MBIdleAllController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    [self createCustomNav];
    [self layoutChildViews];
}
-(void)zj_viewWillAppearForIndex:(NSInteger)index {
    [self loadData];
    MB_WeakSelf(self);
    [[MBLocation shareMBLocation] getCurrentAddress:^(NSDictionary *dic) {
        MBLog(@"%@",dic);
        [weakself loadData];
    }];
}

-(void)loadData {
    NSMutableDictionary *param  = [NSMutableDictionary dictionary];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_secondHand_all setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBIdleAllListModel *model = [MBIdleAllListModel mj_objectWithKeyValues:AJson];
        weakself.dataArray = [NSMutableArray arrayWithArray:model.secondHandList];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
    }];
}

-(void)loadMoreData {
    NSMutableDictionary *param  = [NSMutableDictionary dictionary];
    [param setValue:filter([MBUserModel sharedMBUserModel].userInfo.userId) forKey:@"userId"];
    [param setValue:@(self.pageNo) forKey:@"pageNo"];
    MB_WeakSelf(self);
    [MBHttpRequset requestWithUrl:kLive_secondHand_all setParams:param isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBIdleAllListModel *model = [MBIdleAllListModel mj_objectWithKeyValues:AJson];
        NSArray *tmpArray = model.secondHandList;
        [weakself.dataArray addObjectsFromArray:tmpArray];
        [weakself.collectionView reloadData];
        [weakself.collectionView.mj_header endRefreshing];
        [weakself.collectionView ly_endLoading];
    } failure:^(id  _Nonnull AJson) {
        [weakself.collectionView.mj_header endRefreshing];
    }];
}

-(void)createUI {
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5.0f;
    layout.delegate = self;
    self.collectionView = [[MBBaseCollectionView alloc]initWithFrame:(CGRect){{0,0},{kScreenWidth,kScreenHeight - kNavHeight - kTabHeight}} collectionViewLayout:layout];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MBHomeLocalCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MBIdleAllCollectionViewCellID];
    self.collectionView.backgroundColor = Color_White;
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    MB_WeakSelf(self);
    self.collectionView.mj_header = [UPCustomRefresh getGifHeader:^{
        weakself.pageNo = 1;
        [weakself loadData];
    }];
    self.collectionView.mj_footer = [UPCustomRefresh up_getFooter:^{
        weakself.pageNo++;
        [weakself loadMoreData];
    }];
    MBCustomEmptyView *emptyView = [MBCustomEmptyView mb_emptyView];
    self.collectionView.ly_emptyView = emptyView;
}

-(void)layoutChildViews {
    //    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.height.mas_equalTo(HeadViewHeight);
    //        make.top.left.right.equalTo(self.view);
    //    }];
}

#pragma mark - event


#pragma mark XRWaterfallLayoutDelegate & UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MBHomeLocalCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MBIdleAllCollectionViewCellID forIndexPath:indexPath];
    cell.idleAllmodel = self.dataArray[indexPath.row];
    
    return cell;
}

-(CGFloat)waterfallLayout:(XRWaterfallLayout *)waterfallLayout itemHeightForWidth:(CGFloat)itemWidth atIndexPath:(NSIndexPath *)indexPath {
    MBIdleAllModel *model = self.dataArray[indexPath.row];
    CGFloat scale = itemWidth/model.wide;
    CGFloat height = model.high *scale;
    return height;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    MBIdleAllModel *model = self.dataArray[indexPath.row];
    if ([model.type isEqualToString:@"1"]) {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"1";
        vc.coverImage = model.coverUrl;

        [self.navigationController pushViewController:vc animated:YES];
        
    }else {
        MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
        vc.videoPath = model.videoUrl;
        vc.videoID = model.ID;
        vc.videoType = model.type2;
        vc.entityType = @"2";
        vc.coverImage = model.coverUrl;

        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)createCustomNav {
    self.pageNo = 1;
}


#pragma mark - getter and setter
-(NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}



@end
