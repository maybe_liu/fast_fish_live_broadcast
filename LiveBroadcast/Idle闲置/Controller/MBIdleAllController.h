//
//  MBIdleAllController.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/30.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "ZJScrollPageViewDelegate.h"

NS_ASSUME_NONNULL_BEGIN

/**
 闲置 - 全部
 */
@interface MBIdleAllController : MBBaseController<ZJScrollPageViewChildVcDelegate>

@end

NS_ASSUME_NONNULL_END
