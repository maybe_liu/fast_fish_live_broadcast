//
//  MBIdleUserLiabilityStateModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBIdleUserLiabilityStateModel.h"
@implementation IdleUserLiabilityStateUserInfo
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation IdleUserLiabilityStateText

@end
@implementation MBIdleUserLiabilityStateModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"userInfo":[IdleUserLiabilityStateUserInfo class],
             @"text":[IdleUserLiabilityStateText class],

             };
}
@end
