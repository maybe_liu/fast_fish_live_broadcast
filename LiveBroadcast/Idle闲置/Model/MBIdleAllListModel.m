//
//  MBIdleAllListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/30.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBIdleAllListModel.h"

@implementation MBIdleAllModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end

@implementation MBIdleAllListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"secondHandList":[MBIdleAllModel class],
             };
}
@end
