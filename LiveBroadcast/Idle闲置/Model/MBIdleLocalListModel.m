//
//  MBIdleLocalListModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/30.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBIdleLocalListModel.h"
@implementation MBIdleLocalModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}
@end
@implementation MBIdleLocalListModel
+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"secondHandLocalList":[MBIdleLocalModel class],
             };
}
@end
