//
//  MBIdleUserLiabilityStateModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/9.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IdleUserLiabilityStateUserInfo :NSObject
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , copy) NSString              * liabilityState;

@end

@interface IdleUserLiabilityStateText :NSObject
@property (nonatomic , copy) NSString              * textTitle;
@property (nonatomic , copy) NSString              * textContent;

@end


@interface MBIdleUserLiabilityStateModel : MBBaseModel
@property (nonatomic , strong) IdleUserLiabilityStateUserInfo              * userInfo;
@property (nonatomic , strong) IdleUserLiabilityStateText              * text;
@end

NS_ASSUME_NONNULL_END
