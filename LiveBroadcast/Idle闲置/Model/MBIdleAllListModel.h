//
//  MBIdleAllListModel.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/3/30.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN
@interface MBIdleAllModel :NSObject
@property (nonatomic , copy) NSString              * clickType;
@property (nonatomic , copy) NSString              * coverUrl;
@property (nonatomic , copy) NSString              * headUrl;
@property (nonatomic , copy) NSString              * videoUrl;
@property (nonatomic , assign) CGFloat                high;
@property (nonatomic , copy) NSString              * clickPraise;
@property (nonatomic , copy) NSString              * issueTime;
@property (nonatomic , copy) NSString              * type;
@property (nonatomic , copy) NSString              * buyHeadline;
@property (nonatomic , copy) NSString              * type2;
@property (nonatomic , copy) NSString              * userId;
@property (nonatomic , assign) CGFloat                wide;
@property (nonatomic , copy) NSString              * isCheat;
@property (nonatomic , copy) NSString              * nickname;
@property (nonatomic , copy) NSString              * ID;
@property (nonatomic , copy) NSString              * issueId;
@property (nonatomic , copy) NSString              * videoHeadline;
@property (nonatomic , copy) NSString              * videoIntroduce;
@property (nonatomic , copy) NSString              * viewCount;


@end

@interface MBIdleAllListModel : MBBaseModel
@property (nonatomic , copy) NSArray<MBIdleAllModel *>              * secondHandList;

@end

NS_ASSUME_NONNULL_END
