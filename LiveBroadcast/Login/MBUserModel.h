//
//  MBUserModel.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/18.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBBaseModel.h"
@class  MBUserInfo;
NS_ASSUME_NONNULL_BEGIN

@interface MBUserModel : MBBaseModel<NSCoding>

singleton_interface(MBUserModel);

@property (nonatomic,copy)NSString *phone;

@property (nonatomic,copy)NSString *token;

@property (nonatomic,strong)MBUserInfo *userInfo;

@property (nonatomic,copy)NSString *userSig;

@property (nonatomic,assign)BOOL isLoginFlag;
+ (MBUserModel *)shareInstanceWithDict:(NSDictionary *)dict;
+(void)attempDealloc;

- (void)clearWithLogout;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end

@interface MBUserInfo: MBBaseModel<NSCoding>

@property (nonatomic,copy)NSString *headUrl;

@property (nonatomic,copy)NSString *userId;

@property (nonatomic,copy)NSString *nickname;

@property (nonatomic,copy)NSString *videoLabel;

+ (MBUserInfo *)shareInstanceWithDict:(NSDictionary *)dict;

- (instancetype)initWithDict:(NSDictionary *)dict;

+(void)attempDealloc;
@end

NS_ASSUME_NONNULL_END
