//
//  MBBindingPhoneController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBindingPhoneController.h"
#import "MITimerButton.h"
#import "MBSelectFishController.h"
@interface MBBindingPhoneController ()

@end

@implementation MBBindingPhoneController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"绑定手机号";
    // 初始化控件
    [self initView];
}

// 初始化控件
- (void)initView
{
    // 手机号码
    UILabel *phoneLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 120, 60, 60)];
    phoneLbl.text = @"+86";
    phoneLbl.textColor = kColorRGBA(51, 51, 51, 1);
    [self.view addSubview:phoneLbl];
    
    UIView *phoneLine = [[UIView alloc]initWithFrame:CGRectMake(phoneLbl.right + 10, phoneLbl.top, 0.5, phoneLbl.height)];
    phoneLine.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [self.view addSubview:phoneLine];
    
    UITextField *phoneTf = [[UITextField alloc]initWithFrame:CGRectMake(phoneLine.right + 20, phoneLine.top, IPHONE_SCREEN_WIDTH - phoneLine.left -20, phoneLine.height)];
    phoneTf.placeholder = @"请输入手机号码";
    [self.view addSubview:phoneTf];
    
    UIView *phoneLine1 = [[UIView alloc] init];
    phoneLine1.frame = CGRectMake(phoneLbl.left,phoneLbl.bottom + 10,IPHONE_SCREEN_WIDTH - 40,0.5);
    phoneLine1.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [self.view addSubview:phoneLine1];
    
    // 验证码
    UILabel *codeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, phoneLine1.bottom + 10, 60, 60)];
    codeLbl.text = @"验证码";
    codeLbl.textColor = kColorRGBA(51, 51, 51, 1);
    [self.view addSubview:codeLbl];
    
    UITextField *codeTf = [[UITextField alloc]initWithFrame:CGRectMake(phoneTf.left, codeLbl.top, IPHONE_SCREEN_WIDTH - phoneTf.left -20 - 100, codeLbl.height)];
    codeTf.placeholder = @"请输入验证码";
    [self.view addSubview:codeTf];
    
    //获取验证码
    MITimerButton *codeBtn= [MITimerButton buttonWithType:UIButtonTypeCustom];
    codeBtn.frame = CGRectMake(codeTf.right, codeTf.top + 10, 100, 40);
    [codeBtn setBackgroundColor:[UIColor whiteColor]];
    codeBtn.tag = 101;
    [codeBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateNormal];
    [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    codeBtn.isCountiue = YES;
    [codeBtn addTarget:self action:@selector(addCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:codeBtn];
    
    UIView *codeLine = [[UIView alloc] init];
    codeLine.frame = CGRectMake(phoneLbl.left,codeLbl.bottom + 10,IPHONE_SCREEN_WIDTH - 40,0.5);
    codeLine.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [self.view addSubview:codeLine];
    
    MBButton *bindingBtn = [MBButton buttonWithType:UIButtonTypeCustom];
    bindingBtn.frame = CGRectMake((IPHONE_SCREEN_WIDTH - 310)/2,codeLine.bottom + 60,310,44);;
    [bindingBtn setTitle:@"绑定" forState:UIControlStateNormal];
    [bindingBtn setBackgroundColor:kColorRGBA(255, 128, 0, 1)];
    bindingBtn.layer.cornerRadius = 22;
    [self.view addSubview:bindingBtn];
    
    MBSelectFishController *fishCtr = [[MBSelectFishController alloc]init];
    bindingBtn.miBtnBlock = ^(){
        [self.navigationController pushViewController:fishCtr animated:YES];
    };
}


// 获取验证码
- (void)addCodeBtnClick:(MITimerButton *)btn
{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
