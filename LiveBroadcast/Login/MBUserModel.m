//
//  MBUserModel.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/18.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBUserModel.h"
@implementation MBUserModel
singleton_implementation(MBUserModel);
static dispatch_once_t onceToken;
static MBUserModel *_userModel;
+ (MBUserModel *)shareInstanceWithDict:(NSDictionary *)dict
{
//    static dispatch_once_t once;
    dispatch_once(&onceToken, ^{
        _userModel= [[MBUserModel alloc] initWithDict:dict];
    });
    return _userModel;
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.phone = filter(dict[@"phone"]);
        self.token = filter(dict[@"token"]);
        self.userInfo = [MBUserInfo shareInstanceWithDict:dict[@"userInfo"]];
        self.userSig = filter(dict[@"userSig"]);
    }
    return self;
}

+(void)attempDealloc{
    onceToken = 0; // 只有置成0,GCD才会认为它从未执行过.它默认为0.这样才能保证下次再次调用shareInstance的时候,再次创建对象.
//    [_userModel release];
    _userModel = nil;
}

- (void)clearWithLogout
{
    //沙盒ducument目录
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //完整的文件路径
    NSString *path = [docPath stringByAppendingPathComponent:@"userModel.archive"];
    BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    if (exists) {
        NSLog(@"======================================路径正确");
        [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    }
    [MBUserModel sharedMBUserModel].isLoginFlag = NO;
    [[RCIM sharedRCIM] disconnect:NO];
    MBUserModel *removeModel = [MBUserModel shareInstanceWithDict:@{}];
    [[NSNotificationCenter defaultCenter] postNotificationName:MBIsLoginNotification object:nil userInfo:@{@"isFlag":MBLogoutFlag}];
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    [aCoder encodeObject:self.phone forKey:@"phone"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.userInfo forKey:@"userInfo"];
    [aCoder encodeObject:self.userSig forKey:@"userSig"];
    [aCoder encodeInteger:self.isLoginFlag forKey:@"isLoginFlag"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    if (self = [super init]) {
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.userInfo = [aDecoder decodeObjectForKey:@"userInfo"];
        self.userSig = [aDecoder decodeObjectForKey:@"userSig"];
        self.isLoginFlag = [aDecoder decodeIntegerForKey:@"isLoginFlag"];
    }
    
    return self;
}

@end

@implementation MBUserInfo
static dispatch_once_t once;
static MBUserInfo *_userInfo;
+ (MBUserInfo *)shareInstanceWithDict:(NSDictionary *)dict
{
    dispatch_once(&once, ^{
        _userInfo= [[MBUserInfo alloc] initWithDict:dict];
    });
    return _userInfo;
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.headUrl = filter(dict[@"headUrl"]);
        self.userId = filter(dict[@"id"]);
        self.nickname = filter(dict[@"nickname"]);
        self.videoLabel = filter(dict[@"videoLabel"]);
    }
    return self;
}

+(void)attempDealloc{
    once = 0; // 只有置成0,GCD才会认为它从未执行过.它默认为0.这样才能保证下次再次调用shareInstance的时候,再次创建对象.
    //    [_userModel release];
    _userInfo = nil;
}

- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
    [aCoder encodeObject:self.headUrl forKey:@"headUrl"];
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.nickname forKey:@"nickname"];
    [aCoder encodeObject:self.videoLabel forKey:@"videoLabel"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    if (self = [super init]) {
        self.headUrl = [aDecoder decodeObjectForKey:@"headUrl"];
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.nickname = [aDecoder decodeObjectForKey:@"nickname"];
        self.videoLabel = [aDecoder decodeObjectForKey:@"videoLabel"];
    }
    
    return self;
}

@end
