//
//  MBSelectFishController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBSelectFishController : MBBaseController
@property (nonatomic,copy)NSString *userId;
@end

NS_ASSUME_NONNULL_END
