//
//  MBSelectFishController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSelectFishController.h"

@interface MBSelectFishController ()
{
    NSMutableArray *selectFishArr;
    NSArray *fishArr;
}
@end

@implementation MBSelectFishController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = kColorRGBA(255, 255, 255, 0.1);
    selectFishArr = [NSMutableArray array];
    fishArr = @[@"海水类",@"淡水类",@"飞宠类",@"爬虫类",@"猫",@"狗"];

    // 初始化控件
    [self initView];
}

// 初始化控件
- (void)initView
{
    UILabel *selLabel = [[UILabel alloc] init];
    selLabel.frame = CGRectMake((IPHONE_SCREEN_WIDTH - 200)/2,84,200,23);
    selLabel.numberOfLines = 0;
    selLabel.text = @"您对哪种鱼感兴趣";
    [self.view addSubview:selLabel];
    NSMutableAttributedString *selLabelStr = [[NSMutableAttributedString alloc] initWithString:@"您对哪种鱼感兴趣" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 24],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    selLabel.attributedText = selLabelStr;
    
    UILabel *detailLabel = [[UILabel alloc] init];
    detailLabel.frame = CGRectMake(selLabel.left,118.5,183.5,12);
    detailLabel.numberOfLines = 0;
    [self.view addSubview:detailLabel];
    NSMutableAttributedString *detailLabelsStr = [[NSMutableAttributedString alloc] initWithString:@"选择后，相关内容将优先向您推荐" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    detailLabel.attributedText = detailLabelsStr;
    
    CGFloat btnW = 103;
    CGFloat btnH = 103;
    CGFloat btnWPading = (IPHONE_SCREEN_WIDTH - 103*2)/3;
    CGFloat btnHPading = 40;
    for (int i = 0; i < fishArr.count; i++) {
        MBButton *danBtn = [MBButton buttonWithType:UIButtonTypeCustom];
        danBtn.selected = NO;
        [danBtn setBackgroundImage:[UIImage imageNamed:@"danshuiyu.png"] forState:UIControlStateNormal];
        [danBtn setBackgroundImage:[UIImage imageNamed:@"chose2.png"] forState:UIControlStateSelected];
        [danBtn addTarget:self action:@selector(selectFish:) forControlEvents:UIControlEventTouchUpInside];
        danBtn.tag = i;
        [self.view addSubview:danBtn];
        
        UILabel *danLabel = [[UILabel alloc] init];
        danLabel.numberOfLines = 0;
        danLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:danLabel];
        
        NSMutableAttributedString *danStr = [[NSMutableAttributedString alloc] initWithString:fishArr[i] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
        danLabel.attributedText = danStr;
        
        NSInteger row = i / 2;//所在行
        NSInteger col = i % 2;//所在列
        CGFloat btnX = btnWPading +(btnW + btnWPading) *col;
        CGFloat btnY = btnHPading + (btnH + btnHPading) * row + 120;
        danBtn.frame = CGRectMake(btnX,btnY,btnW,btnH);
        danLabel.frame = CGRectMake(danBtn.left,danBtn.bottom + 10 ,btnW,13.5);
        
        if (i == 5) {
            MBButton *homeBtn = [MBButton buttonWithType:UIButtonTypeCustom];
            homeBtn.frame = CGRectMake((IPHONE_SCREEN_WIDTH - 310)/2,IPHONE_SCREEN_HEIGHT - 54,310,44);;
            [homeBtn setTitle:@"进入首页" forState:UIControlStateNormal];
            [homeBtn setBackgroundColor:kColorRGBA(255, 128, 0, 1)];
            homeBtn.layer.cornerRadius = 22;
            [self.view addSubview:homeBtn];
            
            homeBtn.miBtnBlock = ^(){
                if (self->selectFishArr.count != 0) {
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self->selectFishArr
                                                                       options:kNilOptions
                                                                         error:nil];
                    
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                                 encoding:NSUTF8StringEncoding];
                    NSLog(@"%@   %@    %@",[MBUserModel sharedMBUserModel].userInfo.userId,self->selectFishArr,jsonString);
                    
                    [MBHttpRequset requestWithUrl:kTradeid_userCenter_setVideoLabel setParams:@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId,@"videoLabel":jsonString} isShowProgressView:YES success:^(id  _Nonnull AJson) {
                        NSLog(@"自动登录====================userInfo  %@",AJson);
                    } failure:^(id  _Nonnull AJson) {
                        
                    }];
                }
            };
        }
    }
}

- (void)selectFish:(UIButton *)btn
{
    btn.selected = !btn.selected;
    if (![self->selectFishArr containsObject:fishArr[btn.tag]]) {
        [self->selectFishArr addObject:fishArr[btn.tag]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
