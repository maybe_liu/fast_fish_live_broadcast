//
//  AppDelegate.h
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/14.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBLoginController.h"//登录
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// 需要登录
- (void)needLoginVC:(MBBaseController *)baseVC;

@end

