//
//  MBNotifiController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBNotifiController.h"
#import "MBNotifiCell.h"
#import "MBUserDetailController.h"
@interface MBNotifiController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSArray *dataSource;
@end

@implementation MBNotifiController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getMessages setParams:@{@"userId":[NSString stringWithFormat:@"%@",[MBUserModel sharedMBUserModel].userInfo.userId],@"pageNo":@"1"} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.dataSource = AJson[@"messageList"];
        [self.tableView reloadData];
        NSLog(@"kTradeid_userCenter_getMessages = %@",AJson);
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}

- (NSArray *)dataSource
{
    if (_dataSource == nil) {
        _dataSource = [NSArray array];
    }
    return _dataSource;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MBNotifiCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NOTIFICELL"];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBNotifiCell" owner:nil options:nil];
        cell  = [nibs lastObject];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (self.dataSource.count != 0) {
        NSDictionary *dict = self.dataSource[indexPath.row];
        
        NSTimeInterval interval    =[dict[@"createTime"] doubleValue] / 1000.0;
        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString       = [formatter stringFromDate: date];
        
        cell.titleLbl.text = dict[@"message"];
        cell.detailLbl.text =  dateString;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = self.dataSource[indexPath.row];
    NSInteger type = [[NSString stringWithFormat:@"%@",dict[@"type"]] integerValue];
    switch (type) {
        case 1://发布视频@用户提醒（调用获取短视频详情接口）.
            {
                
            }
            break;
        case 2://直播通知粉丝提醒（调用获取直播详情接口）.
        {
            
        }
            break;
        case 3://活动胜利提醒(填写活动优胜者信息接口).
        {
            
        }
            break;
        case 4:// 用户被关注提醒（调用获取个人主页数据接口）
        {
            MBUserDetailController *detailVC = [[MBUserDetailController alloc]init];
            detailVC.userId = filter( dict[@"initiateId"]);
            [self.navigationController pushViewController:detailVC animated:YES];
        }
            break;
        case 5: // 视频曝光完成提醒(未知)
        {
            
        }
            break;
        case 6://提现审核结果通知(未知)’
        {
            
        }
            break;
        case 7:// 评论里被At用户提醒(调用获取短视频详情接口)’
        {
            
        }
            break;
        case 8://评论里被回复的用户提醒(调用获取短视频详情接口)’
        {
            
        }
            break;
        case 9://有用户评论你的作品提醒(调用获取短视频详情接口)
        {
            
        }
            break;
        default:
            break;
    }
    
}

@end
