//
//  MBPrivateLetterController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/13.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPrivateLetterController.h"

@interface MBPrivateLetterController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataSource;
}
@property (nonatomic,strong)NSIndexPath *indexPath;

@property (nonatomic,weak)UITableView *tableView;
@end

@implementation MBPrivateLetterController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"谁可以私信我";
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    dataSource = @[@"所有人",@"我关注的人",@"互关好友"];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return self.headerTitle; //@"选择可以私信我的人";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"PRIVATECELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idetifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = dataSource[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.indexPath != indexPath) {
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:self.indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;//(cell.accessoryType == UITableViewCellAccessoryCheckmark)?UITableViewCellAccessoryNone:UITableViewCellAccessoryCheckmark;
    self.indexPath = indexPath;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSString *type;
    if ([self.headerTitle isEqualToString:@"选择可以私信我的人"]) {
        type = @"PRIVATE_CONVERSATION";
        
    }else if ([self.headerTitle isEqualToString:@"通知私信"]){
        type = @"INFORM_LETTER";
            
    }else{
        type = @"INFORM_COMMENT";
    }
    
    NSDictionary *dict = @{
             @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
             @"type":type,
             @"newState":[NSString stringWithFormat:@"%ld",self.indexPath.row + 1]};
    [MBHttpRequset requestWithUrl:kTradeid_setting_setUpSecretSwitch setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
        
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    
    if (_privateVCSelectBlock) {
        _privateVCSelectBlock(dataSource[self.indexPath.row]);
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
