//
//  MBSearchController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSearchController.h"
#import "MBUserTableCell.h"
#import "MBSearchUserModel.h"
#import "MBUserDetailController.h"
#import "MBUserCenterModel.h"
@interface MBSearchController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UITableView *tableView;
    UIView *videoView;
}
@property (nonatomic,weak)UITextField *textF;
@property (nonatomic,strong)MBSearchUserModel *userModel;

@property (nonatomic,strong)MBSearchUserModel *videoModel;
@end

@implementation MBSearchController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"搜索";
    self.view.backgroundColor = [UIColor whiteColor];
    [self initView];
    
    // 测试
     [self getUserVedioType:@"SEARCH_USER" pageNo:1 text:@"海水"];
    [tableView reloadData];
}

// 初始化控件
- (void)initView
{
    //  搜索框
    UIView *viewImg = [[UIView alloc]init];
    viewImg.size = CGSizeMake(55, 32);
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.frame = CGRectMake(20, 8, 15, 15);
    imageV.image = [UIImage imageNamed:@"sous.png"];
    [viewImg addSubview:imageV];
    
    UITextField *searchTF = [[UITextField alloc]init];
    searchTF.frame = CGRectMake(15,20,IPHONE_SCREEN_WIDTH - 30,32);
    searchTF.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    searchTF.leftView = viewImg;
    searchTF.returnKeyType = UIReturnKeySearch;
    searchTF.delegate = self;
    searchTF.placeholder = @"请输入昵称/标签关键字";
    searchTF.leftViewMode = UITextFieldViewModeAlways;
    [self.view addSubview:searchTF];
    self.textF = searchTF;
    
    // 选中用户和标签
    // 用户
    CGFloat padding = (IPHONE_SCREEN_WIDTH - 100)/3;
    MBButton *userBtn = [MBButton buttonWithType:UIButtonTypeCustom];
    userBtn.frame = CGRectMake(padding, searchTF.bottom + 20, 40, 30);
    [userBtn setTitle:@"用户" forState:UIControlStateNormal];
    [userBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    userBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [userBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateSelected];
    userBtn.selected = YES;
    [userBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:userBtn];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(userBtn.left, userBtn.bottom + 10, userBtn.width, 2);
    lineView.backgroundColor = userBtn.currentTitleColor;
    [self.view addSubview:lineView];
    // 标签
    MBButton *flagBtn = [MBButton buttonWithType:UIButtonTypeCustom];
    flagBtn.frame = CGRectMake(userBtn.right + padding, userBtn.top, userBtn.width, 30);
    [flagBtn setTitle:@"视频" forState:UIControlStateNormal];
    [flagBtn.titleLabel setFont:[UIFont systemFontOfSize:16.0f]];
    flagBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [flagBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateSelected];
    [flagBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:flagBtn];
    
    UIView *rLineView = [[UIView alloc]init];
    rLineView.frame = CGRectMake(flagBtn.left, flagBtn.bottom + 10, flagBtn.width, 2);
    rLineView.backgroundColor = flagBtn.titleLabel.textColor;
    rLineView.hidden = YES;
    [self.view addSubview:rLineView];
    
    //
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0,rLineView.bottom,IPHONE_SCREEN_WIDTH,30);
    label.numberOfLines = 0;
    label.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    [self.view addSubview:label];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"    你可能感兴趣的人" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 12],NSForegroundColorAttributeName: [UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1.0]}];
    label.attributedText = string;
    
    // 用户
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, label.bottom + 10, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT - label.bottom -  70) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    // 视频
    videoView = [[UIView alloc] initWithFrame:tableView.frame];
    videoView.top = label.top;
    videoView.height += 30;
    videoView.hidden = YES;
    [self.view addSubview:videoView];
    
    //---------------------------------------------事件处理-------------------------------------------------------
    __block MBButton *rUserBtn = userBtn;
    __block MBButton *rFlagBtn = flagBtn;
    UIView *rVideoView = videoView;
    UITableView *rTableView = tableView;
    // 用户
    userBtn.miBtnBlock = ^(){
        rUserBtn.selected = YES;
        lineView.backgroundColor = rUserBtn.currentTitleColor;
        lineView.hidden = NO;
        rFlagBtn.selected = NO;
        label.hidden = NO;
        rLineView.backgroundColor = flagBtn.currentTitleColor;
        rLineView.hidden = YES;
        
        // view 的切换
        rVideoView.hidden = YES;
        rTableView.hidden = NO;
        
        [self.view endEditing:YES];
        
        [self getUserVedioType:@"SEARCH_USER" pageNo:1 text:self.textF.text];
    };
    // 标签
    flagBtn.miBtnBlock = ^(){
        rFlagBtn.selected = YES;
        rLineView.backgroundColor = rFlagBtn.currentTitleColor;
        rLineView.hidden = NO;
        rUserBtn.selected = NO;
        lineView.backgroundColor = rUserBtn.currentTitleColor;
        lineView.hidden = YES;
        label.hidden = YES;
        
        // view 的切换
        rVideoView.hidden = NO;
        rTableView.hidden = YES;
        
        [self.view endEditing:YES];
        
        [self getUserVedioType:@"SEARCH_VIDEO" pageNo:1 text:self.textF.text];
    };
    
}
#pragma mark - textfield delegete
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    if (textField.text.length != 0){
        [self getUserVedioType:@"SEARCH_USER" pageNo:1 text:textField.text];
        
    }
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - 获取搜索用户/视频
- (void)getUserVedioType:(NSString *)type pageNo:(NSInteger)pageNo text:(NSString *)text
{

    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setDictionary:@{
                          @"searchType":type,
                          @"text":text,
                          @"pageNo":[NSString stringWithFormat: @"%ld", (long)pageNo],
                          }];
    
    if ([MBUserModel sharedMBUserModel].userInfo.userId != nil) {
        [dict setObject:[MBUserModel sharedMBUserModel].userInfo.userId forKey:@"userId"];
    }
    __block UITableView *tbView = tableView;
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_searchList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        if ([type isEqualToString:@"SEARCH_USER"]) {// 搜索用户
            self.userModel = [[MBSearchUserModel alloc]initWithDict:AJson];
            [tbView reloadData];
            NSLog(@"=============usermodel=========  %@",self.userModel.userList);
        }else{// 搜索视频
            self.videoModel = [[MBSearchUserModel alloc]initWithDict:AJson];
            NSLog(@"=============videoModel=========  %@",self.videoModel.videoList);
        }
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.userModel != nil)
        return self.userModel.userList.count;
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"USERCELL";
    MBUserTableCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBUserTableCell" owner:nil options:nil];
        cell = [nibs lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.userModel = self.userModel.userList[indexPath.row];
    cell.userIconBtn.miBtnBlock = ^(){
        NSLog(@"用户详情");
        MBSearchUserModelBranch *userModel = self.userModel.userList[indexPath.row];
        NSDictionary *dict = @{
                               @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                               @"beUserId":filter(userModel.userId)
                               };
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_getPersonalData setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
            MBUserCenterModel *centerModel = [[MBUserCenterModel alloc]initWithDict:AJson];
            MBUserDetailController *detailVC = [[MBUserDetailController alloc]init];
            detailVC.userModel = centerModel;
            [self.navigationController pushViewController:detailVC animated:YES];
        } failure:^(id  _Nonnull AJson) {
        }];
        
        
    };
    cell.attentionBtn.miBtnBlock = ^(){
        MBSearchUserModelBranch *userModel = self.userModel.userList[indexPath.row];
        NSDictionary *dict = @{
                               @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                               @"befansId":filter(userModel.userId),
                               @"type":@"3"
                               };
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_setFocusUs setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
            cell.attentionBtn.hidden = YES;
        } failure:^(id  _Nonnull AJson) {
        }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
