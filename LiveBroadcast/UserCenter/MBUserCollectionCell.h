//
//  MBUserCollectionCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/26.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBUserCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoImg;

@property (weak, nonatomic) IBOutlet UIImageView *flagImg;
@end

NS_ASSUME_NONNULL_END
