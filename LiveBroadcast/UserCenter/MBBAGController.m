//
//  MBBAGController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBAGController.h"
#import "MBBAGTableViewCell.h"
#import "MBShortVideoPlayController.h"
#import "MBUserDetailController.h"
@interface MBBAGController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSMutableArray *dataSource;

@end

@implementation MBBAGController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.dataSource = [NSMutableArray array];
    NSDictionary *dict = @{
                           @"userId":[NSString stringWithFormat:@"%@",[MBUserModel sharedMBUserModel].userInfo.userId],
                           @"pageNo":@"1"
                           };
    
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getGossipList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSArray *arr = AJson[@"gossipList"];
        for (NSDictionary *dict in arr) {
            NSArray *beVideoList = dict[@"beVideoList"];
            NSArray *befansList = dict[@"befansList"];
            NSMutableArray *tempArr = [NSMutableArray array];
            if (beVideoList.count != 0) [tempArr addObject:beVideoList];
            if (befansList.count != 0) [tempArr addObject:befansList];
            NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
            [tempDict setObject:tempArr forKey:@"list"];
            [tempDict setObject:dict forKey:@"AJSON"];
            [self.dataSource addObject:tempDict];
        }
        NSLog(@"self.dataSource = %@",self.dataSource);
        [self.tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataSource.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = self.dataSource[section][@"list"];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBBAGTableViewCell" owner:nil options:nil];
    MBBAGTableViewCell *cell  = [nibs lastObject];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dataDict = self.dataSource[indexPath.section];
    NSArray *tempArr = dataDict[@"list"];
    NSArray *dataArr = tempArr[indexPath.row];
    NSDictionary *ajson = dataDict[@"AJSON"];
    
    NSDictionary *dict = [dataArr objectAtIndex:0];
    if (dict[@"befansId"]) {
        cell.timeLbl.text = ajson[@"newFocusTime"];
        cell.detailLbl.text = @"关注了";
        cell.detailLbl.textColor = [UIColor blueColor];
        for (int i = 0; i < dataArr.count; i ++) {
            NSDictionary *tempD = dataArr[i];
            
            MBButton *btn = [MBButton buttonWithType:UIButtonTypeCustom];
            CGFloat btnH = (cell.bottomView.height - 10) / 2;
            CGFloat btnW = (cell.bottomView.width) / 4;
            NSInteger row = i / 4;//所在行
            NSInteger col = i % 4;//所在列
            CGFloat pading = 30;
            if (dataArr.count < 4) {
                pading = 60;
            }
            CGFloat btnX = pading +(btnW + 10) *col;
            CGFloat btnY = (btnH + 10) * row + 5;
            btn.frame = CGRectMake(btnX,btnY,btnW,btnH);
            [btn.titleLabel setFont:[UIFont systemFontOfSize:13.0f]];
            [btn setTitle:tempD[@"nickname"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            [cell.bottomView addSubview:btn];
            
            btn.miBtnBlock = ^(){
                NSDictionary *dict = dataArr[i];
                MBUserDetailController *detailVC = [[MBUserDetailController alloc]init];
                detailVC.userId = filter(dict[@"befansId"]);
                [self.navigationController pushViewController:detailVC animated:YES];
                
            };
        }

        
    }else{
        cell.timeLbl.text = ajson[@"newVideoTime"];
        cell.detailLbl.text = [NSString stringWithFormat:@"喜欢了%ld个作品",dataArr.count];
        for (int i = 0; i < dataArr.count; i ++) {
            NSDictionary *tempD = dataArr[i];
            MBButton *btn = [MBButton buttonWithType:UIButtonTypeCustom];
            CGFloat btnH = cell.bottomView.height;
            CGFloat btnW = btnH;
            CGFloat btnY = 0;
            CGFloat btnX = 60 + (btnW + 10) * i;
            btn.frame = CGRectMake(btnX, btnY, btnW, btnH );
            [btn sd_setImageWithURL:tempD[@"coverUrl"] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"bg.png"]];
            [cell.bottomView addSubview:btn];
            
            btn.miBtnBlock = ^(){
                NSDictionary *dict = dataArr[i];
                if ([dict[@"type"] isEqualToString:@"1"]) {
                    MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
                    vc.videoPath = dict[@"videoUrl"];
                    vc.videoID = dict[@"id"];
                    vc.videoType = dict[@"type2"];
                    vc.entityType = @"1";
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }else {
                    MBShortVideoPlayController *vc = [[MBShortVideoPlayController alloc]init];
                    vc.videoPath = dict[@"videoUrl"];
                    vc.videoID = dict[@"id"];
                    vc.videoType = dict[@"type2"];
                    vc.entityType = @"2";
                    [self.navigationController pushViewController:vc animated:YES];
                }
            };
        }
    }
    [cell.headerImg sd_setImageWithURL:ajson[@"headUrl"] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"bg.png"]];
    cell.userName.text = ajson[@"nickname"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


@end
