//
//  MBBlackListController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBBlackListController : MBBaseController
@property (nonatomic,strong)NSArray *blackList;
@end

NS_ASSUME_NONNULL_END
