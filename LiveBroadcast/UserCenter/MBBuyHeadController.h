//
//  MBBuyHeadController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBBuyHeadController : MBBaseController
@property (nonatomic,copy)NSDictionary *dict;
@end

NS_ASSUME_NONNULL_END
