//
//  MBMessageCenterController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBMessageCenterController : MBBaseController

@property (nonatomic,assign)NSInteger tag;
@end

NS_ASSUME_NONNULL_END
