//
//  MBUserCenterModel.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"
@class  MBUserCenterUserInfoModel;
NS_ASSUME_NONNULL_BEGIN

@interface MBUserCenterModel : MBBaseModel
//singleton_interface(MBUserCenterModel);

@property (nonatomic , copy)NSString *focuState;//关注状态（0.未关注 1.已关注

@property (nonatomic , copy)NSString *likeCount;//-查询喜欢数

@property (nonatomic , copy)NSString *privacyCount;//查询私密数

@property (nonatomic , strong)MBUserCenterUserInfoModel *userInfo;//用户信息

@property (nonatomic , copy)NSString *worksCount;//-查询作品数

- (instancetype)initWithDict:(NSDictionary *)dict;
@end



@interface MBUserCenterUserInfoModel : MBBaseModel

@property (nonatomic , copy)NSString *backgroundUrl;// --背景图像链接 

@property (nonatomic , strong)NSArray *fakeLabel;//多选标签，显示这个

@property (nonatomic , copy)NSString *fansTotal;//粉丝总数

@property (nonatomic , copy)NSString *focusTotal;//关注总数

@property (nonatomic , copy)NSString *gender;//0：未知；1：男；2：女

@property (nonatomic , copy)NSString *headUrl;// --头像

@property (nonatomic , copy)NSString *uId;//用户表主键id

@property (nonatomic , copy)NSString *nickname;//昵称

@property (nonatomic , copy)NSString *praiseTotal;//-获赞总数

@property (nonatomic , copy)NSString *referral;//-个人介绍

@property (nonatomic , copy)NSString *userId;//--用户随机生成唯一id

@property (nonatomic , copy)NSString *videoLabel;//视频标签（用上面那个显示）

- (instancetype)initWithDict:(NSDictionary *)dict;

//+ (MBUserCenterUserInfoModel *)shareInstanceWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
