//
//  MIOperation.m
//  BestMovement
//
//  Created by 肖剑 on 15/2/13.
//  Copyright (c) 2015年 MakingIt. All rights reserved.
//

#import "MIOperation.h"

@implementation MIOperation

+ (NSString *)filterString:(id)value
{
    if (value == [NSNull null] || value == nil) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@",value];
}
//时间转换时间戳
+ (NSString *)dateConversionToTimeStamp:(NSDate *)date {
    NSTimeInterval stamp = [date timeIntervalSince1970];
    NSString *timeStr = [NSString stringWithFormat:@"%.0f",stamp];
    return timeStr;
}

//时间戳转换时间
+ (NSDate *)dateConversionWithTimeStamp:(NSString *)timeStamp {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:timeStamp];
    
    NSDate *date = [formatter dateFromString:@"1283376197"];
    return date;
}

@end
