//
//  MBUserCenterModel.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBUserCenterModel.h"

@implementation MBUserCenterModel
//singleton_implementation(MBUserCenterModel);

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.focuState = filter(dict[@"focuState"]);
        self.likeCount = filter(dict[@"likeCount"]);
        self.userInfo = [[MBUserCenterUserInfoModel alloc]initWithDict:dict[@"userInfo"]];
        self.privacyCount = filter(dict[@"privacyCount"]);
        self.worksCount = filter(dict[@"worksCount"]);
    }
    return self;
}

@end

@implementation MBUserCenterUserInfoModel

//+ (MBUserCenterUserInfoModel *)shareInstanceWithDict:(NSDictionary *)dict
//{
//    static dispatch_once_t once;
//    static MBUserCenterUserInfoModel *userInfo;
//    dispatch_once(&once, ^{
//        userInfo= [[MBUserCenterUserInfoModel alloc] initWithDict:dict];
//    });
//    return userInfo;
//}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.backgroundUrl = filter(dict[@"backgroundUrl"]);
        NSData * jsonData = [dict[@"fakeLabel"] dataUsingEncoding:NSUTF8StringEncoding];
        self.fakeLabel = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        
        self.fansTotal = filter(dict[@"fansTotal"]);
        
        self.focusTotal = filter(dict[@"focusTotal"]);
        self.gender = filter(dict[@"gender"]);
        self.headUrl = filter(dict[@"headUrl"]);
        
        self.uId = filter(dict[@"id"]);
        self.nickname = filter(dict[@"nickname"]);
        self.praiseTotal = filter(dict[@"praiseTotal"]);
        
        self.referral = filter(dict[@"referral"]);
        self.userId = filter(dict[@"userId"]);
        self.videoLabel = filter(dict[@"videoLabel"]);
    }
    return self;
}

@end
