//
//  MBSearchUserModel.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBSearchUserModel : MBBaseModel
@property (nonatomic , strong)NSArray *userList;

@property (nonatomic , strong)NSArray *videoList;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end



@interface MBSearchUserModelBranch : MBBaseModel

@property (nonatomic , copy)NSString *focusType;//关注状态，1：已关注

@property (nonatomic , copy)NSString *headUrl;//-用户头像链接

@property (nonatomic , copy)NSString *userId;//用户信息id

@property (nonatomic , copy)NSString *nickname;//用户昵称

@property (nonatomic , copy)NSString *referral;//用户简介

@property (nonatomic , copy)NSString *videoLabel;//用户视频标签

//- (instancetype)initWithDict:(NSDictionary *)dict;
@end


@interface MBSearchUserModelVedio : MBBaseModel

@property (nonatomic , copy)NSString *clickPraise;//视频点赞数

@property (nonatomic , copy)NSString *clickType;//该用户点赞状态，2：未点赞

@property (nonatomic , copy)NSString *coverUrl;// -视频封面链接

@property (nonatomic , copy)NSString *downloadUrl;//视频下载链接

@property (nonatomic , copy)NSString *headUrl;// 用户头像链接

@property (nonatomic , copy)NSString *high;// 视频高度

@property (nonatomic , copy)NSString *vedioID;// 视频id

@property (nonatomic , copy)NSString *type;// 短视频,图片集类型。1：短视频

@property (nonatomic , copy)NSString *type2;// 视频类型。1：普通视频

@property (nonatomic , copy)NSString *userId;// 用户信息id

@property (nonatomic , copy)NSString *videoHeadline;// --视频标题

@property (nonatomic , copy)NSString *videoIntroduce;// 视频介绍

@property (nonatomic , copy)NSString *videoLabel;// 视频标签

@property (nonatomic , copy)NSString *videoUrl;// 视频链接

@property (nonatomic , copy)NSString *wide;// 视频宽度

//- (instancetype)initWithDict:(NSDictionary *)dict;
@end
NS_ASSUME_NONNULL_END
