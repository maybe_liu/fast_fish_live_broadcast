//
//  MBSearchUserModel.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSearchUserModel.h"

@implementation MBSearchUserModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        if ([[[dict allKeys] firstObject] isEqualToString:@"userList"]) {
            NSArray *arr = dict[@"userList"];
            NSMutableArray *addArr = [NSMutableArray array];
            for (NSDictionary *dict in arr) {
                MBSearchUserModelBranch *branch = [[MBSearchUserModelBranch alloc]init];
                branch.focusType = filter(dict[@"focusType"]);
                branch.headUrl = filter(dict[@"headUrl"]);
                branch.userId = filter(dict[@"id"]);
                branch.nickname = filter(dict[@"nickname"]);
                branch.referral = filter(dict[@"referral"]);
                branch.videoLabel = filter(dict[@"videoLabel"]);
                [addArr addObject:branch];
            }
            self.userList = [NSArray arrayWithArray:addArr];
        }else{
            NSArray *arr = dict[@"videoList"];
            NSMutableArray *addArr = [NSMutableArray array];
            for (NSDictionary *dict in arr) {
                MBSearchUserModelVedio *video = [[MBSearchUserModelVedio alloc]init];
                video.clickPraise = filter(dict[@"clickPraise"]);
                video.clickType = filter(dict[@"clickType"]);
                video.coverUrl = filter(dict[@"coverUrl"]);
                video.downloadUrl = filter(dict[@"downloadUrl"]);
                video.headUrl = filter(dict[@"headUrl"]);
                video.high = filter(dict[@"high"]);
                video.vedioID = filter(dict[@"id"]);
                video.type = filter(dict[@"type"]);
                video.type2 = filter(dict[@"type2"]);
                video.userId = filter(dict[@"userId"]);
                video.videoHeadline = filter(dict[@"videoHeadline"]);
                video.videoIntroduce = filter(dict[@"videoIntroduce"]);
                video.videoLabel = filter(dict[@"videoLabel"]);
                video.videoUrl = filter(dict[@"videoUrl"]);
                video.wide = filter(dict[@"wide"]);
                [addArr addObject:video];
            }
            self.videoList = [NSArray arrayWithArray:addArr];

        }
        
        
    }
    return self;
}

@end

@implementation MBSearchUserModelBranch

//- (instancetype)initWithDict:(NSDictionary *)dict
//{
//    if (self = [super init]) {
//
//    }
//    return self;
//}

@end


@implementation MBSearchUserModelVedio

//- (instancetype)initWithDict:(NSDictionary *)dict
//{
//    if (self = [super init]) {
//
//    }
//    return self;
//}

@end
