//
//  MIOperation.h
//  BestMovement
//
//  Created by 肖剑 on 15/2/13.
//  Copyright (c) 2015年 MakingIt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MIOperation : NSObject

+ (instancetype)shareOperation;

+ (NSString *)filterString:(id)value;

//时间转换时间戳
+ (NSString *)dateConversionToTimeStamp:(NSDate *)date;

//时间戳转换时间
+ (NSDate *)dateConversionWithTimeStamp:(NSString *)timeStamp;

@end
