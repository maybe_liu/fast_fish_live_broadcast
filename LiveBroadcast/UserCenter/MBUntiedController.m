//
//  MBUntiedController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/4.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBUntiedController.h"
#import "MITimerButton.h"
@interface MBUntiedController ()
{
    UIView *untiedView;
    UIView *phoneView;
    UITextField *phoneTf;
    UITextField *codeTf;
    UITextField *codeT;
    NSString *getCodeStr;
}
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UILabel *phoneLbl;
@property (weak, nonatomic) IBOutlet UIButton *untiedBtn;
@property (assign,nonatomic)NSInteger clickTag;

@end

@implementation MBUntiedController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.detailLbl.text = @"当前绑定手机号码";
    self.title = @"解绑";
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSString *phone = [MBUserModel sharedMBUserModel].phone;
    phone = [phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    self.phoneLbl.text = phone;
    self.untiedBtn.layer.cornerRadius = 22;
    
    self.clickTag = 0;
    
    // 控件
    // 解绑View
    untiedView = [[UIView alloc] init];
    untiedView.frame = CGRectMake(0, self.detailLbl.bottom + 10, IPHONE_SCREEN_WIDTH, 60);
    untiedView.hidden = YES;
    [self.view addSubview:untiedView];
    
    // 验证码
    UILabel *codeLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 60)];
    codeLbl.text = @"验证码";
    codeLbl.textColor = kColorRGBA(51, 51, 51, 1);
    [untiedView addSubview:codeLbl];
    
    codeTf = [[UITextField alloc]initWithFrame:CGRectMake(codeLbl.right, codeLbl.top, IPHONE_SCREEN_WIDTH - codeLbl.right - 100, codeLbl.height)];
    codeTf.placeholder = @"请输入验证码";
    [untiedView addSubview:codeTf];
    
    //获取验证码
    MITimerButton *codeBtn= [MITimerButton buttonWithType:UIButtonTypeCustom];
    codeBtn.frame = CGRectMake(codeTf.right, 10, 100, 40);
    [codeBtn setBackgroundColor:[UIColor whiteColor]];
    codeBtn.tag = 101;
    [codeBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateNormal];
    [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
//    codeBtn.isCountiue = NO;
    [codeBtn addTarget:self action:@selector(addCodeUntiedClick:) forControlEvents:UIControlEventTouchUpInside];
    [untiedView addSubview:codeBtn];
    
    UIView *codeLine = [[UIView alloc] init];
    codeLine.frame = CGRectMake(codeLbl.left,59,IPHONE_SCREEN_WIDTH - 40,0.5);
    codeLine.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [untiedView addSubview:codeLine];
    
    
    phoneView = [[UIView alloc] init];
    phoneView.frame = CGRectMake(0, 10, IPHONE_SCREEN_WIDTH, 120);
    phoneView.hidden = YES;
    [self.view addSubview:phoneView];
    
    // 手机号码
    UILabel *phoneLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 60, 60)];
    phoneLbl.text = @"+86";
    phoneLbl.textColor = kColorRGBA(51, 51, 51, 1);
    [phoneView addSubview:phoneLbl];
    
    UIView *phoneLine = [[UIView alloc]initWithFrame:CGRectMake(phoneLbl.right + 10, phoneLbl.top, 0.5, phoneLbl.height)];
    phoneLine.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [phoneView addSubview:phoneLine];
    
    phoneTf = [[UITextField alloc]initWithFrame:CGRectMake(phoneLine.right + 20, phoneLine.top, IPHONE_SCREEN_WIDTH - phoneLine.left -20, phoneLine.height)];
    phoneTf.placeholder = @"请输入手机号码";
    [phoneView addSubview:phoneTf];
    
    UIView *phoneLine1 = [[UIView alloc] init];
    phoneLine1.frame = CGRectMake(phoneLbl.left,phoneLbl.bottom,IPHONE_SCREEN_WIDTH - 40,0.5);
    phoneLine1.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [phoneView addSubview:phoneLine1];
    
    // 验证码
    UILabel *codeL = [[UILabel alloc]initWithFrame:CGRectMake(20, 60, 60, 60)];
    codeL.text = @"验证码";
    codeL.textColor = kColorRGBA(51, 51, 51, 1);
    [phoneView addSubview:codeL];
    
    codeT = [[UITextField alloc]initWithFrame:CGRectMake(phoneTf.left, codeLbl.top + 60, IPHONE_SCREEN_WIDTH - phoneTf.left - 100, codeLbl.height)];
    codeT.placeholder = @"请输入验证码";
    [phoneView addSubview:codeT];
    
    //获取验证码
    MITimerButton *codeB= [MITimerButton buttonWithType:UIButtonTypeCustom];
    codeB.frame = CGRectMake(codeTf.right, 70, 100, 40);
    [codeB setBackgroundColor:[UIColor whiteColor]];
    codeB.tag = 101;
    [codeB setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateNormal];
    [codeB setTitle:@"获取验证码" forState:UIControlStateNormal];
//    codeB.isCountiue = NO;
    [codeB addTarget:self action:@selector(addCodeBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [phoneView addSubview:codeB];
    
    UIView *codeLine1 = [[UIView alloc] init];
    codeLine1.frame = CGRectMake(phoneLbl.left,119,IPHONE_SCREEN_WIDTH - 40,0.5);
    codeLine1.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1.0];
    [phoneView addSubview:codeLine1];
    
}
// 解绑
- (void)addCodeUntiedClick:(id)sender
{
    NSDictionary *dict =@{@"tel":[MBUserModel sharedMBUserModel].phone,@"type":@"3"};
    [MBHttpRequset requestWithUrl:kTradeid_login_sendSms setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self->getCodeStr = AJson;
        NSLog(@"kTradeid_login_sendSms = %@",AJson);
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
// 绑定
- (void)addCodeBtnClick:(id)sender
{//19866070713
    if ([phoneTf.text isEqualToString:@""] && phoneTf.text.length != 11) {
        [MBProgressHUD showMessage:@"请输入完整的手机号码"];
        return;
    }
    NSDictionary *dict =@{@"tel":phoneTf.text,@"type":@"3"};
    [MBHttpRequset requestWithUrl:kTradeid_login_sendSms setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self->getCodeStr = AJson;
        NSLog(@"kTradeid_login_sendSms = %@",AJson);
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

- (IBAction)untiedClick:(id)sender {
    if (self.clickTag == 0) {// 解绑
        self.detailLbl.text = [NSString stringWithFormat:@"验证码已发送至%@",self.phoneLbl.text];
        untiedView.hidden = NO;
        self.phoneLbl.hidden = YES;
        [self.untiedBtn setTitle:@"下一步" forState:UIControlStateNormal];
    }else if(self.clickTag == 1){ // 下一步
        self.title = @"新号码";
        if ([getCodeStr isEqualToString:codeTf.text]) {
            self.detailLbl.hidden = YES;
            untiedView.hidden = YES;
            self.phoneLbl.hidden = YES;
            phoneView.hidden = NO;
            [self.untiedBtn setTitle:@"确定" forState:UIControlStateNormal];
        }else{
            [MBProgressHUD showMessage:@"验证码校验失败"];
            self.clickTag --;
        }
        
    }else if(self.clickTag == 2){// 确定
        if ([phoneTf.text isEqualToString:@""] && phoneTf.text.length != 11 && ![getCodeStr isEqualToString:codeT.text]) {
            [MBProgressHUD showMessage:@"请输入正确的手机号码"];
            return;
        }
        if (![getCodeStr isEqualToString:codeT.text]) {
            [MBProgressHUD showMessage:@"验证码校验失败"];
            return;
        }
        NSDictionary *dict =@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId,@"phone":phoneTf.text};
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_changeNumber setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
            if ([AJson[@"message"] isEqualToString:@"操作成功"]) {
                //沙盒ducument目录
                NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                //完整的文件路径
                NSString *path = [docPath stringByAppendingPathComponent:@"userModel.archive"];
                BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
                if (exists) {
                    NSLog(@"======================================路径正确");
                    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
                }
                
                //        [MBUserModel sharedMBUserModel].isLoginFlag = NO;
                //        MBUserModel *removeModel = [MBUserModel shareInstanceWithDict:@{}];
                //        [self archiveDatas:removeModel];
                
                [MBUserModel attempDealloc];
                [MBUserInfo attempDealloc];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:MBIsLoginNotification object:nil userInfo:@{@"isFlag":MBLogoutFlag}];
                [self.navigationController popToRootViewControllerAnimated:YES];
                [[RCIM sharedRCIM] disconnect:NO];
                [self tencentChatLogout];
                return ;
            }
            [MBProgressHUD showMessage:AJson[@"message"]];
        } failure:^(id  _Nonnull AJson) {
            
        }];
    }else{
        
    }
    self.clickTag ++;
}

-(void)tencentChatLogout {
    [[TIMManager sharedInstance] logout:^() {
        NSLog(@"logout succ");
    } fail:^(int code, NSString * err) {
        NSLog(@"logout fail: code=%d err=%@", code, err);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
