//
//  MBHeadLinesController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBHeadLinesController.h"
#import "MBUserCollectionCell.h"
#import "MBBuyHeadController.h"
@interface MBHeadLinesController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *collectionBgVIew;
@property (nonatomic, strong)NSArray *dataSource;
@property (nonatomic, strong)NSMutableArray *imageArr;
@property (nonatomic,strong)NSIndexPath *currentIndexPath;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation MBHeadLinesController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [NSArray array];
    self.imageArr = [NSMutableArray array];
    self.title = @"热门头条";
    
    UICollectionViewFlowLayout *fl = [[UICollectionViewFlowLayout alloc]init];
//    UICollectionView *colletionV = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 25, self.collectionBgVIew.width, self.collectionBgVIew.height - 20) collectionViewLayout:fl];
    self.collectionView.collectionViewLayout = fl;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = self.collectionBgVIew.backgroundColor;
//    self.collectionBgVIew.backgroundColor = [UIColor redColor];
//    [self.collectionBgVIew addSubview: colletionV];
//    _collectionView = colletionV;
    //布局
    fl.minimumInteritemSpacing = 0;
    fl.minimumLineSpacing = 5;
    [_collectionView registerNib:[UINib nibWithNibName:@"MBUserCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"USERCONLECTIONCELL"];
    
    NSDictionary *dict = @{
                           @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                           @"type":@"1",
                           @"pageNo":@"1"
                           };
    UICollectionView *reCollectionView = _collectionView;
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getPersonalClassify setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.dataSource = AJson[@"worksList"];
        [reCollectionView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];

}

#pragma mark - UICollectionView delegate & dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataSource.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((IPHONE_SCREEN_WIDTH - 20)/3, (IPHONE_SCREEN_WIDTH - 20)/3);
}
//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MBUserCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"USERCONLECTIONCELL" forIndexPath:indexPath];
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.frame = CGRectMake((cell.width - 34)/2, (cell.height - 26)/2, 34, 26);
    imageV.image = [UIImage imageNamed:@"gou.png"];
    imageV.hidden = YES;
    imageV.tag = indexPath.row;
    [cell addSubview:imageV];
    [self.imageArr addObject:imageV];
    
    if (self.dataSource.count != 0){
        NSDictionary *dict = self.dataSource[indexPath.row];
        [cell.videoImg sd_setImageWithURL:dict[@"coverUrl"] placeholderImage:[UIImage imageNamed:@"danshuiyu.png"]];
        if ([dict[@"type"] isEqualToString:@"1"]) {//短视频
            cell.flagImg.image = [UIImage imageNamed:@"duanship.png"];
        }else{//图片
            cell.flagImg.image = [UIImage imageNamed:@"pic.png"];
        }
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"self.dataSource  = %@",self.dataSource);
    NSDictionary *dict = self.dataSource[indexPath.row];
    MBUserCollectionCell *cell = (MBUserCollectionCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.currentIndexPath == indexPath) {
        return;
    }
    
    UIImageView *img = self.imageArr[indexPath.row];
    img.hidden =  NO;
    
    if (self.currentIndexPath == nil) {
        self.currentIndexPath = indexPath;
        return;
    }
    UIImageView *currentImg = self.imageArr[self.currentIndexPath.row];
    currentImg.hidden = YES;
    self.currentIndexPath = indexPath;
}


- (IBAction)buyHeadClick {
    MBBuyHeadController *buyVC = [[MBBuyHeadController alloc]init];
    buyVC.dict = self.dataSource[self.currentIndexPath.row];
    [self.navigationController pushViewController:buyVC animated:YES];
}


@end
