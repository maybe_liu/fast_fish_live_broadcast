//
//  MBMessageCenterController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBMessageCenterController.h"
#import "ZJScrollPageView.h"
#import "MBMessageListController.h"
#import "MBBAGController.h"
#import "MBNotifiController.h"

@interface MBMessageCenterController ()<ZJScrollPageViewDelegate>
@property(nonatomic, strong)UIButton *leftNavButton;
@property(nonatomic, strong)UIButton *rightNavButton;
@property(nonatomic, strong)NSArray  *titleArray;
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;
@property (weak, nonatomic) ZJScrollSegmentView *segmentView;
@property (weak, nonatomic) ZJContentView *contentView;

@end

@implementation MBMessageCenterController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createUI];
    [self createCustomNav];
    [self.segmentView setSelectedIndex:self.tag animated:YES];

}


-(void)createUI {
    self.childVcs = [self setupChildVC];
    [self setupSegmentView];
    [self setupContentView];
}
-(void)createCustomNav {
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightNavButton];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.leftNavButton];
    self.navigationItem.title = @"";
}
-(NSArray *)setupChildVC {
    MBBAGController *allVC = [[MBBAGController alloc]init];
    MBNotifiController *localVC = [[MBNotifiController alloc]init];
    MBMessageListController *msgVC = [[MBMessageListController alloc]init];
    NSArray *vcArray = @[allVC,localVC,msgVC];
    return vcArray;
}

-(void)setupSegmentView {
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.showLine = YES;
    style.scrollLineColor = Color_FF8000;
    style.titleFont = kMFont(14);
    style.titleBigScale = 1.2;
    style.normalTitleColor = Color_666666;
    style.selectedTitleColor = Color_FF8000;
    // 不要滚动标题, 每个标题将平分宽度
    style.scrollTitle = YES;
    style.adjustCoverOrLineWidth = YES;
    style.titleMargin = 30.0f;
    style.autoAdjustTitlesWidth = YES;
    self.titles = self.titleArray;
    
    // 注意: 一定要避免循环引用!!
    __weak typeof(self) weakSelf = self;
    ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, 64.0, 200, 28.0) segmentStyle:style delegate:self titles:self.titles titleDidClick:^(ZJTitleView *titleView, NSInteger index) {
        
        [weakSelf.contentView setContentOffSet:CGPointMake(weakSelf.contentView.bounds.size.width * index, 0.0) animated:YES];
        
    }];
    // 当然推荐直接设置背景图片的方式
    //    segment.backgroundImage = [UIImage imageNamed:@"extraBtnBackgroundImage"];
    
    self.segmentView = segment;
    self.navigationItem.titleView = self.segmentView;
}
-(void)setupContentView {
    ZJContentView *content = [[ZJContentView alloc] initWithFrame:self.view.frame segmentView:self.segmentView parentViewController:self delegate:self];
    self.contentView = content;
    [self.view addSubview:self.contentView];
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}
- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        childVc = self.childVcs[index];
    }
    
    return childVc;
}


#pragma mark - event
//-(void)leftNavButtonClick {
//    MBLoginController *loginVC = [[MBLoginController alloc]init];
//    [self presentViewController:loginVC animated:YES completion:nil];
//}
//
//-(void)rightNavButtonClick:(UIButton *)button {
//    MBShortVideoController *vc = [[MBShortVideoController alloc]init];
//    [self.navigationController pushViewController:vc animated:YES];
//}

#pragma mark - getter and setter
//-(UIButton *)leftNavButton {
//    if (_leftNavButton == nil) {
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:kImage(@"") forState:UIControlStateNormal];
//        [button setTitle:@"登录" forState:UIControlStateNormal];
//        //        button.frame = (CGRect){{0,0},{40,40}};
//        [button setTitleColor:Color_FF8000 forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(leftNavButtonClick) forControlEvents:UIControlEventTouchUpInside];
//        button.titleLabel.font = kFont(14);
//        button.hidden = YES;
//        _leftNavButton = button;
//    }
//    return _leftNavButton;
//}

//-(UIButton *)rightNavButton {
//    if (_rightNavButton == nil) {
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setImage:kImage(@"sheyingji") forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(rightNavButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        _rightNavButton = button;
//    }
//    return _rightNavButton;
//}

-(NSArray *)titleArray {
    if (_titleArray == nil) {
        _titleArray = @[@"八卦",@"通知",@"私信"];
    }
    return _titleArray;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

@end
