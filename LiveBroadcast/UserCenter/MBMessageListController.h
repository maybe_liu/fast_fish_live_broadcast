//
//  MBMessageListController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "ZJScrollPageViewDelegate.h"


NS_ASSUME_NONNULL_BEGIN

@interface MBMessageListController : RCConversationListViewController<ZJScrollPageViewChildVcDelegate>

@end

NS_ASSUME_NONNULL_END
