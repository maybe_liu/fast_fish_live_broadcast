//
//  MBBlackListCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBBlackListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *headBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet MBButton *removeBtn;

@property (nonatomic,strong)NSDictionary *blackModel;
@end

NS_ASSUME_NONNULL_END
