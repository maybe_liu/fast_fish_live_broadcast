//
//  MBBlackListCell.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBlackListCell.h"

@implementation MBBlackListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.headBtn.layer.cornerRadius = 20;
    self.removeBtn.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setBlackModel:(NSDictionary *)blackModel
{
    [self.headBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:blackModel[@"headUrl"]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"danshuiyu.png"]];
    self.nameLbl.text = blackModel[@"nickname"];
}


@end
