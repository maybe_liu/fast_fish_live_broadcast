//
//  MBUserTableCell.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBUserTableCell.h"

@implementation MBUserTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.userIconBtn.layer.cornerRadius = 25;
    self.userIconBtn.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUserModel:(MBSearchUserModelBranch *)userModel
{
    _userModel = userModel;
    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:userModel.headUrl] forState:UIControlStateNormal];
    self.userNameLbl.text = userModel.nickname;
    self.detailLbl.text = userModel.referral;
    
    self.attentionBtn.hidden = ([[NSString stringWithFormat:@"%@",userModel.focusType] isEqualToString:@"1"])?YES:NO;
    
}

@end
