//
//  MBUserTableCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBSearchUserModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MBUserTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MBButton *userIconBtn;

@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet MBButton *attentionBtn;

@property (nonatomic , strong) MBSearchUserModelBranch *userModel;
@end

NS_ASSUME_NONNULL_END
