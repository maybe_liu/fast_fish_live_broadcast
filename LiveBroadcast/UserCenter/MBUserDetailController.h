//
//  MBUserDetailController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/20.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"
#import "MBSearchUserModel.h"
#import "MBUserCenterModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MBUserDetailController : MBBaseController

@property (nonatomic,strong)MBUserCenterModel *userModel;

@property (nonatomic,copy)NSString *userId;
@end

NS_ASSUME_NONNULL_END
