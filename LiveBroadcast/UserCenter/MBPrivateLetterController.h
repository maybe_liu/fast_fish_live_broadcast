//
//  MBPrivateLetterController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/13.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^MBPrivateLetterControllerSelect)(NSString *);

@interface MBPrivateLetterController : MBBaseController

@property (nonatomic, copy)MBPrivateLetterControllerSelect privateVCSelectBlock;

@property (nonatomic, copy)NSString *headerTitle;

@end

NS_ASSUME_NONNULL_END
