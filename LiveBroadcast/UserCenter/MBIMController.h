//
//  MBIMController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <RongIMKit/RongIMKit.h>
#import "MBUserCenterModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MBIMController : RCConversationViewController

@property (nonatomic,strong)MBUserCenterModel *userModel;


@property (nonatomic,copy)NSString *userID;
@end

NS_ASSUME_NONNULL_END
