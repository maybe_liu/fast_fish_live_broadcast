//
//  MBAccoutModel.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBAccoutModel.h"

@implementation MBAccoutModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.cashBalance = [NSString stringWithFormat:@"%@",dict[@"cashBalance"]];
        self.accUsrInfo = [[MBAccoutUserInfoModel alloc]initWithDict:dict[@"userInfo"]];
    }
    return self;
}

@end


@implementation MBAccoutUserInfoModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.fishCoins = [NSString stringWithFormat:@"%.2f",[dict[@"fishCoins"] doubleValue]];
        self.uId = [NSString stringWithFormat:@"%@",dict[@"id"]];
        self.yellowJewel = [NSString stringWithFormat:@"%@",dict[@"yellowJewel"]];
        
        NSLog(@"uid = %@   userId = %@",self.uId,[MBUserModel sharedMBUserModel].userInfo.userId);
    }
    return self;
}

@end
