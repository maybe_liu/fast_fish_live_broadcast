//
//  MBAccoutModel.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/17.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  MBAccoutUserInfoModel;
NS_ASSUME_NONNULL_BEGIN

@interface MBAccoutModel : MBBaseModel
@property (nonatomic,copy)NSString *cashBalance;//可提取总额

@property (nonatomic,strong)MBAccoutUserInfoModel *accUsrInfo;//账户信息


- (instancetype)initWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END

NS_ASSUME_NONNULL_BEGIN

@interface MBAccoutUserInfoModel : MBBaseModel
@property (nonatomic,copy)NSString *fishCoins;//鱼币

@property (nonatomic,copy)NSString *uId;//用户表主键id

@property (nonatomic,copy)NSString *yellowJewel;//黄钻


- (instancetype)initWithDict:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
