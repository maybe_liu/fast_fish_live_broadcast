//
//  MBAESEncrypt.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/5/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBAESEncrypt : NSObject

+ (NSData *)encryptData:(NSData *)data key:(NSData *)key;

+ (NSData *)decryptData:(NSData *)data key:(NSData *)key;

@end

NS_ASSUME_NONNULL_END
