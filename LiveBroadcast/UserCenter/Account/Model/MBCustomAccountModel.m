//
//  MBCustomAccountModel.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/5/8.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBCustomAccountModel.h"
@implementation MBCustomUserInfo
+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
             @"ID": @"id",
             };
}

@end
@implementation MBCustomAccountModel

+(NSDictionary *)mj_objectClassInArray {
    return @{
             @"userInfo":[MBCustomUserInfo class],
             };
}
@end
