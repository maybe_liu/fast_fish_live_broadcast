//
//  MBAccountMainController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBAccountMainController.h"
#import "MBRechargeController.h"// 充值
#import "MBWithdrawController.h"// 提现
#import "MBYellow2FishController.h"// 黄钻到鱼币
#import "MBMyAccountController.h"// 我的账户
#import "MBGetGiftController.h"// 我收到的礼物
#import "MBSendGiftController.h"// 送出的礼物
#import "MBRedEnvelopeController.h"// 红包
#import "MBAccoutModel.h"
@interface MBAccountMainController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_titles;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *fishMoney;// 鱼币
@property (weak, nonatomic) IBOutlet UILabel *yellowLbl;// 黄钻
@property (weak, nonatomic) IBOutlet UILabel *realMoneyLbl;// 可提
@property (strong , nonatomic)MBAccoutModel *model;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MBAccountMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _titles = @[
                @[
                    @{
                      @"title":@"充值鱼币",@"icon":@"yubi2.png"
                      }
                  ],
                @[
                    @{
                      @"title":@"提现",@"icon":@"tixian.png"
                      },
                    @{
                      @"title":@"黄钻兑换鱼币",@"icon":@"zhuanguan.png"
                      }
                  ],
                @[
                    @{
                        @"title":@"我的账单",@"icon":@"zhangdan.png"
                        },
                    @{
                        @"title":@"收到的礼物",@"icon":@"shoudao.png"
                        },
                    @{
                        @"title":@"送出的礼物",@"icon":@"songchu.png"
                        },
                    @{
                        @"title":@"红包记录",@"icon":@"hongbao.png"
                        },
                    ]];
    
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getAccountData setParams:@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.model = [[MBAccoutModel alloc]initWithDict:AJson];
        self.fishMoney.text = self.model.accUsrInfo.fishCoins;
        self.yellowLbl.text = self.model.accUsrInfo.yellowJewel;
        self.realMoneyLbl.text = self.model.cashBalance;
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_titles[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SETTINGCELL"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"SETTINGCELL"];
        cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
        cell.textLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0f];
        cell.detailTextLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    NSDictionary *dict =  _titles[indexPath.section][indexPath.row];
    cell.textLabel.text = dict[@"title"];
    cell.imageView.image = [UIImage imageNamed:dict[@"icon"]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     #import "MBRechargeController.h"// 充值
     #import "MBWithdrawController.h"// 提现
     #import "MBYellow2FishController.h"// 黄钻到鱼币
     #import "MBMyAccountController.h"// 我的账户
     #import "MBGetGiftController.h"// 我收到的礼物
     #import "MBSendGiftController.h"// 送出的礼物
     #import "MBRedEnvelopeController.h"// 红包
     */
    
    if (indexPath.section == 0) {// 充值鱼币
        MBRechargeController *rechargeVC = [[MBRechargeController alloc]init];
        rechargeVC.title = @"充值鱼币";
        rechargeVC.accoutModel = self.model;
        [self.navigationController pushViewController:rechargeVC animated:YES];
    }else if(indexPath.section == 1){
        switch (indexPath.row) {
            case 0:// 提现
            {
                MBWithdrawController *withdraw = [[MBWithdrawController alloc]init];
                withdraw.title = @"提现";
                withdraw.accoutModel = self.model;
                [self.navigationController pushViewController:withdraw animated:YES];
            }
                break;
            case 1:// 黄钻兑换鱼币
            {
                MBYellow2FishController *yellow2FishVC = [[MBYellow2FishController alloc]init];
                yellow2FishVC.title = @"黄钻兑换鱼币";
                yellow2FishVC.accoutModel = self.model;
                [self.navigationController pushViewController:yellow2FishVC animated:YES];
            }
                break;
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:// 我的账单
            {
                MBMyAccountController *myAccount = [[MBMyAccountController alloc]init];
                myAccount.title = @"我的账户";
                myAccount.accoutModel = self.model;
                [self.navigationController pushViewController:myAccount animated:YES];
            }
                break;
            case 1:// 收到的礼物
            {
                MBGetGiftController *getGift = [[MBGetGiftController alloc]init];
                getGift.title = @"收到的礼物";
                getGift.accoutModel = self.model;
                [self.navigationController pushViewController:getGift animated:YES];
            }
                break;
            case 2:// 送出的礼物
            {
                MBSendGiftController *sendGift = [[MBSendGiftController alloc]init];
                sendGift.title = @"送出的礼物";
                sendGift.accoutModel = self.model;
                [self.navigationController pushViewController:sendGift animated:YES];
            }
                break;
            case 3:// 红包记录
            {
                MBRedEnvelopeController *redEnveVC = [[MBRedEnvelopeController alloc]init];
                redEnveVC.title = @"红包记录";
                redEnveVC.accoutModel = self.model;
                [self.navigationController pushViewController:redEnveVC animated:YES];
            }
                break;
            default:
                break;
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //设置导航栏透明
    [self.navigationController.navigationBar setTranslucent:true];
    //把背景设为空
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //处理导航栏有条线的问题
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    if (_titles != nil) {
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_getAccountData setParams:@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId} isShowProgressView:NO success:^(id  _Nonnull AJson) {
            self.model = [[MBAccoutModel alloc]initWithDict:AJson];
            self.fishMoney.text = self.model.accUsrInfo.fishCoins;
            self.yellowLbl.text = self.model.accUsrInfo.yellowJewel;
            self.realMoneyLbl.text = self.model.cashBalance;
        } failure:^(id  _Nonnull AJson) {
            
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
    
