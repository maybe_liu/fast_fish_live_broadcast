//
//  MBMyAccountController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBMyAccountController.h"
#import "MBMyAccountCell.h"
@interface MBMyAccountController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    NSInteger pageCount;
}

@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;
@property (weak, nonatomic) IBOutlet UIButton *withdrawBtn;
@property (weak, nonatomic) IBOutlet UIButton *exChangeBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic)  NSArray *chargeDatas;// 充值
@property (strong, nonatomic)  NSArray *withdrawDatas;// 提现
@property (strong, nonatomic)  NSArray *getChargeDatas;// 兑换
@property (strong, nonatomic)  NSMutableArray *tableViews;
@property (strong, nonatomic)  NSMutableArray *datasArr;
@property (weak, nonatomic)UIPageControl *pageControl;
@end

@implementation MBMyAccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.chargeBtn.selected = YES;
    self.scrollView.contentSize = CGSizeMake(IPHONE_SCREEN_WIDTH * 3, 0);
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    
    self.tableViews = [NSMutableArray array];
    self.chargeDatas = [NSArray array];
    self.withdrawDatas = [NSArray array];
    self.getChargeDatas = [NSArray array];
    
    for (int i = 0; i < 3; i ++) {
        CGRect rect = CGRectMake(i * IPHONE_SCREEN_WIDTH, 0, IPHONE_SCREEN_WIDTH, self.scrollView.height);
        UITableView *tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        [self.scrollView addSubview:tableView];
        [self.tableViews addObject:tableView];
    }
    [self getDatas];
}

- (void)getDatas
{
    NSDictionary *dict = @{
                           @"userId":self.accoutModel.accUsrInfo.uId,
                           @"pageNo":@"0"
                           };
    // 充值列表
    [MBHttpRequset requestWithUrl:kLive_live_getTopUpList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"充值列表 = = = %@",AJson);
        self.chargeDatas = AJson[@"topupRecords"];
        UITableView *tableView = [self.tableViews objectAtIndex:0];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    // 提取列表
    [MBHttpRequset requestWithUrl:kLive_live_getExtractCashList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"提取列表 = = = %@",AJson);
        self.withdrawDatas = AJson[@"topupInfos"];
        UITableView *tableView = [self.tableViews objectAtIndex:1];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    // 兑换列表
    [MBHttpRequset requestWithUrl:kLive_live_getExchangeList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"兑换列表 = = = %@",AJson);
        self.getChargeDatas = AJson[@"exchangeRecords"];
        UITableView *tableView = [self.tableViews objectAtIndex:2];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 3; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 充值
                case 0:
                {
                    arr = self.chargeDatas;
                }
                    break;
                case 1:// 提现
                {
                    arr = self.withdrawDatas;
                }
                    break;
                case 2: // 兑换
                {
                    arr = self.getChargeDatas;
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    NSLog(@"arr===============%@",arr);
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"USERCELL";
    MBMyAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBMyAccountCell" owner:nil options:nil];
        cell = [nibs lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
//    UITableView *tableView = [self.tableViews objectAtIndex:<#(NSUInteger)#>];
    
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 3; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 充值
                case 0:
                {
                    arr = self.chargeDatas;
                    NSDictionary *dict = arr[indexPath.row];
                    NSString *payWay = [dict[@"payWay"] isEqualToString:@"1"]?@"微信":@"支付宝";
                    
                    cell.titleLbl.text = [NSString stringWithFormat:@"%@充值%@鱼币",payWay,dict[@"refundFish"]];
                    
                    NSTimeInterval interval    =[dict[@"payTime"] doubleValue] / 1000.0;
                    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *dateString       = [formatter stringFromDate: date];
                    cell.detailLbl.text = dateString;
                    if ( [dict[@"payState"] isEqualToString:@"1"]) {// 未支付
                        cell.statusLbl.text = @"未支付";
                    }else if ([dict[@"payState"] isEqualToString:@"2"]){// 退款中
                        cell.statusLbl.text = @"退款中";
                    }else if ([dict[@"payState"] isEqualToString:@"3"]){// k退款成功
                        cell.statusLbl.text = @"退款成功";
                    }else if ([dict[@"payState"] isEqualToString:@"4"]){// 已消费
                        cell.statusLbl.text = @"已消费";
                    }else{// 退款
                        cell.statusLbl.text = @"退款";
                    }
                    
                }
                    break;
                case 1:// 提现
                {
                    arr = self.withdrawDatas;
                    NSDictionary *dict = arr[indexPath.row];
//                    NSString *payWay = [dict[@"payWay"] isEqualToString:@"1"]?@"微信":@"支付宝";
                    if ([dict[@"type"]isEqualToString:@"1"]) {
                    
                        cell.titleLbl.text = [NSString stringWithFormat:@"提现%@人民币",dict[@"balance"]];
                        
                        NSTimeInterval interval    =[dict[@"createTime"] doubleValue] / 1000.0;
                        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSString *dateString       = [formatter stringFromDate: date];
                        cell.detailLbl.text = dateString;
                    }
                    
                }
                    break;
                case 2: // 兑换
                {
                    arr = self.getChargeDatas;
                    NSDictionary *dict = arr[indexPath.row];
                    //                    NSString *payWay = [dict[@"payWay"] isEqualToString:@"1"]?@"微信":@"支付宝";
                    if ([dict[@"type"]isEqualToString:@"2"]) {
                        
                        cell.titleLbl.text = [NSString stringWithFormat:@"兑换%@鱼币",dict[@"fishCoin"]];
                        
                        NSTimeInterval interval    =[dict[@"createTime"] doubleValue] / 1000.0;
                        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSString *dateString       = [formatter stringFromDate: date];
                        cell.detailLbl.text = dateString;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}





- (IBAction)segmClick:(UIButton *)sender {
    sender.selected = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.lineView.left = sender.left;
    }];
    [self setStatusWithTag:sender.tag];
}

- (void)setStatusWithTag:(NSInteger)tag
{
    switch (tag) {
        case 1001:
        {
            self.withdrawBtn.selected = NO;
            self.exChangeBtn.selected = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(0, 0);
            }];
        }
            break;
        case 1002:
        {
            self.chargeBtn.selected = NO;
            self.exChangeBtn.selected = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(IPHONE_SCREEN_WIDTH, 0);
            }];
        }
            break;
        case 1003:
        {
            self.withdrawBtn.selected = NO;
            self.chargeBtn.selected = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(IPHONE_SCREEN_WIDTH * 2, 0);
            }];
        }
            break;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 用contentOffset来计算当前的页码
    pageCount = self.scrollView.contentOffset.x/self.scrollView.bounds.size.width;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    if (scrollView.contentOffset.x == 0) {
        self.chargeBtn.selected = YES;
        self.withdrawBtn.selected = NO;
        self.exChangeBtn.selected = NO;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineView.left = self.chargeBtn.left;
        }];
    }else if (scrollView.contentOffset.x > 350 && scrollView.contentOffset.x < 700){
        self.chargeBtn.selected = NO;
        self.withdrawBtn.selected = YES;
        self.exChangeBtn.selected = NO;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineView.left = self.withdrawBtn.left;
        }];
    }else{
        self.chargeBtn.selected = NO;
        self.withdrawBtn.selected = NO;
        self.exChangeBtn.selected = YES;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineView.left = self.exChangeBtn.left;
        }];
    }

}


@end
