//
//  MBYellow2FishController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBYellow2FishController.h"

@interface MBYellow2FishController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *yellowTF;
@property (weak, nonatomic) IBOutlet UITextField *fishTF;
@property (weak, nonatomic) IBOutlet UILabel *yellowLab;

@end

@implementation MBYellow2FishController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.yellowLab.text = filter(self.accoutModel.accUsrInfo.yellowJewel);
    self.yellowTF.delegate = self;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    self.fishTF.text = textField.text;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //设置导航栏透明
    [self.navigationController.navigationBar setTranslucent:true];
    //把背景设为空
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //处理导航栏有条线的问题
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}



- (IBAction)exchangeClick {
    if (self.yellowTF.text.length == 0) {
        [MBProgressHUD showError:@"请输入正确的黄钻金额"];
        return;
    }
    NSDictionary *dict = @{
                           @"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId),
                           @"yellowJewel":filter(self.yellowTF.text),
                           @"fishCoins":filter(self.yellowTF.text)
                           };
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_yellowExchangeFish setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
