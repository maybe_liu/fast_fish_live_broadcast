//
//  MBRedEnvelopeController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBRedEnvelopeController.h"

@interface MBRedEnvelopeController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic)  NSArray *getRedDatas;// 充值
@property (strong, nonatomic)  NSArray *sendRedDatas;// 提现
@property (strong, nonatomic)  NSMutableArray *tableViews;
@property (weak, nonatomic)UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIButton *getRedBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendRedBtn;
@property (weak, nonatomic) IBOutlet UIView *lineV;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation MBRedEnvelopeController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.getRedBtn.selected = YES;
    self.scrollView.contentSize = CGSizeMake(IPHONE_SCREEN_WIDTH * 2, 0);
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    
    self.tableViews = [NSMutableArray array];
    self.getRedDatas = [NSArray array];
    self.sendRedDatas = [NSArray array];
    
    for (int i = 0; i < 2; i ++) {
        CGRect rect = CGRectMake(i * IPHONE_SCREEN_WIDTH, 0, IPHONE_SCREEN_WIDTH, self.scrollView.height);
        UITableView *tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        [self.scrollView addSubview:tableView];
        [self.tableViews addObject:tableView];
    }
    
    
}

- (void)setAccoutModel:(MBAccoutModel *)accoutModel
{
    _accoutModel = accoutModel;
    [self getDatas];
}

- (void)getDatas
{
    NSDictionary *dict = @{
                           @"userId":_accoutModel.accUsrInfo.uId,
                           @"pageNo":@"0"
                           };
    // 收到列表
    [MBHttpRequset requestWithUrl:kLive_live_receivedRedPacketRecord setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"收到 = = = %@",AJson);
        self.getRedDatas = AJson[@"recordList"];
        UITableView *tableView = [self.tableViews objectAtIndex:0];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    // 发出列表
    [MBHttpRequset requestWithUrl:kLive_live_emitRedPacketRecord setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"发出 = = = %@",AJson);
        self.sendRedDatas = AJson[@"recordList"];
        UITableView *tableView = [self.tableViews objectAtIndex:1];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 2; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 收到
                case 0:
                {
                    arr = self.getRedDatas;
                }
                    break;
                case 1:// 发送
                {
                    arr = self.sendRedDatas;
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    NSLog(@"arr===============%@",arr);
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"USERGIFTCELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idetifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.layer.cornerRadius = 22;
        
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(IPHONE_SCREEN_WIDTH - 150, 20, 130, 30);
        label.tag = 1090;
        label.textAlignment = NSTextAlignmentRight;
        [cell addSubview:label];
    }
    //    UITableView *tableView = [self.tableViews objectAtIndex:<#(NSUInteger)#>];
    
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 2; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 收到
                case 0:
                {
                    arr = self.getRedDatas;
                    if (arr.count != 0) {
                        NSDictionary *dict = arr[indexPath.row];
                        cell.textLabel.text = [NSString stringWithFormat:@"收到 %@ 的红包",dict[@"nickname"]];
                        
                        NSTimeInterval interval    = [dict[@"createTime"] doubleValue] / 1000.0;
                        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSString *dateString       = [formatter stringFromDate: date];
                        cell.detailTextLabel.text = dateString;
                        
                        UILabel *label = (UILabel *)[cell viewWithTag:1090];
                        label.text = [NSString stringWithFormat:@"%@ 元",dict[@"money"]];
                    }
                }
                    break;
                case 1:// 发送
                {
                    arr = self.sendRedDatas;
                    NSDictionary *dict = arr[indexPath.row];
                    if ([dict[@"type"] isEqualToString:@"1"]) {
                        cell.textLabel.text = [NSString stringWithFormat:@"当前剩余 %@ 鱼币",dict[@"newFish"]];
                        
                        NSTimeInterval interval    = [dict[@"createTime"] doubleValue] / 1000.0;
                        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSString *dateString       = [formatter stringFromDate: date];
                        cell.detailTextLabel.text = dateString;
                        
                        UILabel *label = (UILabel *)[cell viewWithTag:1090];
                        label.text = [NSString stringWithFormat:@"%@ 元",dict[@"money"]];

                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}





- (IBAction)segmClick:(UIButton *)sender {
    sender.selected = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.lineV.left = sender.left;
    }];
    [self setStatusWithTag:sender.tag];
}

- (void)setStatusWithTag:(NSInteger)tag
{
    switch (tag) {
        case 1002:
        {
            //            self.todayListBtn.selected = YES;
            self.sendRedBtn.selected = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(0, 0);
            }];
        }
            break;
        case 1003:
        {
            self.getRedBtn.selected = NO;
            //            self.weekListBtn.selected = YES;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(IPHONE_SCREEN_WIDTH, 0);
            }];
        }
            break;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 用contentOffset来计算当前的页码
    //    pageCount = self.scrollView.contentOffset.x/self.scrollView.bounds.size.width;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    if (scrollView.contentOffset.x == 0) {
        self.getRedBtn.selected = YES;
        self.sendRedBtn.selected = NO;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineV.left = self.getRedBtn.left;
        }];
    }else{
        self.getRedBtn.selected = NO;
        self.sendRedBtn.selected = YES;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineV.left = self.sendRedBtn.left;
        }];
    }
    
}

@end
