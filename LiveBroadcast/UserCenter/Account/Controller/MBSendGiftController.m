//
//  MBSendGiftController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSendGiftController.h"

@interface MBSendGiftController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong)NSArray *datas;
@end

@implementation MBSendGiftController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.datas = [NSArray array];
}

- (void)setAccoutModel:(MBAccoutModel *)accoutModel
{
    
    _accoutModel = accoutModel;
    [self getDatas];
}

- (void)getDatas
{
    NSDictionary *dict = @{
                           @"userId":_accoutModel.accUsrInfo.uId,
                           @"pageNo":@"0"
                           };
    // 本日列表
    [MBHttpRequset requestWithUrl:kLive_live_userSendGiftList setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"当日 = = = %@",AJson);
        self.datas = AJson[@"giftRecords"];
        [self.tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.datas.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"USERGIFTCELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idetifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.layer.cornerRadius = 22;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(IPHONE_SCREEN_WIDTH - 110, 10, 100, 50);
        button.tag = 1080;
        [cell addSubview:button];
    }
    //    UITableView *tableView = [self.tableViews objectAtIndex:<#(NSUInteger)#>];
    if (self.datas.count != 0) {
        NSDictionary *dict = self.datas[indexPath.row];
        UIButton *btn = (UIButton *)[cell viewWithTag:1080];
        [btn sd_setImageWithURL:[NSURL URLWithString:dict[@"giftUrl"]] forState:UIControlStateNormal];
        [btn setTitle:[NSString stringWithFormat:@"  x%@",dict[@"giftCount"]] forState:UIControlStateNormal];
        
        NSTimeInterval interval    = [dict[@"createTime"] doubleValue] / 1000.0;
        NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString       = [formatter stringFromDate: date];
        
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"headUrl"]] placeholderImage:[UIImage imageNamed:@"danshuiyu.png"]];
        cell.textLabel.text = dict[@"nickname"];
        cell.detailTextLabel.text = dateString;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
