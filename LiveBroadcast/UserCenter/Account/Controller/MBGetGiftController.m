//
//  MBGetGiftController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBGetGiftController.h"

@interface MBGetGiftController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *todayListBtn;
@property (weak, nonatomic) IBOutlet UIButton *weekListBtn;
@property (weak, nonatomic) IBOutlet UIView *lineV;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic)  NSArray *oneDayDatas;// 充值
@property (strong, nonatomic)  NSArray *weekDatas;// 提现
@property (strong, nonatomic)  NSMutableArray *tableViews;
@property (weak, nonatomic)UIPageControl *pageControl;
@end

@implementation MBGetGiftController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.todayListBtn.selected = YES;
    self.scrollView.contentSize = CGSizeMake(IPHONE_SCREEN_WIDTH * 2, 0);
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    
    self.tableViews = [NSMutableArray array];
    self.oneDayDatas = [NSArray array];
    self.weekDatas = [NSArray array];
    
    for (int i = 0; i < 2; i ++) {
        CGRect rect = CGRectMake(i * IPHONE_SCREEN_WIDTH, 0, IPHONE_SCREEN_WIDTH, self.scrollView.height);
        UITableView *tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        [self.scrollView addSubview:tableView];
        [self.tableViews addObject:tableView];
    }
    
    
}

- (void)setAccoutModel:(MBAccoutModel *)accoutModel
{
    _accoutModel = accoutModel;
    [self getDatas];
}

- (void)getDatas
{
    NSDictionary *dict = @{
                           @"userId":_accoutModel.accUsrInfo.uId,
                           @"pageNo":@"0"
                           };
    // 本日列表
    [MBHttpRequset requestWithUrl:kLive_live_receiveGiftThisDay setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"当日 = = = %@",AJson);
        self.oneDayDatas = AJson[@"giftRecords"];
        UITableView *tableView = [self.tableViews objectAtIndex:0];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    // 本周列表
    [MBHttpRequset requestWithUrl:kLive_live_receiveGiftWeeks setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"本周 = = = %@",AJson);
        self.weekDatas = AJson[@"giftRecords"];
        UITableView *tableView = [self.tableViews objectAtIndex:1];
        [tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 2; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 充值
                case 0:
                {
                    arr = self.oneDayDatas;
                }
                    break;
                case 1:// 提现
                {
                    arr = self.weekDatas;
                }
                    break;

                default:
                    break;
            }
        }
    }
    NSLog(@"arr===============%@",arr);
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"USERGIFTCELL";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idetifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imageView.layer.cornerRadius = 22;
    }
    //    UITableView *tableView = [self.tableViews objectAtIndex:<#(NSUInteger)#>];
    
    NSArray *arr = [NSArray array];
    for (int i = 0; i < 2; i ++) {
        UITableView *tabV = self.tableViews[i];
        if (tabV == tableView) {
            switch (i) {// 本日
                case 0:
                {
                    arr = self.oneDayDatas;
                    if (arr.count != 0) {
                        NSDictionary *dict = arr[indexPath.row];
                        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"headUrl"]] placeholderImage:[UIImage imageNamed:@"danshuiyu.png"]];
                        cell.textLabel.text = dict[@"nickname"];
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@个礼物",dict[@"giftCount"]];
                    }
                }
                    break;
                case 1:// 本周
                {
                    arr = self.weekDatas;
                    if (arr.count != 0) {
                        NSDictionary *dict = arr[indexPath.row];
                        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:dict[@"headUrl"]] placeholderImage:[UIImage imageNamed:@"danshuiyu.png"]];
                        cell.textLabel.text = dict[@"nickname"];
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@个礼物",dict[@"giftCount"]];
                    }
                }
                    break;
                
                default:
                    break;
            }
        }
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}





- (IBAction)segmClick:(UIButton *)sender {
    sender.selected = YES;
    [UIView animateWithDuration:0.2f animations:^{
        self.lineV.left = sender.left;
    }];
    [self setStatusWithTag:sender.tag];
}

- (void)setStatusWithTag:(NSInteger)tag
{
    switch (tag) {
        case 1002:
        {
//            self.todayListBtn.selected = YES;
            self.weekListBtn.selected = NO;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(0, 0);
            }];
        }
            break;
        case 1003:
        {
            self.todayListBtn.selected = NO;
//            self.weekListBtn.selected = YES;
            [UIView animateWithDuration:0.2f animations:^{
                self.scrollView.contentOffset = CGPointMake(IPHONE_SCREEN_WIDTH, 0);
            }];
        }
            break;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // 用contentOffset来计算当前的页码
//    pageCount = self.scrollView.contentOffset.x/self.scrollView.bounds.size.width;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    if (scrollView.contentOffset.x == 0) {
        self.todayListBtn.selected = YES;
        self.weekListBtn.selected = NO;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineV.left = self.todayListBtn.left;
        }];
    }else{
        self.todayListBtn.selected = NO;
        self.weekListBtn.selected = YES;
        [UIView animateWithDuration:0.2f animations:^{
            self.lineV.left = self.weekListBtn.left;
        }];
    }
    
}

@end
