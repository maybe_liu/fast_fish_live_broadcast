//
//  MBRechargeController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//充值

#import "MBBaseController.h"
#import "MBAccoutModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MBRechargeController : MBBaseController
@property (strong, nonatomic)MBAccoutModel *accoutModel;

@end

NS_ASSUME_NONNULL_END
