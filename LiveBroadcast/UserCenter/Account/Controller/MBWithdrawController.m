//
//  MBWithdrawController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/16.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBWithdrawController.h"
#import "MBAESEncrypt.h"
#import <CommonCrypto/CommonDigest.h>
@interface MBWithdrawController ()
@property (weak, nonatomic) IBOutlet UITextField *aliPayField;
@property (weak, nonatomic) IBOutlet UITextField *nameField;

@property (weak, nonatomic) IBOutlet UITextField *moneyField;
@property (weak, nonatomic) IBOutlet UILabel *withdrawLbl;// 可以提现余额
@property (weak, nonatomic) IBOutlet UIButton *allMoneyBtn;



@end

@implementation MBWithdrawController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.withdrawLbl.text = self.accoutModel.cashBalance;
}
- (IBAction)withdrawClick {
    if (self.aliPayField.text.length == 0) {
        [MBProgressHUD showError:@"支付宝账号不能为空"];
        return;
    }
    if (self.nameField.text.length == 0) {
        [MBProgressHUD showError:@"收款人姓名不能为空"];
        return;
    }
    if (self.moneyField.text.length == 0) {
        [MBProgressHUD showError:@"提款金额不能为空"];
        return;
    }
    
    
    NSDictionary *dict = @{
                           @"userId":[NSString stringWithFormat:@"%@",self.accoutModel.accUsrInfo.uId],
                           @"money":self.moneyField.text,
                           @"name":self.nameField.text,
                           @"accountNo":self.aliPayField.text
                           };
    
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    [self callApisign:dict1];
    
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_extractCash setParams:dict1 isShowProgressView:YES success:^(id  _Nonnull AJson) {
        dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5/*延迟执行时间*/ * NSEC_PER_SEC));
        
        dispatch_after(delayTime, dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

- (IBAction)allWithdraw {
    self.moneyField.text = self.accoutModel.cashBalance;
}


//对接口参数字典进行签名
- (void)callApisign:(NSMutableDictionary *)dict
{
    
    //    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"Danny",@"name",@"12",@"age",@"man",@"sex",nil];
    NSArray *allKeyArray = [dict allKeys];
    NSArray *afterSortKeyArray = [allKeyArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSComparisonResult resuest = [obj1 compare:obj2];
        return resuest;
    }];
    NSLog(@"afterSortKeyArray:%@",afterSortKeyArray);
    
    //通过排列的key值获取value
    NSMutableArray *valueArray = [NSMutableArray array];
    for (NSString *sortsing in afterSortKeyArray) {
        NSString *valueString = [dict objectForKey:sortsing];
        [valueArray addObject:valueString];
    }
    NSLog(@"valueArray:%@",valueArray);
    
    NSMutableArray *signArray = [NSMutableArray array];
    for (int i = 0 ; i < afterSortKeyArray.count; i++) {
        NSString *keyValue = [NSString stringWithFormat:@"%@=%@",afterSortKeyArray[i],valueArray[i]];
        [signArray addObject:keyValue];
    }
    
    //signString用于签名的原始参数集合
    NSString *signString = [signArray componentsJoinedByString:@"&"];
    NSLog(@"signString:%@",signString);
    //拼接key
    NSString *pinjie = [NSString stringWithFormat:@"%@%@",signString,@"&key=P5obyEgvmSps0o1tdlRuKfWCaGy5eElr"];
    
    NSLog(@"pinjie == %@",pinjie);
    
//    NSLog(@"最终MD5得到 == %@",[self md5daxie32:pinjie]);
    
    //    最终MD5得到 == EF53612625541C8A59FF62F7CB15BB56
    dict[@"sign"] = [self md5daxie32:pinjie];
}

//MD5大写加密
- (NSString *)md5daxie32:(NSString *)input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02X", digest[i]];
    return  output;
}

@end
