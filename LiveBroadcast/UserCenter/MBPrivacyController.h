//
//  MBPrivacyController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/7.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBPrivacyController : MBBaseController
@property (nonatomic,strong)NSDictionary *data;
@end

NS_ASSUME_NONNULL_END
