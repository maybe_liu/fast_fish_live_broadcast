//
//  MBCenterController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/19.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBUserCenterController.h"
#import "MBSearchController.h"
#import <ViewDeck/ViewDeck.h>
#import "MBBaseTabbarController.h"
#import "MBHomeMainController.h"
#import "MBBaseNavigationController.h"
#import "MBUserCenterModel.h"
#import "MBUserDetailController.h"
#import "MBSettingController.h"
#import "MBAccountMainController.h"
#import "MBMessageCenterController.h"
#import "MBHeadLinesController.h"
@interface MBUserCenterController ()<UITableViewDelegate,UITableViewDataSource,IIViewDeckControllerDelegate>
{
    MBButton *headPhotoBtn;
    UILabel *headTitle;
}
@property (nonatomic,strong)NSArray *tableData;

@property (nonatomic,weak)UIImageView *redView;
@end

@implementation MBUserCenterController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.viewDeckController.delegate = self;
    self.tableData = [NSArray arrayWithObjects:@{
                                                 @"title":@"搜索",@"icon":@"sous.png"
                                                 },
//                                                @{
//                                                  @"title":@"商家中心",@"icon":@"dianp.png"
//                                                  },
//                                                @{
//                                                  @"title":@"我的优惠券",@"icon":@"quan.png"
//                                                  },
                                                @{
                                                  @"title":@"我的账户",@"icon":@"zhanghu.png"
                                                  },
                                                @{
                                                  @"title":@"热门头条",@"icon":@"tout.png"
                                                  },
                                                @{
                                                  @"title":@"设置",@"icon":@"shez.png"
                                                  },nil];
    [self initView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(test:) name:MBIsLoginNotification object:nil];
}

- (void)test:(NSNotification *)noti
{
    NSLog(@"noti ==== %@",noti.userInfo);
}
- (void)initView
{
    // 头像
    headPhotoBtn = [MBButton buttonWithType:UIButtonTypeCustom];
    headPhotoBtn.frame = CGRectMake((300 - 90)/2, 60, 90, 90);
    headPhotoBtn.backgroundColor = [UIColor grayColor];
    headPhotoBtn.layer.cornerRadius = 45;
    [headPhotoBtn setBackgroundImage:[UIImage imageNamed:@"danshuiyu.png"] forState:UIControlStateNormal];
    //[MBUserModel sharedMBUserModel].userInfo.headUrl
//    NSURL *url = [NSURL URLWithString:@"https://b-ssl.duitang.com/uploads/item/201210/06/20121006120433_CZXuC.thumb.1400_0.jpeg"];
    [headPhotoBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:[MBUserModel sharedMBUserModel].userInfo.headUrl] forState:UIControlStateNormal];
    NSLog(@"[MBUserModel sharedMBUserModel].userInfo.headUr = %@",[MBUserModel sharedMBUserModel].userInfo.headUrl);
    headPhotoBtn.clipsToBounds = YES;
    [self.view addSubview:headPhotoBtn];
    headPhotoBtn.miBtnBlock = ^(){
        NSDictionary *dict = @{
                               @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                               @"beUserId":[MBUserModel sharedMBUserModel].userInfo.userId
                               };
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_getPersonalData setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
             [self.viewDeckController closeSide:YES];
            MBUserCenterModel *centerModel = [[MBUserCenterModel alloc]initWithDict:AJson];
            MBBaseTabbarController *tabbarVC = (MBBaseTabbarController *)self.viewDeckController.centerViewController;
            MBBaseNavigationController *homeNav = (MBBaseNavigationController *)tabbarVC.viewControllers[0];
            MBHomeMainController *homeVC = (MBHomeMainController *)homeNav.viewControllers[0];
            MBUserDetailController *detailVC = [[MBUserDetailController alloc]init];
            detailVC.userModel = centerModel;
            [homeVC.navigationController pushViewController:detailVC animated:YES];
        } failure:^(id  _Nonnull AJson) {
        }];
    };
    
    // 名称
    UILabel *userLbl = [[UILabel alloc] init];
    userLbl.frame = CGRectMake(headPhotoBtn.left, headPhotoBtn.bottom + 20, headPhotoBtn.width, 20);
    userLbl.textAlignment = NSTextAlignmentCenter;
    NSString *str;
    if ([MBUserModel sharedMBUserModel].userInfo.nickname != nil) {
        str = [MBUserModel sharedMBUserModel].userInfo.nickname;
    }else{
        str = @"";
    }
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 16],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    userLbl.attributedText = string;
    [self.view addSubview:userLbl];
    headTitle = userLbl;
    
    // 八卦、消息、私信
    NSArray *titleArr = [NSArray arrayWithObjects:@"八卦",@"消息",@"私信", nil];
    NSArray *imageArr = [NSArray arrayWithObjects:@"bagua.png",@"news.png",@"sixin.png", nil];
    for (int i = 0; i < 3; i ++) {
        CGFloat btnY = userLbl.bottom + 20;
        CGFloat btnW = 30;
        CGFloat btnH = 50;
        CGFloat pading = (300 - 90)/4;
        CGFloat bntX = pading + i *(btnW + pading);
        
        MBCustomButton *btn = [MBCustomButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        btn.postion = MBImagePositionTop;
        [btn.titleLabel setFont:[UIFont fontWithName:@"PingFangSC-Regular" size: 14]];
        [btn setImage:[UIImage imageNamed:imageArr[i]] forState:UIControlStateNormal];
        [btn setTitleColor: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(msgCenterClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.spacing = 10;
        btn.tag = i;
        btn.frame = CGRectMake(bntX, btnY, btnW, btnH);
        [self.view addSubview:btn];
        
        if (i == 1) {
            UIImageView *redView = [[UIImageView alloc]init];
            redView.frame = CGRectMake(btn.width - 10, 5 , 7, 7);
            redView.image = [UIImage imageNamed:@"red.png"];
//            [btn addSubview:redView];
            //        self.redView = redView;
        }
            }
    
    // 列表
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake((300 - 90)/4 - 15, userLbl.bottom + 100, (300 - ((300 - 90)/4)*2), 300) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    self.view.backgroundColor = tableView.backgroundColor;
}

- (void)updateDatas
{
    NSString *str;
    if ([MBUserModel sharedMBUserModel].userInfo.nickname != nil) {
        str = [MBUserModel sharedMBUserModel].userInfo.nickname;
    }else{
        str = @"";
    }
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Medium" size: 16],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    headTitle.attributedText = string;
}

//
- (void)msgCenterClick:(UIButton *)btn
{
    [self.viewDeckController closeSide:YES];
    MBBaseTabbarController *tabbarVC = (MBBaseTabbarController *)self.viewDeckController.centerViewController;
    MBBaseNavigationController *homeNav = (MBBaseNavigationController *)tabbarVC.viewControllers[0];
    MBHomeMainController *homeVC = (MBHomeMainController *)homeNav.viewControllers[0];
    MBMessageCenterController *msgCenter = [[MBMessageCenterController alloc]init];
    msgCenter.tag = btn.tag;
    [homeVC.navigationController pushViewController:msgCenter animated:YES];

    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dict = self.tableData[indexPath.row];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:dict[@"title"] attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1.0]}];
    
    cell.textLabel.attributedText = string;
    cell.imageView.image = [UIImage imageNamed:dict[@"icon"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.viewDeckController closeSide:YES];
    MBBaseTabbarController *tabbarVC = (MBBaseTabbarController *)self.viewDeckController.centerViewController;
    MBBaseNavigationController *homeNav = (MBBaseNavigationController *)tabbarVC.viewControllers[0];
    MBHomeMainController *homeVC = (MBHomeMainController *)homeNav.viewControllers[0];
    switch (indexPath.row) {
        case 0://搜索
        {
            MBSearchController *searchVC = [[MBSearchController alloc]init];
            [homeVC.navigationController pushViewController:searchVC animated:YES];
        }
            break;
//        case 1:// 商家中心
//        {
//
//        }
//            break;
//        case 2: // 我的优惠券
//        {
//
//        }
            break;
        case 1: // 我的账户
        {
            MBAccountMainController *accountVC = [[MBAccountMainController alloc]init];
            [homeVC.navigationController pushViewController:accountVC animated:YES];
        }
            break;
        case 2: // 热门头条
        {
            MBHeadLinesController *headLine = [[MBHeadLinesController alloc]init];
            [homeVC.navigationController pushViewController:headLine animated:YES];
        }
            break;
        case 3: // 设置
        {
            MBSettingController *setVC = [[MBSettingController alloc]init];
            [homeVC.navigationController pushViewController:setVC animated:YES];
            
        }
            break;
        
        default:
            break;
    }
}






















//------------------------------------------------系统方法---------------------------------------------
- (instancetype)init
{
    if (self = [super init]) {
        self.preferredContentSize = CGSizeMake(300.0, IPHONE_SCREEN_HEIGHT);
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [headPhotoBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:[MBUserModel sharedMBUserModel].userInfo.headUrl] forState:UIControlStateNormal];
    
    NSLog(@"中心更换后 = %@",[MBUserModel sharedMBUserModel].userInfo.headUrl);
    [self updateDatas];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (BOOL)viewDeckController:(IIViewDeckController *)viewDeckController willOpenSide:(IIViewDeckSide)side
{
    //IIViewDeckSideRight
    if (side == IIViewDeckSideRight) {
        return NO;
    }
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
