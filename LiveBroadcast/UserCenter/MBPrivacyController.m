//
//  MBPrivacyController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/7.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBPrivacyController.h"
#import "MBBlackListController.h"
#import "MBPrivateLetterController.h"
@interface MBPrivacyController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_titles;
}
@property (nonatomic,weak)UITableView *tableView;
@end

@implementation MBPrivacyController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"隐私设置";
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    tableView.sectionHeaderHeight = 0.5f;
    tableView.sectionFooterHeight = 10.0f;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    // @[@"水印设置",@"隐匿位置信息"],
    _titles = @[@[@"谁可以私信我",@"只允许我关注的人评论我"],@[@"黑名单"]];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_titles[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SETTINGCELL"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"SETTINGCELL"];
        cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
        cell.textLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0f];
        cell.detailTextLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.section == 0) {
            if ([self.data[@"atMe"] isEqualToString:@"1"]){
                cell.detailTextLabel.text = @"所有人";
            }else if([self.data[@"atMe"] isEqualToString:@"1"]){
                cell.detailTextLabel.text = @"我关注的人";
            }else{
                cell.detailTextLabel.text = @"互关好友";
            }
        }
    }
    if (indexPath.row == 1) {
        UISwitch *swc = [[UISwitch alloc]initWithFrame:CGRectMake(IPHONE_SCREEN_WIDTH - 60, 10, 30, 30)];
        swc.on = ([self.data[@"commentsMe"] isEqualToString:@"1"])?YES:NO;
        [swc addTarget:self action:@selector(swcClick:) forControlEvents:UIControlEventValueChanged];
        swc.tintColor = kColorRGBA(238, 238, 238, 1);
        swc.onTintColor = kColorRGBA(255, 128, 0, 1);
        swc.userInteractionEnabled = NO;
//        swc.thumbTintColor = [UIColor grayColor];
        swc.tag = indexPath.section;
        [cell addSubview:swc];
    }
    cell.textLabel.text = _titles[indexPath.section][indexPath.row];
    return cell;
}

- (void)swcClick:(UISwitch *)swc
{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:// 私信
            {
                MBPrivateLetterController *privateVC = [[MBPrivateLetterController alloc]init];
                privateVC.headerTitle = @"选择可以私信我的人";
                privateVC.title = @"谁可以私信我";
                privateVC.privateVCSelectBlock = ^(NSString * privateStr) {
                    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                    cell.detailTextLabel.text = privateStr;
                };
                [self.navigationController pushViewController:privateVC animated:YES];
                
                UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                UISwitch *swc = (UISwitch *)cell.subviews[1];
//                swc.on = !swc.isOn;
            }
                break;
            case 1:// 私信
            {
                UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                UISwitch *swc = (UISwitch *)cell.subviews[1];
                swc.on = !swc.isOn;
                
                NSDictionary *dict = @{
                                       @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                                       @"type":@"COMMENTS_ME",
                                       @"newState":[NSString stringWithFormat:@"%d",swc.isOn]
                                       };
                [MBHttpRequset requestWithUrl:kTradeid_setting_setUpSecretSwitch setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
                    
                } failure:^(id  _Nonnull AJson) {
                    
                }];
            }
                break;
            default:
                break;
        }
    }
//    else if(indexPath.section == 0){
//        switch (indexPath.row) {
//            case 0:
//            {
//
//            }
//                break;
//
//            default:
//                break;
//        }
//    }
    else{// 黑名单
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_BlacklistList setParams:@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId} isShowProgressView:NO success:^(id  _Nonnull AJson) {
            MBBlackListController *blackListVC = [[MBBlackListController alloc]init];
            blackListVC.blackList = AJson[@"blacklistList"];
            [self.navigationController pushViewController:blackListVC animated:YES];
        } failure:^(id  _Nonnull AJson) {
            
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
