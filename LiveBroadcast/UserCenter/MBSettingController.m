//
//  MBSettingController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/2/28.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSettingController.h"
#import "MBUntiedController.h"
#import "MBPrivacyController.h"
#import "MBNotiSettingController.h"
@interface MBSettingController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_cellTitle;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIButton *loginOut;

@property (nonatomic,strong)NSDictionary *switchData;
@end

@implementation MBSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置按钮
    [self.loginOut setBackgroundColor:kColorRGBA(255, 128, 0, 1)];
    self.loginOut.layer.cornerRadius = 22;
    
    // 设置tableview代理
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // cell显示标题
    _cellTitle = @[@[@"手机号",@"隐私设置",@"通知设置"],@[@"清除缓存",@"反馈与帮助",@"关于我们"]];
    self.view.backgroundColor = kColorRGBA(245, 245, 245, 1.0f);
    self.title = @"设置";

}
// 退出登录
- (IBAction)loginOutClick {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"退出登录" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //沙盒ducument目录
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        //完整的文件路径
        NSString *path = [docPath stringByAppendingPathComponent:@"userModel.archive"];
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:path];
        if (exists) {
            NSLog(@"======================================路径正确");
            [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
        }
        
//        [MBUserModel sharedMBUserModel].isLoginFlag = NO;
//        MBUserModel *removeModel = [MBUserModel shareInstanceWithDict:@{}];
//        [self archiveDatas:removeModel];
        
        [MBUserModel attempDealloc];
        [MBUserInfo attempDealloc];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:MBIsLoginNotification object:nil userInfo:@{@"isFlag":MBLogoutFlag}];
        [self.navigationController popToRootViewControllerAnimated:YES];
        [[RCIM sharedRCIM] disconnect:NO];
        [self tencentChatLogout];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)tencentChatLogout {
    [[TIMManager sharedInstance] logout:^() {
        NSLog(@"logout succ");
    } fail:^(int code, NSString * err) {
        NSLog(@"logout fail: code=%d err=%@", code, err);
    }];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 50;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SETTINGCELL"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"SETTINGCELL"];
        cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
        cell.textLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0f];
        cell.detailTextLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section != 1 || indexPath.row != 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        NSUInteger bytesCache = [[SDImageCache sharedImageCache] getSize];
        //换算成 MB (注意iOS中的字节之间的换算是1000不是1024)
        float MBCache = bytesCache/1000/1000;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2fMB",MBCache];
    }
    cell.textLabel.text = _cellTitle[indexPath.section][indexPath.row];
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSString *phone = [MBUserModel sharedMBUserModel].phone;
        phone = [phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
        cell.detailTextLabel.text = phone;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:// 手机号
            {
                MBUntiedController *untiedVC = [[MBUntiedController alloc]init];
                [self.navigationController pushViewController:untiedVC animated:YES];
            }
                break;
            case 1:// 隐私设置
            {
                MBPrivacyController *privacyVC = [[MBPrivacyController alloc]init];
                privacyVC.data = self.switchData;
                [self.navigationController pushViewController:privacyVC animated:YES];
            }
                break;
            case 2:// 通知设置
            {
                MBNotiSettingController *notiVC = [[MBNotiSettingController alloc]init];
                notiVC.data = self.switchData;
                [self.navigationController pushViewController:notiVC animated:YES];
            }
                break;
                
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:// 清除缓存
            {
                UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"是否清除缓存" message:nil preferredStyle:UIAlertControllerStyleAlert];
                [alertVC addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
                        [MBProgressHUD showMessage:@"清除成功"];
                        NSUInteger bytesCache = [[SDImageCache sharedImageCache] getSize];
                        //换算成 MB (注意iOS中的字节之间的换算是1000不是1024)
                        float MBCache = bytesCache/1000/1000;
                        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2fMB",MBCache];
                    }];
                }]];
                [self presentViewController:alertVC animated:YES completion:nil];
                
            }
                break;
            case 1:// 反馈与帮助
            {
                
            }
                break;
            case 2:// 关于我们
            {
                
            }
                break;
                
            default:
                break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0f;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [MBHttpRequset requestWithUrl:kTradeid_setting_getUpSecretSwitch setParams:@{@"userId":filter([MBUserModel sharedMBUserModel].userInfo.userId)} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.switchData = AJson[@"setUp"];
        NSLog(@"user phone = %@",[MBUserModel sharedMBUserModel].phone);
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
