//
//  MBBAGTableViewCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBBAGTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MBButton *headerImg;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@end

NS_ASSUME_NONNULL_END
