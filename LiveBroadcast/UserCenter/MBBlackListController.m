//
//  MBBlackListController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBlackListController.h"
#import "MBBlackListCell.h"
@interface MBBlackListController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak)UITableView *tableView;
@property (nonatomic,weak)UILabel *blackListLabel;
@end

@implementation MBBlackListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"黑名单";
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    UILabel *blackLabel = [[UILabel alloc]init];
    blackLabel.frame = CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT - 200);
    blackLabel.textAlignment = NSTextAlignmentCenter;
    blackLabel.font = [UIFont boldSystemFontOfSize:28.0f];
    blackLabel.text = @"无数据";
    [self.view addSubview:blackLabel];
    self.blackListLabel = blackLabel;
    
    if (self.blackList.count == 0) {
        self.blackListLabel.hidden = NO;
        self.tableView.hidden = YES;
    }else{
        self.blackListLabel.hidden = YES;
        self.tableView.hidden = NO;
    }
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.blackList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"BLACKLISTCELL";
    MBBlackListCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBBlackListCell" owner:nil options:nil];
        cell = [nibs lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.blackModel = self.blackList[indexPath.row];
    cell.removeBtn.miBtnBlock = ^(){
        [MBHttpRequset requestWithUrl:kTradeid_userCenter_blacklistRemove setParams:@{@"userId":[MBUserModel sharedMBUserModel].userInfo.userId,@"blacklistId":self.blackList[indexPath.row][@"id"]} isShowProgressView:NO success:^(id  _Nonnull AJson) {
            [cell.removeBtn setHidden:YES];
        } failure:^(id  _Nonnull AJson) {
            
        }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

@end
