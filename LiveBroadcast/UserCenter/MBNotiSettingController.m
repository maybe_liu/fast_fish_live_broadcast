//
//  MBNotiSettingController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/14.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBNotiSettingController.h"
#import "MBPrivateLetterController.h"
@interface MBNotiSettingController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *_titles;
}
@property (nonatomic,weak)UITableView *tableView;
@end

@implementation MBNotiSettingController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"通知设置";
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, IPHONE_SCREEN_HEIGHT) style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.scrollEnabled = NO;
    tableView.sectionHeaderHeight = 0.5f;
    tableView.sectionFooterHeight = 10.0f;
    [self.view addSubview:tableView];
    self.tableView = tableView;
    // @[@"水印设置",@"隐匿位置信息"],
    _titles = @[@[@"私信",@"评论"],@[@"被@",@"新粉丝",@"直播",@"我关注的人发布作品"]];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_titles[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SETTINGCELL"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"SETTINGCELL"];
        cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
        cell.textLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.detailTextLabel.font = [UIFont systemFontOfSize:11.0f];
        cell.detailTextLabel.textColor = kColorRGBA(51, 51, 51, 1.0f);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.section == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == 0) {// 私信
            if ([self.data[@"at2Me"] isEqualToString:@"1"]){
                cell.detailTextLabel.text = @"所有人";
            }else if([self.data[@"at2Me"] isEqualToString:@"2"]){
                cell.detailTextLabel.text = @"我关注的人";
            }else{
                cell.detailTextLabel.text = @"互关好友";
            }
        }else{// 评论
            if ([self.data[@"comments2Me"] isEqualToString:@"1"]){
                cell.detailTextLabel.text = @"所有人";
            }else if([self.data[@"comments2Me"] isEqualToString:@"2"]){
                cell.detailTextLabel.text = @"我关注的人";
            }else{
                cell.detailTextLabel.text = @"互关好友";
            }
        }
    }
    if (indexPath.section == 1) {
        UISwitch *swc = [[UISwitch alloc]initWithFrame:CGRectMake(IPHONE_SCREEN_WIDTH - 60, 10, 30, 30)];
        swc.userInteractionEnabled = NO;
        swc.tintColor = kColorRGBA(238, 238, 238, 1);
        swc.onTintColor = kColorRGBA(255, 128, 0, 1);
        //        swc.thumbTintColor = [UIColor grayColor];
        swc.tag = indexPath.section;
        [cell addSubview:swc];
        if (indexPath.row == 0) {
            swc.on = ([self.data[@"byAt"] isEqualToString:@"1"])?YES:NO;
        }else if (indexPath.row == 1) {
            swc.on = ([self.data[@"fansRemind"] isEqualToString:@"1"])?YES:NO;
        }else if (indexPath.row == 2) {
            swc.on = ([self.data[@"liveRemind"] isEqualToString:@"1"])?YES:NO;
        }else{
            swc.on = ([self.data[@"releaseNotice"] isEqualToString:@"1"])?YES:NO;
        }
        
    }
    cell.textLabel.text = _titles[indexPath.section][indexPath.row];
    return cell;
}

- (void)swcRequstWithType:(NSString *)type newState:(NSString *)newState
{
    NSDictionary *dict = @{
                           @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                           @"type":type,
                           @"newState":newState
                           };
    [MBHttpRequset requestWithUrl:kTradeid_setting_setUpSecretSwitch setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
        
    } failure:^(id  _Nonnull AJson) {
        
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
        UISwitch *swc = (UISwitch *)cell.subviews[1];
        swc.on = !swc.isOn;
        //
        switch (indexPath.row) {
            case 0:// 被@
            {
                [self swcRequstWithType:@"AT_ME" newState:[NSString stringWithFormat:@"%d",swc.isOn]];
            }
                break;
            case 1:// 新粉丝
            {
                [self swcRequstWithType:@"FANS_REMIND" newState:[NSString stringWithFormat:@"%d",swc.isOn]];
            }
                break;
            case 2:// 直播
            {
                [self swcRequstWithType:@"LIVE_REMIND" newState:[NSString stringWithFormat:@"%d",swc.isOn]];
            }
                break;
            case 3:// 我关注的的人
            {
                [self swcRequstWithType:@"RELEASE_NOTICE" newState:[NSString stringWithFormat:@"%d",swc.isOn]];
            }
                break;
            default:
                break;
        }
    }else{
        MBPrivateLetterController *privateVC = [[MBPrivateLetterController alloc]init];
        privateVC.privateVCSelectBlock = ^(NSString * privateStr) {
            UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            cell.detailTextLabel.text = privateStr;
        };
        privateVC.headerTitle = (indexPath.row == 0)?@"通知私信":@"通知评论";
        privateVC.title = (indexPath.row == 0)?@"通知私信":@"通知评论";
        [self.navigationController pushViewController:privateVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
