//
//  MBNotifiCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/12.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBNotifiCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@end

NS_ASSUME_NONNULL_END
