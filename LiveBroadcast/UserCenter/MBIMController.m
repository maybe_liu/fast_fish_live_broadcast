//
//  MBIMController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/10.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBIMController.h"
#import "MBUserDetailController.h"
@interface MBIMController ()

@end

@implementation MBIMController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)setUserID:(NSString *)userID
{
    _userID = userID;
    NSDictionary *dict = @{
                           @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                           @"beUserId":self.userID
                           };
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getPersonalData setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBUserCenterModel *centerModel = [[MBUserCenterModel alloc]initWithDict:AJson];
        self.title = centerModel.userInfo.nickname;
    } failure:^(id  _Nonnull AJson) {
    }];
}

- (void)didTapCellPortrait:(NSString *)userId
{
    NSLog(@"userId = %@  [MBUserModel sharedMBUserModel].userInfo.userId] %@",userId,[MBUserModel sharedMBUserModel].userInfo.userId);
//    if (![[NSString stringWithFormat:@"%@",userId] isEqualToString:[NSString stringWithFormat:@"%@",[MBUserModel sharedMBUserModel].userInfo.userId]]) {
//
//    }else{
//        [self.navigationController popViewControllerAnimated:YES];
//    }
    NSDictionary *dict = @{
                           @"userId":[MBUserModel sharedMBUserModel].userInfo.userId,
                           @"beUserId":userId
                           };
    [MBHttpRequset requestWithUrl:kTradeid_userCenter_getPersonalData setParams:dict isShowProgressView:NO success:^(id  _Nonnull AJson) {
        MBUserCenterModel *centerModel = [[MBUserCenterModel alloc]initWithDict:AJson];
        MBUserDetailController *detailVC = [[MBUserDetailController alloc]init];
        detailVC.userModel = centerModel;
        [self.navigationController pushViewController:detailVC animated:YES];
    } failure:^(id  _Nonnull AJson) {
    }];
}
@end
