//
//  MBTaskMainCell.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/3/25.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBTaskMainCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imagV;
@property (weak, nonatomic) IBOutlet UILabel *titileLbl;
@property (weak, nonatomic) IBOutlet UILabel *detailLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;

@end

NS_ASSUME_NONNULL_END
