//
//  MBFinshJoinView.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/5/7.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MBFinshJoinView : UIView
@property (weak, nonatomic) IBOutlet UILabel *videoLbl;
@property (weak, nonatomic) IBOutlet MBButton *videoBtn;
@property (weak, nonatomic) IBOutlet UILabel *fansLbl;
@property (weak, nonatomic) IBOutlet MBButton *fansBtn;
@property (weak, nonatomic) IBOutlet UILabel *nwLbl;
@property (weak, nonatomic) IBOutlet MBButton *nwBtn;
@property (weak, nonatomic) IBOutlet UILabel *praiseLbl;
@property (weak, nonatomic) IBOutlet MBButton *praiseBtn;
@property (weak, nonatomic) IBOutlet MBButton *cancleBtn;
@property (weak, nonatomic) IBOutlet MBButton *commitBtn;

@end

NS_ASSUME_NONNULL_END
