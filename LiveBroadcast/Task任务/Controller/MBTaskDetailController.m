//
//  MBTaskDetailController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTaskDetailController.h"
#import "MBSignUpController.h"
#import "MBFinshJoinView.h"
@interface MBTaskDetailController (){
    MBFinshJoinView *joinView;
    UIView *joinBgView;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageV;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *requestLbl;
@property (weak, nonatomic) IBOutlet UILabel *contentLbl;

@end

@implementation MBTaskDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"活动详情";
    [MBHttpRequset requestWithUrl:kTradeid_recruit_getRecruitData setParams:@{@"recruitId":self.dict[@"id"]} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSLog(@"kTradeid_recruit_getRecruitData = %@",AJson);
        NSDictionary *dict = AJson[@"recruitInfo"];
        [self.imageV sd_setImageWithURL:[NSURL URLWithString:dict[@"recruitUrl"]] placeholderImage:[UIImage imageNamed:@"bg.png"]];
        self.titleLbl.text = dict[@"recruitTitle"];
        self.requestLbl.text = dict[@"recruitRequest"];
        self.contentLbl.text = dict[@"recruitContent"];
        self.timeLbl.text = [NSString stringWithFormat:@"%@ - %@",[self getTime:dict[@"starttime"]],[self getTime:dict[@"starttime"]]];
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    
    joinBgView = [[UIView alloc] init];
    joinBgView.frame = UIScreen.mainScreen.bounds;
    joinBgView.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.5];
    joinBgView.hidden = YES;
    [self.view addSubview: joinBgView];
    
    NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"MBFinshJoinView"owner:self options:nil];
    joinView = [nibView objectAtIndex:0];
    joinView.frame = CGRectMake(20, 140,(IPHONE_SCREEN_WIDTH - 40), 360);
    [joinBgView addSubview:joinView];
    joinView.cancleBtn.miBtnBlock = ^(){
        self->joinBgView.hidden = YES;
    };
    
    // 提交活动
    joinView.commitBtn.miBtnBlock = ^(){
        
    };
    
    
    
}
- (NSString *)getTime:(NSString *)time
{
    NSTimeInterval interval    =[time doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
- (IBAction)requestClick {
    NSLog(@"================= %@",[MBUserModel sharedMBUserModel].userInfo.userId);
    __block MBFinshJoinView *jv = joinView;
    if ([MBUserModel sharedMBUserModel].userInfo.userId != nil) {
        NSDictionary *dcit = @{
                               @"recruitId":[NSString stringWithFormat:@"%@",self.dict[@"id"]],
                               @"userId":[NSString stringWithFormat:@"%@",[MBUserModel sharedMBUserModel].userInfo.userId]
                               };
        [MBHttpRequset requestWithUrl:kTradeid_recruit_participate setParams:dcit isShowProgressView:NO success:^(id  _Nonnull AJson) {
            NSLog(@"kTradeid_recruit_participate = %@",AJson);
            
            NSDictionary *dict = AJson;
            
            jv.videoLbl.text = [NSString stringWithFormat:@"还需再拍摄%@/%@个视频",dict[@"joinVideoCount"],dict[@"joinVideoCount"]];
            jv.fansLbl.text = [NSString stringWithFormat:@"还需%@/%@粉丝量",dict[@"joinFansCount"],dict[@"joinFansCount"]];
            jv.praiseLbl.text = [NSString stringWithFormat:@"还需%@/%@获赞量",dict[@"joinPraiseCount"],dict[@"joinPraiseCount"]];
            jv.nwLbl.text = [NSString stringWithFormat:@"还需拉新%@/%@人",dict[@"joinNewCount"],dict[@"joinNewCount"]];
            
            if ([dict[@"fansResult"] isEqualToString:@"1"]) {
                [jv.fansBtn setTitle:@"已完成" forState:UIControlStateNormal];
            }else{
                jv.fansBtn.miBtnBlock = ^(){
                    NSLog(@"fansBtn");
                };
            }
            
            if ([dict[@"newResult"] isEqualToString:@"1"]) {
                [jv.nwBtn setTitle:@"已完成" forState:UIControlStateNormal];
            }else{
                jv.nwBtn.miBtnBlock = ^(){
                    NSLog(@"nwBtn");
                };
            }
            
            if ([dict[@"praiseResult"] isEqualToString:@"1"]) {
                [jv.praiseBtn setTitle:@"已完成" forState:UIControlStateNormal];
            }else{
                jv.praiseBtn.miBtnBlock = ^(){
                    NSLog(@"praiseBtn");
                };
            }
            
            if ([dict[@"videoResult"] isEqualToString:@"1"]) {
                [jv.videoBtn setTitle:@"已完成" forState:UIControlStateNormal];
            }else{
                jv.videoBtn.miBtnBlock = ^(){
                    NSLog(@"videoBtn");
                };
            }
            
            if ([jv.fansBtn.currentTitle isEqualToString:@"已完成"] && [jv.nwBtn.currentTitle isEqualToString:@"已完成"] && [jv.praiseBtn.currentTitle isEqualToString:@"已完成"] && [jv.videoBtn.currentTitle isEqualToString:@"已完成"]) {
                MBSignUpController *signUPVC = [[MBSignUpController alloc]init];
                signUPVC.dict = self.dict;
                [self.navigationController pushViewController:signUPVC animated:YES];
            }else{
                self->joinBgView.hidden = NO;
            }
            
            
            
        } failure:^(id  _Nonnull AJson) {
            
        }];
    }else{// 请登录
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate needLoginVC:self];
    }
    
    
}

@end
