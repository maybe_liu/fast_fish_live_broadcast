//
//  MBTaskMainController.m
//  LiveBroadcast
//
//  Created by Maybe on 2019/2/15.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBTaskMainController.h"
#import "MBTaskMainCell.h"
#import "MBTaskDetailController.h"
#import "MBRecruitController.h"
#import "SDCycleScrollView.h"
@interface MBTaskMainController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,SDCycleScrollViewDelegate>
{
    UITableView *_tableView;
    UIView *view;
}
@property (nonatomic,strong)NSArray *recruitList;
@property(nonatomic, strong)UIView *rightIMButton;
@property (nonatomic,strong)NSMutableArray *imagesURLStrings;
@end

@implementation MBTaskMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 250)];
    [self.view addSubview:view];
    
    self.imagesURLStrings = [NSMutableArray array];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightIMButton];
    
    UIScrollView *imageScroll = [[UIScrollView alloc]init];
    imageScroll.frame = CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 180);
    imageScroll.contentSize = CGSizeMake(IPHONE_SCREEN_WIDTH * 3, 0);
    imageScroll.backgroundColor = [UIColor redColor];
    imageScroll.delegate = self;
    // 网络加载图片的轮播器
    SDCycleScrollView *cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, IPHONE_SCREEN_WIDTH, 180) delegate:self placeholderImage:[UIImage imageNamed:@"bg"]];
    [view addSubview:cycleScrollView];
    
    UILabel *label = [[UILabel alloc]init];
    label.text = @"招募活动";
    label.frame = CGRectMake(10, imageScroll.bottom + 20, IPHONE_SCREEN_WIDTH - 20, 30);
    [view addSubview:label];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(view.left, 0, IPHONE_SCREEN_WIDTH, self.view.height) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    // banner
    [MBHttpRequset requestWithUrl:kTradeid_recruit_recruitBannerList setParams:@{} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        NSArray *arr = AJson[@"list"];
        NSLog(@"kTradeid_recruit_recruitBannerList = %@",arr);
        for (int i = 0; i < arr.count; i ++) {
            NSDictionary *dict = arr[i];
            if ([dict[@"type"] isEqualToString:@"2"]) {// 招募banner
                NSLog(@"招募banner dict ========= %@",dict);
                [self.imagesURLStrings addObject:dict[@"image"]];
            }
        }
        cycleScrollView.imageURLStringsGroup = self.imagesURLStrings;
    } failure:^(id  _Nonnull AJson) {
        
    }];
    
    // 列表
    [MBHttpRequset requestWithUrl:kTradeid_recruit_getRecruitList setParams:@{@"pageNo":@"1"} isShowProgressView:NO success:^(id  _Nonnull AJson) {
        self.recruitList = AJson[@"list"];
        NSLog(@"kTradeid_recruit_getRecruitList = %@",self.recruitList);
        [self->_tableView reloadData];
    } failure:^(id  _Nonnull AJson) {
        
    }];
}
- (UIView *)rightIMButton
{
    if (_rightIMButton == nil) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(10, 0, 80, 30);
        MBButton *shareBtn = [MBButton buttonWithType:UIButtonTypeCustom];
        shareBtn.frame = CGRectMake(10, 0, 80, 30);
        [shareBtn setTitle:@"我要招人" forState:UIControlStateNormal];
        [shareBtn setTitleColor:kColorRGBA(255, 128, 0, 1) forState:UIControlStateNormal];
        [view addSubview:shareBtn];
        
        shareBtn.miBtnBlock = ^(){
            MBRecruitController *recruitVC = [[MBRecruitController alloc]init];
            [self.navigationController pushViewController:recruitVC animated:YES];
        };
        
        _rightIMButton = view;
    }
    return _rightIMButton;
}

#pragma mark - Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 250;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.recruitList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idetifier = @"TASKMAINCELL";
    MBTaskMainCell *cell = [tableView dequeueReusableCellWithIdentifier:idetifier];
    if (cell == nil) {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"MBTaskMainCell" owner:nil options:nil];
        cell = [nibs lastObject];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *dict = self.recruitList[indexPath.row];
    [cell.imagV sd_setImageWithURL:[NSURL URLWithString:dict[@"recruitUrl"]]];
    cell.titileLbl.text = dict[@"recruitTitle"];
    cell.detailLbl.text = dict[@"recruitContent"];
    cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@",[self getTime:dict[@"starttime"]],[self getTime:dict[@"endtime"]]];
    
    return cell;
}

- (NSString *)getTime:(NSString *)time
{
    NSTimeInterval interval    =[time doubleValue] / 1000.0;
    NSDate *date               = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString       = [formatter stringFromDate: date];
    return dateString;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = self.recruitList[indexPath.row];
    MBTaskDetailController *taskDetailVC = [[MBTaskDetailController alloc] init];
    taskDetailVC.dict = dict;
    [self.navigationController pushViewController:taskDetailVC animated:YES];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    //    NSLog(@"  x %f   y  %f ",scrollView.contentOffset.x,scrollView.contentOffset.y);
//    if (scrollView.contentOffset.y  >= 230) {
//        _tableView.scrollEnabled = YES;
//    }else{
//        _tableView.scrollEnabled = NO;
//    }
//}
@end
