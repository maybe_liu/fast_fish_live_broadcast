//
//  MBTaskDetailController.h
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/2.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBBaseController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MBTaskDetailController : MBBaseController
@property (nonatomic,strong)NSDictionary *dict;
@end

NS_ASSUME_NONNULL_END
