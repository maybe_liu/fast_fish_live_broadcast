//
//  MBSignUpController.m
//  LiveBroadcast
//
//  Created by 肖剑 on 2019/4/5.
//  Copyright © 2019 moonBroadcast. All rights reserved.
//

#import "MBSignUpController.h"
#import "MBSignUpView.h"
#import "MITextView.h"
@interface MBSignUpController ()
{
    MBSignUpView *tempV;
}
@property (nonatomic,strong)UIScrollView *scrollView;

@end

@implementation MBSignUpController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"报名";
    [self initView];
}

- (void)initView
{
    self.scrollView = [[UIScrollView alloc]init];
    self.scrollView.frame = self.view.frame;
    self.scrollView.backgroundColor = self.view.backgroundColor;
    [self.view addSubview:self.scrollView];
    
    NSArray *views = @[@"姓名",@"用户ID",@"用户类型",@"手机号",@"微信号"];
    NSArray *placeHolder = @[@"请输入您的真实姓名(必填）",@"",@"",@"请输入您的常用手机号（必填）",@"请输入您的微信号"];
    NSArray *userDatas = @[@"",[MBUserModel sharedMBUserModel].userInfo.userId,[MBUserModel sharedMBUserModel].userInfo.videoLabel];
    for (int i = 0; i < views.count; i ++) {
        NSArray *nibView =  [[NSBundle mainBundle] loadNibNamed:@"MBSignUpView"owner:self options:nil];
        MBSignUpView *signUpView = [nibView objectAtIndex:0];
        signUpView.tag = 1000 + i;
        signUpView.backgroundColor = [UIColor whiteColor];
        signUpView.titleL.text = views[i];
        signUpView.textF.placeholder = placeHolder[i];
        CGFloat signViewH = 60;
        CGFloat signViewY;
        if (i > 2) {
            signViewY = i * signViewH + 20;
        }else{
            signViewY = i * signViewH + 1;
        }
        if (i >0 && i < 3) {
            signUpView.textF.text = [NSString stringWithFormat:@"%@",userDatas[i]];
            signUpView.textF.enabled = NO;
        }
        signUpView.frame = CGRectMake(0, signViewY, IPHONE_SCREEN_WIDTH, signViewH);
        [self.scrollView addSubview:signUpView];
        tempV = signUpView;
    }
    
    UIView *bottomV = [[UIView alloc]init];
    bottomV.frame = CGRectMake(0, tempV.bottom + 20, IPHONE_SCREEN_WIDTH , 190);
    bottomV.backgroundColor = [UIColor whiteColor];
    [self.scrollView addSubview:bottomV];
    
    UILabel *lbl = [[UILabel alloc]init];
    lbl.frame = CGRectMake(10, 10, IPHONE_SCREEN_WIDTH - 20, 20);
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"自荐陈述" attributes:@{NSFontAttributeName: [UIFont fontWithName:@"PingFangSC-Regular" size: 14],NSForegroundColorAttributeName: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0]}];
    lbl.attributedText = string;
    [bottomV addSubview:lbl];
    
    MITextView *textView = [[MITextView alloc]init];
    textView.backgroundColor = [UIColor whiteColor];
    textView.placeholder = @"请输入您的自荐陈述...";
    textView.frame = CGRectMake(10, lbl.bottom, IPHONE_SCREEN_WIDTH-20, 150);
    [bottomV addSubview:textView];
    
    MBButton *commitBtn = [MBButton buttonWithType:UIButtonTypeCustom];
    commitBtn.frame = CGRectMake(40, bottomV.bottom + 20, IPHONE_SCREEN_WIDTH - 80, 44);
    [commitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [commitBtn setBackgroundColor:kColorRGBA(255, 128, 0, 1)];
    commitBtn.layer.cornerRadius = 22;
    [self.scrollView addSubview:commitBtn];
    __block MBSignUpView *signUpViewName = (MBSignUpView *)[_scrollView viewWithTag:1000];
    __block MBSignUpView *signUpViewPhone = (MBSignUpView *)[_scrollView viewWithTag:1003];
    __block MBSignUpView *signUpViewWechat = (MBSignUpView *)[_scrollView viewWithTag:1004];
    commitBtn.miBtnBlock = ^(){
        NSLog(@"signUpViewName = %@",signUpViewName.textF.text);
        NSLog(@"signUpViewPhone = %@",signUpViewPhone.textF.text);
        NSLog(@"signUpViewWechat = %@",signUpViewWechat.textF.text);
        if (signUpViewName.textF.text.length < 2 || signUpViewPhone.textF.text.length != 11 || signUpViewWechat.textF.text.length < 2 || textView.text.length < 1) {
            [MBProgressHUD showError:@"请输入正确的个人信息"];
            return ;
        }
        NSDictionary *dict = @{
                               @"recruitId":self.dict[@"id"],
                               @"realname":signUpViewName.textF.text,
                               @"userId":[NSString stringWithFormat:@"%@",[MBUserModel sharedMBUserModel].userInfo.userId],
                               @"phoneNumber":signUpViewPhone.textF.text,
                               @"wechatId":signUpViewWechat.textF.text,
                               @"description":textView.text
                               };
        [MBHttpRequset requestWithUrl:kTradeid_recruit_participateData setParams:dict isShowProgressView:YES success:^(id  _Nonnull AJson) {
            NSLog(@"kTradeid_recruit_participateData = %@",AJson);
            [self.navigationController popViewControllerAnimated:YES];
        } failure:^(id  _Nonnull AJson) {
            
        }];
    };
    
    
    self.scrollView.contentSize = CGSizeMake(IPHONE_SCREEN_WIDTH, commitBtn.bottom + 20);
}

@end
